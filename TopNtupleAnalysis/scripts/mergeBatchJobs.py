from subprocess import Popen,PIPE
import os,sys,json
# output directory
outdir = sys.argv[1]#'jobsOnFlash_testMerge_2016-03-22-06-23/'

region = []
with open(outdir + '/jobSummary.json') as job_file:
    jobinfo = json.load(job_file)
for keys in jobinfo.keys():
    region += [keys]
for reg in region:
    samples = jobinfo[reg].keys()
print samples
# 25 ns datasets

channels = ['resolved_e', 'resolved_mu', 'boosted_e', 'boosted_mu']
#channels = ['boosted_e', 'boosted_mu']

import glob
loc = os.environ['PWD']+"/"+outdir
os.chdir(loc)

os.system('pwd')

os.system('mkdir -p mergedJobs')

for reg in region:
    os.chdir(loc+"/mergedJobs")
    os.system('mkdir -p '+str(reg))
    os.chdir(loc+'/BatchJobOutput/')
    os.chdir(loc+"/BatchJobOutput/"+reg)
    for sample in samples:
        os.chdir(loc+"/BatchJobOutput/"+reg+"/"+sample)
        os.system('pwd')
        for ch in channels:
            filesToMerge = []
            for subdirs,dirs, files in os.walk(loc):
                for file in files:
                    if file.find(ch+"_"+sample+".root") != -1:
                        filesToMerge.append(os.path.join(subdirs, file))
                    else:
                        continue

            cmd = 'hadd -f -k '+loc+"/mergedJobs/"+str(reg)+"/merged_"+ch+"_"+sample+".root"+"  "
            if len(filesToMerge) == 0:
                print "Failed to find any result from channel "+ch+" for sample "+sample
                continue
            for i in filesToMerge:
                cmd = cmd + "  " + i

            print(cmd)
            os.system(cmd)



#for ch in channels:
#    os.system('hadd -f -k '+outdir+"/"+ch+'_MC15_13TeV_25ns_FS_EXOT4_ttbarPowhegPythia_all.root '+outdir+"/"+ch+'_MC15_13TeV_25ns_FS_EXOT4_ttbarPowhegPythia.root '+outdir+"/"+ch+'_MC15_13TeV_25ns_FS_EXOT4_ttbarPowhegPythia_mttsliced.root')

