import os,sys
import helpers

lumi=3.20905
configFile=sys.argv[1]
extraText = helpers.getTopology( os.path.basename( os.environ['PWD'] ) ) #extracts info from folder name
extraText += ";loose"
lep = ['e']
ch = ['resolved','boosted']
hists = ['HT','MET','nBtagJets','nJets']

print 'INFO :: Start plotting ...'
for hist in hists:
    for channel in ch:
        for lepton in lep:
            print hist, lep
            command = '../../../plotting/plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '" -C ' + str(configFile)
#            subprocess.check_output(command, shell=True)
            os.system(str(command))
            print command

# for hist in HT MET MET_phi boostedW_eta boostedW_m boostedW_phi boostedW_pt closeJetPt closejl_minDeltaR closejl_minDeltaR_effBins closejl_pt dR_lepnu jet0_eta jet0_m jet0_phi jet0_pt jet1_eta jet1_m jet1_phi jet1_pt jet2_eta jet2_m jet2_phi jet2_pt jet3_eta jet3_m jet3_phi jet3_pt jet4_eta jet4_m jet4_phi jet4_pt jet5_eta jet5_m jet5_phi jet5_pt leadJetPt leadTrkbJetPt leadbJetPt lepEta lepPhi lepPt lepPt_effBins mtlep_boo mtt mu mu_original mwt nBoostedW nBtagJets nJets nTrkBtagJets npv vtxz weight weight_leptSF yields weight_leptPt hadronicT_eta hadronicT_m hadronicT_phi hadronicT_pt hadronicW_eta hadronicW_m hadronicW_phi hadronicW_pt hadronicT_eta hadronicT_m hadronicT_phi hadronicT_pt hadronicW_eta hadronicW_m hadronicW_phi hadronicW_pt; do
#for hist in mass_lepb ; do

# for hist in HT nBtagJets nJets; do
# for ch in resolved boosted ; do

#    for lep in e; do
#        plot -c $lep -p $ch -h $hist -l $LUMI --smoothen 0 --mcOnly 0 --extraText ">=3j >=1b  0W" -C $configFile
# #       ../../../plotting/plotChannelRatio -c $lep -p $ch -h $hist -l $LUMI -T $ch --smoothen 1 --yTitle "e/#mu" -C $configFile
#    done
#  done
#  done
