from subprocess import Popen, PIPE, STDOUT
import os,time

def getJobIDAndRun(jobfile):
    p = Popen(['qsub', '-V', jobfile], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    return p.stdout.readline().rstrip()

def monitorJobs(JobRunningFlag):
    user = os.environ['USER']
    if os.popen('qstat -u '+user).read().find(".atlas-t3-ubc")!=-1:
        print 'INFO :: jobs are still running ...'
        time.sleep(30)
        JobRunningFlag = True
    else:
        print 'INFO :: jobs have finished '
        print 'INFO :: check if they were successful '
        JobRunningFlag = False

def deleteJobs(jobNumber):
     p = Popen(['qdel', jobNumber], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
     return p.stdout.readline().rstrip()


#JOB_ID_1=`qsub first_job.sh`
#JOB_ID_2=`qsub -W depend=after:$JOB_ID_1 second_job.sh`

#JOB_ID_2=`qsub -W depend=afterok:$JOB_ID_1 second_job.sh`

#JOB_ARRAY_ID=`qsub -t 2-6 array_job.sh`
#JOB_SYNC_ID=`qsub -W depend=afterokarray:$JOB_ARRAY_ID sync_job.sh`
