import re
def getTopology(topo):
        '''Reads in the directory topology name and transforms it into root latex string'''
        topol = []
        dummy = str(topo).split('_')
        if "jin" in topo:
            topol.append("#geq" +str([re.split(r'(\d+)', s) for s in dummy][0][1]) +"j")
        if "jex" in topo:
            topol.append("" +str([re.split(r'(\d+)', s) for s in dummy][0][1]) +"j")
        if "bin" in topo:
            topol.append("#geq" +str([re.split(r'(\d+)', s) for s in dummy][1][1]) +"b")
        if "bex" in topo:
            topol.append("" +str([re.split(r'(\d+)', s) for s in dummy][1][1]) +"b")
        if "Win" in topo:
            topol.append("#geq" +str([re.split(r'(\d+)', s) for s in dummy][2][1]) +"W^{cand.}")
        if "Wex" in topo:
            topol.append("" +str([re.split(r'(\d+)', s) for s in dummy][2][1]) +"W^{cand.}")
        outstring = ""
        counter = 0
        for top in topol:
            counter +=1
            if(counter == len(topol)):
                outstring += top
            else:
                outstring += top + str(", ")

        #remove last comma
        return outstring


