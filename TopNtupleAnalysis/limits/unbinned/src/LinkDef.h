#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class BkgFallPdf+;
#pragma link C++ class BkgDijetsPdf+;
#pragma link C++ class BkgWtbPdf+;
#pragma link C++ class SignalPdf+;
#pragma link C++ class SimpleSignalPdf+;
#pragma link C++ class SkewNormalPdf+;
#endif
