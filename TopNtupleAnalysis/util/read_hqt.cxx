/**
 * @brief Main executable to run over input mini flat ntuples,
 * read events in an object-oriented way and
 * run the event analysis class that derives from Analysis.
 * The main objective is to be minimal and to allow users to run this part
 * of the code in their laptops.
 *
 * @author Danilo Enoque Ferreira de Lima <dferreir@mail.cern.ch>
 */

#include "TopNtupleAnalysis/Event.h"
#include "TChain.h"
#include "TopNtupleAnalysis/MiniTree_HQT.h"

#include <iostream>
#include <string>
#include <stdlib.h>
#include <assert.h>


//#include "TopNtupleAnalysis/EventCount.h"
#include "TopNtupleAnalysis/Analysis_HQT.h"
#include "TopNtupleAnalysis/AnaHQTVLQWbX.h"
#include "TopNtupleAnalysis/AnaHQTVLQWtX.h"
#include "TopNtupleAnalysis/ControlPlots.h"
#include "TopNtupleAnalysis/AnaTtresSL.h"
#include "TopNtupleAnalysis/AnaTtresSLMtt.h"
#include "TopNtupleAnalysis/AnaTtresQCD.h"

#include "TopDataPreparation/SampleXsection.h"


#include "TopNtupleAnalysis/ParseUtils.h"
#include "TopNtupleAnalysis/VLQUtils.h"

#include <fstream>

#include "TROOT.h"
#include "TInterpreter.h"

#include <sstream>

#include "TopNtupleAnalysis/WeakCorrScaleFactorParam.h"
//QCD fakes
//FakesTools-00-00-03
#include "TopNtupleAnalysis/FakeEffProvider.h"
//TopFakes-00-00-11
#include "TopFakes/FakesWeights.h"
#include "TopFakes/MatrixUtils.h"
#include "TopFakes/MMEfficiency.h"
#include "TopFakes/MMEffSet.h"

// To apply b-tagging offline
//#include "TopNtupleAnalysis/BTaggingOffline.h"


double ttWeight(Event &sel) {
  double w1 = 1;
  double w2 = 1;
  //  std::cout <<"DEBUG :: evtNo .:> " << eventNumber << std::endl;

  TLorentzVector t    = sel.MC_t();
  TLorentzVector tbar = sel.MC_tbar();

  float pT_ttbar = (t + tbar).Perp();
  float pT_top = t.Perp();
  std::cout <<"DEBUG :: >> t pT > " << pT_top << std::endl;
  std::cout <<"DEBUG :: >> tt pT > " << pT_ttbar << std::endl;
  static Float_t ttbarPt_NOMINAL[4] = { 0.0409119,  -0.0121258,  -0.188448,  -0.316183 };
  static Float_t ttbarPtUpBins[4]= { 40.e3, 170.e3, 340.e3, 1000.e3 };
  int index = 3;
  for (unsigned int i = 0; i < 4; i++) {
    if (pT_ttbar < ttbarPtUpBins[i]) {
      index = i;
      break;
    }
  }
  w1 = 1 + ttbarPt_NOMINAL[index];
  static Float_t sequential_topPt_NOMINAL[7] = { 0.0139729, 0.0128711, 0.00951743, 0.00422323, -0.0352631, -0.0873852, -0.120025, };
  static Float_t sequential_topPtUpBins[7]= { 50.e3, 100.e3, 150.e3, 200.e3, 250.e3, 350.e3, 800.e3 };
  int index2 = 6;
  for (unsigned int i = 0; i < 7; i++) {
    if (pT_top < sequential_topPtUpBins[i]) {
      index2=i;
      break;
    }
  }
  w2 = 1 + sequential_topPt_NOMINAL[index2];
  return w1*w2;
}

int main(int argc, char **argv) {

  //ROOT::Cintex::Cintex::Enable();
  gROOT->ProcessLine("#include <vector>");
  gInterpreter->GenerateDictionary("vector<vector<float> >", "vector");
  //  load_packages();
  //initEventCount();


  // input files
  int help = 0;
  int isData = 0;
  int isAtlFastII = 0;
  std::string outFile = "hist_re.root,hist_rmu.root,hist_be.root,hist_bmu.root";
  std::string files = "output.root";
  std::string input_fullFileList = "";
  std::string analysis = "AnaHQTVLQWbX";
  std::string systs = "nominal";
  //  std::string sigBR = "WbWb";
  std::string resolvedSelection = "";
  std::string boostedSelection = "";
  int skipRegionPlots = 0;
  int doWeightSystematics = 0;
  std::string doSingleWeightSystematics = "";
  int loose = 0;
  int _nentries = -1;
  int _btags = 1;
  int btagEx = 0;
  int _njets = 1;
  int njetEx = 0;
  int _Wtags = 0;
  int WtagEx = 0;
  int grid2pre = 0;
  int removeOverlapHighMtt = 0;
  int runMM = 0;
  int runMM_StatErr = 0;
  int applyPtRew = 0;
  std::string pdf = "";
  int applyWSF = 0;
  int applyEWK = 0;
  int PRWHashing = 0;
  int TreeFriends = 0;
  int btagSFoffline = 0;
  int printWeights = 0;
  std::string DSyear = "";
  int write_ntuple = 0;
  std::string only = "";

  static struct extendedOption extOpt[] = {
        {"help",                  no_argument,       &help,   1, "Display help", &help, extendedOption::eOTInt},
        {"data",                  required_argument,     0, 'd', "Is this data?", &isData, extendedOption::eOTInt},
        {"atlFastII",             required_argument,     0, 'a', "Is this AtlFastII? (0/1)", &isAtlFastII, extendedOption::eOTInt},
        {"files",                 required_argument,     0, 'f', "Input list of comma-separated files to apply the selection on. If argument has input_ and .txt in the name, it is assumed that a file list is inside the text file provided as argument.", &files, extendedOption::eOTString},
        {"fullFiles",             required_argument,     0, 'F', "Full list of input files in this sample to be used to calculate the sum of weights for the normalisation. If left untouched, the contents of --files will be assumed to represent the full sample.", &input_fullFileList, extendedOption::eOTString},
        {"analysis",              required_argument,     0, 'A', "Analysis to run. Choices: AnaHQTVLQWbX, AnaHQTVLQWtX, ControlPlots", &analysis, extendedOption::eOTString},
        {"output",                required_argument,     0, 'o', "Comma-separated list of output files.", &outFile, extendedOption::eOTString},
        {"systs",                 required_argument,     0, 's', "Comma-separated list of systematics.", &systs, extendedOption::eOTString},
	//	{"sigBR",                 required_argument,     0, 'B', "Define the signal decay.", &sigBR, extendedOption::eOTString},
	{"resolvedSelection",     required_argument,     0, 'R', "Define the analysis selection (resolved) on top of pre selection.", &resolvedSelection, extendedOption::eOTString},
	{"boostedSelection",      required_argument,     0, 'B', "Define the analysis selection (boosted) on top of pre selection.", &boostedSelection, extendedOption::eOTString},
        {"loose",                 required_argument,     0, 'l', "Should I run over the loose TTree too?", &loose, extendedOption::eOTInt},
        {"nentries",              required_argument,     0, 'N', "Run over only the first entries if > 0.", &_nentries, extendedOption::eOTInt},
        {"btags",                 required_argument,     0, 'b', "Add cut on b-tagged jets >= abs(X). If negative use track-jet b-tagging.", &_btags, extendedOption::eOTInt},
	{"btagEx",                required_argument,     0, 'x', "Exclusive b-tag b-tagged jets == abs(X). ", &btagEx, extendedOption::eOTInt},
        {"njets",                 required_argument,     0, 'n', "Add cut on n-jets >= abs(X).", &_njets, extendedOption::eOTInt},
	{"njetEx",                required_argument,     0, 'y', "Exclusive njet jets == abs(X). ", &njetEx, extendedOption::eOTInt},
	{"Wtags",                 required_argument,     0, 'W', "Add cut on W-tagged jets >= abs(X)", &_Wtags, extendedOption::eOTInt},
	{"WtagEx",                required_argument,     0, 'z', "Exclusive W-tag W-tagged large jets == abs(X). ", &WtagEx, extendedOption::eOTInt},
	{"grid2pre",              required_argument,     0, 'g', "Performs selection on grid samples. ", &grid2pre, extendedOption::eOTInt},
        {"removeOverlapHighMtt",  required_argument,     0, 'r', "Veto events with true mtt > 1.1 TeV in the 410000 sample only (to be activated if one wnats to use the mtt sliced samples).", &removeOverlapHighMtt, extendedOption::eOTInt},
        {"doWeightSystematics",   required_argument,     0, 'S', "Include the variation of the systematics in the SFs.", &doWeightSystematics, extendedOption::eOTInt},
        {"doSingleWeightSystematics", required_argument, 0, 'I', "Include the variation of a single systematics in the SFs.", &doSingleWeightSystematics, extendedOption::eOTString},
        {"runMM",                 required_argument,     0, 'M', "Implement the QCD weigths to data", &runMM, extendedOption::eOTInt},
	{"runMM_StatErr",         required_argument,     0, 'm', "Shift the real/fake efficiencies to estimate the stattistical error", &runMM_StatErr, extendedOption::eOTInt},
        {"applyPtRew",            required_argument,     0, 'P', "Apply pt reweighting.", &applyPtRew, extendedOption::eOTInt},
        {"pdf",                   required_argument,     0, 'p', "Only run PDF variations.", &pdf, extendedOption::eOTString},
        {"applyWSF",              required_argument,     0, 'w', "Apply W SF.", &applyWSF, extendedOption::eOTInt},
        {"applyEWK",              required_argument,     0, 'E', "Apply electroweak correction.", &applyEWK, extendedOption::eOTInt},
        {"skipRegionPlots",       required_argument,     0, 'X', "Skip large parts of the selected region plots and plot directly defined CRs or SRs.", &skipRegionPlots, extendedOption::eOTInt},
	{"PRWHashing",            required_argument,     0, 'H', "Apply PRWHashing for new incoming data.", &PRWHashing, extendedOption::eOTInt},
	{"TreeFriends",           required_argument,     0, 'T', "Add friends to the nominal tree. (==1): PRWHash PileUp (==3): adding the truth tree", &TreeFriends, extendedOption::eOTInt},
	{"btagSFoffline",         required_argument,     0, 'Z', "Apply b-tagging SFs offline.", &btagSFoffline, extendedOption::eOTInt},
	{"printWeights",          required_argument,     0, 'k', "Print event weights ", &printWeights, extendedOption::eOTInt},
        {"onlyChannel",           required_argument,     0, 'O', "Run code only for comma-separated channels.", &only, extendedOption::eOTString},
	{"DSyear",                required_argument,     0, 'Y', "Define if you want to run on a subset of the current data, specified by the year (2015/2016).", &DSyear, extendedOption::eOTString},
	{"write_ntuple",          required_argument,     0, 'U', "Whether to write out ntUple files after selection.",&write_ntuple, extendedOption::eOTInt},
        {0, 0, 0, 0, 0, 0, extendedOption::eOTInt}
      };

  if (!parseArguments(argc, argv, extOpt) || help) {
    dumpHelp("read", extOpt, "read\nRead selected events after preselection and generate histograms.\n");
    return 0;
  } else {
    std::cout << "Dumping options:" << std::endl;
    dumpOptions(extOpt);
  }

  //(SH) is setup regardless of it being used or not (temporarily solution anyways)
  //  BTaggingOfflineScaleFactorParam btagOffline;
 if(btagSFoffline){ //apply btagging SF offline
   std::cout <<"INFO :: BTagging SFs will be applied from offline implementation ..." << std::endl;
 }

  std::vector<int> onlyChannelList;
  std::string ocStr = only;
  for (size_t i = 0,n; i <= ocStr.length(); i=n+1) {
    n = ocStr.find_first_of(',',i);
    if (n == std::string::npos)
      n = ocStr.length();
    std::string tmp = ocStr.substr(i,n-i);
    if (tmp.length() > 0)
      onlyChannelList.push_back(std::atoi(tmp.c_str()));
  }

  // parse file list
  std::vector<std::string> fileList;
  std::vector<std::string> fullFileList;

  if ( ((files.find("input") != std::string::npos) && (files.find(".txt") != std::string::npos)) ) {
    std::cout << "Using file given as text list." << std::endl;
    ifstream f(files.c_str());
    if (!f) {
      std::cerr << "Cannot open " << files << std::endl;
      std::exit(-2);
    }
    std::string thePathStr;
    while (std::getline(f, thePathStr)) {
      if (thePathStr != "") {
        size_t idx = std::string::npos;
        idx = thePathStr.find("\n");
        std::string aFile = thePathStr.substr(0, idx);
        fileList.push_back(aFile);
      }
    }
    for (std::vector<std::string>::const_iterator it = fileList.begin(); it != fileList.end(); ++it) {
      std::cout << "Input file \""<<*it<<"\""<< std::endl;
    }
  } else {
    // split by ','
    std::string argStr = files;
    for (size_t i = 0,n; i <= argStr.length(); i=n+1) {
      n = argStr.find_first_of(',',i);
      if (n == std::string::npos)
        n = argStr.length();
      std::string tmp = argStr.substr(i,n-i);
      fileList.push_back(tmp);
    }
  }
  if (fileList.size() == 0) {
    std::cerr << "ERROR: You must input at least one file." << std::endl;
    std::exit(-1);
  }
  if (input_fullFileList == "") {
    fullFileList = fileList;
  } else {
    if ( ((input_fullFileList.find("input") != std::string::npos) && (input_fullFileList.find(".txt") != std::string::npos)) ) {
      std::cout << "Using file given as text list." << std::endl;
      ifstream f(input_fullFileList.c_str());
      if (!f) {
        std::cerr << "Cannot open " << input_fullFileList << std::endl;
        std::exit(-2);
      }
      std::string thePathStr;
      while (std::getline(f, thePathStr)) {
        if (thePathStr != "") {
          size_t idx = std::string::npos;
          idx = thePathStr.find("\n");
          std::string aFile = thePathStr.substr(0, idx);
          fullFileList.push_back(aFile);
        }
      }
      for (std::vector<std::string>::const_iterator it = fullFileList.begin(); it != fullFileList.end(); ++it) {
        std::cout << "(full input for norm.) Input file \""<<*it<<"\""<< std::endl;
      }
    } else {
      // split by ','
      std::string argStr = input_fullFileList;
      for (size_t i = 0,n; i <= argStr.length(); i=n+1) {
        n = argStr.find_first_of(',',i);
        if (n == std::string::npos)
          n = argStr.length();
        std::string tmp = argStr.substr(i,n-i);
        fullFileList.push_back(tmp);
      }
    }
  }

  // get output files
  std::string outStr = outFile;
  std::vector<std::string> outList;
  for (size_t i = 0,n; i <= outStr.length(); i=n+1) {
    n = outStr.find_first_of(',',i);
    if (n == std::string::npos)
      n = outStr.length();
    std::string tmp = outStr.substr(i,n-i);
    // if(files.find("TTS_M") != std::string::npos) {
    //   std::string ext ="";
    //   if(tmp.find_last_of(".") != std::string::npos)
    // 	ext = tmp.substr(tmp.find_last_of(".")+1);
    //   const size_t period_idx = tmp.rfind('.');
    //   if (std::string::npos != period_idx)
    //   	tmp.erase(period_idx);
    //   tmp += "_";
    //   tmp += sigBR;
    //   tmp += ".";
    //   tmp += ext;
    //   outList.push_back(tmp);
    //   std::cout <<"DEBUG :: outlist "<< ext << " tmp : "<< tmp<< std::endl;
    // }else{
      outList.push_back(tmp);
      //    }
  }

  // split systs by comma
  std::vector<std::string> systsList;
  std::vector<std::string> systsListWithBlankNominal;
  if (systs.find(".txt") != std::string::npos) {
    // systs is a file with the list of systematics
    ifstream f(systs.c_str());
    if (!f) {
      std::cerr << "Cannot open " << systs << std::endl;
      std::exit(-2);
    }
    std::string finalSysts = "";
    std::string theLineStr;
    while (std::getline(f, theLineStr)) {
      if (theLineStr != "") {
        size_t idx = std::string::npos;
        idx = theLineStr.find("\n");
        std::string aLine = theLineStr.substr(0, idx);
        if (finalSysts.size() != 0 && finalSysts[finalSysts.size()-1] != ',') finalSysts += ",";
        finalSysts += aLine;
      }
    }
    systs = finalSysts;
  }

  for (size_t i = 0,n; i <= systs.length(); i=n+1) {
    n = systs.find_first_of(',',i);
    if (n == std::string::npos)
      n = systs.length();
    std::string tmp = systs.substr(i,n-i);
    std::string tmpWithBlankNominal = tmp;
    if (tmpWithBlankNominal == "nominal") tmpWithBlankNominal = "";
    systsList.push_back(tmp);
    systsListWithBlankNominal.push_back(tmpWithBlankNominal);
    if (loose) {
      systsListWithBlankNominal.push_back(tmpWithBlankNominal+std::string("_Loose"));
    }
  }



  std::vector<std::string> pdfList;
  for (size_t i = 0,n; i <= pdf.length(); i=n+1) {
    n = pdf.find_first_of(',',i);
    if (n == std::string::npos)
      n = pdf.length();
    std::string tmp = pdf.substr(i,n-i);
    pdfList.push_back(tmp);
  }

  // retrieve, list of sum of weights
  std::map<int, float> sumOfWeights;
  std::map<int, std::map<std::string, std::vector<float> > > PDFsumOfWeights;
  TChain t_sumWeights("sumWeights");
  TChain t_PDFsumWeights("PDFsumWeights");

  if(!isData) {
    for (int k = 0; k < fullFileList.size(); ++k) {
      t_sumWeights.Add(fullFileList[k].c_str());
      if (pdf != "") t_PDFsumWeights.Add(fullFileList[k].c_str());
    }
    int dsid;
    float value;
    int dsidPdf;
    std::map<std::string, std::vector<float> *> valuePdf;
    t_sumWeights.SetBranchAddress("dsid", &dsid);
    t_sumWeights.SetBranchAddress("totalEventsWeighted", &value);
    t_PDFsumWeights.SetBranchAddress("dsid", &dsidPdf);
    for (int k = 0; k < pdfList.size(); ++k) {
      valuePdf[pdfList[k].c_str()] = 0;
    }
    for (int k = 0; k < pdfList.size(); ++k) {
      t_PDFsumWeights.SetBranchAddress(pdfList[k].c_str(), &valuePdf[pdfList[k]]);
    }
    for (int k = 0; k < t_sumWeights.GetEntries(); ++k) {
      t_sumWeights.GetEntry(k);
      if (sumOfWeights.find(dsid) == sumOfWeights.end()) sumOfWeights[dsid] = 0;
      sumOfWeights[dsid] += value;
    }
    cout <<"INFO :: SumOfWeights for sample ("<< dsid <<") :> "<<  sumOfWeights[dsid] << endl;
    for (int k = 0; k < t_PDFsumWeights.GetEntries(); ++k) {
      t_PDFsumWeights.GetEntry(k);
      if (PDFsumOfWeights.find(dsid) == PDFsumOfWeights.end()) PDFsumOfWeights[dsid] = std::map<std::string, std::vector<float> >();
      for (int l = 0; l < pdfList.size(); ++l) {
        if (PDFsumOfWeights[dsid].find(pdfList[l]) == PDFsumOfWeights[dsid].end()) PDFsumOfWeights[dsid][pdfList[l]] = std::vector<float>();
        if (PDFsumOfWeights[dsid][pdfList[l]].size() == 0)
          PDFsumOfWeights[dsid][pdfList[l]].resize(valuePdf[pdfList[l]]->size());

        for (int m = 0; m < PDFsumOfWeights[dsid][pdfList[l]].size(); ++m)
          PDFsumOfWeights[dsid][pdfList[l]][m] += valuePdf[pdfList[l]]->at(m);
      }
    }
  }


  int n_eigenvars_b = 0;
  int n_eigenvars_c = 0;
  int n_eigenvars_l = 0;
  //std::vector<std::string> trackjetSFs;
  //std::string trackjet_pre = "trackjet_btagSF_70_eigenvars";
  //size_t trackjet_presize = std::string("trackjet_btagSF_70_eigenvars").size();

  if (doWeightSystematics && pdf == "" && doSingleWeightSystematics == "") { // include SF systs. too
    std::cout << "adding more systematics" << std::endl;
    systsListWithBlankNominal.push_back("eTrigSF__1up");
    systsListWithBlankNominal.push_back("eTrigSF__1down");
    systsListWithBlankNominal.push_back("eRecoSF__1up");
    systsListWithBlankNominal.push_back("eRecoSF__1down");
    systsListWithBlankNominal.push_back("eIDSF__1up");
    systsListWithBlankNominal.push_back("eIDSF__1down");
    systsListWithBlankNominal.push_back("eIsolSF__1up");
    systsListWithBlankNominal.push_back("eIsolSF__1down");

    systsListWithBlankNominal.push_back("muTrigStatSF__1up");
    systsListWithBlankNominal.push_back("muTrigStatSF__1down");
    systsListWithBlankNominal.push_back("muIDStatSF__1up");
    systsListWithBlankNominal.push_back("muIDStatSF__1down");
    systsListWithBlankNominal.push_back("muIDlowPTStatSF__1up");
    systsListWithBlankNominal.push_back("muIDlowPTStatSF__1down");
    systsListWithBlankNominal.push_back("muIsolStatSF__1up");
    systsListWithBlankNominal.push_back("muIsolStatSF__1down");
    systsListWithBlankNominal.push_back("muTTVAStatSF__1up");
    systsListWithBlankNominal.push_back("muTTVAStatSF__1down");

    systsListWithBlankNominal.push_back("muTrigSystSF__1up");
    systsListWithBlankNominal.push_back("muTrigSystSF__1down");
    systsListWithBlankNominal.push_back("muIDSystSF__1up");
    systsListWithBlankNominal.push_back("muIDSystSF__1down");
    systsListWithBlankNominal.push_back("muIDlowPTSystSF__1up");
    systsListWithBlankNominal.push_back("muIDlowPTSystSF__1down");
    systsListWithBlankNominal.push_back("muIsolSystSF__1up");
    systsListWithBlankNominal.push_back("muIsolSystSF__1down");
    systsListWithBlankNominal.push_back("muTTVASystSF__1up");
    systsListWithBlankNominal.push_back("muTTVASystSF__1down");

    systsListWithBlankNominal.push_back("boostedWSF__1up");
    systsListWithBlankNominal.push_back("boostedWSF__1down");

    systsListWithBlankNominal.push_back("pileup__1up");
    systsListWithBlankNominal.push_back("pileup__1down");

    systsListWithBlankNominal.push_back("jvt__1up");
    systsListWithBlankNominal.push_back("jvt__1down");
    // count b-tagging variations
    MiniTree_HQT mt(false, fileList[0].c_str(), "nominal", TreeFriends);
    Event sel;
    mt.read(0, sel);

    if (_btags > 0 || (_btags ==0 && !btagEx)) {

      n_eigenvars_b = mt.vf("weight_bTagSF_77_eigenvars_B_up")->size();
      n_eigenvars_c = mt.vf("weight_bTagSF_77_eigenvars_C_up")->size();
      n_eigenvars_l = mt.vf("weight_bTagSF_77_eigenvars_Light_up")->size();

      for (int i = 0; i < n_eigenvars_b; ++i) {
        systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1up");
        systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1down");
	// if(i == 0)
	//   for(int j = 1 ; j < 4; ++j){
	//     systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"_pt"+to_string(j)+"__1up");
	//     systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"_pt"+to_string(j)+"__1down");
	//   }
      }
      for (int i = 0; i < n_eigenvars_c; ++i) {
        systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1up");
        systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1down");
	// if(i == 0)
	//   for(int j = 1 ; j < 4; ++j){
	//     systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"_pt"+to_string(j)+"__1up");
	//     systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"_pt"+to_string(j)+"__1down");
	//   }
      }
      for (int i = 0; i < n_eigenvars_l; ++i) {
        systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1up");
        systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1down");
	// if(i == 0)
	//   for(int j = 1 ; j < 4; ++j){
	//     systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"_pt"+to_string(j)+"__1up");
	//     systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"_pt"+to_string(j)+"__1down");
	//   }
      }
      systsListWithBlankNominal.push_back("btageSF_0__1up");
      systsListWithBlankNominal.push_back("btageSF_0__1down");
      systsListWithBlankNominal.push_back("btageSF_1__1up");
      systsListWithBlankNominal.push_back("btageSF_1__1down");
      if (loose) {
        for (int i = 0; i < n_eigenvars_b; ++i) {
          systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1up_Loose");
          systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1down_Loose");
	  // if(i == 0)
	  //   for(int j = 1 ; j < 4; ++j){
	  //     systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"_pt"+to_string(j)+"__1up_Loose");
	  //     systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"_pt"+to_string(j)+"__1down_Loose");
	  // }
        }
        for (int i = 0; i < n_eigenvars_c; ++i) {
          systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1up_Loose");
          systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1down_Loose");
	  // if(i == 0)
	  //   for(int j = 1 ; j < 4; ++j){
	  //     systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"_pt"+to_string(j)+"__1up_Loose");
	  //     systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"_pt"+to_string(j)+"__1down_Loose");
	  // }
        }
        for (int i = 0; i < n_eigenvars_l; ++i) {
          systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1up_Loose");
          systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1down_Loose");
	  // if(i == 0)
	  //   for(int j = 1 ; j < 4; ++j){
	  //     systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"_pt"+to_string(j)+"__1up_Loose");
	  //     systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"_pt"+to_string(j)+"__1down_Loose");
	  // }
        }
        systsListWithBlankNominal.push_back("btageSF_0__1up_Loose");
        systsListWithBlankNominal.push_back("btageSF_0__1down_Loose");
        systsListWithBlankNominal.push_back("btageSF_1__1up_Loose");
        systsListWithBlankNominal.push_back("btageSF_1__1down_Loose");
      }
    }
    if (_btags < 0) {

      /*
      for (std::map<std::string, MiniTree_HQT::MTType>::const_iterator it = mt.m_brs.begin(); it != mt.m_brs.end(); ++it) {
        if (it->first.find(trackjet_pre) != std::string::npos) {
          systsListWithBlankNominal.push_back(std::string("bsf_")+it->first.substr(trackjet_presize));
          if (loose) systsListWithBlankNominal.push_back(std::string("bsf_")+it->first.substr(trackjet_presize)+std::string("_Loose"));
          trackjetSFs.push_back(std::string("bsf_")+it->first.substr(trackjet_presize));
          if (loose) trackjetSFs.push_back(std::string("bsf_")+it->first.substr(trackjet_presize)+std::string("_Loose"));
        }
      }
      */

      n_eigenvars_b = mt.vf("weight_trackjet_bTagSF_70_eigenvars_B_up")->size();
      n_eigenvars_c = mt.vf("weight_trackjet_bTagSF_70_eigenvars_C_up")->size();
      n_eigenvars_l = mt.vf("weight_trackjet_bTagSF_70_eigenvars_Light_up")->size();

      for (int i = 0; i < n_eigenvars_b; ++i) {
        systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1up");
        systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1down");
      }
      for (int i = 0; i < n_eigenvars_c; ++i) {
        systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1up");
        systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1down");
      }
      for (int i = 0; i < n_eigenvars_l; ++i) {
        systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1up");
        systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1down");
      }
      systsListWithBlankNominal.push_back("btageSF_0__1up");
      systsListWithBlankNominal.push_back("btageSF_0__1down");
      systsListWithBlankNominal.push_back("btageSF_1__1up");
      systsListWithBlankNominal.push_back("btageSF_1__1down");
      if (loose) {
        for (int i = 0; i < n_eigenvars_b; ++i) {
          systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1up_Loose");
          systsListWithBlankNominal.push_back("btagbSF_"+to_string(i)+"__1down_Loose");
        }
        for (int i = 0; i < n_eigenvars_c; ++i) {
          systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1up_Loose");
          systsListWithBlankNominal.push_back("btagcSF_"+to_string(i)+"__1down_Loose");
        }
        for (int i = 0; i < n_eigenvars_l; ++i) {
          systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1up_Loose");
          systsListWithBlankNominal.push_back("btaglSF_"+to_string(i)+"__1down_Loose");
        }
        systsListWithBlankNominal.push_back("btageSF_0__1up_Loose");
        systsListWithBlankNominal.push_back("btageSF_0__1down_Loose");
        systsListWithBlankNominal.push_back("btageSF_1__1up_Loose");
        systsListWithBlankNominal.push_back("btageSF_1__1down_Loose");
      }
    }
    if (loose) {
      systsListWithBlankNominal.push_back("eTrigSF__1up_Loose");
      systsListWithBlankNominal.push_back("eTrigSF__1down_Loose");
      systsListWithBlankNominal.push_back("eRecoSF__1up_Loose");
      systsListWithBlankNominal.push_back("eRecoSF__1down_Loose");
      systsListWithBlankNominal.push_back("eIDSF__1up_Loose");
      systsListWithBlankNominal.push_back("eIDSF__1down_Loose");
      systsListWithBlankNominal.push_back("eIsolSF__1up_Loose");
      systsListWithBlankNominal.push_back("eIsolSF__1down_Loose");

      systsListWithBlankNominal.push_back("muTrigStatSF__1up_Loose");
      systsListWithBlankNominal.push_back("muTrigStatSF__1down_Loose");
      systsListWithBlankNominal.push_back("muIDStatSF__1up_Loose");
      systsListWithBlankNominal.push_back("muIDStatSF__1down_Loose");
      systsListWithBlankNominal.push_back("muIDlowPTStatSF__1up_Loose");
      systsListWithBlankNominal.push_back("muIDlowPTStatSF__1down_Loose");
      systsListWithBlankNominal.push_back("muIsolStatSF__1up_Loose");
      systsListWithBlankNominal.push_back("muIsolStatSF__1down_Loose");
      systsListWithBlankNominal.push_back("muTTVAStatSF__1up_Loose");
      systsListWithBlankNominal.push_back("muTTVAStatSF__1down_Loose");

      systsListWithBlankNominal.push_back("muTrigSystSF__1up_Loose");
      systsListWithBlankNominal.push_back("muTrigSystSF__1down_Loose");
      systsListWithBlankNominal.push_back("muIDSystSF__1up_Loose");
      systsListWithBlankNominal.push_back("muIDSystSF__1down_Loose");
      systsListWithBlankNominal.push_back("muIDlowPTSystSF__1up_Loose");
      systsListWithBlankNominal.push_back("muIDlowPTSystSF__1down_Loose");
      systsListWithBlankNominal.push_back("muIsolSystSF__1up_Loose");
      systsListWithBlankNominal.push_back("muIsolSystSF__1down_Loose");
      systsListWithBlankNominal.push_back("muTTVASystSF__1up_Loose");
      systsListWithBlankNominal.push_back("muTTVASystSF__1down_Loose");

      systsListWithBlankNominal.push_back("boostedWSF__1up_Loose");
      systsListWithBlankNominal.push_back("boostedWSF__1down_Loose");

      systsListWithBlankNominal.push_back("pileup__1up_Loose");
      systsListWithBlankNominal.push_back("pileup__1down_Loose");

      systsListWithBlankNominal.push_back("jvt__1up_Loose");
      systsListWithBlankNominal.push_back("jvt__1down_Loose");

    }
  } else if (pdf != "" && doSingleWeightSystematics == "") {
    for (int m = 0; m < pdfList.size(); ++m) {
      int nvar = (*PDFsumOfWeights.begin()).second[pdfList[m]].size();
      for (int l = 0; l < nvar; ++l) {
        std::string s = "pdf_";
        s += pdfList[m];
        s += "_";
        s += std::to_string(l);
        systsListWithBlankNominal.push_back(s);
      }
    }
  }
  else if (doSingleWeightSystematics != "") {
    systsListWithBlankNominal.push_back(doSingleWeightSystematics);
    cout << "*** running on SingleWeight systematic " << doSingleWeightSystematics << endl;
  }


  std::vector<Analysis_HQT *> vec_analysis;
  if (analysis == "AnaHQTVLQWbX") {
    if(resolvedSelection != ""){
      vec_analysis.push_back(new AnaHQTVLQWbX(outList[0], true,  false, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // resolved electron not combined
      vec_analysis.push_back(new AnaHQTVLQWbX(outList[1], false, false, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // resolved muon not combined
      vec_analysis.push_back(new AnaHQTVLQWbX(outList[2], false, false, true, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // resolved combined
    }else if(boostedSelection != ""){
      vec_analysis.push_back(new AnaHQTVLQWbX(outList[3], true,  true, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // boosted  electron not combined selection
      vec_analysis.push_back(new AnaHQTVLQWbX(outList[4], false, true, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // boosted  muon not combined
      vec_analysis.push_back(new AnaHQTVLQWbX(outList[5], true, true, true, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // boosted combined
    }
  }
  else if (analysis == "AnaHQTVLQWtX") {
    if(resolvedSelection != ""){
      vec_analysis.push_back(new AnaHQTVLQWtX(outList[0], true,  false, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // resolved electron not combined
      vec_analysis.push_back(new AnaHQTVLQWtX(outList[1], false, false, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // resolved muon not combined
      vec_analysis.push_back(new AnaHQTVLQWtX(outList[2], false, false, true, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // resolved combined
    }else if(boostedSelection != ""){
      vec_analysis.push_back(new AnaHQTVLQWtX(outList[3], true,  true, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // boosted  electron not combined selection
      vec_analysis.push_back(new AnaHQTVLQWtX(outList[4], false, true, false, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // boosted  muon not combined
      vec_analysis.push_back(new AnaHQTVLQWtX(outList[5], true, true, true, write_ntuple, skipRegionPlots, resolvedSelection, boostedSelection, systsListWithBlankNominal)); // boosted combined
    }
  }

  else if (analysis == "ControlPlots") {
    vec_analysis.push_back(new ControlPlots(outList[2], true, false, systsListWithBlankNominal)); //  electron not combined selection
    vec_analysis.push_back(new ControlPlots(outList[3], false, false, systsListWithBlankNominal)); // muon not combined
    vec_analysis.push_back(new ControlPlots(outList[4], true, true, systsListWithBlankNominal)); // combined
    }
  /*
  else if (analysis == "AnaTtresSL") {
    vec_analysis.push_back(new AnaTtresSL(outList[0], true,  false, systsListWithBlankNominal)); // resolved electron
    vec_analysis.push_back(new AnaTtresSL(outList[1], false, false, systsListWithBlankNominal)); // resolved muon
    vec_analysis.push_back(new AnaTtresSL(outList[2], true,  true,  systsListWithBlankNominal)); // boosted  electron
    vec_analysis.push_back(new AnaTtresSL(outList[3], false, true,  systsListWithBlankNominal)); // boosted  muon
    }*/

  SampleXsection sampleXsection;
  //  sampleXsection.readFromFile("scripts/XSection-MC15-13TeV-ttres.data");
  char* pPath;
  pPath = getenv("ANATOP");
  if(pPath==NULL)
    std::cout << "WARNING :: Please setup the global variable $ANATOP pointing to the directory that includes TopDataPreparation" << std::endl;
  stringstream ss;
  string path;
  ss << pPath;
  ss >> path;
  //  path.append("/TopDataPreparation/data/XSection-MC15-13TeV-fromSusyGrp.data");
  path.append("/TopDataPreparation/data/XSection-MC15-13TeV.data");
  sampleXsection.readFromFile( path.c_str() );
  //  sampleXsection.readFromFile("scripts/XSection-MC15-13TeV-fromSusyGrp.data");

  //  WeakCorr::WeakCorrScaleFactorParam ewkTool("$ANALYSISFOLDER/share/EWcorr_param.root");
  unsigned long procEvents = 0;

  // systsList contains the list of TTrees representing systematics
  // given by the user
  for (size_t systIdx = 0; systIdx < systsList.size(); ++systIdx) {
    std::cout<< "* Running on syst: " << systIdx << std::endl;
    // it does not have the xxx_Loose TTrees, so we add it ourselves and run over it too
    std::vector<std::string> lepton_modes;

    if (!runMM && loose){
        lepton_modes.push_back(systsList[systIdx]+std::string("_Loose"));
    } else if (!runMM && !loose){
        lepton_modes.push_back(systsList[systIdx]);
    } else {
        //lepton_modes.push_back(systsList[systIdx]);
        lepton_modes.push_back(systsList[systIdx]+std::string("_Loose"));
	std::cout <<"INFO :: runMM selected... using chain >  "<< systsList[systIdx]+std::string("_Loose") << std::endl;
    }

    for (size_t lmodeIdx = 0; lmodeIdx < lepton_modes.size(); ++lmodeIdx) {
      std::cout<< "** Running on lepton mode: " << lmodeIdx << std::endl;

      // now we are looping over all possible TTrees with all systematic uncertainties
      // Call MiniTree_HQT to open the files and read that TTree
      std::string tname = lepton_modes[lmodeIdx];
      std::cout <<"INFO :: running on chain called > "<< tname << std::endl;
      MiniTree_HQT mt(false, fileList[0], tname.c_str(), TreeFriends);
      for (int k = 1; k < fileList.size(); ++k) {
        mt.addFileToRead(fileList[k], tname.c_str());
      }
      // for the nominal "systematic unc.", use an empty string as suffix to be backward compatible
      // also, it would be horrible to plot histograms with the nominal suffix all the time ...
      std::string systSuffixForHistograms = tname;
      if (systSuffixForHistograms == "nominal")
        systSuffixForHistograms = "";
      else if (systSuffixForHistograms == "nominal_Loose")
        systSuffixForHistograms = "_Loose";

      Event sel; // selected objects

      // now loop over all entries for this systematic uncertainty
      int nentries = mt.GetEntries();
      if (_nentries < nentries && _nentries > 0)
        nentries = _nentries;
      for (int k = 0; k < nentries; ++k) {
        if (k % 1000 == 0)
          std::cout << "("<< tname << ") Entry " << k << "/" << nentries << std::endl;

        bool isTight = false;

        // read the event into an Event object to be sent to the Analysis code later
        mt.read(k, sel);
        int channel = sel.channelNumber();
	//	MMUtils_HQT * MM_nominal      = NULL;
	//	MMUtils_HQT * MM_nominal_e    = NULL;

	//	 FakesWeights* m_efakes = NULL;
	//	 FakesWeights* m_mfakes = NULL;
	//	 if (runMM){
	  	   //  	MM_nominal      = new MMUtils_HQT("scripts/QCDestimation/eff_ttbar.root", "scripts/QCDestimation/fake.root");
	   //  	MM_nominal_e    = new MMUtils_HQT("scripts/QCDestimation/eff_ttbar.root", "scripts/QCDestimation/fake_CR4_be_withFJ.root");
	   //   MM_nominal      = new MMUtils_HQT("scripts/QCDestimation/eff_ttbar.root", "ttrescr_invsd0/fake.root");
	//}//runMM


        if (sel.electron().size() == 1 && sel.muon().size() == 0)
            isTight = sel.electron()[0].isTightPP();
        else if (sel.electron().size() == 0 && sel.muon().size() == 1)
            isTight = sel.muon()[0].isTight();

        if (!runMM && loose) {
            // for the fake rate or efficiency estimate, just separate loose and tight
            if (isTight)
                systSuffixForHistograms = "";
            else
                systSuffixForHistograms = "_Loose";
        } else if (runMM && loose) { // run the QCD matrix method in the SR adding weights
            systSuffixForHistograms = "";
        }

        if (channel == 410000 && removeOverlapHighMtt) // for SM ttbar, we have mtt sliced samples above 1.1 TeV
          if (sel.MC_ttbar_beforeFSR().M() > 1.1e6)
            continue;


        // this is list just contains systSuffixForHistograms for all systematics
        // except for the nominal and nominal_Loose, on which it contains
        // the electron SF, muon SF and b-tagging SF systematics
        // these systematics do not show up as separate TTrees, so they need special treatment
        std::vector<std::string> weightSystematics;
        if ( !(doWeightSystematics || pdf != "" || doSingleWeightSystematics != "") || (systSuffixForHistograms != "" && systSuffixForHistograms != "_Loose") ) {
          weightSystematics.push_back(systSuffixForHistograms);
	} else if (pdf != "") {
          weightSystematics.push_back(systSuffixForHistograms);
          if (pdfList.size() != 0) {
            for (int m = 0; m < pdfList.size(); ++m) {
              int nvar = (*PDFsumOfWeights.begin()).second[pdfList[m]].size();
              for (int l = 0; l < nvar; ++l) {
                std::string s = "pdf_";
                s += pdfList[m];
                s += "_";
                s += std::to_string(l);
		std::cout <<"todo : "<< s << std::endl;
                weightSystematics.push_back(s);
              }
            }
          }
        } else if (doSingleWeightSystematics != "") {
	  weightSystematics.push_back(doSingleWeightSystematics+systSuffixForHistograms);
	}
	else { // apply variations on the nominal
          weightSystematics.push_back(systSuffixForHistograms);
          weightSystematics.push_back(std::string("eTrigSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("eTrigSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("eRecoSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("eRecoSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("eIDSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("eIDSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("eIsolSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("eIsolSF__1down")+systSuffixForHistograms);

          weightSystematics.push_back(std::string("muTrigStatSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muTrigStatSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIDStatSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIDStatSF__1down")+systSuffixForHistograms);
	  weightSystematics.push_back(std::string("muIDlowPTStatSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIDlowPTStatSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIsolStatSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIsolStatSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muTTVAStatSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muTTVAStatSF__1down")+systSuffixForHistograms);

          weightSystematics.push_back(std::string("muTrigSystSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muTrigSystSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIDSystSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIDSystSF__1down")+systSuffixForHistograms);
	  weightSystematics.push_back(std::string("muIDlowPTSystSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIDlowPTSystSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIsolSystSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muIsolSystSF__1down")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muTTVASystSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("muTTVASystSF__1down")+systSuffixForHistograms);

          weightSystematics.push_back(std::string("boostedWSF__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("boostedWSF__1down")+systSuffixForHistograms);

          weightSystematics.push_back(std::string("pileup__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("pileup__1down")+systSuffixForHistograms);

          weightSystematics.push_back(std::string("jvt__1up")+systSuffixForHistograms);
          weightSystematics.push_back(std::string("jvt__1down")+systSuffixForHistograms);


          //if (_btags > 0) {
            for (int i = 0; i < n_eigenvars_b; ++i) {
              weightSystematics.push_back(std::string("btagbSF_"+to_string(i)+"__1up")+systSuffixForHistograms);
              weightSystematics.push_back(std::string("btagbSF_"+to_string(i)+"__1down")+systSuffixForHistograms);
	      	// if(i == 0)
		//   for(int j = 1 ; j < 4; ++j){
		//     weightSystematics.push_back(std::string("btagbSF_"+to_string(i)+"_pt"+to_string(j)+"__1up")+systSuffixForHistograms);
		//     weightSystematics.push_back(std::string("btagbSF_"+to_string(i)+"_pt"+to_string(j)+"__1down")+systSuffixForHistograms);
		//   }
            }
            for (int i = 0; i < n_eigenvars_c; ++i) {
              weightSystematics.push_back(std::string("btagcSF_"+to_string(i)+"__1up")+systSuffixForHistograms);
              weightSystematics.push_back(std::string("btagcSF_"+to_string(i)+"__1down")+systSuffixForHistograms);
	      // if(i == 0)
	      // 	for(int j = 1 ; j < 4; ++j){
	      // 	  weightSystematics.push_back(std::string("btagcSF_"+to_string(i)+"_pt"+to_string(j)+"__1up")+systSuffixForHistograms);
	      // 	  weightSystematics.push_back(std::string("btagcSF_"+to_string(i)+"_pt"+to_string(j)+"__1down")+systSuffixForHistograms);
	      // 	  }
            }
            for (int i = 0; i < n_eigenvars_l; ++i) {
              weightSystematics.push_back(std::string("btaglSF_"+to_string(i)+"__1up")+systSuffixForHistograms);
              weightSystematics.push_back(std::string("btaglSF_"+to_string(i)+"__1down")+systSuffixForHistograms);
	      // if(i == 0)
	      // 	for(int j = 1 ; j < 4; ++j){
	      // 	  weightSystematics.push_back(std::string("btaglSF_"+to_string(i)+"_pt"+to_string(j)+"__1up")+systSuffixForHistograms);
	      // 	  weightSystematics.push_back(std::string("btaglSF_"+to_string(i)+"_pt"+to_string(j)+"__1down")+systSuffixForHistograms);
	      // 	  }
            }
            weightSystematics.push_back(std::string("btageSF_0__1up")+systSuffixForHistograms);
            weightSystematics.push_back(std::string("btageSF_0__1down")+systSuffixForHistograms);
            weightSystematics.push_back(std::string("btageSF_1__1up")+systSuffixForHistograms);
            weightSystematics.push_back(std::string("btageSF_1__1down")+systSuffixForHistograms);
          //} else if (_btags < 0) {
          //  for (size_t i = 0; i < trackjetSFs.size(); ++i) {
          //    weightSystematics.push_back(trackjetSFs[i]+systSuffixForHistograms);
          //  }
          //}
        }

        // loop over weight systematics
        for (size_t wIdx = 0; wIdx < weightSystematics.size(); ++wIdx) {
          std::string &suffix = weightSystematics[wIdx];
          double weight = 1;
          if (!isData) {
	    bool showWeights = false;
	    if(printWeights)
	      showWeights = true;

	    weight *= sel.weight_mc();
	    if (showWeights) cout << "\nSTART :: MC " << weight << endl;

	    // Switching off the PWR for fake estimates
	    //if (channel >= 361300 && channel <= 361368) weight *= sel.weight_mc();
            //else                                        weight *= sel.weight_mc()*sel.weight_pileup();

            weight *= sampleXsection.getXsection(channel);
	    if (showWeights) cout << "+Xsec: " << weight << " channel : " << channel<<" Xsec * Kfactor "<< sampleXsection.getXsection(channel)<<endl;

	    // Only apply to W+jets and Z+jets

	    //	    if( channel >= 363331 && channel <= 363411 ){
	    if(sel.weight_sherpa22()){
	      weight *= sel.weight_sherpa22();
	      if (showWeights) cout << "+Sherpa22 weight: " << weight << endl;
	    }
	      //	    }
	    /*(SH) In case of a signal sample.
	      So far only implemented for TTS* with DSIDS:
	      302468,302469,302470,302471,302472,302473,302474,302475,
	      302476,302477,302478,302479,302480,302481,302482)
	    The initial sample was produced with equal branching to Wb,Zt and Ht*/


	    /* (SH) For the singlet model, we can apply the following reweightings:
	       model = {
	       "Wb" : [0.49676,0.49379,0.49340,0.49347,0.49361,0.49378,0.49395,0.49412,0.49427,0.49442,0.49455,0.49467,0.49478,0.49497,0.49512],
	       "Zt" : [0.17293,0.19469,0.20914,0.21457,0.21911,0.22296,0.22624,0.22906,0.23150,0.23362,0.23548,0.23712,0.23857,0.24101,0.24298],
	       "Ht" : [0.33031,0.31151,0.29746,0.29196,0.28727,0.28326,0.27981,0.27682,0.27423,0.27196,0.26997,0.26821,0.26665,0.26402,0.26190],
	       "masses" : [500,600,700,750,800,850,900,950,1000,1050,1100,1150,1200,1300,1400]
            }

	    if 'WbWb' in key:
	    h.Scale(9.0*BR_Wb*BR_Wb)
	    if 'ZtZt' in key:
	    h.Scale(9.0*BR_Zt*BR_Zt)
	    if 'HtHt' in key:
	    h.Scale(9.0*BR_Ht*BR_Ht)
	    if 'WbZt' in key:
	    h.Scale((9.0/2.0)*BR_Wb*BR_Zt*2)
	    if 'WbHt' in key:
	    h.Scale((9.0/2.0)*BR_Wb*BR_Ht*2)
	    if 'ZtHt' in key:
	    h.Scale((9.0/2.0)*BR_Zt*BR_Ht*2)

	     */
	    if( files.find("TTS_M") != std::string::npos ||
		files.find("BBS_M") != std::string::npos
		){
	      if(files.find("_WbWb") != std::string::npos ||
		 files.find("_WtWt") != std::string::npos
		 ){
		// will give a 100 per cent branching to WbWb
		if(sel.vlq_evtype() != 0 ) continue;
		//		weight *= 9;
		if (showWeights)	std::cout <<"DEBUG2 :: wbwb " << weight<<std::endl;
	      }
	      if(files.find("_ZtZt") != std::string::npos ||
		 files.find("_ZbZb") != std::string::npos){
		if(sel.vlq_evtype() != 1 ) continue;
		//		weight *= 9;
		if (showWeights)		std::cout <<"DEBUG :: ztzt" << std::endl;
	      }
	      if(files.find("_HtHt") != std::string::npos ||
		 files.find("_HbHb") != std::string::npos){
		if(sel.vlq_evtype() != 2 ) continue;
		//		weight *= 9;
		if (showWeights)		std::cout <<"DEBUG :: htht" << std::endl;
	      }
	      if( files.find("_WbZt") != std::string::npos ||
		  files.find("_WtZb") != std::string::npos){
		if(sel.vlq_evtype() != 3 ) continue;
		//		weight *= 4.5;
		if (showWeights)		std::cout <<"DEBUG2 :: wbzt" << std::endl;
	      }
	      if( files.find("_WbHt") != std::string::npos ||
		  files.find("_WtHb") != std::string::npos){
		if(sel.vlq_evtype() != 4 ) continue;
		//		weight *= 4.5;
		if (showWeights)		std::cout <<"DEBUG2 :: wbht" << std::endl;
	      }
	      if( files.find("_ZtHt") != std::string::npos ||
		  files.find("_ZbHb") != std::string::npos){
		if(sel.vlq_evtype() != 5 ) continue;
		//		weight *= 4.5;
		if (showWeights)		std::cout <<"DEBUG :: ztht" << std::endl;
	      }
	      //	      if(sigBR == ""){
	      //		std::cerr <<"ERROR :: Exiting because no sigBR was chosen"<< std::endl;
	      //		exit (EXIT_FAILURE);
	      //	      }
	    }
	    if (showWeights) cout << "+Xsec*BR: " << weight << endl;
            double pdfw = 1.0;
            bool isPdf = false;
            std::string pdfname = "";
            int pdfvar = -1;
            if (suffix.find("pdf_") != std::string::npos) {
              isPdf = true;
              size_t last = suffix.rfind("_");
              size_t first = std::string("pdf_").size();
              pdfname = suffix.substr(first, last - first);
              pdfvar = atoi(suffix.substr(last+1).c_str());
              pdfw = mt.vf(pdfname)->at(pdfvar);
            }
            weight *= pdfw;
	    if (showWeights)  cout << "+pdf: " << weight << endl;


	    //(SH) implement here
	    if(channel == 410000 && applyPtRew){
	      std::cout <<"DEBUG :: b weight :> "<< weight << std::endl;
     	      weight *= ttWeight(sel);
	      std::cout <<"DEBUG :: a weight :> "<< weight << std::endl;
	    }
            double btagsf = 1.0;

            std::string pref = "weight_bTagSF_77";
            //std::string prefe = "weight_bTagSF_70_env";
            std::string prefe = "weight_bTagSF_77";
            if (_btags < 0 && _btags > -10) {
              pref = "weight_trackjet_bTagSF_70";
              //prefe = "weight_trackjet_bTagSF_70_env";
              prefe = "weight_trackjet_bTagSF_70";
            } else if (_btags < 0 && _btags <= -10) { // tag leading track jet only
              pref = "tjet_bTagSF_70";
              prefe = "tjet_bTagSF_70";
            } else if ((_btags > 0 || (_btags == 0 && !btagEx)) && _btags >= 10) { // tag leading calo jet only
	      //	      pref = "jet_bTagSF_70";
	      //	      prefe = "jet_bTagSF_70";
	      pref  = "weight_bTagSF_77";
	      prefe = "weight_bTagSF_77";
            }

	    // Standard b-tagging
            if (std::fabs(_btags) < 10) {
	      if(btagSFoffline){ //apply btagging SF offline
		std::cerr << "ERROR :: tries applying offline b-tagging SFs which is currently commented out"<< std::endl;
		std::exit(-2);
		//		btagsf = btagOffline.applyBTagSF(sel, suffix); // mv1c10 @77 % WP
		//std::cout <<"DEBUG :: offline btagging > "<< btagsf << std::endl;
	      }
	      else{
		size_t last = suffix.find("__1");
		if (suffix.find("btagbSF_") != std::string::npos) {
		  size_t first = std::string("btagbSF_").size();
		  int eig = atoi(suffix.substr(first, last - first).c_str());
		  if (suffix.find("1up") != std::string::npos) {
		    btagsf = mt.vf(pref+"_eigenvars_B_up")->at(eig);
		  } else {
		    btagsf = mt.vf(pref+"_eigenvars_B_down")->at(eig);
		  }
		} else if (suffix.find("btagcSF_") != std::string::npos) {
		  size_t first = std::string("btagcSF_").size();
		  int eig = atoi(suffix.substr(first, last - first).c_str());
		  if (suffix.find("1up") != std::string::npos) {
		    btagsf = mt.vf(pref+"_eigenvars_C_up")->at(eig);
		  } else {
		    btagsf = mt.vf(pref+"_eigenvars_C_down")->at(eig);
		  }
		} else if (suffix.find("btaglSF_") != std::string::npos) {
		  size_t first = std::string("btaglSF_").size();
		  int eig = atoi(suffix.substr(first, last - first).c_str());
		  if (suffix.find("1up") != std::string::npos) {
		    btagsf = mt.vf(pref+"_eigenvars_Light_up")->at(eig);
		  } else {
		    btagsf = mt.vf(pref+"_eigenvars_Light_down")->at(eig);
		  }
		} else if (suffix.find("btageSF_0") != std::string::npos) {
		  if (suffix.find("1up") != std::string::npos) {
		    btagsf = mt.f(prefe+"_extrapolation_up");
		  } else {
		    btagsf = mt.f(prefe+"_extrapolation_down");
		  }
		} else if (suffix.find("btageSF_1") != std::string::npos) {
		  if (suffix.find("1up") != std::string::npos) {
		    btagsf = mt.f(prefe+"_extrapolation_from_charm_up");
		  } else {
		    btagsf = mt.f(prefe+"_extrapolation_from_charm_down");
		  }
		} else {
		  btagsf = mt.f(pref);

		}
		//	      std::cout <<"DEBUG :: ntuples btagging > "<< btagsf << std::endl;
	      }





            } else if (std::fabs(_btags) >= 10) {
              if (mt.vf("tjet_pt")->size() > 0) {
                size_t last = suffix.find("__1");
                if (suffix.find("btagbSF_") != std::string::npos) {
                  size_t first = std::string("btagbSF_").size();
                  int eig = atoi(suffix.substr(first, last - first).c_str());
                  if (suffix.find("1up") != std::string::npos) {
                    btagsf = mt.vvf(pref+"_eigen_B_up")->at(0)[eig];
                  } else {
                    btagsf = mt.vvf(pref+"_eigen_B_down")->at(0)[eig];
                  }
                } else if (suffix.find("btagcSF_") != std::string::npos) {
                  size_t first = std::string("btagcSF_").size();
                  int eig = atoi(suffix.substr(first, last - first).c_str());
                  if (suffix.find("1up") != std::string::npos) {
                    btagsf = mt.vvf(pref+"_eigen_C_up")->at(0)[eig];
                  } else {
                    btagsf = mt.vvf(pref+"_eigen_C_down")->at(0)[eig];
                  }
                } else if (suffix.find("btaglSF_") != std::string::npos) {
                  size_t first = std::string("btaglSF_").size();
                  int eig = atoi(suffix.substr(first, last - first).c_str());
                  if (suffix.find("1up") != std::string::npos) {
                    btagsf = mt.vvf(pref+"_eigen_Light_up")->at(0)[eig];
                  } else {
                    btagsf = mt.vvf(pref+"_eigen_Light_down")->at(0)[eig];
                  }
                } else if (suffix.find("btageSF_0") != std::string::npos) {
                  if (suffix.find("1up") != std::string::npos) {
                    btagsf = mt.vf(prefe+"_syst_extrapolation_up")->at(0);
                  } else {
                    btagsf = mt.vf(prefe+"_syst_extrapolation_down")->at(0);
                  }
                } else if (suffix.find("btageSF_1") != std::string::npos) {
                  if (suffix.find("1up") != std::string::npos) {
                    btagsf = mt.vf(prefe+"_syst_extrapolation_from_charm_up")->at(0);
                  } else {
                    btagsf = mt.vf(prefe+"_syst_extrapolation_from_charm_down")->at(0);
                  }
                } else {
                  btagsf = mt.vf(pref)->at(0);
                }
              }
            }

            weight *= btagsf;
	    if (showWeights) cout << "+btag: " << weight << endl;
            if (suffix == "eTrigSF__1up" || suffix == "eTrigSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_Trigger_UP");
            } else if (suffix == "eTrigSF__1down" || suffix == "eTrigSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_Trigger_DOWN");
            } else if (suffix == "eRecoSF__1up" || suffix == "eRecoSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_Reco_UP");
            } else if (suffix == "eRecoSF__1down" || suffix == "eRecoSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_Reco_DOWN");
            } else if (suffix == "eIDSF__1up" || suffix == "eIDSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_ID_UP");
            } else if (suffix == "eIDSF__1down" || suffix == "eIDSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_ID_DOWN");
            } else if (suffix == "eIsolSF__1up" || suffix == "eIsolSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_Isol_UP");
            } else if (suffix == "eIsolSF__1down" || suffix == "eIsolSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_EL_SF_Isol_DOWN");
            } else if (suffix == "muTrigStatSF__1up" || suffix == "muTrigStatSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Trigger_STAT_UP");
            } else if (suffix == "muTrigStatSF__1down" || suffix == "muTrigStatSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Trigger_STAT_DOWN");
            } else if (suffix == "muIDStatSF__1up" || suffix == "muIDStatSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_STAT_UP");
            } else if (suffix == "muIDStatSF__1down" || suffix == "muIDStatSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_STAT_DOWN");
	    } else if (suffix == "muIDlowPTStatSF__1up" || suffix == "muIDlowPTStatSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP");
            } else if (suffix == "muIDlowPTStatSF__1down" || suffix == "muIDlowPTStatSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN");
            } else if (suffix == "muIsolStatSF__1up" || suffix == "muIsolStatSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Isol_STAT_UP");
            } else if (suffix == "muIsolStatSF__1down" || suffix == "muIsolStatSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Isol_STAT_DOWN");
	    } else if (suffix == "muTTVAStatSF__1up" || suffix == "muTTVAStatSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_TTVA_STAT_UP");
            } else if (suffix == "muTTVAStatSF__1down" || suffix == "muTTVAStatSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_TTVA_STAT_DOWN");
            } else if (suffix == "muTrigSystSF__1up" || suffix == "muTrigSystSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Trigger_SYST_UP");
            } else if (suffix == "muTrigSystSF__1down" || suffix == "muTrigSystSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Trigger_SYST_DOWN");
            } else if (suffix == "muIDSystSF__1up" || suffix == "muIDSystSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_SYST_UP");
            } else if (suffix == "muIDSystSF__1down" || suffix == "muIDSystSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_SYST_DOWN");
	       } else if (suffix == "muIDlowPTSystSF__1up" || suffix == "muIDlowPTSystSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP");
            } else if (suffix == "muIDlowPTSystSF__1down" || suffix == "muIDlowPTSystSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN");
            } else if (suffix == "muIsolSystSF__1up" || suffix == "muIsolSystSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Isol_SYST_UP");
            } else if (suffix == "muIsolSystSF__1down" || suffix == "muIsolSystSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_Isol_SYST_DOWN");
	    } else if (suffix == "muTTVASystSF__1up" || suffix == "muTTVASystSF__1up_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_TTVA_SYST_UP");
            } else if (suffix == "muTTVASystSF__1down" || suffix == "muTTVASystSF__1down_Loose") {
              weight *= mt.f("weight_leptonSF_MU_SF_TTVA_SYST_DOWN");
	    } else {
              weight *= sel.weight_leptonSF();
            }
	    if (showWeights) cout << "+lepton " << weight << endl;


	    if (suffix == "pileup__1up" || suffix == "pileup__1up_Loose") {
              weight *= mt.f("weight_pileup_UP");
            } else if (suffix == "pileup__1down" || suffix == "pileup__1down_Loose") {
              weight *= mt.f("weight_pileup_DOWN");
	    } else{
	      if(PRWHashing)
		weight *= sel.PileupWeight();
	      else
		weight *= sel.weight_pileup();
	    }
	    if (showWeights) cout << "+PileUp " << weight << endl;
	    //	    if (showWeights) cout << "+PileUp PRW " << sel.PileupWeight() << endl;
	    //	    if (showWeights) cout << "+PileUp PU " << sel.weight_pileup() << endl;



	    if (suffix == "jvt__1up" || suffix == "jvt__1up_Loose") {
              weight *= mt.f("weight_jvt_UP");
            } else if (suffix == "jvt__1down" || suffix == "jvt__1down_Loose") {
              weight *= mt.f("weight_jvt_DOWN");
	    } else{
	      weight *= sel.weight_jvt();
	    }
	    if (showWeights) cout << "+JVT " << weight << endl;



            if (sumOfWeights[channel] != 0 && !isPdf) {
              weight /= sumOfWeights[channel];
            } else if (isPdf) {
              weight /= PDFsumOfWeights[channel][pdfname][pdfvar];
            }
	    if (showWeights) cout << "+sumWeight: " << weight << endl;



	    // (SH) for comparisons
	    // weight =1 ;


	  }//!isData



	  // Select data corresponding to the year
	  if(DSyear != ""){
	    //Select 2015 data only
	    if(DSyear == "2015"){
	      if (!(sel.passes("ejets_2015") || sel.passes("mujets_2015"))
		  && !(sel.randomRunNumber() >= 276262 && sel.randomRunNumber() <= 284484))
		continue;
	    }else if(DSyear == "2016"){
	      //Select 2016 data only
	      if (!(sel.passes("ejets_2016") || sel.passes("mujets_2016"))
		  && !(sel.randomRunNumber() >= 297730))
		continue;
	    }else{
	      	std::cerr <<"ERROR :: Something not intended is happening in the DS year selection. Exiting ..."<< std::endl;
		exit (EXIT_FAILURE);
	    }
	  }


	  // this applies jet selection early
	  int nJets = 0;
	  if(_njets >= 0) {
	    for (size_t jidx = 0; jidx < sel.jet().size(); ++jidx) {
	      //(SH), skip events with eta == 2.5 since no btag SF are available
	      if(!sel.jet()[jidx].pass()) continue;
	      nJets += 1;
	    }
	    if(nJets < abs(_njets) && !njetEx)
	      continue;
	    else if(nJets != abs(_njets) && njetEx)
	      continue;
	  }else
	    continue;

	  // Apply offline trigger threshold due to turn on curve in 2016 trigger
	  if( ((sel.muon().size() == 0 && sel.electron().size() == 1 ) && sel.electron()[0].mom().Perp()*1e-3 <= 28. ) ||
	      ((sel.muon().size() == 1 && sel.electron().size() == 0 ) && sel.muon()[0].mom().Perp()*1e-3 <= 28. ) )// || sel.muon()[0].mom().Perp()*1e-3 <= 28. )
	    continue;

	  // apply pre-selections for analyses

	  if(grid2pre && analysis.find("AnaHQTVLQWbX") != std::string::npos ){
	    if( nJets <= 3 && sel.largeJet().size() < 1 )
	      continue;
	  }
	  else if(grid2pre && analysis.find("AnaHQTVLQWtX") != std::string::npos){
	    if( nJets < 4 )
	      continue;
	  }




          // this applies b-tagging early
          int nBtagged = 0;
          int nLeadTagged = 0;

	  if (_btags > 0 || (_btags ==0 && !btagEx)) {
            for (size_t bidx = 0; bidx < sel.jet().size(); ++bidx) {
              if (sel.jet()[bidx].btag_mv2c10_77()) {
                nBtagged += 1;
              }
            }
            if (sel.jet()[0].btag_mv2c10_77()) nLeadTagged++;
          } else if (_btags < 0) {
            for (size_t bidx = 0; bidx < mt.vf("tjet_mv2c20")->size(); ++bidx) {
              if (mt.vf("tjet_mv2c20")->at(bidx) > -0.3098 && mt.vf("tjet_pt")->at(bidx) > 10e3 &&
                  std::fabs(mt.vf("tjet_eta")->at(bidx)) < 2.5 && mt.vi("tjet_numConstituents")->at(bidx) >= 2) {
                nBtagged += 1;
              }
            }
            if (mt.vf("tjet_pt")->size() > 0) {
              if (mt.vf("tjet_mv2c20")->at(0) > -0.3098 && mt.vf("tjet_pt")->at(0) > 10e3 &&
                  std::fabs(mt.vf("tjet_eta")->at(0)) < 2.5 && mt.vi("tjet_numConstituents")->at(0) >= 2) {
                nLeadTagged += 1;
              }
            }
          }
          if (_btags != 0 && std::fabs(_btags) < 10) {
            if (nBtagged < abs(_btags) && !btagEx)
              continue;
	    else if (nBtagged != abs(_btags) && btagEx)
	      continue;
	  } else if (_btags != 0 && std::fabs(_btags) >= 10) {
            if (nLeadTagged == 0)
	      continue;
          }



	  VLQUtils vlqutils;
	  // int Wtags = 0;
	  // if(_Wtags >= 0) {
	  //   Wtags = vlqutils.boostedWtags(sel.largeJet(), "loose");
	  //  if(Wtags < abs(_Wtags) && !WtagEx)
	  //    continue;
	  //  else if(Wtags != abs(_Wtags) && WtagEx)
	  //    continue;
	  // }else
	  //  continue;

	  //(SH)	  std::cout <<"DEBUG :: MET1  : " << sel.met().Perp() << " MET2 : "<< mt.f("met_met")<< std::endl;



	  // (SH) for the requested N minus 1 plots
	  //	  if ( sel.met().Perp()*1e-3 < 60. )
	  //	    continue;



          procEvents++;
	  if (runMM) {
	    bool ATweights = false; // FakesTools weights directly stored in n-tuple coming out ot AnalysisTop
	    bool topfakes = false;  // offline derivation of QCD weights using the topfakes method
	    bool fakesTools = true; // old 20.1 approach which goes back to the 13 TeV DiffXsec measurement and to the group of Manchester

	    bool showWeights = false;
	    if(printWeights)
	      showWeights = true;


	    std::string year = "";
	    if( (sel.passes("ejets_2015") || sel.passes("mujets_2015")) )
	      year = "2015";
	    else if ( (sel.passes("ejets_2016") || sel.passes("mujets_2016")) )
	      year = "2016";

	    int debug = 0;
	    weight = 1.;
	    float fakesWeight = 1; // calculate the Matrix Method weight



	    if(ATweights){
	      if( (sel.passes("ejets_2015"))){
		fakesWeight = mt.f("fakesMM_weight_ejets_2015_nedaa");
		if (showWeights && isTight)	cout <<"INFO :: 2015 ele > isTight" << endl;
		if (showWeights && !isTight)	cout <<"INFO :: 2015 ele > " << endl;
	      }	      else if( (sel.passes("mujets_2015"))) {
		fakesWeight =  mt.f("fakesMM_weight_mujets_2015_nedaa");
		if (showWeights && isTight)	cout <<"INFO :: 2015 mu > isTight" << endl;
		if (showWeights && !isTight)	cout <<"INFO :: 2015 mu > " << endl;
	      }	      else if( (sel.passes("ejets_2016"))) {
		fakesWeight =  mt.f("fakesMM_weight_ejets_2016_nedaa");
		if (showWeights && isTight)	cout <<"INFO :: 2016 e > isTight" << endl;
		if (showWeights && !isTight)	cout <<"INFO :: 2016 e > " << endl;
	      } else if( (sel.passes("mujets_2016"))) {
		fakesWeight =  mt.f("fakesMM_weight_mujets_2016_nedaa");
		if (showWeights && isTight)	cout <<"INFO :: 2016 mu > isTight" << endl;
		if (showWeights && !isTight)	cout <<"INFO :: 2016 mu > " << endl;
	    }

	      weight *= fakesWeight;
	      if (showWeights)
		cout << "+QCD weight: " << weight << endl;



	    }else if(topfakes){
	      if(pPath==NULL)
		std::cout << "WARNING :: Please setup the global variable $ANATOP pointing to the directory that includes TopDataPreparation" << std::endl;
	      stringstream sfake;
	      string datadir;
	      sfake << pPath;
	      sfake >> datadir;
	      //  path.append("/TopDataPreparation/data/XSection-MC15-13TeV-fromSusyGrp.data");
	      datadir.append("/TopFakes/data/20p7/Standard/");
	      std::cout <<"DEBUG :: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << datadir<<std::endl;
	      MMEvent event;
	      event.njets = nJets;
	      event.ntag = nBtagged;
	      event.jetpt = sel.jet()[0].mom().Perp()*1e-3;
	      MMLepton lepton;
	      if(sel.electron().size() == 1 && sel.muon().size() == 0  /*&& sel.passes("ejets")*/){
		// for ejet events

		FakesWeights* m_efakes = new FakesWeights();
		m_efakes->SetDataPath(datadir);
		if (year.find("2015") != std::string::npos){
		  m_efakes->SetPeriod(1); // isData is 1 for 2015 and 2 for 2016 (QCD)
		  //config1		   NS:pt:dR,Nedaa2015:NS:eta:pt:dPhi
		  //config 5  NS:pt:dR,Nedaa2015:NB/NS:eta:pt:dPhi
		  //		  m_efakes->SetupWeighter(FakesWeights::EJETS, "Nedaa2015_2412-fakesStd:NS:eta:pt:jetpt:dR", "Nedaa2015_2412-fakesStd:NB/NS:eta:pt:jetpt:dR");
		  m_efakes->SetupWeighter( FakesWeights::EJETS, "Fred2015_2412-fakesStd:NS:eta:pt:jetpt:dR", "Fred2015_2412-fakesStd:NB/NS:eta:pt:jetpt:dR");

		} else if (year.find("2016") != std::string::npos){
		  m_efakes->SetPeriod(2); // isData is 1 for 2015 and 2 for 2016 (QCD)
		  //		  m_efakes->SetupWeighter(FakesWeights::EJETS, "Nedaa2016_2412-fakesStd:NS/TR:eta:pt:jetpt:dR", "Nedaa2016_2412-fakesStd:NB/NS/TR:eta:pt:jetpt:dR");
		  m_efakes->SetupWeighter( FakesWeights::EJETS, "Fred2016_2412-fakesStd:NS/TR:eta:pt:jetpt:dR", "Fred2016_2412-fakesStd:NB/NS/TR:eta:pt:jetpt:dR");
		}



		int trigmatch = 0;
		if (year.find("2015") != std::string::npos) trigmatch = 0;
		if (year.find("2016") != std::string::npos){
		  if (mt.vc("el_trigMatch_HLT_e24_lhtight_nod0_ivarloose")) trigmatch |= 0x1 << 0;
		  if (mt.vc("el_trigMatch_HLT_e60_lhmedium")) trigmatch |= 0x1 << 1;
		  if (mt.vc("el_trigMatch_HLT_e24_lhmedium_L1EM20VH")) trigmatch  |= 0x1 << 2;
		}
		TLorentzVector l;
		l = sel.electron()[0].mom();
		// for ejet events
		lepton.pt = l.Perp()*1e-3;
		lepton.eta = l.Eta();
		lepton.trigger = trigmatch; // not used for ejets

		lepton.dPhi = TVector2::Phi_mpi_pi(l.Phi()-sel.met().Phi());
		lepton.dR = 99;
		for(Int_t i=0; i < nJets; ++i){
		  Float_t this_dR = vlqutils.deltaR(sel.jet()[i].mom().Eta(), lepton.eta, sel.jet()[i].mom().Phi(), l.Phi());
		  if(this_dR < lepton.dR){
	  	   lepton.dR = this_dR;
		  }
		}

		// sanity checks
		if(debug>2){
		  // cout<<"Event "<<jentry<<endl;
		  cout<<"ejets = "<<sel.passes("ejets")<<" , mujets = "<<sel.passes("mujets")<<endl;
		  cout<<"istight = "<<isTight<<endl;
		  cout<<"lepton.pt   = "<<lepton.pt<<endl;
		  cout<<"lepton.eta  = "<<lepton.eta<<endl;
		  cout<<"lepton.dR   = "<<lepton.dR<<endl;
		  cout<<"lepton.dPhi = "<<lepton.dPhi<<endl;
		  cout<<"lepton.trig = "<<lepton.trigger<<endl;
		  cout<<"event.njets = "<<event.njets<<endl;
		  cout<<"event.ntag  = "<<event.ntag<<endl;
		  cout<<"event.jetpt = "<<event.jetpt<<endl;
		}
		assert(lepton.dR < 50);
		m_efakes->SetLepton(event,lepton);
		fakesWeight = m_efakes->GetFakesWeightLJets(isTight);
		// END OF ELECTRON EFFICIENCIES
	      }
	      if(sel.electron().size() == 0 && sel.muon().size() == 1 /* && sel.passes("mujets")*/){
		// for mujet events
		FakesWeights* m_mfakes = new FakesWeights();
		m_mfakes->SetDataPath(datadir);
		if (year.find("2015") != std::string::npos){
		  m_mfakes->SetPeriod(1); // isData is 1 for 2015 and 2 for 2016 (QCD)
		  //config 1  NS/TR:dR,Nedaa2015:NS/TR:pt:dR
		  //config 5  NS/TR:dR,Nedaa2015:NB/NS/TR:pt:dR
		  //		  m_mfakes->SetupWeighter(FakesWeights::MUJETS, "Nedaa2015_2412-fakesStd:NS/TR:eta:pt:jetpt:dR", "Nedaa2015_2412-fakesStd:NB/NS/TR:eta:pt:jetpt:dR");
		  m_mfakes->SetupWeighter( FakesWeights::MUJETS, "Fred2015_2412-fakesStd:NS/TR:eta:pt:jetpt:dR", "Fred2015_2412-fakesStdNoTTVA_CRfaked0:NB/NS/TR:eta:pt:jetpt:dR");

		}
		else if (year.find("2016") != std::string::npos){
		  m_mfakes->SetPeriod(2); // isData is 1 for 2015 and 2 for 2016 (QCD)
		  //		  m_mfakes->SetupWeighter(FakesWeights::MUJETS, "Nedaa2016_2412-fakesStd:NS/TR:eta:pt:jetpt:dR", "Nedaa2016_2412-fakesStd:NB/NS/TR:eta:pt:jetpt:dR");
		  m_mfakes->SetupWeighter( FakesWeights::MUJETS, "Fred2016_2412-fakesStd:NS/TR:eta:pt:jetpt:dR", "Fred2016_2412-fakesStd:NB/NS/TR:eta:pt:jetpt:dR");

		}


		int trigmatch = 0;
		if (year.find("2015") != std::string::npos){
		  if (mt.vc("mu_trigMatch_HLT_mu20_iloose_L1MU15")) trigmatch |= 0x1 << 0;
		  if (mt.vc("mu_trigMatch_HLT_mu50")) trigmatch |= 0x1 << 1;
		  if (mt.vc("mu_fakeTrigMatch_HLT_mu20_L1MU15")) trigmatch  |= 0x1 << 2;
		}
		if (year.find("2016") != std::string::npos){
		  if (mt.vc("mu_trigMatch_HLT_mu24_ivarmedium")) trigmatch |= 0x1 << 0;
		  if (mt.vc("mu_trigMatch_HLT_mu50")) trigmatch |= 0x1 << 1;
		  if (mt.vc("mu_fakeTrigMatch_HLT_mu24")) trigmatch  |= 0x1 << 2;
		}

		TLorentzVector l;
		l = sel.muon()[0].mom();
		// for mujet events
		lepton.pt = l.Perp()*1e-3;
		lepton.eta = l.Eta();

		//		if (sel.muon()[0].HLT_mu20_iloose_L1MU15() ) trigger |= 0x1 << 0;
		//		if (sel.muon()[0].HLT_mu50() ) trigger |= 0x1 << 1;
		//(SH) HACK!!! Use same trigger twice since we do not use the recommeded trigger from TopFakes
		//		if (sel.muon()[0].HLT_mu20_iloose_L1MU15()) trigger |= 0x1 << 2;
		lepton.trigger = trigmatch;
		lepton.dPhi = TVector2::Phi_mpi_pi(l.Phi()-sel.met().Phi());
		lepton.dR = 99;
		for(Int_t i=0; i < nJets; ++i){
		  Float_t this_dR = vlqutils.deltaR(sel.jet()[i].mom().Eta(), lepton.eta, sel.jet()[i].mom().Phi(), l.Phi());
		  if(this_dR < lepton.dR){
		    lepton.dR = this_dR;
		  }
		}

		// sanity checks
		if(debug>2){
		  // cout<<"Event "<<jentry<<endl;
		  cout<<"ejets = "<<sel.passes("ejets")<<" , mujets = "<<sel.passes("mujets")<<endl;
		  cout<<"istight = "<<isTight<<endl;
		  cout<<"lepton.pt   = "<<lepton.pt<<endl;
		  cout<<"lepton.eta  = "<<lepton.eta<<endl;
		  cout<<"lepton.dR   = "<<lepton.dR<<endl;
		  cout<<"lepton.dPhi = "<<lepton.dPhi<<endl;
		  cout<<"lepton.trig = "<<lepton.trigger<<endl;
		  cout<<"event.njets = "<<event.njets<<endl;
		  cout<<"event.ntag  = "<<event.ntag<<endl;
		  cout<<"event.jetpt = "<<event.jetpt<<endl;
		}
	        assert(lepton.dR < 50);
		m_mfakes->SetLepton(event,lepton);
		fakesWeight = m_mfakes->GetFakesWeightLJets(isTight);
		// END OF MUON EFFICIENCIES
	      }

	     weight *= fakesWeight; // apply the Matrix Method weight to the global event weight (which should be 1 for data)
	     //	     cout<<"fakes weight = "<<weight<<endl;
	    }

	    //FakesTools-00-00-03 Implementation
	    else if(fakesTools){
	      weight = 1.;
	      int syst_fakeEff = FakeEffProvider::Systematic::nominal;
	      int syst_realEff = FakeEffProvider::Systematic::nominal;

	      double ef = 0.0;
	      double er = 0.0;
	      double mf = 0.0;
	      double mr = 0.0;
	      double delta = 0.0;

	      if(sel.electron().size() == 1 && sel.muon().size() == 0  /*&& sel.passes("ejets")*/){
	        FakeEffProvider* e_fakeEff  = new FakeEffProvider(0, FakeEffProvider::Type::fakeEff, syst_fakeEff);
	        FakeEffProvider* e_realEff  = new FakeEffProvider(0, FakeEffProvider::Type::realEff, syst_realEff);
		TLorentzVector l;
		l = sel.electron()[0].mom();
		float dphi_l_met = TVector2::Phi_mpi_pi(l.Phi()-sel.met().Phi());
		// ELECTRON EFFICIENCIES
		if (sel.electron()[0].isTightPP()) delta = 1.0;

		ef = e_fakeEff->GetEfficiency2D(FakeEffProvider::Var::TWODIM_PT_NBTAG_G1J, l.Perp()*1e-3, nBtagged, true);
		ef *= e_fakeEff->GetEfficiency2D(FakeEffProvider::Var::TWODIM_DPHI_ETA_G1J, fabs(dphi_l_met), fabs(l.Eta()), true);
		ef *= e_fakeEff->GetEfficiency2D(FakeEffProvider::Var::TWODIM_PT_DPHI_G1J, l.Perp()*1e-3, fabs(dphi_l_met), true);
		ef  = TMath::Power(ef,0.333);

		er = e_realEff->GetEfficiency(FakeEffProvider::Var::LPT, l.Perp()*1e-3, true);

		// END OF ELECTRON EFFICIENCIES
	      }
	      if(sel.electron().size() == 0 && sel.muon().size() == 1 /* && sel.passes("mujets")*/){
		FakeEffProvider* mu_fakeEff  = new FakeEffProvider(1, FakeEffProvider::Type::fakeEff, syst_fakeEff);
		FakeEffProvider* mu_realEff  = new FakeEffProvider(1, FakeEffProvider::Type::realEff, syst_realEff);
		TLorentzVector l;
		l = sel.muon()[0].mom();
		float dphi_l_met = TVector2::Phi_mpi_pi(l.Phi()-sel.met().Phi());
	        // MUON EFFICIENCIES
		if (sel.muon()[0].isTight()) delta = 1.0;
		// HIGH-PT EFFICIENCIES
		double mf_highpt=0;
		double mr_highpt=0;
		mf = mu_fakeEff->GetEfficiency(FakeEffProvider::Var::LPT_G1J, l.Perp()*1e-3, true);
		mf_highpt = mf;

		mr = mu_realEff->GetEfficiency(FakeEffProvider::Var::LPT, l.Perp()*1e-3, true);
		mr_highpt = mr;

		// LOW-PT EFFICIENCIES
		double mf_lowpt=0;
		double mr_lowpt=0;
		mf  = mu_fakeEff->GetEfficiency(FakeEffProvider::Var::MET_G1J, sel.met().Perp()*1e-3, true);
		mf *= mu_fakeEff->GetEfficiency2D(FakeEffProvider::Var::TWODIM_PT_DPHI_G1J, l.Perp()*1e-3, fabs(dphi_l_met), true);
		mf =  TMath::Sqrt(mf);
		mf_lowpt = mf;

		mr =  mu_realEff->GetEfficiency(FakeEffProvider::Var::DPHILMET, fabs(dphi_l_met),true);
		mr *= mu_realEff->GetEfficiency(FakeEffProvider::Var::LPT, l.Perp()*1e-3, true);
		mr =  TMath::Sqrt(mr);
		mr_lowpt = mr;

		// MERGE LOW- AND HIGH-PT
		double x = l.Perp()*1e-3;
		double fhigh = 1./(1+exp(-(x-60.)/10.));
		mf = (1.-fhigh)*mf_lowpt + fhigh*mf_highpt;
		mr = (1.-fhigh)*mr_lowpt + fhigh*mr_highpt;

		// END OF MUON EFFICIENCIES
	      }


	      // some tricks to correct unphysical efficiency values -- should be rare!
	      if (ef>1) {
		ef = 1;
	      }
	      if (er>1) {
		er = 1;
	      }
	     if (ef>=0.99*er) {
	       ef = 0.99*er;
	     }
	     if (mf>1) {
	       mf = 1;
	     }
	     if (mr>1) {
	       mr = 1;
	     }
	     if (mf>=0.99*mr) {
	       mf = 0.99*mr;
	     }
	     //	     cout <<"DEBUG :: " << endl;
	     //	     cout<<"erealeff = "<<er<<endl;
	     //	     cout<<"efakeeff = "<<ef<<endl;
	     //	     cout<<"mrealeff = "<<mr<<endl;
	     //	     cout<<"mfakeeff = "<<mf<<endl;
	     if(sel.electron().size() == 0 && sel.muon().size() == 1){
	       er = mr;
	       ef = mf;
	     }
	     if(er==0){
	       cout<<"WARNING! er=0!  Setting to 1e-6..."<<endl;
	       er=1e-6;
	     }
	     if(ef==0){
	       cout<<"WARNING! ef=0!  Setting to 1e-7..."<<endl;
	       ef=1e-7;
	     }
	     //	     cout<<"weight = "<<weight<<endl;
	     double fakesWeight = (ef)*(er-delta)/(er-ef); // calculate the Matrix Method weight
	     weight *= fakesWeight; // apply the Matrix Method weight to the global event weight (which should be 1 for data)
	     //	     cout<<"fakes weight = "<<weight<<endl;

	    }else{
	      	std::cerr <<"ERROR :: No QCD estimation selected. Exiting ..."<< std::endl;
		exit (EXIT_FAILURE);
	    }
	    if (showWeights) cout << "+QCD weight: " << weight << endl;
	  }//runMM


	  if (analysis=="AnaHQTVLQWbX") {
            for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna) {
                bool good = false;
                if (onlyChannelList.size() == 0) good = true;
                for (size_t iChannel = 0; iChannel < onlyChannelList.size(); ++iChannel) {
                  if (iAna == onlyChannelList[iChannel]) good = true;
                }
                if (good)
                  vec_analysis[iAna]->run(sel, weight, suffix);
            }
	  }
	  else if (analysis=="AnaHQTVLQWtX") {
            for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna) {
	      bool good = false;
	      if (onlyChannelList.size() == 0) good = true;
                for (size_t iChannel = 0; iChannel < onlyChannelList.size(); ++iChannel) {
                  if (iAna == onlyChannelList[iChannel]) good = true;
                }
                if (good)
                  vec_analysis[iAna]->run(sel, weight, suffix);
            }
	  }

	  else if (analysis=="ControlPlots") {
            for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna) {
                bool good = false;
                if (onlyChannelList.size() == 0) good = true;
                for (size_t iChannel = 0; iChannel < onlyChannelList.size(); ++iChannel) {
                  if (iAna == onlyChannelList[iChannel]) good = true;
                }
                if (good)
                  vec_analysis[iAna]->run(sel, weight, suffix);
            }
	  }

	    /*
	   else if (analysis=="AnaTtresSL") {
	      for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna) {
                bool good = false;
                if (onlyChannelList.size() == 0) good = true;
                for (size_t iChannel = 0; iChannel < onlyChannelList.size(); ++iChannel) {
                  if (iAna == onlyChannelList[iChannel]) good = true;
                }
                if (good)
                  vec_analysis[iAna]->run(sel, weight, suffix);
	      }
	    } else if (analysis=="AnaTtresQCDreal") {
	      for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna)
		(dynamic_cast<AnaTtresQCD*>(vec_analysis[iAna]))->runEfficiency(sel, weight, suffix);
	    } else if (analysis=="AnaTtresQCDfake") {
	      for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna)
		(dynamic_cast<AnaTtresQCD*>(vec_analysis[iAna]))->runFakeRate(sel, weight, suffix);
	    } else if (analysis=="AnaTtresSLMtt") {
	      for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna)
                vec_analysis[iAna]->run(sel, weight, suffix);
	    } //if
	    */

	  } // end of loop over weight systematics
	} // end of loop over entries
      } // end of loop over lepton modes (tight and loose)
    } // end of loop over systematics

  for (size_t iAna = 0; iAna < vec_analysis.size(); ++iAna) {
      std::cout << "Removed " << vec_analysis[iAna]->getNduplicate() << " duplicate entries" << std::endl;
      vec_analysis[iAna]->terminate();
      delete vec_analysis[iAna];
    }

    vec_analysis.clear();



    std::cout << "Called analysis code " << procEvents << " times." << std::endl;

    return 0;
  }



