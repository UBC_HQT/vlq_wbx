/**
 * @brief Event class, with information read off the input file read using MiniTree.
 * @author Danilo Enoque Ferreira de Lima <dferreir@cern.ch>
 */
#ifndef EVENT_H
#define EVENT_H

#include <vector>
#include "TopNtupleAnalysis/Electron.h"
#include "TopNtupleAnalysis/Muon.h"
#include "TopNtupleAnalysis/Lepton.h"
#include "TopNtupleAnalysis/Jet.h"
#include "TopNtupleAnalysis/LargeJet.h"
#include "TopNtupleAnalysis/RecoObject.h"

class Event {
  public:
    Event();
    virtual ~Event();

    void clear();

    std::vector<Electron> &electron();
    std::vector<Muon> &muon();
    std::vector<Lepton> &lepton();
    std::vector<Jet> &jet();
    std::vector<LargeJet> &largeJet();
    std::vector<RecoObject> &recoObject();
    std::vector<RecoObject> &resolvedW();
    std::vector<RecoObject> &boostedW();
    std::vector<RecoObject> &reclusteredW();
    std::vector<RecoObject> &reclusteredWsub();
    std::vector<RecoObject> &hadronicT();
    std::vector<RecoObject> &leptonicT();
    std::vector<RecoObject> &hadronicW();
    std::vector<RecoObject> &leptonicW();
    std::vector<RecoObject> &nu();

    std::vector<Jet> &bjet();
    std::vector<Jet> &bhad();
    std::vector<Jet> &blep();
    std::vector<Jet> &tjet();

    const bool trigger(const std::string &t) const;
    void setTrigger(const std::string &t, bool passes);

    const std::vector<Electron> &electron() const;
    const std::vector<Muon> &muon() const;
    const std::vector<Lepton> &lepton() const;
    const std::vector<Jet> &jet() const;
    const std::vector<LargeJet> &largeJet() const;
    const std::vector<RecoObject> &recoObject() const;
    const std::vector<RecoObject> &resolvedW() const;
    const std::vector<RecoObject> &boostedW() const;
    const std::vector<RecoObject> &reclusteredW() const;
    const std::vector<RecoObject> &reclusteredWsub() const;
    const std::vector<RecoObject> &hadronicT() const;
    const std::vector<RecoObject> &leptonicT() const;
    const std::vector<RecoObject> &hadronicW() const;
    const std::vector<RecoObject> &leptonicW() const;
    const std::vector<RecoObject> &nu() const;
    
    const std::vector<Jet> &bjet() const;
    const std::vector<Jet> &bhad() const;
    const std::vector<Jet> &blep() const;
    const std::vector<Jet> &tjet() const;
    
    void met(float met_x, float met_y);
    TLorentzVector met() const;

    unsigned int &runNumber();
    const unsigned int runNumber() const;

    unsigned int &randomRunNumber();
    const unsigned int randomRunNumber() const;


    
    ULong64_t &eventNumber();
    const ULong64_t eventNumber() const;

    ULong64_t &PRWHash();
    const ULong64_t PRWHash() const;

    float &PileupWeight();
    const float PileupWeight() const;
    
     unsigned int &lumiBlock();
    const unsigned int lumiBlock() const;
    
    int &isBoosted();
    const int isBoosted() const;

    int &isResolved();
    const int isResolved() const;
    
    bool &isData();
    const bool isData() const;

    int &channelNumber();
    const int channelNumber() const;

    float &weight_pileup();
    const float weight_pileup() const;

    float &mu();
    const float mu() const;

    //    float &mu_original();
    //    const float mu_original() const;

    float &mu_original();
    const float mu_original() const;
    
    float &weight_mc();
    const float weight_mc() const;

    float &weight_jvt();
    const float weight_jvt() const;

    float &AMI();
    const float AMI() const;

    float &XSection();
    const float XSection() const;

    float &KFactor();
    const float KFactor() const;

    float &FilterEff();
    const float FilterEff() const;

    float &LUMI();
    const float LUMI() const;

    float &weight_sherpa22();
    const float weight_sherpa22() const;
    
    float &TRFWeight();
    const float TRFWeight() const;

    float &HT();
    const float HT() const;

    float &A_distance();
    const float A_distance() const;

     float &A_dr();
     const float A_dr() const;

     float &A_mass();
    const float A_mass() const;
    
    float &DeltaM_hadtopleptop();
    const float DeltaM_hadtopleptop() const;

    float &DeltaR_bjet1bjet2();
    const float DeltaR_bjet1bjet2() const;

    float &DeltaR_lepnu();
    const float DeltaR_lepnu() const;

    float &minDeltaR_lepbjet();
    const float minDeltaR_lepbjet() const;

    float &minDeltaR_hadWbjet();
    const float minDeltaR_hadWbjet() const;

    float &mass_lepb();
    const float mass_lepb() const;
    
    int &btagsN();
    const int btagsN() const;
    
    float &weight_bTagSF();
    const float weight_bTagSF() const;

    float &weight_leptonSF();
    const float weight_leptonSF() const;

    float &weight_lept_eff();
    const float weight_lept_eff() const;
    
    //    float &mtw();
    //    const float mtw() const;

    float &mtw();
    const float mtw() const;
	
    int &vlq_evtype();
    const int vlq_evtype() const;

    std::vector<std::string> &passes();
    const bool passes(const std::string &selection) const;

    // not implemented yet:
    unsigned int &lbn();
    const unsigned int lbn() const;

    const int hfor() const;
    int &hfor();

    int &npv();
    const int npv() const;

    float &vtxz();
    const float vtxz() const;


    TLorentzVector &MC_Wminus();
    const TLorentzVector &MC_Wminus() const;
    TLorentzVector &MC_Wplus();
    const TLorentzVector &MC_Wplus() const;
    TLorentzVector &MC_b_q();
    const TLorentzVector &MC_b_q() const;
    TLorentzVector &MC_bbar_q();
    const TLorentzVector &MC_bbar_q() const;
    TLorentzVector &MC_lep();
    const TLorentzVector &MC_lep() const;
    int &MC_lep_charge();
    const int MC_lep_charge() const;
    int &MC_lep_pdgId();
    const int MC_lep_pdgId() const;
    TLorentzVector &MC_nu();
    const TLorentzVector &MC_nu() const;
    int &MC_nu_pdgId();
    const int MC_nu_pdgId() const;
    TLorentzVector &MC_q1();
    const TLorentzVector &MC_q1() const;
    int &MC_q1_pdgId();
    const int MC_q1_pdgId() const;
    TLorentzVector &MC_q2();
    const TLorentzVector &MC_q2() const;
    int &MC_q2_pdgId();
    const int MC_q2_pdgId() const;
    TLorentzVector &MC_t_q();
    const TLorentzVector &MC_t_q() const;
    TLorentzVector &MC_tbar_q();
    const TLorentzVector &MC_tbar_q() const;



    
    TLorentzVector &MC_w1h();
    const TLorentzVector &MC_w1h() const;
    int &MC_w1h_pdgId();
    const int MC_w1h_pdgId() const;

    TLorentzVector &MC_w2h();
    const TLorentzVector &MC_w2h() const;
    int &MC_w2h_pdgId();
    const int MC_w2h_pdgId() const;

    TLorentzVector &MC_bh();
    const TLorentzVector &MC_bh() const;

    TLorentzVector &MC_w1l();
    const TLorentzVector &MC_w1l() const;
    int &MC_w1l_pdgId();
    const int MC_w1l_pdgId() const;

    TLorentzVector &MC_w2l();
    const TLorentzVector &MC_w2l() const;
    int &MC_w2l_pdgId();
    const int MC_w2l_pdgId() const;

    TLorentzVector &MC_bl();
    const TLorentzVector &MC_bl() const;

    TLorentzVector &MC_ttbar_beforeFSR();
    const TLorentzVector &MC_ttbar_beforeFSR() const;

    //MA
    TLorentzVector &MA_w1h();
    const TLorentzVector &MA_w1h() const;
    int &MA_w1h_pdgId();
    const int MA_w1h_pdgId() const;

    TLorentzVector &MA_w2h();
    const TLorentzVector &MA_w2h() const;
    int &MA_w2h_pdgId();
    const int MA_w2h_pdgId() const;

    TLorentzVector &MA_bh();
    const TLorentzVector &MA_bh() const;

    TLorentzVector &MA_w1l();
    const TLorentzVector &MA_w1l() const;

    int &MA_w1l_pdgId();
    const int MA_w1l_pdgId() const;

    TLorentzVector &MA_w2l();
    const TLorentzVector &MA_w2l() const;

    int &MA_w2l_pdgId();
    const int MA_w2l_pdgId() const;

    TLorentzVector &MA_bl();
    const TLorentzVector &MA_bl() const;

    TLorentzVector &MC_t();
    const TLorentzVector &MC_t() const;

    TLorentzVector &MC_tbar();
    const TLorentzVector &MC_tbar() const;

  protected:

    std::vector<std::string> m_trigger;
    std::vector<std::string> m_passes;

    int m_hfor;
    std::vector<Electron> m_electron;
    std::vector<Muon> m_muon;
    std::vector<Lepton> m_lepton;
    std::vector<Jet> m_jet;
    std::vector<LargeJet> m_largeJet;
    std::vector<RecoObject> m_recoObject;
    std::vector<RecoObject> m_resolvedW;
    std::vector<RecoObject> m_boostedW;
    std::vector<RecoObject> m_reclusteredW;
    std::vector<RecoObject> m_reclusteredWsub;
    std::vector<RecoObject> m_hadronicT;
    std::vector<RecoObject> m_leptonicT;
    std::vector<RecoObject> m_hadronicW;
    std::vector<RecoObject> m_leptonicW;
    std::vector<RecoObject> m_nu;
    TLorentzVector m_met;

    std::vector<Jet> m_bjet;
    std::vector<Jet> m_tjet;
    std::vector<Jet> m_bhad;
    std::vector<Jet> m_blep;


  TLorentzVector m_MC_Wminus;
  TLorentzVector m_MC_Wplus;
  TLorentzVector m_MC_b_q;
  TLorentzVector m_MC_bbar_q;
  TLorentzVector m_MC_lep;
  int m_MC_lep_charge;
  int m_MC_lep_pdgId;
  TLorentzVector m_MC_nu;
  int m_MC_nu_pdgId;
  TLorentzVector m_MC_q1;
  int m_MC_q1_pdgId;
  TLorentzVector m_MC_q2;
  int m_MC_q2_pdgId;
  TLorentzVector m_MC_t_q;
  TLorentzVector m_MC_tbar_q;






    
    TLorentzVector m_MC_w1h;
    int m_MC_w1h_pdgId;
    TLorentzVector m_MC_w2h;
    int m_MC_w2h_pdgId;
    TLorentzVector m_MC_bh;
    TLorentzVector m_MC_w1l;
    int m_MC_w1l_pdgId;
    TLorentzVector m_MC_w2l;
    int m_MC_w2l_pdgId;
    TLorentzVector m_MC_bl;

    TLorentzVector m_MC_ttbar_beforeFSR;

    TLorentzVector m_MC_t;
    TLorentzVector m_MC_tbar;

    TLorentzVector m_MA_w1h;
    int m_MA_w1h_pdgId;
    TLorentzVector m_MA_w2h;
    int m_MA_w2h_pdgId;
    TLorentzVector m_MA_bh;
    TLorentzVector m_MA_w1l;
    int m_MA_w1l_pdgId;
    TLorentzVector m_MA_w2l;
    int m_MA_w2l_pdgId;
    TLorentzVector m_MA_bl;

    unsigned int m_runNumber;
    unsigned int m_randomRunNumber;
    ULong64_t m_eventNumber;
    ULong64_t m_PRWHash;
    float m_PileupWeight;
    int m_channelNumber;
    unsigned int m_lumiBlock;
    int m_isBoosted;
    int m_isResolved;
    bool m_isData;
    int m_vlq_evtype;
    float m_mu;
    //    float m_mu_original;
    float m_mu_original;
    //    float m_mtw;
    float m_mtw;

    int m_npv;
    float m_vtxz;
    float m_rho;

    float m_AMI;
    float m_XSection;    
    float m_KFactor;
    float m_FilterEff;
    float m_LUMI;
    float m_weight_sherpa22;
    float m_TRFWeight;

    float m_HT;
    float m_A_distance;
    float m_A_dr;
    float m_A_mass;
    float m_DeltaM_hadtopleptop;
    float m_DeltaR_bjet1bjet2;
    float m_DeltaR_lepnu;
    float m_minDeltaR_hadWbjet;
    float m_minDeltaR_lepbjet;
    float m_mass_lepb;
    
    int m_btagsN;
    
    float m_weight_mc;
    float m_weight_pileup;
    float m_weight_jvt;
    float m_weight_bTagSF;
    float m_weight_leptonSF;
    float m_weight_lept_eff;

    unsigned int m_lbn;
};

#endif

