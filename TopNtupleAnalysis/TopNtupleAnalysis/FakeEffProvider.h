// -*- C++ -*-

/**
 * Fake (and real) lepton efficiency provider.
 */
#ifndef FAKEEFFPROVIDER_H
#define FAKEEFFPROVIDER_H

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include <TEfficiency.h>
#include <getopt.h>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <map>

#include <vector>
#include <algorithm>

class FakeEffProvider {
  
public:
  FakeEffProvider(int channel, int type=Type::fakeEff, int syst=Systematic::nominal); //channel: e=0, mu=1
  ~FakeEffProvider() {}
  enum Type {fakeEff, realEff, tightEff};
  enum Systematic {nominal, stat_D, stat_U, fakeEff_MCscale_D, fakeEff_MCscale_U, fakeEff_CR_S};
  enum Var {NJETS, NBTAG, LPT, LETA, JET1PT, MINDRLJET, DPHILMET, MET, 
            TWODIM_PT_ETA, TWODIM_PT_DPHI, TWODIM_PT_NBTAG, TWODIM_PT_MET, TWODIM_PT_JET1PT,
            TWODIM_DPHI_NJETS, TWODIM_DPHI_NBTAG, TWODIM_DPHI_MET, TWODIM_DPHI_ETA,
            TWODIM_ETA_NBTAG,
            TWODIM_JET1PT_DPHI, TWODIM_JET1PT_NJETS, TWODIM_JET1PT_NBTAG,
            TWODIM_NJETS_NBTAG,
            LPT_E1J, LPT_G1J, MET_E1J, MET_G1J,
            TWODIM_PT_NBTAG_E1J, TWODIM_PT_NBTAG_G1J,
            TWODIM_PT_ETA_E1J, TWODIM_PT_ETA_G1J,
            TWODIM_PT_DPHI_E1J, TWODIM_PT_DPHI_G1J,
            TWODIM_DPHI_ETA_E1J, TWODIM_DPHI_ETA_G1J,
            TWODIM_JET1PT_DPHI_E1J, TWODIM_JET1PT_DPHI_G1J,
           };
  double GetEfficiency(int var, float value, bool binned=false);
  double GetEfficiency2D(int var, float xvalue, float yvalue, bool binned=false);
  double GetAverageEfficiency() {return m_average;}
  
  static double ErrorWald(double nPas, double nTot, double ePas, double eTot, double z, bool upper, bool correlated);
  static double ErrorNormal(double nPas, double nTot, double ePas, double eTot, double z, bool upper, bool correlated);
  static double ErrorAC(double nPas, double nTot, double ePas, double eTot, double z, bool upper, bool correlated);

private:
  TFile* m_file;
  std::map<int, TEfficiency*> m_hist;
  std::map<int, TF1*> m_func;
  double m_average;
  int m_syst;
  double (*m_ErrorFunc)(double,double,double,double,double,bool,bool);
  
  double ErrorFuncWrapper(TEfficiency*,int,double,bool,bool);
  int GetRightBin(TEfficiency* h, double v);
  int GetRightBin2D(TEfficiency* h, double vx, double vy);
};

//____________________________________________________________________________________________________
#endif
