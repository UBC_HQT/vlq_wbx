/**
 * @brief Analysis class for VLQ->WbX.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#ifndef ANAHQTVLQWBX_PRE_H
#define ANAHQTVLQWBX_PRE_H

#include <string>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TopNtupleAnalysis/Event.h"
#include "TopNtupleAnalysis/Analysis_HQT.h"

#include "TopNtupleAnalysis/VLQUtils.h"
#include "TopNtupleAnalysis/TtresNeutrinoBuilder.h"
#include "TopNtupleAnalysis/TtresChi2.h"

class AnaHQTVLQWbX_pre : public Analysis_HQT {
  public:
    AnaHQTVLQWbX_pre(const std::string &filename, bool lepton, bool boosted, std::vector<std::string> &systList);
    virtual ~AnaHQTVLQWbX_pre();

    void run(const Event &e, double weight, const std::string &syst);
    void terminate() {};
    void setIsData(bool isData) {};

    
  protected:
    bool m_lepton;
    bool m_electron;
    bool m_boosted;

    //    TtresNeutrinoBuilder m_neutrinoBuilder;
    //    TtresChi2 m_chi2;
    VLQUtils m_vlqutils;
    
    double _tree_truemtt;
    double _tree_mtt;
    double _tree_weight;
    int     _tree_cat;
    std::string _tree_syst;
};

#endif

