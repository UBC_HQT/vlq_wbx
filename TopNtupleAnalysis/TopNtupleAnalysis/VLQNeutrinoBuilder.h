#ifndef VLQNEUTRINOBUILDER_H
#define VLQNEUTRINOBUILDER_H

#include<iostream>
#include <string>

// ROOT classes
#include "TLorentzVector.h"
#include "TopNtupleAnalysis/Neutrino.h"

class VLQNeutrinoBuilder{

 public:
  VLQNeutrinoBuilder();
  virtual ~VLQNeutrinoBuilder();
  VLQNeutrinoBuilder(const VLQNeutrinoBuilder&);
  VLQNeutrinoBuilder& operator=(const VLQNeutrinoBuilder&);
  
  inline void setdebuglevel(int level){m_debug = level;};  

  TLorentzVector GetNeutrino(TLorentzVector &lep, Float_t met_et, Float_t met_phi);
  int pz_of_W(TLorentzVector lep, TLorentzVector* met, double* pz);
  double metfit(double fitterPrintLevel, int ysol, double mW);
  
 protected:

  int m_debug;
  
};
#endif
