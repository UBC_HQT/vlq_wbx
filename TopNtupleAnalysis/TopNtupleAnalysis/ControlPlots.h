/**
 * @brief Analysis to provide Control Plots.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#ifndef CONTROLPLOTS_H
#define CONTROLPLOTS_H

#include <string>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TopNtupleAnalysis/Event.h"
#include "TopNtupleAnalysis/Analysis_HQT.h"

#include "TopNtupleAnalysis/VLQUtils.h"
#include "TopNtupleAnalysis/TtresNeutrinoBuilder.h"
#include "TopNtupleAnalysis/TtresChi2.h"

class ControlPlots : public Analysis_HQT {
  public:
  ControlPlots(const std::string &filename, bool electron, bool combined, std::vector<std::string> &systList);
    virtual ~ControlPlots();

    void run(const Event &e, double weight, const std::string &syst);
    void terminate() {};
    void setIsData(bool isData) {};
    void CountEvent(unsigned int& icut, double weight);
    
  protected:
    bool m_electron;
    bool m_combined;

    
    TtresNeutrinoBuilder m_neutrinoBuilder;
    TtresChi2 m_chi2;
    VLQUtils m_vlqutils;
    
    double _tree_truemtt;
    double _tree_mtt;
    double _tree_weight;
    int     _tree_cat;
    std::string _tree_syst;
};

#endif

