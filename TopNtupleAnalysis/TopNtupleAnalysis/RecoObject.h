/**
 * @brief Reconstructed objects from the input file.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#ifndef RECOOBJECT_H
#define RECOOBJECT_H

#include "TopNtupleAnalysis/MObject.h"
#include "TLorentzVector.h"

class RecoObject : public MObject {
  public:
    RecoObject();
    RecoObject(const TLorentzVector &v);
    RecoObject(const RecoObject &l);
    virtual ~RecoObject();

    bool pass() const;
    bool passLoose() const;
    bool passFakeSelection(const TLorentzVector &lept, const TLorentzVector &selJet) const;

    bool passWtagLoose() const;
    bool passWtagTight(const TLorentzVector &lept, const TLorentzVector &selJet) const;
    
    bool good() const;
    bool &good();
    void setGood(bool);

    const double mv2c20() const;
    double &mv2c20();
    
    //    float &subs(const std::string &s);
    //    const float subs(const std::string &s) const;

    double &subs(const std::string &s);
    const double subs(const std::string &s) const;

 protected:
    bool m_good;
    double m_mv2c20;
    //    std::map<std::string, float> m_subs;    
    std::map<std::string, double> m_subs;

};

#endif
