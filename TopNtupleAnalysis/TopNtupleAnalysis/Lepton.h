/**
 * @brief Lepton representation for information read off input file.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#ifndef LEPTON_H
#define LEPTON_H

#include "TopNtupleAnalysis/MObject.h"
#include "TLorentzVector.h"

class Lepton : public MObject {
  public:
    Lepton();
    Lepton(const TLorentzVector &v);
    virtual ~Lepton();

    void setMI(float iso);
    float mi() const;

    void setTight(int isTight);
    bool isTight() const;
    
    int charge() const;
    int &charge();
    
    bool pass() const;
    bool passLoose() const;

  protected:
    float m_mi;
    int m_charge;
    bool m_isTight;
};

#endif
