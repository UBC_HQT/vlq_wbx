#ifndef VLQUTILS_H
#define VLQUTILS_H

#include<iostream>
#include <string>

// ROOT classes
#include "TLorentzVector.h"

//#include "TopNtupleAnalysis/RecoObject.h"
#include "TopNtupleAnalysis/LargeJet.h"
#include "TopNtupleAnalysis/Jet.h"

class VLQUtils{

 public:
  VLQUtils();
  virtual ~VLQUtils();
  VLQUtils(const VLQUtils&);
  VLQUtils& operator=(const VLQUtils&);

  inline void setdebuglevel(int level){m_debug = level;};

  bool TTcandidates(const TLorentzVector*, double, Double_t, std::vector<TLorentzVector*>&);
  //  const TLorentzVector boostedWcandidate(const std::vector<RecoObject> &obj, std::string sel);

  const TLorentzVector boostedWcandidate(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, TLorentzVector lep, bool eEvent, std::string sel);
  const TLorentzVector boostedTcandidate(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, std::string sel);
  const TLorentzVector resolvedWcandidate(const std::vector<Jet> &jet,  std::vector<TLorentzVector> jets, std::vector<TLorentzVector> bJets, std::string sel);
  //int boostedWtags(const std::vector<RecoObject> &obj, std::string sel);
  int boostedWtags(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, TLorentzVector lep, bool eEvent, std::string sel);
  int boostedTopTags(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, std::string sel);

  const TLorentzVector GetResolvedWCandidate(std::vector<TLorentzVector> jets, const TLorentzVector resWhad);

  float GetST(float lepPt, float MET, const std::vector<LargeJet> &lJet, const std::vector<Jet> &jet, std::vector<TLorentzVector>* bjet, int nBtags);
  float GetSTboosted(float lepPt, float MET, const TLorentzVector lJet, const std::vector<Jet> &jet, int nWtags);
  float GetSTSmallJets(float lepPt, float MET, const std::vector<Jet> &jet);
  float CalcMassAsymmetry(const TLorentzVector obj1, const TLorentzVector obj2);
  float CalcMassDifference(const TLorentzVector obj1, const TLorentzVector obj2);
  double deltaR(double etajet, double etalept, double phijet, double philept);
  std::vector<TLorentzVector*>TTcandidates(const TLorentzVector*, const Double_t, const Double_t);
  std::vector<TLorentzVector*>TTcandidates(const TLorentzVector*, const TLorentzVector*);
  std::vector<TLorentzVector*>TTcandidates(const TLorentzVector*,  const Double_t, const Double_t, const bool);
  std::map<std::string, TLorentzVector> BoostedTTcandidates(const TLorentzVector,  const TLorentzVector, std::vector<TLorentzVector>, std::vector<TLorentzVector>, const bool, std::string method, double OR_dRcut);
  std::map<std::string, TLorentzVector> PseudoKLFitter(const TLorentzVector,  const TLorentzVector, std::vector<TLorentzVector>, std::vector<TLorentzVector>, const bool, std::string method, double OR_dRcut);
  std::map<std::string, TLorentzVector>  GetCandidatesAsymmMin(const TLorentzVector,  const TLorentzVector, std::vector<TLorentzVector>, std::map<std::string, TLorentzVector> );
  std::map<std::string, TLorentzVector>  GetCandidatesDeltaMassMin(const TLorentzVector, const TLorentzVector, std::vector<TLorentzVector>, std::map<std::string, TLorentzVector>  ObjectCollection);
  std::vector<TLorentzVector*> RealTTcandidates(const TLorentzVector,  const Double_t, const Double_t, const bool);

 template<typename KeyType, typename ValueType>
      std::pair<KeyType,ValueType> get_min( const std::map<KeyType,ValueType>& x ) {
      using pairtype=std::pair<KeyType,ValueType>;
      return *std::min_element(x.begin(), x.end(), [] (const pairtype & p1, const pairtype & p2) {
	  return p1.second < p2.second;
	});
    }



 protected:

  int m_debug;
  float m_Ar_min;
  float m_DeltaM_min;
};
#endif
