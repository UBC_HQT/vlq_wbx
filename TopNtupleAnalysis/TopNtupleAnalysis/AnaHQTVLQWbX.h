/**
 * @brief Analysis class for VLQ->WbX.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#ifndef ANAHQTVLQWBX_H
#define ANAHQTVLQWBX_H

#include <string>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TopNtupleAnalysis/Event.h"
#include "TopNtupleAnalysis/Analysis_HQT.h"

#include "TopNtupleAnalysis/VLQUtils.h"
#include "TopNtupleAnalysis/TtresNeutrinoBuilder.h"
#include "TopNtupleAnalysis/VLQNeutrinoBuilder.h"

#include "TopNtupleAnalysis/TtresChi2.h"

class AnaHQTVLQWbX : public Analysis_HQT {
  public:
  AnaHQTVLQWbX(const std::string &filename, bool electron, bool boosted, bool combined, int ntuple, int skipRegionPlots, std::string resolvedselection, std::string boostedselection, std::vector<std::string> &systList);
    virtual ~AnaHQTVLQWbX();

    void run(const Event &e, double weight, const std::string &syst);
    void terminate() {};
    void setIsData(bool isData) {};
    void CountEvent(unsigned int& icut, double weight);
    void CountEventRes(unsigned int& icut, double weight);

  protected:
    bool m_electron;
    bool m_boosted;
    bool m_combined;
    int m_ntuple;
    int m_skipRegionPlots;
    std::string m_resolvedselection;
    std::string m_boostedselection;

    TtresNeutrinoBuilder m_neutrinoBuilder;
    TtresChi2 m_chi2;
    VLQUtils m_vlqutils;
    VLQNeutrinoBuilder m_vlqneutrinoBuilder;

    int _tree_WbWb;
    int _tree_ZtZt;
    int _tree_HtHt;
    int _tree_WbZt;
    int _tree_WbHt;
    int _tree_ZtHt;
    double _tree_lep_mass;
    double _tree_had_mass;
    double _tree_MET;
    double _tree_MWT;
    double _tree_ST;
    double _tree_dR_lnu;
    double _tree_lb_mass;
    double _tree_lb_minmass;
    double _tree_lblep_mass;
    double _tree_Delta_mass;
    double _tree_DeltaR_TlepThad;
    double _tree_weight;
    int     _tree_cat;
    std::string _tree_syst;

    unsigned long long int _tree_evid;
    int _tree_Njet;
    int _tree_Nbjet;
    int _tree_Nlep;
    std::vector<int> _tree_region;

};

#endif

