/**
 * @brief Event class, with information read off the input file read using MiniTree.
 * @author Danilo Enoque Ferreira de Lima <dferreir@cern.ch>
 */
#include "TopNtupleAnalysis/Event.h"
#include <vector>
#include <cmath>
#include <algorithm>

Event::Event() {
}

Event::~Event() {
}

std::vector<std::string> &Event::passes() {
  return m_passes;
}

const bool Event::trigger(const std::string &t) const {
  return (std::find(m_trigger.begin(), m_trigger.end(), t) != m_trigger.end());
}

void Event::setTrigger(const std::string &t, bool passes) {
  std::vector<std::string>::iterator it = std::find(m_trigger.begin(), m_trigger.end(), t);
  if (it != m_trigger.end() && !passes) {
    m_trigger.erase(it);
  } else if (it == m_trigger.end() && passes) {
    m_trigger.push_back(t);
  }
}

const bool Event::passes(const std::string &selection) const {
  return std::find(m_passes.begin(), m_passes.end(), selection) != m_passes.end();
}

const int Event::hfor() const {
  return m_hfor;
}

int &Event::hfor() {
  return m_hfor;
}

void Event::clear() {
  m_trigger.clear();
  m_electron.clear();
  m_lepton.clear();
  m_muon.clear();
  m_jet.clear();
  m_bjet.clear();
  m_bhad.clear();
  m_blep.clear();
  m_tjet.clear();
  m_largeJet.clear();
  m_recoObject.clear();
  m_resolvedW.clear();
  m_boostedW.clear();
  m_reclusteredW.clear();
  m_reclusteredWsub.clear();
  m_hadronicT.clear();
  m_leptonicT.clear();
  m_hadronicW.clear();
  m_leptonicW.clear();
  m_nu.clear();
  m_met.SetPxPyPzE(0,0,0,0);
  m_passes.clear();
}

float &Event::weight_pileup() {
  return m_weight_pileup;
}
const float Event::weight_pileup() const {
  return m_weight_pileup;
}

float &Event::weight_jvt() {
  return m_weight_jvt;
}
const float Event::weight_jvt() const {
  return m_weight_jvt;
}

float &Event::weight_mc() {
  return m_weight_mc;
}

const float Event::weight_mc() const {
  return m_weight_mc;
}

float &Event::AMI() {
  return m_AMI;
}

const float Event::AMI() const {
  return m_AMI;
}

float &Event::XSection() {
  return m_XSection;
}

const float Event::XSection() const {
  return m_XSection;
}

float &Event::KFactor() {
  return m_KFactor;
}

const float Event::KFactor() const {
  return m_KFactor;
}

float &Event::FilterEff() {
  return m_FilterEff;
}

const float Event::FilterEff() const {
  return m_FilterEff;
}

float &Event::LUMI() {
  return m_LUMI;
}

const float Event::LUMI() const {
  return m_LUMI;
}

float &Event::weight_sherpa22() {
  return m_weight_sherpa22;
}

const float Event::weight_sherpa22() const {
  return m_weight_sherpa22;
}



float &Event::TRFWeight() {
  return m_TRFWeight;
}

const float Event::TRFWeight() const {
  return m_TRFWeight;
}

float &Event::A_distance() {
  return m_A_distance;
}

const float Event::A_distance() const {
  return m_A_distance;
}

float &Event::A_dr() {
  return m_A_dr;
}

const float Event::A_dr() const {
  return m_A_dr;
}


float &Event::A_mass() {
  return m_A_mass;
}

const float Event::A_mass() const {
  return m_A_mass;
}

float &Event::HT() {
  return m_HT;
}


const float Event::HT() const {
  return m_HT;
}

float &Event::DeltaM_hadtopleptop() {
  return m_DeltaM_hadtopleptop;
}

const float Event::DeltaM_hadtopleptop() const {
  return m_DeltaM_hadtopleptop;
}

float &Event::DeltaR_bjet1bjet2() {
  return m_DeltaR_bjet1bjet2;
}

const float Event::DeltaR_bjet1bjet2() const {
  return m_DeltaR_bjet1bjet2;
}

float &Event::DeltaR_lepnu() {
  return m_DeltaR_lepnu;
}

const float Event::DeltaR_lepnu() const {
  return m_DeltaR_lepnu;
}

float &Event::minDeltaR_hadWbjet() {
  return m_minDeltaR_hadWbjet;
}

const float Event::minDeltaR_hadWbjet() const {
  return m_minDeltaR_hadWbjet;
}

float &Event::minDeltaR_lepbjet() {
  return m_minDeltaR_lepbjet;
}

const float Event::minDeltaR_lepbjet() const {
  return m_minDeltaR_lepbjet;
}

float &Event::mass_lepb() {
  return m_mass_lepb;
}

const float Event::mass_lepb() const {
  return m_mass_lepb;
}

int &Event::btagsN() {
  return m_btagsN;
}

const int Event::btagsN() const {
  return m_btagsN;
}

float &Event::mtw() {
  return m_mtw;
}

const float Event::mtw() const {
  return m_mtw;
}

int &Event::vlq_evtype() {
  return m_vlq_evtype;
}

const int Event::vlq_evtype() const {
  return m_vlq_evtype;
}

float &Event::weight_bTagSF() {
  return m_weight_bTagSF;
}

const float Event::weight_bTagSF() const {
  return m_weight_bTagSF;
}

float &Event::weight_leptonSF() {
  return m_weight_leptonSF;
}

const float Event::weight_leptonSF() const {
  return m_weight_leptonSF;
}

float &Event::weight_lept_eff() {
  return m_weight_lept_eff;
}

const float Event::weight_lept_eff() const {
  return m_weight_lept_eff;
}



std::vector<Electron> &Event::electron() {
  return m_electron;
}

std::vector<Muon> &Event::muon() {
  return m_muon;
}

std::vector<Lepton> &Event::lepton() {
  return m_lepton;
}

std::vector<Jet> &Event::jet() {
  return m_jet;
}

std::vector<Jet> &Event::tjet() {
  return m_tjet;
}

std::vector<Jet> &Event::bjet() {
  return m_bjet;
}

std::vector<Jet> &Event::bhad() {
  return m_bhad;
}

std::vector<Jet> &Event::blep() {
  return m_blep;
}

std::vector<LargeJet> &Event::largeJet() {
  return m_largeJet;
}
std::vector<RecoObject> &Event::recoObject() {
  return m_recoObject;
}

std::vector<RecoObject> &Event::resolvedW() {
  return m_resolvedW;
}

std::vector<RecoObject> &Event::boostedW() {
  return m_boostedW;
}

std::vector<RecoObject> &Event::reclusteredW() {
  return m_reclusteredW;
}

std::vector<RecoObject> &Event::reclusteredWsub() {
  return m_reclusteredWsub;
}

std::vector<RecoObject> &Event::hadronicT() {
  return m_hadronicT;
}

std::vector<RecoObject> &Event::leptonicT() {
  return m_leptonicT;
}

std::vector<RecoObject> &Event::hadronicW() {
  return m_hadronicW;
}

std::vector<RecoObject> &Event::leptonicW() {
  return m_leptonicW;
}


std::vector<RecoObject> &Event::nu() {
  return m_nu;
}



void Event::met(float met_x, float met_y) {
  m_met.SetPxPyPzE(met_x, met_y, 0, std::sqrt(std::pow(met_x, 2) + std::pow(met_y, 2)));
}

TLorentzVector Event::met() const {
  return m_met;
}

const std::vector<Electron> &Event::electron() const {
  return m_electron;
}

const std::vector<Muon> &Event::muon() const {
  return m_muon;
}

const std::vector<Lepton> &Event::lepton() const {
  return m_lepton;
}
const std::vector<Jet> &Event::jet() const {
  return m_jet;
}

const std::vector<Jet> &Event::tjet() const {
  return m_tjet;
}

const std::vector<Jet> &Event::bjet() const {
  return m_bjet;
}
const std::vector<Jet> &Event::bhad() const {
  return m_bhad;
}

const std::vector<Jet> &Event::blep() const {
  return m_blep;
}
const std::vector<LargeJet> &Event::largeJet() const {
  return m_largeJet;
}

const std::vector<RecoObject> &Event::recoObject() const {
  return m_recoObject;
}

const std::vector<RecoObject> &Event::resolvedW() const {
  return m_resolvedW;
}

const std::vector<RecoObject> &Event::boostedW() const {
  return m_boostedW;
}

const std::vector<RecoObject> &Event::reclusteredW() const {
  return m_reclusteredW;
}
const std::vector<RecoObject> &Event::reclusteredWsub() const {
  return m_reclusteredWsub;
}

const std::vector<RecoObject> &Event::hadronicT() const {
  return m_hadronicT;
}

const std::vector<RecoObject> &Event::leptonicT() const {
  return m_leptonicT;
}


const std::vector<RecoObject> &Event::hadronicW() const {
  return m_hadronicW;
}

const std::vector<RecoObject> &Event::leptonicW() const {
  return m_leptonicW;
}

const std::vector<RecoObject> &Event::nu() const {
  return m_nu;
}

unsigned int &Event::runNumber() {
  return m_runNumber;
}

const unsigned int Event::runNumber() const {
  return m_runNumber;
}

unsigned int &Event::randomRunNumber() {
  return m_randomRunNumber;
}

const unsigned int Event::randomRunNumber() const {
  return m_randomRunNumber;
}



unsigned int &Event::lumiBlock() {
  return m_lumiBlock;
}

const unsigned int Event::lumiBlock() const {
  return m_lumiBlock;
}

ULong64_t &Event::eventNumber() {
  return m_eventNumber;
}

const ULong64_t Event::eventNumber() const {
  return m_eventNumber;
}

ULong64_t &Event::PRWHash() {
  return m_PRWHash;
}

const ULong64_t Event::PRWHash() const {
  return m_PRWHash;
}

float &Event::PileupWeight() {
  return m_PileupWeight;
}
const float Event::PileupWeight() const {
  return m_PileupWeight;
}


int &Event::isBoosted() {
  return m_isBoosted;
}

const int Event::isBoosted() const {
  return m_isBoosted;
}

int &Event::isResolved() {
  return m_isResolved;
}

const int Event::isResolved() const {
  return m_isResolved;
}



bool &Event::isData() {
  return m_isData;
}
const bool Event::isData() const {
  return m_isData;
}

int &Event::channelNumber() {
  return m_channelNumber;
}
const int Event::channelNumber() const {
  return m_channelNumber;
}

float &Event::mu() {
  return m_mu;
}
const float Event::mu() const {
  return m_mu;
}

// float &Event::mu_original() {
//   return m_mu_original;
// }
// const float Event::mu_original() const {
//   return m_mu_original;
// }

float &Event::mu_original() {
  return m_mu_original;
}
const float Event::mu_original() const {
  return m_mu_original;
}


int &Event::npv() {
  return m_npv;
}
const int Event::npv() const {
  return m_npv;
}

float &Event::vtxz(){
  return m_vtxz;
}

const float Event::vtxz() const{
  return m_vtxz;
}

unsigned int &Event::lbn() {
  return m_lbn;
}

const unsigned int Event::lbn() const {
  return m_lbn;
}



//Variables from the truth: lepton+jet channel


TLorentzVector &Event::MC_Wminus() {
  return m_MC_Wminus;
}
const TLorentzVector &Event::MC_Wminus() const {
  return m_MC_Wminus;
}

TLorentzVector &Event::MC_Wplus() {
  return m_MC_Wplus;
}
const TLorentzVector &Event::MC_Wplus() const {
  return m_MC_Wplus;
}

TLorentzVector &Event::MC_b_q() {
  return m_MC_b_q;
}
const TLorentzVector &Event::MC_b_q() const {
  return m_MC_b_q;
}

TLorentzVector &Event::MC_bbar_q() {
  return m_MC_bbar_q;
}
const TLorentzVector &Event::MC_bbar_q() const {
  return m_MC_bbar_q;
}

TLorentzVector &Event::MC_lep() {
  return m_MC_lep;
}
const TLorentzVector &Event::MC_lep() const {
  return m_MC_lep;
}

int &Event::MC_lep_charge() {
  return m_MC_lep_charge;
}
const int Event::MC_lep_charge() const {
  return m_MC_lep_charge;
}

int &Event::MC_lep_pdgId() {
  return m_MC_lep_pdgId;
}
const int Event::MC_lep_pdgId() const {
  return m_MC_lep_pdgId;
}


TLorentzVector &Event::MC_nu() {
  return m_MC_nu;
}
const TLorentzVector &Event::MC_nu() const {
  return m_MC_nu;
}

int &Event::MC_nu_pdgId() {
  return m_MC_nu_pdgId;
}
const int Event::MC_nu_pdgId() const {
  return m_MC_nu_pdgId;
}

TLorentzVector &Event::MC_q1() {
  return m_MC_q1;
}
const TLorentzVector &Event::MC_q1() const {
  return m_MC_q1;

}

int &Event::MC_q1_pdgId() {
  return m_MC_q1_pdgId;
}
const int Event::MC_q1_pdgId() const {
  return m_MC_q1_pdgId;
}

TLorentzVector &Event::MC_q2() {
  return m_MC_q2;
}
const TLorentzVector &Event::MC_q2() const {
  return m_MC_q2;
}

int &Event::MC_q2_pdgId() {
  return m_MC_q2_pdgId;
}
const int Event::MC_q2_pdgId() const {
  return m_MC_q2_pdgId;
}

TLorentzVector &Event::MC_t_q() {
  return m_MC_t_q;
}
const TLorentzVector &Event::MC_t_q() const {
  return m_MC_t_q;
}

TLorentzVector &Event::MC_tbar_q() {
  return m_MC_tbar_q;
}
const TLorentzVector &Event::MC_tbar_q() const {
  return m_MC_tbar_q;
}




//hadronic top decay
TLorentzVector &Event::MC_w1h() {
  return m_MC_w1h;
}
const TLorentzVector &Event::MC_w1h() const {
  return m_MC_w1h;
}

int &Event::MC_w1h_pdgId() {
  return m_MC_w1h_pdgId;
}
const int Event::MC_w1h_pdgId() const {
  return m_MC_w1h_pdgId;
}

TLorentzVector &Event::MC_w2h() {
  return m_MC_w2h;
}
const TLorentzVector &Event::MC_w2h() const {
  return m_MC_w2h;
}

int &Event::MC_w2h_pdgId() {
  return m_MC_w2h_pdgId;
}
const int Event::MC_w2h_pdgId() const {
  return m_MC_w2h_pdgId;
}

TLorentzVector &Event::MC_bh() {
  return m_MC_bh;
}
const TLorentzVector &Event::MC_bh() const {
  return m_MC_bh;
}

//leptonic top decay
TLorentzVector &Event::MC_w1l() {
  return m_MC_w1l;
}
const TLorentzVector &Event::MC_w1l() const {
  return m_MC_w1l;
}

signed int &Event::MC_w1l_pdgId() {
  return m_MC_w1l_pdgId;
}
const signed int Event::MC_w1l_pdgId() const {
  return m_MC_w1l_pdgId;
}

TLorentzVector &Event::MC_w2l() {
  return m_MC_w2l;
}
const TLorentzVector &Event::MC_w2l() const {
  return m_MC_w2l;
}

int &Event::MC_w2l_pdgId() {
  return m_MC_w2l_pdgId;
}
const int Event::MC_w2l_pdgId() const {
  return m_MC_w2l_pdgId;
}

TLorentzVector &Event::MC_bl() {
  return m_MC_bl;
}
const TLorentzVector &Event::MC_bl() const {
  return m_MC_bl;
}

TLorentzVector &Event::MC_ttbar_beforeFSR() {
  return m_MC_ttbar_beforeFSR;
}
const TLorentzVector &Event::MC_ttbar_beforeFSR() const {
  return m_MC_ttbar_beforeFSR;
}

TLorentzVector &Event::MC_t() {
  return m_MC_t;
}
const TLorentzVector &Event::MC_t() const {
  return m_MC_t;
}

TLorentzVector &Event::MC_tbar() {
  return m_MC_tbar;
}
const TLorentzVector &Event::MC_tbar() const {
  return m_MC_tbar;
}

//MA
//hadronic top decay

TLorentzVector &Event::MA_w1h() {
  return m_MA_w1h;
}
const TLorentzVector &Event::MA_w1h() const {
  return m_MA_w1h;
}

int &Event::MA_w1h_pdgId() {
  return m_MA_w1h_pdgId;
}
const int Event::MA_w1h_pdgId() const {
  return m_MA_w1h_pdgId;
}

TLorentzVector &Event::MA_w2h() {
  return m_MA_w2h;
}
const TLorentzVector &Event::MA_w2h() const {
  return m_MA_w2h;
}

int &Event::MA_w2h_pdgId() {
  return m_MA_w2h_pdgId;
}
const int Event::MA_w2h_pdgId() const {
  return m_MA_w2h_pdgId;
}

TLorentzVector &Event::MA_bh() {
  return m_MA_bh;
}
const TLorentzVector &Event::MA_bh() const {
  return m_MA_bh;
}

//leptonic top decay
TLorentzVector &Event::MA_w1l() {
  return m_MA_w1l;
}
const TLorentzVector &Event::MA_w1l() const {
  return m_MA_w1l;
}

signed int &Event::MA_w1l_pdgId() {
  return m_MA_w1l_pdgId;
}
const signed int Event::MA_w1l_pdgId() const {
  return m_MA_w1l_pdgId;
}

TLorentzVector &Event::MA_w2l() {
  return m_MA_w2l;
}
const TLorentzVector &Event::MA_w2l() const {
  return m_MA_w2l;
}

int &Event::MA_w2l_pdgId() {
  return m_MA_w2l_pdgId;
}
const int Event::MA_w2l_pdgId() const {
  return m_MA_w2l_pdgId;
}

TLorentzVector &Event::MA_bl() {
  return m_MA_bl;
}
const TLorentzVector &Event::MA_bl() const {
  return m_MA_bl;
}
