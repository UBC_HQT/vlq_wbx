/**
 * @brief Analysis class for VLQ->WtX.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>

 __      _________________  ___
/  \    /  \__    ___/\   \/  /
\   \/\/   / |    |    \     /
 \        /  |    |    /     \
  \__/\  /   |____|   /___/\  \
       \/                   \_/

 */
#include "TopNtupleAnalysis/Analysis_HQT.h"
#include "TopNtupleAnalysis/AnaHQTVLQWtX.h"
#include "TopNtupleAnalysis/Event.h"
#include "TLorentzVector.h"
#include <vector>
#include <string>
#include "TopNtupleAnalysis/HistogramService.h"

AnaHQTVLQWtX::AnaHQTVLQWtX(const std::string &filename, bool electron, bool boosted, bool combined, int ntuple, int skipRegionPlots, std::string resolvedselection, std::string boostedselection, std::vector<std::string> &systList)
  : Analysis_HQT(filename, systList, ntuple), m_electron(electron), m_boosted(boosted), m_combined(combined), m_ntuple(ntuple), m_skipRegionPlots(skipRegionPlots), m_resolvedselection(resolvedselection), m_boostedselection(boostedselection),
    m_neutrinoBuilder("MeV"), m_chi2("MeV"), m_vlqutils(), m_vlqneutrinoBuilder() {

  m_chi2.Init(TtresChi2::DATA2015_MC15);

  if (m_ntuple==1){
    m_hSvc.m_tree->Branch("WbWb",    &_tree_WbWb);
    m_hSvc.m_tree->Branch("ZtZT",    &_tree_ZtZt);
    m_hSvc.m_tree->Branch("HtHt",    &_tree_HtHt);
    m_hSvc.m_tree->Branch("WbZt",    &_tree_WbZt);
    m_hSvc.m_tree->Branch("WbHt",    &_tree_WbHt);
    m_hSvc.m_tree->Branch("ZtHt",    &_tree_ZtHt);
    m_hSvc.m_tree->Branch("lep_mass",    &_tree_lep_mass);
    m_hSvc.m_tree->Branch("had_mass",    &_tree_had_mass);
    m_hSvc.m_tree->Branch("MET",    &_tree_MET);
    m_hSvc.m_tree->Branch("MWT",    &_tree_MWT);
    m_hSvc.m_tree->Branch("ST",    &_tree_ST);
    m_hSvc.m_tree->Branch("dR_lnu",    &_tree_dR_lnu);
    m_hSvc.m_tree->Branch("lb_mass",    &_tree_lb_mass);
    m_hSvc.m_tree->Branch("lb_minmass",    &_tree_lb_minmass);
    m_hSvc.m_tree->Branch("lblep_mass",    &_tree_lblep_mass);
    m_hSvc.m_tree->Branch("Delta_mass",    &_tree_Delta_mass);
    m_hSvc.m_tree->Branch("DeltaR_TlepThad",    &_tree_DeltaR_TlepThad);
    m_hSvc.m_tree->Branch("weight", &_tree_weight);
    m_hSvc.m_tree->Branch("cat",    &_tree_cat);
    m_hSvc.m_tree->Branch("syst",   &_tree_syst);
  }
  if (m_ntuple==2){
    m_hSvc.m_tree->Branch("event_number",    &_tree_evid);
    m_hSvc.m_tree->Branch("jets_n",    &_tree_Njet);
    m_hSvc.m_tree->Branch("bjets_n",    &_tree_Nbjet);
    m_hSvc.m_tree->Branch("lep_n",    &_tree_Nlep);
    m_hSvc.m_tree->Branch("met",    &_tree_MET);
    m_hSvc.m_tree->Branch("mtw",    &_tree_MWT);
    m_hSvc.m_tree->Branch("weight", &_tree_weight);
    m_hSvc.m_tree->Branch("region",   &_tree_region);
  }


  double varBin1[] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 220, 240, 260, 280, 300, 340, 380, 450, 500};
  int varBinN1 = sizeof(varBin1)/sizeof(double) - 1;
  double varBin2[] = {300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500, 540, 580, 620, 660, 700, 800, 1e3, 1.2e3, 1.5e3};
  int varBinN2 = sizeof(varBin2)/sizeof(double) - 1;
  double varBin3[] = {0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 340, 380, 420, 460, 500};
  int varBinN3 = sizeof(varBin3)/sizeof(double) - 1;
  double varBin4[] = {80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 340, 380, 420, 460, 500};
  int varBinN4 = sizeof(varBin4)/sizeof(double) - 1;
  double varBin05[] = {200, 600, 1000, 1400, 1800, 2200, 2600, 3000};
  int varBinN05 = sizeof(varBin05)/sizeof(double) - 1;

  double varBin5[] = {0, 80, 160, 240, 320, 400, 480, 560, 640,720,800,920,1040,1160,1280,1400,1550,1700,2000,2300,2600,2900,3200,3600,4100,4600,5100,6000};
  int varBinN5 = sizeof(varBin5)/sizeof(double) - 1;

  //Matthias binning choices
  //
  //std::string binning_sigBoost = "0,300,500,700,900,1200,2000";
  //std::string binning_sigResol = "0,300,500,700,900,2000";
  //std::string binning_ttCR = "0,150,300,500,700,2000";


  double varBinBoostedSR[] = {0,300,500,700,900,1200,2000};
  int varBinNBoostedSR = sizeof(varBinBoostedSR)/sizeof(double) - 1;

  double varBinResolvedSR[] = {0,300,500,700,900,2000};
  int varBinNResolvedSR = sizeof(varBinResolvedSR)/sizeof(double) - 1;

  double varBinBoostedCR[] = {0,150,300,500,700,2000};
  int varBinNBoostedCR = sizeof(varBinBoostedCR)/sizeof(double) - 1;

  //Plots for QCD validation
  if (m_electron) {
     if(m_boosted){
        double varBin6[6] = {30, 40, 60, 120, 400, 700};
	double varBin7[5]  = {0., 0.4, 0.6, 1.0, 1.5};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);

     }else{
        double varBin6[8] = {30, 35, 40, 50, 60, 120, 400, 700};
	double varBin7[7]  = {0., 0.4, 0.6, 1.0, 1.5, 2.5, 7.0};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);

     }//m_boosted

  }else{
     if(m_boosted){
        double varBin6[7] = {25, 30, 40, 50, 100, 400, 700};
	double varBin7[6] = {0., 0.4, 0.6, 0.8, 1.0, 1.5};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);

     }else{
        double varBin6[9] = {25, 30, 35, 40, 50, 70, 100, 400, 700};
	double varBin7[7] = {0., 0.4, 0.6, 1.0, 1.5, 2.5, 7.0};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);

     }//m_boosted
  }//m_electron


  if(m_skipRegionPlots < 1){
    m_hSvc.create1D("yields", "; One ; Events", 1, 0.5, 1.5);
    //m_hSvc.create1DVar("lepPt", "; lepton p_{T} [GeV]; Events", varBinN1, varBin1);
    m_hSvc.create1D("lepPt",    "; Pt of lept [GeV]; Events", 200, 0, 500);
    m_hSvc.create1D("lepEta", "; lepton #eta ; Events", 20, -2.5, 2.5);
    m_hSvc.create1D("lepPhi", "; lepton #phi [rd] ; Events", 32, -3.2, 3.2);
    m_hSvc.create1DVar("leadJetPt", "; leading Jet p_{T} [GeV]; Events", varBinN1, varBin1);
    m_hSvc.create1DVar("JetPt", "; Jet transverse momentum [GeV]; Events", varBinN1, varBin1);
    m_hSvc.create1D("nJets", "; number of jets ; Events", 15, -0.5, 14.5);

    m_hSvc.create1D("WtagMed", "; isWtaggedMed ; Events", 11,-5.5,5.5);
    m_hSvc.create1D("TopTagMed50", "; isSmoothTopTagged50 ; Events", 2,-0.5,1.5);

    m_hSvc.create1DVar("leadTrkbJetPt", "; leading b-jet p_{T} [GeV]; Events", varBinN1, varBin1);
    m_hSvc.create1D("nBtagJets", "; number of b-tagged jets ; Events", 10, -0.5, 9.5);
    m_hSvc.create1D("nTrkBtagJets", "; number of b-tagged track jets ; Events", 10, 0, 10);

    m_hSvc.create1DVar("MET", "; missing E_{T} [GeV]; Events", varBinN1, varBin1);
    m_hSvc.create1D("MET_phi", "; missing E_{T} #phi [rd] ; Events", 32, -3.2, 3.2);
    m_hSvc.create1D("ST", "; S_{T} [GeV]; Events", 39, 100., 4000.);
    m_hSvc.create1DVar("ST_effBins", "; S_{T} [GeV]; Events", varBinN05, varBin05);
    m_hSvc.create1D("mwt", "; W transverse mass [GeV]; Events", 20, 0, 200);

    m_hSvc.create1D("DeltaR_lepnu", "; #Delta R(lep, #nu); Events", 50, 0., 5.);
    m_hSvc.create1D("mass_lepblead", "; m_{l b_{lead}}; Events", 32,  0.,  800.);

    m_hSvc.create1D("mu", "; <mu>; Events", 40, 0, 40);
    m_hSvc.create1D("mu_original", "; <mu_original>; Events", 25, 0, 25);
    m_hSvc.create1D("vtxz", ";Z position of truth primary vertex; Events", 40, -400, 400);
    m_hSvc.create1D("npv", "; npv; Events", 5, 0, 5);

    m_hSvc.create1D("minDeltaR_lepjet", "; min #Delta R(lep, jet); Events", 50, 0, 5);
    m_hSvc.create1DVar("closejl_pt", "; Pt of closest jet to lep [GeV]; Events", varBinN1, varBin1);

    m_hSvc.create1D("weight_leptSF", "; Lepton SF weights; Events", 200, 0, 2);
    m_hSvc.create1D("weight", "; Weights; Events", 2000, -100, 100);
    m_hSvc.create2D("weight_leptPt", ";lept Pt (GeV); QCD weights",100, 25, 525, 2000, -100, 100);
    m_hSvc.create2D("MET_MWT", ";W transverse mass [GeV]; missing E_{T} [GeV]",100, 0, 500, 2000, 0, 100);
    m_hSvc.create2D("dR_ST", ";#Delta R(lep, #nu); S_{T} [GeV]",50, 0, 5, 40, 0, 4000);
    m_hSvc.create2D("nJet_dR", ";number of jets ;#Delta R(lep, #nu)",11, -0.5, 10.5, 50, 0., 5.);
    m_hSvc.create2D("nJet_ST", ";number of jets; S_{T} [GeV]",11, -0.5, 10.5, 40, 0., 4000.);

    m_hSvc.create2D("nBtagJets_dR", ";number of b-tagged jets ;#Delta R(lep, #nu)",11, -0.5, 10.5, 50, 0., 5.);
    m_hSvc.create2D("nBtagJets_ST", ";number of b-tagged jets; S_{T} [GeV]",11, -0.5, 10.5, 40, 0., 4000.);

    m_hSvc.create1D("jet0_m", "; mass of the leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
    m_hSvc.create1D("jet1_m", "; mass of the sub-leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
    m_hSvc.create1D("jet2_m", "; mass of the third leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
    m_hSvc.create1D("jet3_m", "; mass of the fourth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
    m_hSvc.create1D("jet4_m", "; mass of the fifth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
    m_hSvc.create1D("jet5_m", "; mass of the sixth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);

    m_hSvc.create1DVar("jet0_pt", "; p_{T} of the leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
    m_hSvc.create1DVar("jet1_pt", "; p_{T} of the sub-leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
    m_hSvc.create1DVar("jet2_pt", "; p_{T} of the third leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
    m_hSvc.create1DVar("jet3_pt", "; p_{T} of the fourth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
    m_hSvc.create1DVar("jet4_pt", "; p_{T} of the fifth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
    m_hSvc.create1DVar("jet5_pt", "; p_{T} of the sixth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);

    m_hSvc.create1D("jet0_eta", "; #eta of the leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
    m_hSvc.create1D("jet1_eta", "; #eta of the sub-leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
    m_hSvc.create1D("jet2_eta", "; #eta of the third leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
    m_hSvc.create1D("jet3_eta", "; #eta of the fourth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
    m_hSvc.create1D("jet4_eta", "; #eta of the fifth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
    m_hSvc.create1D("jet5_eta", "; #eta of the sixth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);

    m_hSvc.create1D("jet0_phi", "; #phi of the leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
    m_hSvc.create1D("jet1_phi", "; #phi of the sub-leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
    m_hSvc.create1D("jet2_phi", "; #phi of the third leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
    m_hSvc.create1D("jet3_phi", "; #phi of the fourth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
    m_hSvc.create1D("jet4_phi", "; #phi of the fifth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
    m_hSvc.create1D("jet5_phi", "; #phi of the sixth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  }//skipRegionPlots


  m_hSvc.create1D("cutflow_boo", "; cutflow boo; Events", 10, -0.5, 9.5);
  m_hSvc.create1D("cutflow_res", "; cutflow res; Events", 10, -0.5, 9.5);

  m_hSvc.create1D("DeltaM_hadtopleptop", "; #Delta m (T_{had},T_{lep}) [GeV] ; Events", 20, 0., 1000.);
  m_hSvc.create1D("DeltaR_lepnu_1", "; #Delta R(lep, #nu); Events", 50, 0., 5.);
  m_hSvc.create1D("DeltaR_lepnu_2", "; #Delta R(lep, #nu); Events", 50, 0., 5.);
  m_hSvc.create1D("DeltaR_lepnu_3", "; #Delta R(lep, #nu); Events", 50, 0., 5.);

  //correlation plots
  m_hSvc.create2D("ST_lepM", "; S_{T} [GeV]; leptonic T mass [GeV]", 40, 0., 4000., 40,  0., 2000.);
  m_hSvc.create2D("dRlepnu_lepM", ";#Delta R(lep, #nu); leptonic T mass [GeV]", 50, 0, 5., 40,  0., 2000.);
  m_hSvc.create2D("leadJetPt_lepM", "; leading Jet p_{T} [GeV]; leptonic T mass [GeV]", 100, 0., 1000., 40,  0., 2000.);
  m_hSvc.create2D("MET_lepM", "; missing E_{T} [GeV]; leptonic T mass [GeV]", 2000, 0, 1000, 40,  0., 2000.);
  m_hSvc.create2D("hadM_lepM", "; hadronic T mass [GeV]; leptonic T mass [GeV]", 40,  0., 2000. , 40,  0., 2000.);
  m_hSvc.create2D("DeltaR_lepWhad_lepM", "; #DeltaR(W_{had},lep)[GeV]; leptonic T mass [GeV]", 200, 0., 8., 40,  0., 2000.);
  m_hSvc.create2D("DeltaR_ThadTlep_lepM", "; #DeltaR(T_{lep},T_{had})[GeV]; leptonic T mass [GeV]", 200, 0., 8., 40,  0., 2000.);


  m_hSvc.create1DVar("leadbJetPt", "; leading b-jet p_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("nBoostedW", "; number of boosted W's ; Events", 10, -0.5, 9.5);
  m_hSvc.create1D("nBoostedTop", "; number of boosted Top's; Events", 10, -0.5, 9.5);
  //  m_hSvc.create1D("nBoostedTop50", "; number of boosted Top's (50% eff.); Events", 10, -0.5, 9.5);
  //  m_hSvc.create1D("nBoostedTop80", "; number of boosted Top's (80% eff.); Events", 10, -0.5, 9.5);

  m_hSvc.create1D("DeltaR_bjet1bjet2", "; #Delta R(lead- and subleading b) ; Events", 10, 0., 5.);
  m_hSvc.create1D("mass_lepb", "; m_{lb}; Events", 32,  0.,  800.);


  m_hSvc.create1D("hadronicT_pt", "; hadronic T p_{T} [GeV] ; Events", 30, 0., 1500.);
  m_hSvc.create1D("hadronicT_m", "; hadronic T mass [GeV] ; Events", 40,  0., 2000.);
  m_hSvc.create1D("hadronicT_eta", "; hadronic T #eta ; Events", 25, -2.5,   2.5);
  m_hSvc.create1D("hadronicT_phi", "; hadronic T #phi [rad] ; Events", 32, -3.2, 3.2);



  m_hSvc.create1D("leptonicT_pt", "; leptonic T p_{T} [GeV] ; Events", 30, 0., 1500.);
  m_hSvc.create1D("leptonicT_m", "; leptonic T mass [GeV] ; Events", 40,  0., 2000.);
  m_hSvc.create1D("leptonicT_eta", "; leptonic T #eta ; Events", 20, -2., 2.);
  m_hSvc.create1D("leptonicT_phi", "; leptonic T #phi [rad] ; Events", 32, -3.2, 3.2);

  m_hSvc.create1DVar("leptonicT_m_boostedSR", "; leptonic T mass [GeV] ; Events", varBinNBoostedSR, varBinBoostedSR);
  m_hSvc.create1DVar("leptonicT_m_resolvedSR", "; leptonic T mass [GeV] ; Events", varBinNResolvedSR, varBinResolvedSR);
  m_hSvc.create1DVar("leptonicT_m_boostedCR", "; leptonic T mass [GeV] ; Events", varBinNBoostedCR, varBinBoostedCR);

  m_hSvc.create1D("nleptonicW", "; number of leptonic W's ; Events", 10, -0.5, 9.5);
  m_hSvc.create1D("leptonicW_pt", "; leptonic W p_{T} [GeV] ; Events", 16, 200., 1000.);
  m_hSvc.create1D("leptonicW_m", "; leptonic W mass [GeV] ; Events", 14, 50.,  120.);
  m_hSvc.create1D("leptonicW_eta", "; leptonic W #eta ; Events", 20,-2.5,   2.5);
  m_hSvc.create1D("leptonicW_phi", "; leptonic W #phi [rad] ; Events", 32, -3.2, 3.2);

  if (m_boosted) {
    m_hSvc.create1DVar("closeJetPt_boo", "; selected Jet p_{T} [GeV] ; Events", varBinN1, varBin1);
    m_hSvc.create1D("nLargeJet", "; number of large jets ; Events", 6, -0.5, 5.5);
    m_hSvc.create1D("largeJetPt", "; large jet p_{T} [GeV] ; Events", 16, 0, 1600);
    m_hSvc.create1D("largeJetM", "; large jet mass [GeV] ; Events", 30, 0, 300);
    m_hSvc.create1D("largeJetEta", "; large jet #eta ; Events", 20, -2., 2.);
    m_hSvc.create1D("largeJetPhi", "; large jet #phi [rd] ; Events", 32, -3.2, 3.2);
    m_hSvc.create1D("largeJetSd12", "; large jet #sqrt{d_{12}} [GeV] ; Events", 20, 0, 200);
    m_hSvc.create1DVar("mtlep_boo", "; leptonic top mass [GeV] ; Events", varBinN4, varBin4);
    m_hSvc.create1D("MET_boo", "; missing E_{T} [GeV]; Events", 40, 0, 800);
    m_hSvc.create1D("MWT_boo", "; W transverse mass [GeV]; Events", 50, 0, 500);
    m_hSvc.create1D("ST_boo", "; S_{T} [GeV]; Events", 39, 100., 4000.);
    m_hSvc.create2D("MET_MWT_boo", ";W transverse mass [GeV]; missing E_{T} [GeV]",100, 0, 500, 2000, 0, 100);
    m_hSvc.create2D("dR_ST_boo", ";#Delta R(lep, #nu); S_{T} [GeV]",50, 0, 5, 80, 0, 4000);
    m_hSvc.create1D("DeltaR_lepnu_boo", "; #Delta R(lep, #nu); Events", 50, 0., 5.);

    m_hSvc.create1D("nJets_boo", "; number of jets ; Events", 15, -0.5, 14.5);
    m_hSvc.create1D("nBtagJets_boo", "; number of b-tagged jets ; Events", 10, -0.5, 9.5);
    m_hSvc.create1D("nBoostedW_boo", "; number of boosted W's ; Events", 10, -0.5, 9.5);
    m_hSvc.create1D("nBoostedTop_boo", "; number of boosted Top's ; Events", 10, -0.5, 9.5);
    m_hSvc.create1D("nJets", "; number of jets ; Events", 15, -0.5, 14.5);

    m_hSvc.create2D("nJet_dR_boo", ";number of jets ;#Delta R(lep, #nu)",11, -0.5, 10.5, 50, 0., 5.);
    m_hSvc.create2D("nJet_ST_boo", ";number of jets; S_{T} [GeV]",11, -0.5, 10.5, 40, 0., 4000.);

    m_hSvc.create2D("nBtagJets_dR_boo", ";number of b-tagged jets ;#Delta R(lep, #nu)",11, -0.5, 10.5, 50, 0., 5.);
    m_hSvc.create2D("nBtagJets_ST_boo", ";number of b-tagged jets; S_{T} [GeV]",11, -0.5, 10.5, 40, 0., 4000.);
    m_hSvc.create2D("DeltaR_Whadbjet_boo_ThadMass", "; #Delta R(W_{had}, bjet); hadronic T mass [GeV]", 50, 0, 5, 40,  0., 2000.);


    m_hSvc.create1D("mass_lepb_boo", "; M(lep, b); Events", 32,  0.,  800.);
    m_hSvc.create1D("minMass_lepb_boo", "; min[M(lep,b)]; Events", 32,  0.,  800.);
    m_hSvc.create1D("mass_lepleptonicb_boo", "; M(lep, b_{lep}); Events", 32,  0.,  800.);

    m_hSvc.create1D("mass_lepjetlead_boo", "; M(lep,j_{lead}); Events", 32,  0.,  800.);
    m_hSvc.create1D("mass_lepjet_boo", "; M(lep, j)]; Events", 32,  0.,  800.);
    m_hSvc.create1D("minMass_lepjet_boo", "; min[M(lep, j)]; Events", 32,  0.,  800.);

    m_hSvc.create1D("mass_lepblead_boo", "; M(lep, b_{lead}); Events", 32,  0.,  800.);
    m_hSvc.create1D("avgMassT_boo", "; (m_{T}^{had}+m_{T}^{lep})/2; Events", 40,  0.,  2000.);

    m_hSvc.create1D("minDeltaR_lepbjet_boo", "; min #Delta R(lep, bjet) ; Events", 20, 0., 8.);
    m_hSvc.create1DVar("closebjl_pt_boo", "; Pt of closest bjet to lep [GeV]; Events", varBinN1, varBin1);
    m_hSvc.create1D("minDeltaR_hadWbjet_boo", "; min #Delta R(W_{had}, bjet) ; Events", 30, 0., 3.);
    m_hSvc.create1DVar("closebjHadW_pt_boo", "; Pt of closest bjet to hadronic W candidate [GeV]; Events", varBinN1, varBin1);

    m_hSvc.create1D("DeltaR_lepblead_boo", "; #Delta R(lep, b-jet_{lead}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_lepbsublead_boo", "; #Delta R(lep, b-jet_{sublead}); Events", 50, 0, 5);
    m_hSvc.create2D("dR_lepblead_dR_lepbsublead_boo", ";#Delta R(lep, b-jet_{sublead});  #Delta R(lep, jet_{lead})",50, 0, 5, 50, 0, 5);

    m_hSvc.create1D("DeltaR_lepjetlead_boo", "; #Delta R(lep, jet_{lead}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_lepjetsublead_boo", "; #Delta R(lep, jet_{sublead}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_lepjetthird_boo", "; #Delta R(lep, jet_{3^{rd} leading}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_lepjetfourth_boo", "; #Delta R(lep, jet_{4^{th} leading}); Events", 50, 0, 5);
    m_hSvc.create2D("dR_lepjetlead_dR_lepjetsublead_boo", ";#Delta R(lep, jet_{sublead});  #Delta R(lep, jet_{lead})",50, 0, 5, 50, 0, 5);

    m_hSvc.create1D("DeltaR_Whadbjet_boo", "; #Delta R(W_{had}, bjet); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Whadbjetlead_boo", "; #Delta R(W_{had}, bjet_{lead}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Whadbjetsublead_boo", "; #Delta R(W_{had}, bjet_{sublead}); Events", 50, 0, 5);
    m_hSvc.create2D("DeltaR_Whadbjetlead_boo_ThadMass", "; #Delta R(W_{had}, bjet_{lead}); hadronic T mass [GeV]", 50, 0, 5, 40,  0., 2000.);
    m_hSvc.create2D("DeltaR_Whadbjetsublead_boo_ThadMass", "; #Delta R(W_{had}, bjet_{sublead}); hadronic T mass [GeV]", 50, 0, 5, 40,  0., 2000.);

    m_hSvc.create1D("DeltaR_WhadbjetCandidateNotb_boo", "; #Delta R(W_{had}, bjet candidate (not b-tagged)) [1bex]; Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_WhadbjetCandidate_boo", "; #Delta R(W_{had}, bjet candidate (b-tagged)) [1bex]; Events", 50, 0, 5);

    m_hSvc.create1D("DeltaR_Whadjetlead_boo", "; #Delta R(W_{had}, jet_{lead}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Whadjetsublead_boo", "; #Delta R(W_{had}, jet_{sublead}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Whadjetthird_boo", "; #Delta R(W_{had}, jet_{3^{rd} leading}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Whadjetfourth_boo", "; #Delta R(W_{had}, jet_{4^{th} leading}); Events", 50, 0, 5);

    m_hSvc.create2D("DeltaR_Whadjetlead_boo_ThadMass", "; #Delta R(W_{had}, jet_{lead}); hadronic T mass [GeV]", 50, 0, 5, 40,  0., 2000.);
    m_hSvc.create2D("DeltaR_Whadjetsublead_boo_ThadMass", "; #Delta R(W_{had}, jet_{sublead}); hadronic T mass [GeV]", 50, 0, 5, 40,  0., 2000.);
    m_hSvc.create2D("DeltaR_Whadjetthird_boo_ThadMass", "; #Delta R(W_{had}, jet_{3^{rd} leading}); hadronic T mass [GeV]", 50, 0, 5, 40,  0., 2000.);
    m_hSvc.create2D("DeltaR_Whadjetfourth_boo_ThadMass", "; #Delta R(W_{had}, jet_{4^{th} leading}); hadronic T mass [GeV]", 50, 0, 5, 40,  0., 2000.);

    m_hSvc.create2D("Whad_eta_phi_boo", ";#phi;  #eta", 32, -3.2, 3.2, 50, -2.5, 2.5);
    m_hSvc.create2D("bjets_eta_phi_boo", ";#phi;  #eta", 32, -3.2, 3.2, 50, -2.5, 2.5);
    m_hSvc.create2D("dR_Whadjetlead_dR_Whadjetsublead_boo", ";#Delta R(W_{had}, jet_{sublead});  #Delta R(W_{had}, jet_{lead})",50, 0, 5, 50, 0, 5);

    m_hSvc.create1D("DeltaR_Whadbhad", "; #Delta R(W_{had}, b_{had}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Wlepbhad", "; #Delta R(W_{lep}, b_{had}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Wlepblep", "; #Delta R(W_{lep}, b_{lep}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Whadblep", "; #Delta R(W_{had}, b_{lep}); Events", 50, 0, 5);

    m_hSvc.create1D("DeltaR_WhadCThadC", "; #Delta R(W_{had}^{cand.}, T_{had}^{cand.}); Events", 50, 0, 5);

    m_hSvc.create1D("bjet1Pt_boo", "; leading b-jet p_{T} [GeV] ; Events", 10, 0., 500.);
    m_hSvc.create1D("bjet2Pt_boo", "; subleading b-jet p_{T} [GeV] ; Events", 10, 0., 500.);

    m_hSvc.create1D("lepPt_boo",    "; Pt of lept [GeV]; Events", 100, 25, 525);
    m_hSvc.create1D("lepEta_boo", "; lepton #eta ; Events", 20, -2.5, 2.5);
    m_hSvc.create1D("lepPhi_boo", "; lepton #phi [rd] ; Events", 32, -3.2, 3.2);




    m_hSvc.create1D("boostedWhadCand_pt", "; hadronic W candidate p_{T} [GeV] ; Events", 20, 0., 1000.);
    m_hSvc.create1D("boostedWhadCand_m", "; hadronic W candidate mass [GeV] ; Events", 20, 0.,  200.);
    m_hSvc.create1D("boostedWhadCand_eta", "; hadronic W candidate #eta ; Events", 20, -2.5, 2.5);
    m_hSvc.create1D("boostedWhadCand_phi", "; hadronic W candidate #phi [rad] ; Events", 32, -3.2, 3.2);

    m_hSvc.create1D("boostedThadCand_pt", "; hadronic T candidate p_{T} [GeV] ; Events", 20, 0., 1000.);
    m_hSvc.create1D("boostedThadCand_m", "; hadronic T candidate mass [GeV] ; Events", 40, 0.,  400.);
    m_hSvc.create1D("boostedThadCand_eta", "; hadronic T candidate #eta ; Events", 20, -2.5, 2.5);
    m_hSvc.create1D("boostedThadCand_phi", "; hadronic T candidate #phi [rad] ; Events", 32, -3.2, 3.2);




    m_hSvc.create1D("DeltaR_ThadTlep", "; #Delta R(T_{had}, T_{lep}) ; Events", 20, 0., 8.);
    m_hSvc.create1D("DeltaR_WhadThad", "; #Delta R(W_{had}, T_{had}) ; Events", 20, 0., 8.);
    m_hSvc.create1D("DeltaR_WhadTlep", "; #Delta R(W_{had}, T_{lep}) ; Events", 20, 0., 8.);

    m_hSvc.create1D("DeltaR_WlepThad", "; #Delta R(W_{lep}, T_{had}) ; Events", 20, 0., 8.);
    m_hSvc.create1D("DeltaR_WlepTlep", "; #Delta R(W_{lep}, T_{lep}) ; Events", 20, 0., 8.);


    m_hSvc.create1D("DeltaR_lepWhad", "; #Delta R(W_{had}, lep) ; Events", 200, 0., 8.);


    // m_hSvc.create1D("nhadronicW", "; number of hadronic W's ; Events", 10, -0.5, 9.5);
    // m_hSvc.create1D("hadronicW_pt", "; hadronic W p_{T} [GeV] ; Events", 16, 200., 1000.);
    // m_hSvc.create1D("hadronicW_m", "; hadronic W mass [GeV] ; Events", 14, 50.,  120.);
    // m_hSvc.create1D("hadronicW_eta", "; hadronic W #eta ; Events", 20, -2.5, 2.5);
    // m_hSvc.create1D("hadronicW_phi", "; hadronic W #phi [rad] ; Events", 32, -3.2, 3.2);

    // m_hSvc.create1D("nhadronicb", "; number of hadronic b's ; Events", 10, -0.5, 9.5);
    // m_hSvc.create1D("hadronicb_pt", "; hadronic b p_{T} [GeV] ; Events", 16, 200., 1000.);
    // m_hSvc.create1D("hadronicb_m", "; hadronic b mass [GeV] ; Events", 14, 50.,  120.);
    // m_hSvc.create1D("hadronicb_eta", "; hadronic b #eta ; Events", 20, -2.5, 2.5);
    // m_hSvc.create1D("hadronicb_phi", "; hadronic b #phi [rad] ; Events", 32, -3.2, 3.2);

    // m_hSvc.create1D("nleptonicb", "; number of leptonic b's ; Events", 10, -0.5, 9.5);
    // m_hSvc.create1D("leptonicb_pt", "; leptonic b p_{T} [GeV] ; Events", 16, 200., 1000.);
    // m_hSvc.create1D("leptonicb_m", "; leptonic b mass [GeV] ; Events", 14, 50.,  120.);
    // m_hSvc.create1D("leptonicb_eta", "; leptonic b #eta ; Events", 20,-2.5,   2.5);
    // m_hSvc.create1D("leptonicb_phi", "; leptonic b #phi [rad] ; Events", 32, -3.2, 3.2);


  } else {
    m_hSvc.create1DVar("mtlep_res", "; leptonic top mass [GeV]; Events", varBinN4, varBin4);
    m_hSvc.create1DVar("mthad_res", "; hadronic top mass [GeV]; Events", varBinN4, varBin4);
    m_hSvc.create1DVar("mwhad_res", "; hadronic W boson mass [GeV]; Events", 40, 0, 400);
    m_hSvc.create1D("chi2", "; log(#chi^{2}) ; Events", 50, -3, 7);

    m_hSvc.create1D("MET_res", "; missing E_{T} [GeV]; Events", 40, 0, 800);
    m_hSvc.create1D("MWT_res", "; W transverse mass [GeV]; Events", 50, 0, 500);
    m_hSvc.create1D("ST_res", "; S_{T} [GeV]; Events", 39, 100., 4000.);
    m_hSvc.create2D("MET_MWT_res", ";W transverse mass [GeV]; missing E_{T} [GeV]",100, 0, 500, 2000, 0, 100);
    m_hSvc.create2D("dR_ST_res", ";#Delta R(lep, #nu); S_{T} [GeV]",50, 0, 5, 80, 0, 4000);
    m_hSvc.create1D("DeltaR_lepnu_res", "; #Delta R(lep, #nu); Events", 50, 0., 5.);
    m_hSvc.create1D("nBtagJets_res", "; number of b-tagged jets ; Events", 10, -0.5, 9.5);
    m_hSvc.create1D("nJets_res", "; number of jets ; Events", 15, -0.5, 14.5);
    m_hSvc.create1D("nResolvedW_res", "; number of boosted W's ; Events", 10, -0.5, 9.5);


    m_hSvc.create1D("DeltaR_Whadbjet_res", "; #Delta R(W_{had}, bjet); Events", 50, 0, 5);
    m_hSvc.create2D("bjets_eta_phi_res", ";#phi;  #eta", 32, -3.2, 3.2, 50, -2.5, 2.5);

    m_hSvc.create1D("DeltaR_Whadbjetlead_res", "; #Delta R(W_{had}, bjet_{lead}); Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_Whadbjetsublead_res", "; #Delta R(W_{had}, bjet_{sublead}); Events", 50, 0, 5);


    m_hSvc.create1D("DeltaR_WhadbjetCandidateNotb_res", "; #Delta R(W_{had}, bjet candidate (not b-tagged)) [1bex]; Events", 50, 0, 5);
    m_hSvc.create1D("DeltaR_WhadbjetCandidate_res", "; #Delta R(W_{had}, bjet candidate (b-tagged)) [1bex]; Events", 50, 0, 5);


    m_hSvc.create1D("avgMassT_res", "; (m_{T}^{had}+m_{T}^{lep})/2; Events", 40,  0.,  2000.);

    m_hSvc.create2D("nJet_dR_res", ";number of jets ;#Delta R(lep, #nu)",11, -0.5, 10.5, 50, 0., 5.);
    m_hSvc.create2D("nJet_ST_res", ";number of jets; S_{T} [GeV]",11, -0.5, 10.5, 40, 0., 4000.);

    m_hSvc.create2D("nBtagJets_dR_res", ";number of b-tagged jets ;#Delta R(lep, #nu)",11, -0.5, 10.5, 50, 0., 5.);
    m_hSvc.create2D("nBtagJets_ST_res", ";number of b-tagged jets; S_{T} [GeV]",11, -0.5, 10.5, 40, 0., 4000.);


     m_hSvc.create1D("resolvedWhadCand_pt", "; hadronic W candidate p_{T} [GeV] ; Events", 20, 0., 1000.);
    m_hSvc.create1D("resolvedWhadCand_m", "; hadronic W candidate mass [GeV] ; Events", 20, 0.,  200.);
    m_hSvc.create1D("resolvedWhadCand_eta", "; hadronic W candidate #eta ; Events", 20, -2.5, 2.5);
    m_hSvc.create1D("resolvedWhadCand_phi", "; hadronic W candidate #phi [rad] ; Events", 32, -3.2, 3.2);
  }

  m_hSvc.create1DVar("mtt", "; mass of the top-antitop system [GeV]; Events", varBinN5, varBin5);
  m_hSvc.create1DVar("trueMtt", "; mass of the top-antitop system [GeV]; Events", varBinN5, varBin5);

  m_hSvc.create1D("largeJet_tau32", "; #tau_{32}; Events", 20, 0, 1);
  m_hSvc.create1D("largeJet_tau32_wta", "; #tau_{32} wta; Events", 20, 0, 1);

  m_hSvc.create1D("largeJet_tau21", "; #tau_{21}; Events", 20, 0, 1);
  m_hSvc.create1D("largeJet_tau21_wta", "; #tau_{21} wta; Events", 20, 0, 1);
}

AnaHQTVLQWtX::~AnaHQTVLQWtX() {
}

void AnaHQTVLQWtX::run(const Event &evt, double weight, const std::string &s) {
  // check channel
  //
  // Setup TT system reconstruction algorithm
  std::string TTrecoAlg = "massdiff"; //"asymmmin"

  bool isBoosted = (evt.largeJet().size() >= 1)?true:false;

  // Apply single lepton selection
  bool eEvent=false;
  bool muEvent=false;
   if(m_combined){
     if((evt.electron().size() == 1 && evt.muon().size() == 0))
       eEvent=true;
     else if((evt.electron().size() == 0 && evt.muon().size() == 1))
       muEvent=true;
   }else{
     if (m_electron && (evt.electron().size() != 1 || evt.muon().size() != 0))
       return;
     if (!m_electron && (evt.electron().size() != 0 || evt.muon().size() != 1))
       return;
   }

  HistogramService *h = &m_hSvc;
  std::string suffix = s;

  bool isTight = false;
  TLorentzVector l;
  // Get leptons
  if(m_combined){
    if(eEvent){
      l = evt.electron()[0].mom();
      isTight = evt.electron()[0].isTightPP();
    }else if(muEvent){
      l = evt.muon()[0].mom();
      isTight = evt.muon()[0].isTight();
    }else{
      std::cout <<"WARNING :: No selected lepton in combined channel..." << std::endl;
    }
  }else{
    if (m_electron) {
      l = evt.electron()[0].mom();
      isTight = evt.electron()[0].isTightPP();
      eEvent = true;
    } else {
      l = evt.muon()[0].mom();
      isTight = evt.muon()[0].isTight();
      muEvent = true;
    }//m_electron
  }

  //Build variables that are crucial for the analysis
  ///////////////////////////////////////////////////

  float MET = evt.met().Perp()*1e-3;
  float MWT = sqrt(2. * l.Perp() * evt.met().Perp() * (1. - cos(l.Phi() - evt.met().Phi())))*1e-3;
  _tree_MET = MET;
  _tree_MWT = MWT;

  // Jets
  std::vector<TLorentzVector> jets;
  size_t iJet = 0;
  for (; iJet < evt.jet().size(); ++iJet){
    jets.push_back(evt.jet()[iJet].mom());
  }
  // B-tagging
  int nBtagged = 0; //nB-tagged jets
  std::vector<TLorentzVector> bJets;
  for (size_t bidx = 0; bidx < evt.jet().size(); ++bidx)
    if (evt.jet()[bidx].btag_mv2c10_77()){
      if(nBtagged==0) h->FillFlowHandler(h->h1D("leadbJetPt", "", suffix), evt.jet()[bidx].mom().Perp()*1e-3, weight);
      h->FillFlowHandler(h->h1D("mass_lepb", "", suffix), (l + evt.jet()[bidx].mom() ).M()*1e-3, weight );
      bJets.push_back(evt.jet()[bidx].mom());
      nBtagged += 1;
    }

  int nJets = evt.jet().size(); //njets
  float ST =  m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
  float dR_lepnu(0.);

  // Neutrino Reconstruction
  // Using neutrino implementation from Michigan
  // Tested by two students from Michigan and Oklahoma and ought to be the mist suitable

  TLorentzVector lep4nu;
  lep4nu.SetPtEtaPhiE( l.Perp()*1e-3, l.Eta(), l.Phi(), l.E()*1e-3 ); // Make TLorentzVector of the lepton in GeV

  TLorentzVector nu;
  TLorentzVector nuTmp = m_vlqneutrinoBuilder.GetNeutrino( lep4nu, evt.met().Perp()*1e-3, evt.met().Phi() ); // Tool needs GeV
  nu.SetPtEtaPhiE( nuTmp.Perp()*1000., nuTmp.Eta(), nuTmp.Phi(), nuTmp.E()*1000. ); // set the neutrino to MeV like other objects
  dR_lepnu = l.DeltaR(nu);
  const TLorentzVector &j = evt.jet()[0].mom();
  _tree_dR_lnu = dR_lepnu;

  float mtt = -1;

  //deltaR between lepton and the closest narrow jet
  float closejl_deltaR  = 99;
  float deltaR_tmp      = 99;
  int closejl_idx       = -1;

  if(m_skipRegionPlots < 1){
    h->FillFlowHandler(h->h1D("nLargeJet", "", suffix), evt.largeJet().size() , weight);
    h->FillFlowHandler(h->h1D("weight_leptSF", "", suffix), evt.weight_leptonSF(), 1);
    h->FillFlowHandler(h->h1D("weight", "", suffix), weight, 1);
    h->h2D("weight_leptPt", "", suffix)->Fill(l.Perp()*1e-3, weight);
    h->h2D("MET_MWT", "", suffix)->Fill(MWT, MET, weight);
    h->h2D("dR_ST", "", suffix)->Fill(dR_lepnu, ST, weight);
    h->h2D("nJet_dR", "", suffix)->Fill(nJets, dR_lepnu, weight);
    h->h2D("nJet_ST", "", suffix)->Fill(nJets, ST, weight);
    h->h2D("nBtagJets_dR", "", suffix)->Fill(nBtagged,dR_lepnu, weight);
    h->h2D("nBtagJets_ST", "", suffix)->Fill(nBtagged,ST, weight);

    h->FillFlowHandler(h->h1D("lepPt", "", suffix), l.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("lepPt_effBins", "", suffix), l.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("lepEta", "", suffix), l.Eta(), weight);
    h->FillFlowHandler(h->h1D("lepPhi", "", suffix), l.Phi(), weight);

    h->FillFlowHandler(h->h1D("leadJetPt", "", suffix), j.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("nJets", "", suffix), nJets, weight);
    h->FillFlowHandler(h->h1D("nBtagJets", "", suffix), nBtagged, weight);
    h->FillFlowHandler(h->h1D("ST", "", suffix), ST, weight);
    h->FillFlowHandler(h->h1D("ST_effBins", "", suffix), ST, weight);


    //missing et
    h->FillFlowHandler(h->h1D("MET", "", suffix), MET, weight);
    h->FillFlowHandler(h->h1D("MET_phi", "", suffix), evt.met().Phi(), weight);
    h->FillFlowHandler(h->h1D("DeltaR_lepnu", "", suffix), dR_lepnu, weight );

    //transverse W mass
    h->FillFlowHandler(h->h1D("mwt", "", suffix), MWT, weight);
    if(bJets.size()!=0){
      h->FillFlowHandler(h->h1D("mass_lepblead", "", suffix), (l + bJets.at(0) ).M()*1e-3, weight );
    }
    h->FillFlowHandler(h->h1D("yields", "", suffix), 1, weight);
    //mu
    h->FillFlowHandler(h->h1D("mu", "", suffix), evt.mu()*1.16, weight);
    h->FillFlowHandler(h->h1D("mu_original", "", suffix), evt.mu_original(), weight);

    //npv
    h->FillFlowHandler(h->h1D("npv", "", suffix), evt.npv(), weight);

    //z prosition of primary vertex
    h->FillFlowHandler(h->h1D("vtxz", "", suffix), evt.vtxz(), weight);

    int nTrkBtagged = 0; //nB-tagged jets
    for (size_t bidx = 0; bidx < evt.tjet().size(); ++bidx)
      if (evt.tjet()[bidx].btag_mv2c20_70_trk() && evt.tjet()[bidx].pass_trk()){
	if(nTrkBtagged==0)h->FillFlowHandler(h->h1D("leadTrkbJetPt", "", suffix), evt.tjet()[bidx].mom().Perp()*1e-3, weight);
	nTrkBtagged += 1;
      }
    h->FillFlowHandler(h->h1D("nTrkBtagJets", "", suffix), nTrkBtagged, weight);

    // Jet kinematics
    std::vector<float> jetMass_vector;
    std::vector<float> jetPt_vector;
    std::vector<float> jetEta_vector;
    std::vector<float> jetPhi_vector;

    jetMass_vector.resize(evt.jet().size());
    jetPt_vector.resize(evt.jet().size());
    jetEta_vector.resize(evt.jet().size());
    jetPhi_vector.resize(evt.jet().size());

    std::vector<TLorentzVector> jets;
    size_t iJet = 0;
    for (; iJet < evt.jet().size(); ++iJet){
      jets.push_back(evt.jet()[iJet].mom());
      const TLorentzVector &jet_p4 = evt.jet()[iJet].mom();
      jetMass_vector[iJet] =  evt.jet()[iJet].mom().M();
      jetPt_vector[iJet]   =  evt.jet()[iJet].mom().Pt();
      jetEta_vector[iJet]  =  evt.jet()[iJet].mom().Eta();
      jetPhi_vector[iJet]  =  evt.jet()[iJet].mom().Phi();
      h->FillFlowHandler(h->h1D("JetPt", "", suffix),  jet_p4.Perp()*1e-3, weight);
    }//for

    int maxNjet = (evt.jet().size()<6) ? evt.jet().size() : 6;
    for (int i = 0; i < maxNjet; ++i){

      std::string nameJet_m = "jet" + std::to_string(i)+"_m";
      h->FillFlowHandler(h->h1D(nameJet_m, "", suffix), jetMass_vector[i]*1e-3, weight);

      std::string nameJet_pt = "jet" + std::to_string(i)+"_pt";
      h->FillFlowHandler(h->h1D(nameJet_pt, "", suffix), jetPt_vector[i]*1e-3, weight);

      std::string nameJet_eta = "jet" + std::to_string(i)+"_eta";
      h->FillFlowHandler(h->h1D(nameJet_eta, "", suffix), jetEta_vector[i], weight);

      std::string nameJet_phi = "jet" + std::to_string(i)+"_phi";
      h->FillFlowHandler(h->h1D(nameJet_phi, "", suffix), jetPhi_vector[i], weight);

    }//for



     mtt = -1;

    //deltaR between lepton and the closest narrow jet
     closejl_deltaR  = 99;
     deltaR_tmp      = 99;
     closejl_idx       = -1;

    size_t jet_idx = 0;
    for (; jet_idx < evt.jet().size(); ++jet_idx){
      deltaR_tmp = 99;
      float dphi = l.DeltaPhi(evt.jet()[jet_idx].mom());
      float dy = l.Rapidity() - evt.jet()[jet_idx].mom().Rapidity();
      deltaR_tmp = std::sqrt(dy*dy + dphi*dphi);
      if (deltaR_tmp < closejl_deltaR){
        closejl_deltaR = deltaR_tmp;
        closejl_idx = jet_idx;
      }
    }//for

    float closejl_pt = -1;
    if (closejl_idx>0)    closejl_pt = evt.jet()[closejl_idx].mom().Perp();
    h->FillFlowHandler(h->h1D("minDeltaR_lepjet", "", suffix), closejl_deltaR, weight);
    h->FillFlowHandler(h->h1D("closejl_minDeltaR_effBins", "", suffix), closejl_deltaR, weight);
    h->FillFlowHandler(h->h1D("closejl_pt", "", suffix), closejl_pt*1e-3, weight);

    h->FillFlowHandler(h->h1D("trueMtt", "", suffix), evt.MC_ttbar_beforeFSR().M()*1e-3, weight);
    //  _tree_truemtt = evt.MC_ttbar_beforeFSR().M()*1e-3;


    //testing purposes
    size_t boostedW_idx = 0;
    for (; boostedW_idx< evt.largeJet().size(); ++boostedW_idx) {
      h->FillFlowHandler(h->h1D("WtagMed", "", suffix), evt.largeJet()[boostedW_idx].isWTaggedMed(), weight);
      //std::cout << "run DEBUG :: wtag "<< evt.largeJet()[boostedW_idx].isWTaggedMed() << std::endl;
      h->FillFlowHandler(h->h1D("TopTagMed50", "", suffix), evt.largeJet()[boostedW_idx].isSmoothTopTagged_50(), weight);
      //	std::cout << "run DEBUG :: top tag "<< evt.largeJet()[boostedW_idx].isSmoothTopTagged_50() << std::endl;
    }


  }// endSkipRegionPlots



  //DeltaR_bjet1bjet2
  if(bJets.size()>1)
    h->FillFlowHandler(h->h1D("DeltaR_bjet1bjet2", "", suffix),  bJets.at(0).DeltaR(bJets.at(1)), weight );
  int nWtags(0.);
  nWtags = m_vlqutils.boostedWtags( evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed" );
  //nWtags = m_vlqutils.boostedWtags( evt.largeJet(), bJets, "Checks" );
  h->FillFlowHandler(h->h1D("nBoostedW", "", suffix), nWtags, weight);
  int nTopTags(0.);
  nTopTags = m_vlqutils.boostedTopTags( evt.largeJet(), bJets, "INTERNAL_Smooth50" );
  h->FillFlowHandler(h->h1D("nBoostedTop", "", suffix), nTopTags, weight);

  TLorentzVector boostedHadWcand;
  TLorentzVector boostedHadTcand;

  TLorentzVector resolvedHadWcand;
  float ST_boo(0.);// = m_vlqutils.GetHTboosted(l.Perp()*1e-3, evt.met().Perp()*1e-3, boostedHadWcand, evt.jet(), &bJets, nBtagged, nWtags);
  float ST_res(0.);// = m_vlqutils.GetHTboosted(l.Perp()*1e-3, evt.met().Perp()*1e-3, boostedHadWcand, evt.jet(), &bJets, nBtagged, nWtags);


  // Overlap removal between the hadronic W candidate and b-jet candidates
  double OR_dRcut = 0.;


  //BOOSTED SELECTION
  ///////////////////////
  unsigned int icut = 0;
  unsigned int icut_res = 0;
  this->CountEvent(icut, weight);
  this->CountEventRes(icut_res, weight);
  if ( m_boosted ) {
    //Boosted Event Selections
    this->CountEvent(icut, weight);
    if(m_boostedselection == "W1_T1_INTERNAL"){
      OR_dRcut = 1.0;
      this->CountEvent(icut, weight);
      if( MET < 60.) return;
      this->CountEvent(icut, weight);
      if( bJets.size() < 1) return;
      this->CountEvent(icut, weight);
      if( nJets < 6 ) return;
      this->CountEvent(icut, weight);
      if( nWtags < 1 ) return;
      this->CountEvent(icut, weight);
      if( nTopTags < 1 ) return;
      this->CountEvent(icut, weight);
      boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed");
      boostedHadTcand = m_vlqutils.boostedTcandidate(evt.largeJet(), bJets, "INTERNAL_Smooth50");
      ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
    }else if(m_boostedselection == "W0_T1_INTERNAL"){
	OR_dRcut = 1.0;
	this->CountEvent(icut, weight);
	if( MET < 60.) return;
	this->CountEvent(icut, weight);
	if( bJets.size() < 1) return;
	this->CountEvent(icut, weight);
	if( nJets < 6 ) return;
	this->CountEvent(icut, weight);
	if( nWtags != 0 ) return;
	this->CountEvent(icut, weight);
	if( nTopTags < 1 ) return;
	this->CountEvent(icut, weight);
	boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed");
	boostedHadTcand = m_vlqutils.boostedTcandidate(evt.largeJet(), bJets, "INTERNAL_Smooth50");
	ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
    }else if(m_boostedselection == "W1_T0_INTERNAL"){
      OR_dRcut = 1.0;
      this->CountEvent(icut, weight);
      if( MET < 60.) return;
      this->CountEvent(icut, weight);
      if( bJets.size() < 1) return;
      this->CountEvent(icut, weight);
      if( nJets < 6 ) return;
      this->CountEvent(icut, weight);
      if( nWtags < 1 ) return;
      this->CountEvent(icut, weight);
      if( nTopTags !=0 ) return;
      this->CountEvent(icut, weight);
      boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed");
      boostedHadTcand = m_vlqutils.boostedTcandidate(evt.largeJet(), bJets, "INTERNAL_Smooth50");
      ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
  }else if(m_boostedselection == "BAG_SR_Mor17"){
      OR_dRcut = 1.0;
      this->CountEvent(icut, weight);
      if( MET < 60.) return;
      this->CountEvent(icut, weight);
      if( nWtags < 1 ) return;
      this->CountEvent(icut, weight);
      if( bJets.size() < 1) return;
      this->CountEvent(icut, weight);
      boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed");
      ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( ST_boo < 1800) return;
      this->CountEvent(icut, weight);
      if( dR_lepnu > .7) return;
      this->CountEvent(icut, weight);
    }else  if(m_boostedselection == "boosted_CR_Mor17"){
      OR_dRcut = 1.0;
      this->CountEvent(icut, weight);
      if( MET < 60.) return;
      this->CountEvent(icut, weight);
      if( nWtags < 1 ) return;
      this->CountEvent(icut, weight);
      if( bJets.size() < 1) return;
      this->CountEvent(icut, weight);
      boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed");
      ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( ST_boo < 1000 || ST_boo > 1800) return;
      this->CountEvent(icut, weight);
      if( dR_lepnu > .7) return;
      this->CountEvent(icut, weight);
    }else if(m_boostedselection == "boosted_VR_Mor17"){
      OR_dRcut = 1.0;
      this->CountEvent(icut, weight);
      if( MET < 60.) return;
      this->CountEvent(icut, weight);
      if( nWtags < 1 ) return;
      this->CountEvent(icut, weight);
      if( bJets.size() < 1) return;
      this->CountEvent(icut, weight);
      boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed");
      ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( ST_boo < 1200) return;
      this->CountEvent(icut, weight);
      if( dR_lepnu <= .7 || dR_lepnu > 1.5) return;
      this->CountEvent(icut, weight);
    }else if(m_boostedselection == "custom_Mor17"){
      OR_dRcut = 1.0;
      this->CountEvent(icut, weight);
      if( nWtags < 1 ) return;
      this->CountEvent(icut, weight);
      if( bJets.size() < 1) return;
      this->CountEvent(icut, weight);
      boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, l, eEvent, "INTERNAL_WTX_WtagMed");
      //boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), bJets, "Checks");
      ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( MET < 60.) return;
      this->CountEvent(icut, weight);
      ST_boo = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
    }else{
      std::cout <<"ERROR :: No selection for boosted case selected ... " << std::endl;
      return;
    }

    // (SH)
    // Apply Overlap Removal between the selected boosted hadronic W candidate and all b-jets
    // Comment: In addition, for the reconstruction of the TT system, the jets in the == 1b-jet category are removed
    if(bJets.size() > 0) {
      for ( size_t bidx = 0; bidx < bJets.size(); ++bidx){
    	if(boostedHadWcand.DeltaR(bJets.at(bidx)) < OR_dRcut){
    	  this->CountEvent(icut, weight);
    	  return;
    	}
	h->FillFlowHandler(h->h1D("DeltaR_Whadbjet_boo", "", suffix),  boostedHadWcand.DeltaR(bJets.at(bidx)), weight );
	h->h2D("bjets_eta_phi_boo", "", suffix)->Fill(bJets.at(bidx).Eta(), bJets.at(bidx).Phi(), weight);
      }
      h->FillFlowHandler(h->h1D("DeltaR_Whadbjetlead_boo", "", suffix),  boostedHadWcand.DeltaR(bJets.at(0)), weight );
    }
    if(bJets.size() > 1) h->FillFlowHandler(h->h1D("DeltaR_Whadbjetsublead_boo", "", suffix),  boostedHadWcand.DeltaR(bJets.at(1)), weight );

    const TLorentzVector lepW = l+nu;
    std::map<std::string, TLorentzVector> TTcandidates = m_vlqutils.BoostedTTcandidates(boostedHadWcand, lepW, jets, bJets, true, TTrecoAlg, OR_dRcut);

    TLorentzVector hadT, lepT, hadb, lepb;
    for(auto ele: TTcandidates) {
      //veto events in which the TT reconstruction yields no candidates
      if( ele.second.M()  <= 0. )
	return;
      if(ele.first == "hadT"){
	hadT = ele.second;
      }
       if(ele.first == "lepT"){
	 lepT = ele.second;
       }
       if(ele.first == "hadb"){
	 hadb = ele.second;
       }
       if(ele.first == "lepb"){
	 lepb = ele.second;
       }

    }

    //
    // APPLY OVERLAP REMOVAL BETWEEN THE W- and T-Candidates
    //
    if( boostedHadWcand.DeltaR(boostedHadTcand) < 1.0 ) return;


    h->FillFlowHandler(h->h1D("DeltaR_WhadCThadC", "", suffix), boostedHadWcand.DeltaR(boostedHadTcand), weight );

    h->FillFlowHandler(h->h1D("hadronicT_pt", "", suffix), hadT.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("hadronicT_m", "", suffix), hadT.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("hadronicT_eta", "", suffix), hadT.Eta(), weight);
    h->FillFlowHandler(h->h1D("hadronicT_phi", "", suffix), hadT.Phi(), weight);


    h->FillFlowHandler(h->h1D("leptonicT_pt", "", suffix), lepT.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicT_m", "", suffix), lepT.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicT_m_boostedSR", "", suffix), lepT.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicT_m_boostedCR", "", suffix), lepT.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicT_eta", "", suffix), lepT.Eta(), weight);
    h->FillFlowHandler(h->h1D("leptonicT_phi", "", suffix), lepT.Phi(), weight);


    size_t close_idx = 0;
    for (; close_idx < evt.jet().size(); ++close_idx)
      //      if (evt.jet()[close_idx].closeToLepton())
      if (l.DeltaPhi(evt.jet()[close_idx].mom()) < 1.0)
    	break;
    const TLorentzVector &sj = evt.jet()[close_idx].mom();
    h->FillFlowHandler(h->h1D("closeJetPt_boo", "", suffix), sj.Perp()*1e-3, weight);

    size_t goodljet_idx = 0;
    for (; goodljet_idx < evt.largeJet().size(); ++goodljet_idx) {
      //std::cout << "jet " << goodljet_idx << " pt = " << evt.largeJet()[goodljet_idx].mom().Perp() << ", good = " << evt.largeJet()[goodljet_idx].good() << std::endl;
      if (evt.largeJet()[goodljet_idx].good())
	break;
    }

    //Event kinematics
    h->FillFlowHandler(h->h1D("ST_boo", "", suffix), ST_boo, weight);
    h->FillFlowHandler(h->h1D("MET_boo", "", suffix), MET, weight);
    h->FillFlowHandler(h->h1D("MWT_boo", "", suffix), MWT, weight);
    h->h2D("MET_MWT_boo", "", suffix)->Fill(MWT, MET, weight);
    h->h2D("dR_ST_boo", "", suffix)->Fill(dR_lepnu, ST_boo, weight);
    h->h2D("nJet_dR_boo", "", suffix)->Fill(nJets,dR_lepnu, weight);
    h->h2D("nJet_ST_boo", "", suffix)->Fill(nJets,ST, weight);
    h->h2D("nBtagJets_dR_boo", "", suffix)->Fill(bJets.size(),dR_lepnu, weight);
    h->h2D("nBtagJets_ST_boo", "", suffix)->Fill(bJets.size(),ST, weight);
    h->h2D("Whad_eta_phi_boo", "", suffix)->Fill(boostedHadWcand.Eta(),boostedHadWcand.Phi(), weight);

    h->FillFlowHandler(h->h1D("DeltaR_lepnu_boo", "", suffix), dR_lepnu, weight );
    h->FillFlowHandler(h->h1D("nJets_boo", "", suffix), nJets, weight);
    h->FillFlowHandler(h->h1D("nBtagJets_boo", "", suffix), bJets.size(), weight);
    h->FillFlowHandler(h->h1D("nBoostedW_boo", "", suffix), nWtags, weight);
    h->FillFlowHandler(h->h1D("nBoostedTop_boo", "", suffix), nTopTags, weight);

    if(bJets.size() !=0) h->FillFlowHandler(h->h1D("bjet1Pt_boo", "", suffix),  bJets.at(0).Perp()*1e-3, weight );
    if(bJets.size() > 1) h->FillFlowHandler(h->h1D("bjet2Pt_boo", "", suffix),  bJets.at(1).Perp()*1e-3, weight );
    // lepton
    h->FillFlowHandler(h->h1D("lepPt_boo", "", suffix), l.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("lepEta_boo", "", suffix), l.Eta(), weight);
    h->FillFlowHandler(h->h1D("lepPhi_boo", "", suffix), l.Phi(), weight);

    const TLorentzVector &lj = evt.largeJet()[goodljet_idx].mom();
    h->FillFlowHandler(h->h1D("largeJetPt", "", suffix), lj.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("largeJetM", "", suffix), lj.M()*1e-3, weight);
    //h->h1D("largeJetEta", "", suffix)->Fill(lj.Eta(), weight);

    h->FillFlowHandler(h->h1D("largeJetPhi", "", suffix), lj.Phi(), weight);
    h->FillFlowHandler(h->h1D("largeJetSd12", "", suffix), evt.largeJet()[goodljet_idx].split12()*1e-3, weight);
    //h->h1D("largeJet_tau32", "", suffix)->Fill(evt.largeJet()[goodljet_idx].subs("tau32"), weight);
    //    h->h1D("largeJet_tau32_wta", "", suffix)->Fill(evt.largeJet()[goodljet_idx].subs("tau32_wta"), weight);
    //    h->h1D("largeJet_tau21", "", suffix)->Fill(evt.largeJet()[goodljet_idx].subs("tau21"), weight);
    //    h->h1D("largeJet_tau21_wta", "", suffix)->Fill(evt.largeJet()[goodljet_idx].subs("tau21_wta"), weight);


    //    const TLorentzVector boostedHadWcand = m_vlqutils.boostedWcandidate(evt.largeJet(), "loose");

    for ( size_t bidx = 0; bidx < bJets.size(); ++bidx){
      h->FillFlowHandler2D(    h->h2D("DeltaR_Whadbjet_boo_ThadMass", "", suffix), boostedHadWcand.DeltaR(bJets.at(bidx)), hadT.M()*1e-3, weight);
    }
    if(bJets.size() > 0)       h->FillFlowHandler2D(    h->h2D("DeltaR_Whadbjetlead_boo_ThadMass", "", suffix), boostedHadWcand.DeltaR(bJets.at(0)), hadT.M()*1e-3, weight);
    if(bJets.size() > 1)       h->FillFlowHandler2D(    h->h2D("DeltaR_Whadbjetsublead_boo_ThadMass", "", suffix), boostedHadWcand.DeltaR(bJets.at(1)), hadT.M()*1e-3, weight);


    if(bJets.size() == 1){
      if(bJets.at(0) == hadb){
	h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidate_boo", "", suffix),  boostedHadWcand.DeltaR(hadb), weight );
	h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidateNotb_boo", "", suffix),  boostedHadWcand.DeltaR(lepb), weight );
      }      else{
	h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidate_boo", "", suffix),  boostedHadWcand.DeltaR(lepb), weight );
	h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidateNotb_boo", "", suffix),  boostedHadWcand.DeltaR(hadb), weight );

      }
    }

    //correlation plots
    h->FillFlowHandler2D(    h->h2D("ST_lepM", "", suffix), ST_boo, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("dRlepnu_lepM", "", suffix), dR_lepnu, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("leadJetPt_lepM", "", suffix), j.Perp()*1e-3, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("MET_lepM", "", suffix), MET, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("hadM_lepM", "", suffix), hadT.M()*1e-3, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("DeltaR_lepWhad_lepM", "", suffix), boostedHadWcand.DeltaR(l), lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("DeltaR_ThadTlep_lepM", "", suffix), hadT.DeltaR(lepT), lepT.M()*1e-3, weight);

    //DeltaM_hadtopleptop
    h->FillFlowHandler(h->h1D("DeltaM_hadtopleptop", "", suffix), fabs( hadT.M()*1e-3 - lepT.M()*1e-3 ), weight);
    h->FillFlowHandler(h->h1D("avgMassT_boo", "", suffix), fabs( (hadT.M()*1e-3 + lepT.M()*1e-3)/2 ), weight);
    h->FillFlowHandler(h->h1D("DeltaR_ThadTlep", "", suffix),  hadT.DeltaR(lepT), weight);
    h->FillFlowHandler(h->h1D("mass_lepleptonicb_boo", "", suffix), (l + lepb ).M()*1e-3, weight );

    h->FillFlowHandler(h->h1D("nleptonicW", "", suffix), 1, weight);
    h->FillFlowHandler(h->h1D("leptonicW_pt", "", suffix), lepW.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicW_m", "", suffix), lepW.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicW_eta", "", suffix), lepW.Eta(), weight);
    h->FillFlowHandler(h->h1D("leptonicW_phi", "", suffix), lepW.Phi(), weight);


    h->FillFlowHandler(h->h1D("boostedWhadCand_pt", "", suffix), boostedHadWcand.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("boostedWhadCand_m", "", suffix), boostedHadWcand.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("boostedWhadCand_eta", "", suffix), boostedHadWcand.Eta(), weight);
    h->FillFlowHandler(h->h1D("boostedWhadCand_phi", "", suffix), boostedHadWcand.Phi(), weight);

    h->FillFlowHandler(h->h1D("boostedThadCand_pt", "", suffix), boostedHadTcand.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("boostedThadCand_m", "", suffix), boostedHadTcand.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("boostedThadCand_eta", "", suffix), boostedHadTcand.Eta(), weight);
    h->FillFlowHandler(h->h1D("boostedThadCand_phi", "", suffix), boostedHadTcand.Phi(), weight);


    h->FillFlowHandler(h->h1D("DeltaR_WhadThad", "", suffix),  boostedHadWcand.DeltaR(hadT), weight);
    h->FillFlowHandler(h->h1D("DeltaR_WhadTlep", "", suffix),  boostedHadWcand.DeltaR(lepT), weight);
    h->FillFlowHandler(h->h1D("DeltaR_lepWhad", "", suffix),  boostedHadWcand.DeltaR(l), weight);

    h->FillFlowHandler(h->h1D("DeltaR_WlepThad", "", suffix),  lepW.DeltaR(hadT), weight);
    h->FillFlowHandler(h->h1D("DeltaR_WlepTlep", "", suffix),  lepW.DeltaR(lepT), weight);

    h->FillFlowHandler(h->h1D("DeltaR_Whadbhad", "", suffix),  boostedHadWcand.DeltaR(hadb), weight);
    h->FillFlowHandler(h->h1D("DeltaR_Whadblep", "", suffix),  boostedHadWcand.DeltaR(lepb), weight);
    h->FillFlowHandler(h->h1D("DeltaR_Wlepbhad", "", suffix),  lepW.DeltaR(hadb), weight);
    h->FillFlowHandler(h->h1D("DeltaR_Wlepblep", "", suffix),  lepW.DeltaR(lepb), weight);
    //min. deltaR between hadronic W candidate and b-jet
    float closebjHadW_deltaR  = 99;
    deltaR_tmp      = 99;
    int closebjHadW_idx       = -1;

    for (size_t bidx = 0; bidx < bJets.size(); ++bidx){
      deltaR_tmp = 99;
      deltaR_tmp = boostedHadWcand.DeltaR(bJets.at(bidx));
      if (deltaR_tmp < closebjHadW_deltaR){
        closebjHadW_deltaR = deltaR_tmp;
        closebjHadW_idx = bidx;
      }
    }//for

    h->FillFlowHandler(h->h1D("minDeltaR_hadWbjet_boo", "", suffix),  closebjHadW_deltaR, weight);
    if(bJets.size() !=0)     h->FillFlowHandler(h->h1D("closebjHadW_pt_boo", "", suffix), bJets.at(closebjHadW_idx).Perp()*1e-3, weight);

    if(bJets.size() !=0)     h->FillFlowHandler(h->h1D("DeltaR_lepblead_boo", "", suffix), l.DeltaR(bJets.at(0) ), weight );
    if(bJets.size() > 1){
      h->FillFlowHandler(h->h1D("DeltaR_lepbsublead_boo", "", suffix), l.DeltaR(bJets.at(1) ), weight );
      h->h2D("dR_lepblead_dR_lepbsublead_boo", "", suffix)->Fill(l.DeltaR(bJets.at(1)), l.DeltaR(bJets.at(0)), weight);
    }
    //min. mass between lepton and b-jet
    float minMass_lepb  = 9e6;
    float minMass_lepb_tmp      = 9e6;
    int minMass_lepb_idx       = -1;
    for (size_t bidx = 0; bidx < bJets.size(); ++bidx){
      h->FillFlowHandler(h->h1D("mass_lepb_boo", "", suffix), (l + bJets.at(bidx) ).M()*1e-3, weight );
      minMass_lepb_tmp = 9e6;
      minMass_lepb_tmp = (l + bJets.at(bidx) ).M()*1e-3;
      if (minMass_lepb_tmp < minMass_lepb){
        minMass_lepb = minMass_lepb_tmp;
        minMass_lepb_idx = bidx;
      }
    }//for

    if(bJets.size()!=0){
      h->FillFlowHandler(h->h1D("mass_lepblead_boo", "", suffix), (l + bJets.at(0) ).M()*1e-3, weight );
      h->FillFlowHandler(h->h1D("minMass_lepb_boo", "", suffix), (l + bJets.at(minMass_lepb_idx) ).M()*1e-3, weight );
    }
    //min. mass between lepton and jet
    float minMass_lepjet  = 9e6;
    float minMass_lepjet_tmp      = 9e6;
    int minMass_lepjet_idx       = -1;
    for (size_t jidx = 0; jidx < jets.size(); ++jidx){
      h->FillFlowHandler(h->h1D("mass_lepjet_boo", "", suffix), (l + jets.at(jidx) ).M()*1e-3, weight );
      minMass_lepjet_tmp = 9e6;
      minMass_lepjet_tmp = (l + jets.at(jidx) ).M()*1e-3;
      if (minMass_lepjet_tmp < minMass_lepjet){
        minMass_lepjet = minMass_lepjet_tmp;
        minMass_lepjet_idx = jidx;
      }
    }//for
    if(jets.size()!=0)
      h->FillFlowHandler(h->h1D("mass_lepjetlead_boo", "", suffix), (l + jets.at(0) ).M()*1e-3, weight );
    h->FillFlowHandler(h->h1D("minMass_lepjet_boo", "", suffix), (l + jets.at(minMass_lepjet_idx) ).M()*1e-3, weight );

    h->FillFlowHandler(h->h1D("DeltaR_lepjetlead_boo", "", suffix), l.DeltaR(jets.at(0) ), weight );
    h->FillFlowHandler(h->h1D("DeltaR_lepjetsublead_boo", "", suffix), l.DeltaR(jets.at(1) ), weight );
    if(jets.size()>2) h->FillFlowHandler(h->h1D("DeltaR_lepjetthird_boo", "", suffix), l.DeltaR(jets.at(2) ), weight );
    if(jets.size()>3) h->FillFlowHandler(h->h1D("DeltaR_lepjetfourth_boo", "", suffix), l.DeltaR(jets.at(3) ), weight );
    h->h2D("dR_lepjetlead_dR_lepjetsublead_boo", "", suffix)->Fill(l.DeltaR(jets.at(1)), l.DeltaR(jets.at(0)), weight);

    h->FillFlowHandler(h->h1D("", "", suffix), boostedHadWcand.DeltaR(jets.at(0) ), weight );

    h->FillFlowHandler(h->h1D("DeltaR_Whadjetlead_boo", "", suffix), boostedHadWcand.DeltaR(jets.at(0) ), weight );
    h->FillFlowHandler(h->h1D("DeltaR_Whadjetsublead_boo", "", suffix), boostedHadWcand.DeltaR(jets.at(1) ), weight );
    if(jets.size()>2) h->FillFlowHandler(h->h1D("DeltaR_Whadjetthird_boo", "", suffix), boostedHadWcand.DeltaR(jets.at(2) ), weight );
    if(jets.size()>3) h->FillFlowHandler(h->h1D("DeltaR_Whadjetfourth_boo", "", suffix), boostedHadWcand.DeltaR(jets.at(3) ), weight );
    h->FillFlowHandler2D(    h->h2D("DeltaR_Whadjetlead_boo_ThadMass", "", suffix), boostedHadWcand.DeltaR(jets.at(0)), hadT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("DeltaR_Whadjetsublead_boo_ThadMass", "", suffix), boostedHadWcand.DeltaR(jets.at(1)), hadT.M()*1e-3, weight);
    if(jets.size()>2)  h->FillFlowHandler2D(    h->h2D("DeltaR_Whadjetthird_boo_ThadMass", "", suffix), boostedHadWcand.DeltaR(jets.at(2)), hadT.M()*1e-3, weight);
    if(jets.size()>3)  h->FillFlowHandler2D(    h->h2D("DeltaR_Whadjetfourth_boo_ThadMass", "", suffix), boostedHadWcand.DeltaR(jets.at(3)), hadT.M()*1e-3, weight);

    h->h2D("dR_Whadjetlead_dR_Whadjetsublead_boo", "", suffix)->Fill(boostedHadWcand.DeltaR(jets.at(1)), boostedHadWcand.DeltaR(jets.at(0)), weight);


  //min. deltaR between lepton and b-jet
  float closebjl_deltaR  = 99;
  deltaR_tmp      = 99;
  int closebjl_idx       = -1;
  for ( size_t bidx = 0; bidx < bJets.size(); ++bidx){
    deltaR_tmp = 99;
    deltaR_tmp = l.DeltaR(bJets.at(bidx));
    if (deltaR_tmp < closebjl_deltaR){
        closebjl_deltaR = deltaR_tmp;
        closebjl_idx = bidx;
    }
  }//for

  h->FillFlowHandler(h->h1D("minDeltaR_lepbjet_boo", "", suffix),  closebjl_deltaR, weight );
  if(bJets.size() !=0)   h->FillFlowHandler(h->h1D("closebjl_pt_boo", "", suffix), bJets.at(closebjl_idx).Perp()*1e-3, weight);


  if (evt.largeJet().size()!=0)    mtt = (lj+sj+nu+l).M();
  h->FillFlowHandler(h->h1D("mtt", "", suffix), mtt*1e-3, weight);
  h->FillFlowHandler(h->h1D("mtlep_boo", "", suffix), (sj+nu+l).M()*1e-3, weight);


  if (m_ntuple==1){

    _tree_WbWb = 0;
    _tree_ZtZt = 0;
    _tree_HtHt = 0;
    _tree_WbZt = 0;
    _tree_WbHt = 0;
    _tree_ZtHt = 0;
    int this_vlq_evtype = evt.vlq_evtype();
        if (this_vlq_evtype==0) _tree_WbWb = 1;
    else if (this_vlq_evtype==1) _tree_ZtZt = 1;
    else if (this_vlq_evtype==2) _tree_HtHt = 1;
    else if (this_vlq_evtype==3) _tree_WbZt = 1;
    else if (this_vlq_evtype==4) _tree_WbHt = 1;
    else if (this_vlq_evtype==5) _tree_ZtHt = 1;


    _tree_lep_mass = lepT.M()*1e-3;
    _tree_had_mass = hadT.M()*1e-3;
    _tree_lb_mass = -1; // ???
    _tree_lb_minmass = minMass_lepb;
    _tree_lblep_mass = (l + lepb ).M()*1e-3;
    _tree_Delta_mass = abs(_tree_lep_mass - _tree_had_mass);
    _tree_DeltaR_TlepThad = hadT.DeltaR(lepT);
    _tree_ST = ST;
    _tree_weight = weight;
    _tree_cat = -1;
    if ( m_boosted ) _tree_cat = 1;
    _tree_syst = s;
    h->m_tree->Fill();
  }
  if (m_ntuple==2){
    bool selected = false;
    std::vector<int> vec_regions;

    if( MET >= 60. && nWtags >= 1 &&  bJets.size() >= 1){
      // SR index 300
      if ( ST_boo >= 1800 && dR_lepnu <= .7 ) {vec_regions.push_back(300); selected = true;}
      // CR index 301
      if ( ST_boo >= 1000 && ST_boo < 1800 && dR_lepnu <= .7 ) {vec_regions.push_back(301); selected = true;}
      // VR index 302
      if ( ST_boo >= 1200 && dR_lepnu > .7  && dR_lepnu <= 1.5) {vec_regions.push_back(302);selected = true;}
    }

    if (selected){
      _tree_evid = evt.eventNumber() ;
      _tree_Njet = nJets;
      _tree_Nbjet = nBtagged;
      _tree_Nlep = 1;
      _tree_weight = weight;
      _tree_region = vec_regions;
      h->m_tree->Fill();
    }
  }
  }


  //RESOLVED SELECTION
  ////////////////////
  if (!m_boosted &&  nWtags < 1 ) {
    this->CountEventRes(icut_res, weight);
    if(m_resolvedselection == "resolved_SR_Mor17"){
      OR_dRcut = 0.0;
      this->CountEventRes(icut_res, weight);
      if( MET < 60.) return;
      this->CountEventRes(icut_res, weight);
      if( nWtags > 0 ) return;
      this->CountEventRes(icut_res, weight);
      if( nJets < 4) return;
      this->CountEventRes(icut_res, weight);
      if( bJets.size() < 1) return;
      this->CountEventRes(icut_res, weight);
      ST_res = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( ST_res < 1800) return;
      this->CountEventRes(icut_res, weight);
      if( dR_lepnu > 0.7) return;
      this->CountEventRes(icut_res, weight);
    }else if(m_resolvedselection == "resolved_CR_Mor17"){
      OR_dRcut = 0.0;
      this->CountEventRes(icut_res, weight);
      if( MET < 60.) return;
      this->CountEventRes(icut_res, weight);
      if( nWtags > 0 ) return;
      this->CountEventRes(icut_res, weight);
      if( nJets < 4) return;
      this->CountEventRes(icut_res, weight);
      if( bJets.size() < 1) return;
      this->CountEventRes(icut_res, weight);
      ST_res = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( ST_res < 1000 || ST_res > 1800) return;
      this->CountEventRes(icut, weight);
      if( dR_lepnu > .7) return;
      this->CountEventRes(icut, weight);
    }else if(m_resolvedselection == "resolved_VR_Mor17"){
      OR_dRcut = 0.0;
      this->CountEventRes(icut_res, weight);
      if( MET < 60.) return;
      this->CountEventRes(icut_res, weight);
      if( nWtags > 0 ) return;
      this->CountEventRes(icut_res, weight);
      if( nJets < 4) return;
      this->CountEventRes(icut_res, weight);
      if( bJets.size() < 1) return;
      this->CountEventRes(icut_res, weight);
      ST_res = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( ST_res < 1200) return;
      this->CountEventRes(icut, weight);
      if( dR_lepnu < .7) return;
      this->CountEventRes(icut, weight);
    }else if(m_resolvedselection == "custom_res_Mor17"){
      OR_dRcut = 0.0;
      this->CountEventRes(icut_res, weight);
      if( MET < 60.) return;
      this->CountEventRes(icut_res, weight);
      if( nWtags > 0 ) return;
      this->CountEventRes(icut_res, weight);
      if( nJets < 4) return;
      this->CountEventRes(icut_res, weight);
      if( bJets.size() < 1) return;
      this->CountEventRes(icut_res, weight);
      ST_res = m_vlqutils.GetSTSmallJets(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.jet());
      if( ST_res < 500) return;
      this->CountEventRes(icut, weight);
    }else{
      //      std::cerr <<"ERROR :: No selection for boosted case selected ... " << std::endl;
      std::cout <<"INFO :: No selection for resolved case selected ... " << std::endl;
      return;
      //      exit (EXIT_FAILURE);
    }
    // (SH) currently taking all jets vetoing the two leading pT b-jets and applying dR(j,j) < 1.2
    resolvedHadWcand = m_vlqutils.resolvedWcandidate(evt.jet(), jets, bJets, "TOP2016");
    if(resolvedHadWcand.M() <= 0.)
      return;

    // (SH)
    // Apply Overlap Removal between the selected resolved hadronic W candidate and all b-jets
    // Comment: In addition, for the reconstruction of the TT system, the jets in the == 1b-jet category are removed
    /* MIST --> Matthias ;-)
    if(bJets.size() > 0) {
      for ( size_t bidx = 0; bidx < bJets.size(); ++bidx){
        if(resolvedHadWcand.DeltaR(bJets.at(bidx)) < OR_dRcut){
          this->CountEvent(icut, weight);
          return;
        }
        h->FillFlowHandler(h->h1D("DeltaR_Whadbjet_res", "", suffix),  resolvedHadWcand.DeltaR(bJets.at(bidx)), weight );
        h->h2D("bjets_eta_phi_res", "", suffix)->Fill(bJets.at(bidx).Eta(), bJets.at(bidx).Phi(), weight);
      }
      h->FillFlowHandler(h->h1D("DeltaR_Whadbjetlead_res", "", suffix),  resolvedHadWcand.DeltaR(bJets.at(0)), weight );
    }
    if(bJets.size() > 1) h->FillFlowHandler(h->h1D("DeltaR_Whadbjetsublead_res", "", suffix), resolvedHadWcand.DeltaR(bJets.at(1)), weight );
    */

    h->FillFlowHandler(h->h1D("ST_res", "", suffix), ST_res, weight);
    h->FillFlowHandler(h->h1D("MET_res", "", suffix), MET, weight);
    h->FillFlowHandler(h->h1D("MWT_res", "", suffix), MWT, weight);
    h->h2D("MET_MWT_res", "", suffix)->Fill(MWT, MET, weight);
    h->h2D("dR_ST_res", "", suffix)->Fill(dR_lepnu, ST_res, weight);
    h->h2D("nJet_dR_res", "", suffix)->Fill(nJets,dR_lepnu, weight);
    h->h2D("nJet_ST_res", "", suffix)->Fill(nJets, ST, weight);
    h->h2D("nBtagJets_dR_res", "", suffix)->Fill(bJets.size(), dR_lepnu, weight);
    h->h2D("nBtagJets_ST_res", "", suffix)->Fill(bJets.size(), ST, weight);

    h->FillFlowHandler(h->h1D("DeltaR_lepnu_res", "", suffix), dR_lepnu, weight );
    h->FillFlowHandler(h->h1D("nBtagJets_res", "", suffix), bJets.size(), weight);
    h->FillFlowHandler(h->h1D("nJets_res", "", suffix), nJets, weight);
    h->FillFlowHandler(h->h1D("nResolvedW_res", "", suffix), nWtags, weight);

    h->FillFlowHandler(h->h1D("resolvedWhadCand_pt", "", suffix), resolvedHadWcand.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("resolvedWhadCand_m", "", suffix), resolvedHadWcand.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("resolvedWhadCand_eta", "", suffix), resolvedHadWcand.Eta(), weight);
    h->FillFlowHandler(h->h1D("resolvedWhadCand_phi", "", suffix), resolvedHadWcand.Phi(), weight);




    const TLorentzVector lepW = l+nu;
    std::map<std::string, TLorentzVector> TTcandidates = m_vlqutils.BoostedTTcandidates(resolvedHadWcand, lepW, jets, bJets, true, TTrecoAlg, OR_dRcut);

    TLorentzVector hadT, lepT, hadb, lepb;
    for(auto ele: TTcandidates) {
      if(ele.first == "hadT"){

    	h->FillFlowHandler(h->h1D("hadronicT_pt", "", suffix), ele.second.Perp()*1e-3, weight);
    	h->FillFlowHandler(h->h1D("hadronicT_m", "", suffix), ele.second.M()*1e-3, weight);
    	h->FillFlowHandler(h->h1D("hadronicT_eta", "", suffix), ele.second.Eta(), weight);
    	h->FillFlowHandler(h->h1D("hadronicT_phi", "", suffix), ele.second.Phi(), weight);
	hadT = ele.second;
      }
       if(ele.first == "lepT"){

    	 h->FillFlowHandler(h->h1D("leptonicT_pt", "", suffix), ele.second.Perp()*1e-3, weight);
    	 h->FillFlowHandler(h->h1D("leptonicT_m", "", suffix), ele.second.M()*1e-3, weight);
	 //	  h->FillFlowHandler(h->h1D("leptonicT_m_boostedSR", "", suffix), ele.second.M()*1e-3, weight);
	 //       h->FillFlowHandler(h->h1D("leptonicT_m_boostedCR", "", suffix), ele.second.M()*1e-3, weight);
	 h->FillFlowHandler(h->h1D("leptonicT_m_resolvedSR", "", suffix), ele.second.M()*1e-3, weight);
    	 h->FillFlowHandler(h->h1D("leptonicT_eta", "", suffix), ele.second.Eta(), weight);
    	 h->FillFlowHandler(h->h1D("leptonicT_phi", "", suffix), ele.second.Phi(), weight);
	 lepT = ele.second;
       }
       if(ele.first == "hadb"){
	 hadb = ele.second;
       }
       if(ele.first == "lepb"){
	 lepb = ele.second;
       }

    }

    if(bJets.size() == 1){
      if(bJets.at(0) == hadb){
        h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidate_res", "", suffix),  boostedHadWcand.DeltaR(hadb), weight );
        h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidateNotb_res", "", suffix),  boostedHadWcand.DeltaR(lepb), weight );
      }      else{
        h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidate_res", "", suffix),  boostedHadWcand.DeltaR(lepb), weight );
        h->FillFlowHandler(h->h1D("DeltaR_WhadbjetCandidateNotb_res", "", suffix),  boostedHadWcand.DeltaR(hadb), weight );

      }
    }


    //correlation plots
    h->FillFlowHandler2D(    h->h2D("ST_lepM", "", suffix), ST_res, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("dRlepnu_lepM", "", suffix), dR_lepnu, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("leadJetPt_lepM", "", suffix), j.Perp()*1e-3, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("MET_lepM", "", suffix), MET, lepT.M()*1e-3, weight);
    h->FillFlowHandler2D(    h->h2D("hadM_lepM", "", suffix), hadT.M()*1e-3, lepT.M()*1e-3, weight);


    h->FillFlowHandler(h->h1D("DeltaM_hadtopleptop", "", suffix), fabs( hadT.M()*1e-3 - lepT.M()*1e-3 ), weight);
    h->FillFlowHandler(h->h1D("avgMassT_res", "", suffix), fabs( (hadT.M()*1e-3 + lepT.M()*1e-3)/2 ), weight);

    h->FillFlowHandler(h->h1D("nleptonicW", "", suffix), 1, weight);
    h->FillFlowHandler(h->h1D("leptonicW_pt", "", suffix), lepW.Perp()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicW_m", "", suffix), lepW.M()*1e-3, weight);
    h->FillFlowHandler(h->h1D("leptonicW_eta", "", suffix), lepW.Eta(), weight);
    h->FillFlowHandler(h->h1D("leptonicW_phi", "", suffix), lepW.Phi(), weight);
    //Resolved Event Selection (loose)
    // if( nJets < 4) return;
    // if( bJets.size() < 1) return;
    //if( MET < 20.) return;
    //if( MET+MWT < 60.) return;
    // inputs
    // LEPTON --> TLorentzVector for your lepton
    // vjets -->  std::vector<TLorentzVector*> for the jets
    // vjets_btagged --> std::vector<bool> to say if the jets are btagged or not
    // met --> TLorentzVector for your MET

    // outputs, they will be filled by the TTBarLeptonJetsBuilder_chi2
    // int  igj3, igj4; // index for the Whad
    // int igb3, igb4; // index for the b's
    // int  ign1;  // index for the neutrino (because chi2 can test both pz solution)
    // double chi2ming1, chi2ming1H, chi2ming1L;

    // std::vector<TLorentzVector *> vjets;
    // std::vector<bool> vjets_btagged;
    // for (size_t z = 0; z < evt.jet().size(); ++z) {
    //   vjets.push_back(new TLorentzVector(0,0,0,0));
    //   vjets[z]->SetPtEtaPhiE(evt.jet()[z].mom().Perp(), evt.jet()[z].mom().Eta(), evt.jet()[z].mom().Phi(), evt.jet()[z].mom().E());
    //   // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTagingxAODEDM
    //   // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarks
    //   vjets_btagged.push_back(evt.jet()[z].btag_mv2c20_60());
    // }
    // TLorentzVector met = evt.met();
    // bool status = m_chi2.findMinChiSquare(&l, &vjets, &vjets_btagged, &met, igj3, igj4, igb3, igb4, ign1, chi2ming1, chi2ming1H, chi2ming1L);

    // float chi2Value = 1000000; // log10(1000000) = 6
    // float mwh = -1;
    // float mtl = -1;
    // float mth = -1;

    // if (status){
    //     chi2Value = chi2ming1;
    // 	mwh = m_chi2.getResult_Mwh();
    // 	mtl = m_chi2.getResult_Mtl();
    // 	mth = m_chi2.getResult_Mth();
    // 	mtt = m_chi2.getResult_Mtt();
    // }

    // for (size_t z = 0; z < vjets.size(); ++z) {
    //   delete vjets[z];
    // }
    // vjets.clear();
    // vjets_btagged.clear();

    // //Fill histograms
    // h->FillFlowHandler(h->h1D("mtt", "", suffix), mtt*1e-3, weight);
    // h->FillFlowHandler(h->h1D("mtlep_res", "", suffix), mtl*1e-3, weight);
    // h->FillFlowHandler(h->h1D("mthad_res", "", suffix), mth*1e-3, weight);
    // h->FillFlowHandler(h->h1D("mwhad_res", "", suffix), mwh*1e-3, weight);
    // h->FillFlowHandler(h->h1D("chi2", "", suffix), log10(chi2Value), weight);
    if (m_ntuple!=0){

      _tree_WbWb = 0;
      _tree_ZtZt = 0;
      _tree_HtHt = 0;
      _tree_WbZt = 0;
      _tree_WbHt = 0;
      _tree_ZtHt = 0;
      int this_vlq_evtype= evt.vlq_evtype();
      if (this_vlq_evtype==0) _tree_WbWb = 1;
      else if (this_vlq_evtype==1) _tree_ZtZt = 1;
      else if (this_vlq_evtype==2) _tree_HtHt = 1;
      else if (this_vlq_evtype==3) _tree_WbZt = 1;
      else if (this_vlq_evtype==4) _tree_WbHt = 1;
      else if (this_vlq_evtype==5) _tree_ZtHt = 1;

      _tree_lep_mass = lepT.M()*1e-3;
      _tree_had_mass = hadT.M()*1e-3;
      _tree_lb_mass = -1;
      _tree_lb_minmass = -1;
      _tree_lblep_mass = -1;
      _tree_Delta_mass = abs(_tree_lep_mass - _tree_had_mass);
      _tree_ST = ST;
      _tree_weight = weight;
      _tree_syst = s;
      _tree_cat = -1;
      if (!m_boosted) _tree_cat = 2;
      h->m_tree->Fill();
    }
  }
 }

void AnaHQTVLQWtX::CountEvent(unsigned int& icut, double weight){
  std::string suffix = "";
  HistogramService *h = &m_hSvc;
  h->h1D("cutflow_boo", "", suffix)->Fill(icut, weight);
  ++icut;
}

void AnaHQTVLQWtX::CountEventRes(unsigned int& icut, double weight){
  std::string suffix = "";
  HistogramService *h = &m_hSvc;
  h->h1D("cutflow_res", "", suffix)->Fill(icut, weight);
  ++icut;
}
