/**
 * @brief Reconstructed objects from the input file.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#include "TopNtupleAnalysis/MObject.h"
#include "TopNtupleAnalysis/RecoObject.h"
#include <cmath>
#include "TLorentzVector.h"

RecoObject::RecoObject()
  : MObject(){
  m_type = MObject::recoobject;
}

RecoObject::RecoObject(const TLorentzVector &v)
  : MObject(v, MObject::recoobject){
}

RecoObject::RecoObject(const RecoObject &l)
  : MObject(l.mom(), MObject::recoobject) {
  m_good = l.m_good;
  m_subs = l.m_subs;
}

RecoObject::~RecoObject() {
}

void RecoObject::setGood(bool b) {
  m_good = b;
}

bool RecoObject::good() const {
  return m_good;
}

bool &RecoObject::good() {
  return m_good;
}

const double RecoObject::mv2c20() const {
  return m_mv2c20;
}
double &RecoObject::mv2c20() {
  return m_mv2c20;
}


bool RecoObject::pass() const {
  if (std::fabs(mom().Eta()) > 1.2) return false;
  if (mom().Perp() < 200e3) return false;
  return true;
}

bool RecoObject::passLoose() const {
  if (std::fabs(mom().Eta()) > 2.0) return false;
  if (mom().Perp() < 200e3) return false;
  return true;
}

bool RecoObject::passFakeSelection(const TLorentzVector &lept, const TLorentzVector &selJet) const {
  if (std::fabs(mom().Eta()) > 2.0) 			return false;
  if (mom().Perp() < 200e3) 				return false;
  if (mom().M() > 70e3) 				return false;  
  if(std::fabs(mom().DeltaPhi(lept)) < 2.3)		return false;  
  if(mom().DeltaR(selJet) < 1.5)			return false;
        
  return true;
}

bool RecoObject::passWtagLoose() const {
  if (subs("isWmed")  < 1) return false;
  //  if (subs("isSmoothTop50") != 0 ) return false;
        
  return true;
}

bool RecoObject::passWtagTight(const TLorentzVector &lept, const TLorentzVector &selJet) const {
  if (std::fabs(mom().Eta()) > 2.0) 			return false;
  if (mom().Perp() < 200e3) 				return false;
  if (mom().M() > 70e3) 				return false;  
  if(std::fabs(mom().DeltaPhi(lept)) < 2.3)		return false;  
  if(mom().DeltaR(selJet) < 1.5)			return false;
        
  return true;
}

// float &RecoObject::subs(const std::string &s) {
//   return m_subs[s];
// }

// const float RecoObject::subs(const std::string &s) const {
//   return m_subs.at(s);
// }

double &RecoObject::subs(const std::string &s) {
  return m_subs[s];
}

const double RecoObject::subs(const std::string &s) const {
  return m_subs.at(s);
}

