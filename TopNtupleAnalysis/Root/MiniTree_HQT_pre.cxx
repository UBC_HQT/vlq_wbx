/**
 * @brief Class that reads information from the input file and puts it in a object-oriented format in Event.
 * @author Danilo Enoque Ferreira de Lima <dferreir@cern.ch>
 */
#include "TopNtupleAnalysis/MiniTree_HQT_pre.h"
#include "TopNtupleAnalysis/Event.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include <vector>
#include <string>
#include <iostream>
#include "TChain.h"
#include "TBranch.h"
#include "TLeaf.h"
#include <algorithm>

#include "TTree.h"

MiniTree_HQT_pre::MiniTree_HQT_pre(bool toWrite, const std::string &file, const std::string &name)
  : m_file(0), m_chain(0), m_name(name) {
  //m_file = TFile::Open(file.c_str());
  m_chain = new TChain(name.c_str());
  ((TChain *) m_chain)->Add(file.c_str());
  m_sumWeights = 0;

  prepareBranches();
}

MiniTree_HQT_pre::~MiniTree_HQT_pre() {
  if (m_chain) delete m_chain;
  if (m_file) delete m_file;
}

void MiniTree_HQT_pre::read(int event, Event &e) {
  e.clear();
  m_chain->GetEntry(event);
  e.channelNumber() = ui("mcChannelNumber");
  e.eventNumber() = ul64("eventNumber");
  e.runNumber() = ui("runNumber");
  e.lumiBlock() = ui("lumiblock");

  e.mu() = d("mu");
  e.mu_original() = d("mu_original");
  e.weight_mc() = d("weight_mc");
  e.weight_pileup() =  d("weight_pileup");

  e.isBoosted() = i("isBoosted");
  e.isResolved() = i("isResolved");

  e.AMI() = d("AMI");
  e.XSection() = d("XSection");
  e.KFactor() = d("KFactor");
  e.FilterEff() = d("FilterEff");
  e.LUMI() = d("LUMI");
  e.TRFWeight() = d("TRFWeight");  

  e.HT() = d("HT");
  e.DeltaM_hadtopleptop() = d("DeltaM_hadtopleptop");
  e.DeltaR_bjet1bjet2() = d("DeltaR_bjet1bjet2");
  e.DeltaR_lepnu() = d("DeltaR_lepnu");
  e.mass_lepb() = d("mass_lb");
  e.A_distance() = d("A_distance");
  e.A_dr() = d("A_dr");
  e.A_mass() = d("A_mass");
  
  e.minDeltaR_hadWbjet() = d("minDeltaR_hadWbjet");
  e.minDeltaR_lepbjet() = d("minDeltaR_lepbjet");

  
  e.btagsN() = i("btags_n");
  

  //@todo: read-in the truth information here, maybe add truth identifier (SH)
  e.MC_t_q().SetPtEtaPhiE(d("truth_t_pt"), d("truth_t_eta"), d("truth_t_phi"), d("truth_t_e"));
  e.MC_tbar_q().SetPtEtaPhiE(d("truth_tbar_pt"), d("truth_tbar_eta"), d("truth_tbar_phi"), d("truth_tbar_e"));

  e.MC_Wminus().SetPtEtaPhiE(d("truth_Wminus_pt"), d("truth_Wminus_eta"), d("truth_Wminus_phi"), d("truth_Wminus_e"));
  e.MC_Wplus().SetPtEtaPhiE(d("truth_Wplus_pt"), d("truth_Wplus_eta"), d("truth_Wplus_phi"), d("truth_Wplus_e"));

  e.MC_b_q().SetPtEtaPhiE(d("truth_b_pt"), d("truth_b_eta"), d("truth_b_phi"), d("truth_b_e"));
  e.MC_bbar_q().SetPtEtaPhiE(d("truth_bbar_pt"), d("truth_bbar_eta"), d("truth_bbar_phi"), d("truth_bbar_e"));

  e.MC_lep().SetPtEtaPhiE(d("truth_lep_pt"), d("truth_lep_eta"), d("truth_lep_phi"), d("truth_lep_e"));
  e.MC_lep_charge() 	= i("truth_lep_charge");
  e.MC_lep_pdgId() 	= i("truth_lep_pdgId");

  e.MC_nu().SetPtEtaPhiE(d("truth_nu_pt"), d("truth_nu_eta"), d("truth_nu_phi"), d("truth_nu_e"));
  e.MC_nu_pdgId() 	= i("truth_nu_pdgId");

  e.MC_q1().SetPtEtaPhiE(d("truth_q1_pt"), d("truth_q1_eta"), d("truth_q1_phi"), d("truth_q1_e"));
  e.MC_q1_pdgId() 	= i("truth_q1_pdgId");

  e.MC_q2().SetPtEtaPhiE(d("truth_q2_pt"), d("truth_q2_eta"), d("truth_q2_phi"), d("truth_q2_e"));
  e.MC_q2_pdgId() 	= i("truth_q2_pdgId");
  
  // >>> get lepton information
  //
  
  e.lepton().push_back(Lepton());
  e.lepton()[0].mom().SetPtEtaPhiE(d("lep_pt"), d("lep_eta"), d("lep_phi"), d("lep_e"));
  e.lepton()[0].charge() = i("lep_charge");
  e.lepton()[0].setTight(i("lep_isTight"));
    
  // >>> get jet information
  //
   for (int k = 0; k < vd("jet_pt")->size(); ++k) {
    e.jet().push_back(Jet());
    e.jet()[k].mom().SetPtEtaPhiE(vd("jet_pt")->at(k), vd("jet_eta")->at(k), vd("jet_phi")->at(k), vd("jet_e")->at(k));
    e.jet()[k].trueFlavour() = vi("jet_true_flavor")==0?-99:vi("jet_true_flavor")->at(k);
    e.jet()[k].mv2c20() = vd("jet_mv2c20")==0?-99:vd("jet_mv2c20")->at(k);
    e.jet()[k].jvt() = vd("jet_jvt")==0?-99:vd("jet_jvt")->at(k);
  }
   // >>> get bjet information
   //
 //   for (int k = 0; k < vd("bjet_pt")->size(); ++k) {
     //     e.bjet().push_back(Jet());
     //     e.bjet()[k].mom().SetPtEtaPhiE(vd("bjet_pt")->at(k), vd("bjet_eta")->at(k), vd("bjet_phi")->at(k), vd("bjet_e")->at(k));
     // e.bjet()[k].trueFlavour() = vi("bjet_true_flavor")==0?-99:vi("bjet_true_flavor")->at(k);
     //     e.bjet()[k].mv2c20() = vd("bjet_mv2c20")==0?-99:vd("bjet_mv2c20")->at(k);
     //     e.bjet()[k].jvt() = vd("bjet_jvt")==0?-99:vd("bjet_jvt")->at(k);
  //   }
   
   e.bhad().push_back(Jet());
   e.bhad()[0].mom().SetPtEtaPhiE(d("bhad_pt"), d("bhad_eta"), d("bhad_phi"), d("bhad_e"));
   e.bhad()[0].trueFlavour() = d("bhad_true_flavor")==0?-99:d("bhad_true_flavor");
   e.bhad()[0].mv2c20() = d("bhad_mv2c20")==0?-99:d("bhad_mv2c20");
   e.bhad()[0].jvt() = d("bhad_jvt")==0?-99:d("bhad_jvt");

   
   e.blep().push_back(Jet());
   e.blep()[0].mom().SetPtEtaPhiE(d("blep_pt"), d("blep_eta"), d("blep_phi"), d("blep_e"));
   e.blep()[0].trueFlavour() = d("blep_true_flavor")==0?-99:d("blep_true_flavor");
   e.blep()[0].mv2c20() = d("blep_mv2c20")==0?-99:d("blep_mv2c20");
   e.blep()[0].jvt() = d("blep_jvt")==0?-99:d("blep_jvt");
 

   
   // >>> get track jet information
   //
   for (int k = 0; k < vd("tjet_pt")->size(); ++k) {
     e.tjet().push_back(Jet());
     e.tjet()[k].mom().SetPtEtaPhiE(vd("tjet_pt")->at(k), vd("tjet_eta")->at(k), vd("tjet_phi")->at(k), vd("tjet_e")->at(k));
     e.tjet()[k].trueFlavour() = vi("tjet_true_flavor")==0?-99:vi("tjet_true_flavor")->at(k);
     e.tjet()[k].mv2c20() = vd("tjet_mv2c20")==0?-99:vd("tjet_mv2c20")->at(k);
     e.tjet()[k].numConstituents() = vi("tjet_numConstituents")->at(k);
   }
   
   // >>> get reconstructed object
   //
  for (int k = 0; k < vd("resolvedW_pt")->size(); ++k) {
    e.resolvedW().push_back(RecoObject());
    e.resolvedW()[k].mom().SetPtEtaPhiE(vd("resolvedW_pt")->at(k), vd("resolvedW_eta")->at(k), vd("resolvedW_phi")->at(k), vd("resolvedW_e")->at(k));
    e.resolvedW()[k].subs("mass") = vd("resolvedW_m")->at(k);
  }
  for (int k = 0; k < vd("boostedW_pt")->size(); ++k) {
    e.boostedW().push_back(RecoObject());
    e.boostedW()[k].mom().SetPtEtaPhiE(vd("boostedW_pt")->at(k), vd("boostedW_eta")->at(k), vd("boostedW_phi")->at(k), vd("boostedW_e")->at(k));
    e.boostedW()[k].setGood((vd("boostedW_isGood")->at(k) == 1.)?true:false);
    e.boostedW()[k].subs("D2") = vd("boostedW_D2")->at(k);
    e.boostedW()[k].subs("LHtop") = vd("boostedW_LHtop")->at(k);
    e.boostedW()[k].subs("LHw") = vd("boostedW_LHw")->at(k);
    if(e.boostedW()[k].subs("tau32_wta"))e.boostedW()[k].subs("tau32_wta") = vd("boostedW_tau32_wta")->at(k);
    e.boostedW()[k].subs("isSmoothTop50") = vi("boostedW_isSmoothTop50")->at(k);
    e.boostedW()[k].subs("isSmoothTop80") = vi("boostedW_isSmoothTop80")->at(k);
    e.boostedW()[k].subs("isWmed") = vi("boostedW_isWmed")->at(k);
    e.boostedW()[k].subs("isWtight") = vi("boostedW_isWtight")->at(k);    
  }

for (int k = 0; k < vd("reclusteredW_pt")->size(); ++k) {
    e.reclusteredW().push_back(RecoObject());
    e.reclusteredW()[k].mom().SetPtEtaPhiE(vd("reclusteredW_pt")->at(k), vd("reclusteredW_eta")->at(k), vd("reclusteredW_phi")->at(k), vd("reclusteredW_e")->at(k));
    e.reclusteredW()[k].subs("mass") = vd("reclusteredW_m")->at(k);
    e.reclusteredW()[k].subs("rctau") = vd("reclusteredW_rctau")->at(k);
    e.reclusteredW()[k].subs("nsub") = vi("reclusteredW_nsub")->at(k);
  }

for (int k = 0; k < vvd("reclusteredWsub_pt")->size(); ++k) {
     e.reclusteredWsub().push_back(RecoObject());
     e.reclusteredWsub()[k].mom().SetPtEtaPhiE(vd("reclusteredWsub_pt")->at(k), vd("reclusteredWsub_eta")->at(k), vd("reclusteredWsub_phi")->at(k), vd("reclusteredWsub_e")->at(k));
     e.reclusteredWsub()[k].subs("mass") = vd("reclusteredWsub_m")->at(k);
     e.reclusteredWsub()[k].mv2c20() = vd("reclusteredWsub_mv2c20")==0?-99:vd("reclusteredWsub_mv2c20")->at(k);    
  }
 

 e.hadronicT().push_back(RecoObject());
 e.hadronicT()[0].mom().SetPtEtaPhiE(d("hadronicT_pt"), d("hadronicT_eta"), d("hadronicT_phi"), d("hadronicT_e"));
 e.leptonicT().push_back(RecoObject());
 e.leptonicT()[0].mom().SetPtEtaPhiE(d("leptonicT_pt"), d("leptonicT_eta"), d("leptonicT_phi"), d("leptonicT_e"));
 e.hadronicW().push_back(RecoObject());
 e.hadronicW()[0].mom().SetPtEtaPhiE(d("hadronicW_pt"), d("hadronicW_eta"), d("hadronicW_phi"), d("hadronicW_e"));
 e.leptonicW().push_back(RecoObject());
 e.leptonicW()[0].mom().SetPtEtaPhiE(d("leptonicW_pt"), d("leptonicW_eta"), d("leptonicW_phi"), d("leptonicW_e"));
 e.nu().push_back(RecoObject());
 e.nu()[0].mom().SetPtEtaPhiE(d("nu_pt"), d("nu_eta"), d("nu_phi"), d("nu_e"));

 
 e.met(d("met_met")*std::cos(d("met_phi")), d("met_met")*std::sin(d("met_phi")));
 e.mtw() = d("mtw");
 e.vlq_evtype() = i("vlq_evtype");
 
 e.passes().clear();
 
 //if (i("isBoosted")) e.passes().push_back("isBoosted"); // MD -> is not really meaningful in tree according to Dan
 //if (i("isResolved")) e.passes().push_back("isResolved");
 //if (i("bejets")) e.passes().push_back("bejets");
 //if (i("bmujets")) e.passes().push_back("bmujets");
 //if (i("bmu2jets")) e.passes().push_back("bmu2jets");
 //if (i("rejets")) e.passes().push_back("rejets");
 //if (i("rmujets")) e.passes().push_back("rmujets");
 //if (i("rmu2jets")) e.passes().push_back("rmu2jets");
 if (i("ejets")) e.passes().push_back("ejets");
 if (i("mujets")) e.passes().push_back("mujets");
 //if (i("ee")) e.passes().push_back("ee");
 //if (i("emu")) e.passes().push_back("emu");
 //if (i("mumu")) e.passes().push_back("mumu");
 
}

ULong64_t          &MiniTree_HQT_pre::ul64(const std::string &n) {
  return m_ul64[n];
}
unsigned int         &MiniTree_HQT_pre::ui(const std::string &n) {
  return m_ui[n];
}
int                  &MiniTree_HQT_pre::i(const std::string &n) {
  return m_i[n];
}
float                &MiniTree_HQT_pre::f(const std::string &n) {
  return m_f[n];
}
double                &MiniTree_HQT_pre::d(const std::string &n) {
  return m_d[n];
}
char                 &MiniTree_HQT_pre::c(const std::string &n) {
  return m_c[n];
}

std::vector<std::vector<float> > *MiniTree_HQT_pre::vvf(const std::string &n) {
  if (m_vvf.find(n) == m_vvf.end()) return 0;
  return m_vvf[n];
}
std::vector<std::vector<double> > *MiniTree_HQT_pre::vvd(const std::string &n) {
  if (m_vvd.find(n) == m_vvd.end()) return 0;
  return m_vvd[n];
}
std::vector<std::vector<int> > *MiniTree_HQT_pre::vvi(const std::string &n) {
  if (m_vvi.find(n) == m_vvi.end()) return 0;
  return m_vvi[n];
}
std::vector<int>     *MiniTree_HQT_pre::vi(const std::string &n) {
  if (m_vi.find(n) == m_vi.end()) return 0;
  return m_vi[n];
}
std::vector<char>    *MiniTree_HQT_pre::vc(const std::string &n) {
  if (m_vc.find(n) == m_vc.end()) return 0;
  return m_vc[n];
}
std::vector<float>   *MiniTree_HQT_pre::vf(const std::string &n) {
  if (m_vf.find(n) == m_vf.end()) return 0;
  return m_vf[n];
}

std::vector<double>   *MiniTree_HQT_pre::vd(const std::string &n) {
  if (m_vd.find(n) == m_vd.end()) return 0;
  return m_vd[n];
}

double &MiniTree_HQT_pre::sumWeights() {
  return m_sumWeights;
}

void MiniTree_HQT_pre::addFileToRead(const std::string &fname) {
  ((TChain *) m_chain)->Add(fname.c_str());
}

void MiniTree_HQT_pre::addFileToRead(const std::string &fname, const std::string &treeName) {
  ((TChain *) m_chain)->AddFile(fname.c_str(), -1, treeName.c_str());
}

double MiniTree_HQT_pre::getSumWeights() {
  return m_sumWeights;
}

int MiniTree_HQT_pre::GetEntries() {
  return m_chain->GetEntries();
}

void MiniTree_HQT_pre::prepareBranches() {
  TObjArray *l = m_chain->GetListOfBranches();
  for (size_t z = 0; z < l->GetEntries(); ++z) {
    std::string name = l->At(z)->GetName();
    std::string type = ((TBranch *) l->At(z))->GetLeaf(name.c_str())->GetTypeName();
    //std::cout<< name << "\t" << type << std::endl;
    if (type == "Float_t" || type == "float") {
      m_f[name] = -50000;
    } else if (type == "Double_t" || type == "double") {
      m_d[name] = -50000;
    } else if (type == "Int_t" || type == "int") {
      m_i[name] = -50000;
    } else if (type == "ULong64_t") {
      m_ul64[name] = 0;
    } else if (type == "UInt_t" || type == "unsigned int") {
      m_ui[name] = 0;
    } else if (type == "char" || type == "Char_t") {
      m_c[name] = 0;
    } else if (type == "vector<char>" || type == "std::vector<char>" || type == "vector<Char_t>" || type == "std::vector<Char_t>") {
      m_vc[name] = 0;
    } else if (type == "vector<float>" || type == "std::vector<float>" || type == "vector<Float_t>" || type == "std::vector<Float_t>") {
      m_vf[name] = 0;
    } else if (type == "vector<double>" || type == "std::vector<double>" || type == "vector<Double_t>" || type == "std::vector<Double_t>") {
      m_vd[name] = 0;
    } else if (type == "vector<int>" || type == "std::vector<int>" || type == "vector<Int_t>" || type == "std::vector<Int_t>") {
      m_vi[name] = 0;
    } else if (type == "vector<vector<int> >" || type == "std::vector<std::vector<int> >" || type == "vector<vector<Int_t> >" || type == "std::vector<std::vector<Int_t> >") {
      m_vvi[name] = 0;
    } else if (type == "vector<vector<double> >" || type == "std::vector<std::vector<double> >" || type == "vector<vector<Double_t> >" || type == "std::vector<std::vector<Double_t> >") {
      m_vvd[name] = 0;
    } else if (type == "vector<vector<float> >" || type == "std::vector<std::vector<float> >" || type == "vector<vector<Float_t> >" || type == "std::vector<std::vector<Float_t> >") {
      m_vvf[name] = 0;
    } else {
      std::cout << "ERROR: I could not figure out the type of this branch! Name = " << name << ", type = " << type << std::endl;
    }
  }

  for (auto& it : m_f) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtFloat;
  }
  for (auto& it : m_d) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtDouble;
  }
  for (auto& it : m_ui) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtUint;
  }
  for (auto& it : m_ul64) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtULong64;
  }
  for (auto& it : m_i) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtInt;
  }
  for (auto& it : m_c) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtChar;
  }
  for (auto& it : m_vc) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVChar;
  }
  for (auto& it : m_vf) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVFloat;
  }
  for (auto& it : m_vd) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVDouble;
  }
  for (auto& it : m_vi) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVInt;
  }
  for (auto& it : m_vvi) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVVInt;
  }
  for (auto& it : m_vvf) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVVFloat;
  }
  for (auto& it : m_vvd) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVVDouble;
  }
}

