/**
 * @brief Analysis to provide Control Plots.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#include "TopNtupleAnalysis/Analysis_HQT.h"
#include "TopNtupleAnalysis/ControlPlots.h"
#include "TopNtupleAnalysis/Event.h"
#include "TLorentzVector.h"
#include <vector>
#include <string>
#include "TopNtupleAnalysis/HistogramService.h"

ControlPlots::ControlPlots(const std::string &filename, bool electron, bool combined, std::vector<std::string> &systList)
  : Analysis_HQT(filename, systList, 0), m_electron(electron), m_combined(combined),
    m_neutrinoBuilder("MeV"), m_chi2("MeV"), m_vlqutils() {

  m_chi2.Init(TtresChi2::DATA2015_MC15);

  //  m_hSvc.m_tree->Branch("mtt",    &_tree_mtt);

  double varBin1[] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 220, 240, 260, 280, 300, 340, 380, 450, 500};
  int varBinN1 = sizeof(varBin1)/sizeof(double) - 1;
  double varBin2[] = {300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500, 540, 580, 620, 660, 700, 800, 1e3, 1.2e3, 1.5e3};
  int varBinN2 = sizeof(varBin2)/sizeof(double) - 1;
  double varBin3[] = {0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 340, 380, 420, 460, 500};
  int varBinN3 = sizeof(varBin3)/sizeof(double) - 1;
  double varBin4[] = {80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 340, 380, 420, 460, 500};
  int varBinN4 = sizeof(varBin4)/sizeof(double) - 1;
  double varBin05[] = {200, 600, 1000, 1400, 1800, 2200, 2600, 3000};
  int varBinN05 = sizeof(varBin05)/sizeof(double) - 1;

  double varBin5[] = {0, 80, 160, 240, 320, 400, 480, 560, 640,720,800,920,1040,1160,1280,1400,1550,1700,2000,2300,2600,2900,3200,3600,4100,4600,5100,6000};
  int varBinN5 = sizeof(varBin5)/sizeof(double) - 1;

  //Plots for QCD validation
  if (m_electron) {
        double varBin6[8] = {30, 35, 40, 50, 60, 120, 400, 700};
	double varBin7[7]  = {0., 0.4, 0.6, 1.0, 1.5, 2.5, 7.0};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);

     }//m_electron

  else{
        double varBin6[9] = {25, 30, 35, 40, 50, 70, 100, 400, 700};
	double varBin7[7] = {0., 0.4, 0.6, 1.0, 1.5, 2.5, 7.0};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);

  }//m_electron

  m_hSvc.create1D("cutflow", "; cutflow ; Events", 4, -0.5, 3.5);
  m_hSvc.create1D("yields", "; One ; Events", 1, 0.5, 1.5);
  m_hSvc.create1D("lepPt",    "; Pt of lept [GeV]; Events", 100, 25, 525);
  m_hSvc.create1D("lepEta", "; lepton #eta ; Events", 20, -2.5, 2.5);
  m_hSvc.create1D("lepPhi", "; lepton #phi [rd] ; Events", 32, -3.2, 3.2);
  m_hSvc.create1DVar("leadJetPt", "; leading Jet p_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1DVar("JetPt", "; Jet transverse momentum [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("nJets", "; number of jets ; Events", 15, -0.5, 14.5);

  m_hSvc.create1DVar("leadbJetPt", "; leading b-jet p_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1DVar("leadTrkbJetPt", "; leading b-jet p_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("nBtagJets", "; number of b-tagged jets ; Events", 10, -0.5, 9.5);
  m_hSvc.create1D("nTrkBtagJets", "; number of b-tagged track jets ; Events", 10, 0, 10);


  m_hSvc.create1D("nBoostedW", "; number of boosted W's ; Events", 10, -0.5, 9.5);

  m_hSvc.create1DVar("MET", "; missing E_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("MET_phi", "; missing E_{T} #phi [rd] ; Events", 32, -3.2, 3.2);

  m_hSvc.create1D("ST", "; S_{T} [GeV]; Events", 39, 100., 4000.);

  m_hSvc.create1DVar("ST_effBins", "; H_{T} [GeV]; Events", varBinN05, varBin05);

  m_hSvc.create1D("mwt", "; W transverse mass [GeV]; Events", 20, 0, 200);
  m_hSvc.create1D("DeltaM_hadtopleptop", "; #Delta m (T_{had},T_{lep}) [GeV] ; Events", 20, 0., 500.);
  m_hSvc.create1D("DeltaR_bjet1bjet2", "; #Delta R(lead- and subleading b) ; Events", 10, 0., 5.);
  m_hSvc.create1D("DeltaR_lepnu", "; #Delta R(lep, #nu); Events", 12, 0., 3.);
  m_hSvc.create1D("mass_lepb", "; m_{#ell b_{lead}}; Events", 32,  0.,  800.);
  m_hSvc.create1D("mu", "; <mu>; Events", 100, 0, 100);
  m_hSvc.create1D("mu_original", "; <mu_original>; Events", 100, 0, 100);
  m_hSvc.create1D("vtxz", ";Z position of truth primary vertex; Events", 40, -400, 400);
  m_hSvc.create1D("npv", "; npv; Events", 50, 0, 50);
  m_hSvc.create1D("minDeltaR_lepjet", "; min #Delta R(lep, jet); Events", 50, 0, 5);
  m_hSvc.create1DVar("closejl_pt", "; Pt of closest jet to lep [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("minDeltaR_lepbjet", "; min #Delta R(lep, bjet) ; Events", 20, 0., 8.);
  m_hSvc.create1DVar("closebjl_pt", "; Pt of closest bjet to lep [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("minDeltaR_hadWbjet", "; min #Delta R(W_{had}, bjet) ; Events", 12, 0., 3.);
  m_hSvc.create1DVar("closebjHadW_pt", "; Pt of closest bjet to hadronic W candidate [GeV]; Events", varBinN1, varBin1);

  m_hSvc.create1D("weight_leptSF", "; QCD weights; Events", 200, 0, 2);
  m_hSvc.create1D("weight", "; QCD weights; Events", 2000, -100, 100);
  m_hSvc.create2D("weight_leptPt", ";lept Pt (GeV); QCD weights",100, 25, 525, 2000, -100, 100);
  m_hSvc.create2D("MET_MWT", ";W transverse mass [GeV]; missing E_{T} [GeV]",100, 0, 500, 2000, 0, 100);

  m_hSvc.create1D("jet0_m", "; mass of the leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet1_m", "; mass of the sub-leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet2_m", "; mass of the third leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet3_m", "; mass of the fourth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet4_m", "; mass of the fifth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet5_m", "; mass of the sixth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);

  m_hSvc.create1DVar("jet0_pt", "; p_{T} of the leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet1_pt", "; p_{T} of the sub-leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet2_pt", "; p_{T} of the third leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet3_pt", "; p_{T} of the fourth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet4_pt", "; p_{T} of the fifth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet5_pt", "; p_{T} of the sixth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);

  m_hSvc.create1D("jet0_eta", "; #eta of the leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet1_eta", "; #eta of the sub-leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet2_eta", "; #eta of the third leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet3_eta", "; #eta of the fourth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet4_eta", "; #eta of the fifth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet5_eta", "; #eta of the sixth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);

  m_hSvc.create1D("jet0_phi", "; #phi of the leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet1_phi", "; #phi of the sub-leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet2_phi", "; #phi of the third leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet3_phi", "; #phi of the fourth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet4_phi", "; #phi of the fifth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet5_phi", "; #phi of the sixth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);


}

ControlPlots::~ControlPlots() {
}

void ControlPlots::run(const Event &evt, double weight, const std::string &s) {
  // check channel
  //
  bool isBoosted = (evt.largeJet().size() >= 1)?true:false;
  //if (m_electron && (evt.electron().size() != 1 || evt.muon().size() != 0))
  //  return;
  //if (!m_electron && (evt.electron().size() != 0 || evt.muon().size() != 1))
  //  return;


  // if((evt.electron().size() >=1) ){
  //   this->CountEvent(icut, weight);
  //   if((evt.electron().size() ==1) ){
  //     this->CountEvent(icut, weight);
  //     if((evt.muon().size() ==0) ){
  // 	  this->CountEvent(icut, weight);
  // 	  if((evt.jet().size() >=1) ){
  // 	    this->CountEvent(icut, weight);
  // 	    if((evt.jet().size() >=2) ){
  // 	      this->CountEvent(icut, weight);
  // 	      if((evt.jet().size() >=3) ){
  // 		this->CountEvent(icut, weight);
  // 		if((evt.jet().size() >=4) ){
  // 		  this->CountEvent(icut, weight);
  // 		  if((nBtagged >=1) ){
  // 		    this->CountEvent(icut, weight);
  // 		    if((nBtagged >=2) ){
  // 		      this->CountEvent(icut, weight);
  // 		    }
  // 		  }
  // 		}
  // 	      }
  // 	    }
  // 	  }
  //     }
  //   }
  // }


  //(SH) starting from grid pre selection (gives same as single lepton selection)
  //if (m_electron && !evt.passes("ejets")) return;
  //if (!m_electron && !evt.passes("mujets")) return;

  // Apply single lepton selection
  bool eEvent=false;
  bool muEvent=false;
   if(m_combined){
     if((evt.electron().size() == 1 && evt.muon().size() == 0))
       eEvent=true;
     else if((evt.electron().size() == 0 && evt.muon().size() == 1))
       muEvent=true;
   }else{
     if (m_electron && (evt.electron().size() != 1 || evt.muon().size() != 0))
       return;
     if (!m_electron && (evt.electron().size() != 0 || evt.muon().size() != 1))
       return;
   }

  HistogramService *h = &m_hSvc;
  std::string suffix = s;

  bool isTight = false;
  TLorentzVector l;
  // Get leptons
  if(m_combined){
    if(eEvent){
      l = evt.electron()[0].mom();
      isTight = evt.electron()[0].isTightPP();
    }else if(muEvent){
      l = evt.muon()[0].mom();
      isTight = evt.muon()[0].isTight();
    }else{
      std::cout <<"WARNING :: No selected lepton in combined channel..." << std::endl;
    }
  }else{
    if (m_electron) {
      l = evt.electron()[0].mom();
      isTight = evt.electron()[0].isTightPP();

    } else {
      l = evt.muon()[0].mom();
      isTight = evt.muon()[0].isTight();
    }//m_electron
  }





  int nBtagged = 0; //nB-tagged jets
  std::vector<TLorentzVector> bJets;
  for (size_t bidx = 0; bidx < evt.jet().size(); ++bidx)
    if (evt.jet()[bidx].btag_mv2c20_77()){
      if(nBtagged==0) h->FillFlowHandler(h->h1D("leadbJetPt", "", suffix), evt.jet()[bidx].mom().Perp()*1e-3, weight);
      bJets.push_back(evt.jet()[bidx].mom());
      nBtagged += 1;
    }



 //(SH) cutflow on top of grid pre selection for DiffXsec comparison purposes
  unsigned int icut = 0;
  this->CountEvent(icut, weight);
  if(evt.jet().size() >=4 ){
    this->CountEvent(icut, weight);
    if((nBtagged >=1) ){
      this->CountEvent(icut, weight);
      if((nBtagged >=2) ){
	this->CountEvent(icut, weight);
      }
    }
  }


  float MET = evt.met().Perp()*1e-3;
  float MWT = sqrt(2. * l.Perp() * evt.met().Perp() * (1. - cos(l.Phi() - evt.met().Phi())))*1e-3;
  int nJets = evt.jet().size(); //njets


  float ST = m_vlqutils.GetST(l.Perp()*1e-3, evt.met().Perp()*1e-3, evt.largeJet(), evt.jet(), &bJets, nBtagged);
  float dR_lepnu(0.);

  std::vector<TLorentzVector*> vec_nu = m_neutrinoBuilder.candidatesFromWMass_Rotation(&l, evt.met().Perp(), evt.met().Phi(), true);
  TLorentzVector nu(0,0,0,0);
  if (vec_nu.size() > 0) {
    nu = *(vec_nu[0]);
    dR_lepnu = l.DeltaR(nu);
    for (size_t z = 0; z < vec_nu.size(); ++z) delete vec_nu[z];
    vec_nu.clear();
  }

  h->FillFlowHandler(h->h1D("weight_leptSF", "", suffix), evt.weight_leptonSF(), 1);
  h->FillFlowHandler(h->h1D("weight", "", suffix), weight, 1);
  h->h2D("weight_leptPt", "", suffix)->Fill(l.Perp()*1e-3, weight);
  h->h2D("MET_MWT", "", suffix)->Fill(MWT, MET, weight);

  h->FillFlowHandler(h->h1D("lepPt", "", suffix), l.Perp()*1e-3, weight);
  h->FillFlowHandler(h->h1D("lepPt_effBins", "", suffix), l.Perp()*1e-3, weight);
  h->FillFlowHandler(h->h1D("lepEta", "", suffix), l.Eta(), weight);
  h->FillFlowHandler(h->h1D("lepPhi", "", suffix), l.Phi(), weight);

  const TLorentzVector &j = evt.jet()[0].mom();
  h->FillFlowHandler(h->h1D("leadJetPt", "", suffix), j.Perp()*1e-3, weight);
  h->FillFlowHandler(h->h1D("nJets", "", suffix), nJets, weight);
  h->FillFlowHandler(h->h1D("nBtagJets", "", suffix), nBtagged, weight);
  h->FillFlowHandler(h->h1D("ST", "", suffix), ST, weight);
  h->FillFlowHandler(h->h1D("ST_effBins", "", suffix), ST, weight);
 // returns all possible boosted Ws passing the specified selection


   //missing et
  h->FillFlowHandler(h->h1D("MET", "", suffix), MET, weight);
  h->FillFlowHandler(h->h1D("MET_phi", "", suffix), evt.met().Phi(), weight);
  h->FillFlowHandler(h->h1D("DeltaR_lepnu", "", suffix), dR_lepnu, weight );

  //transverse W mass
  h->FillFlowHandler(h->h1D("mwt", "", suffix), MWT, weight);
  if(bJets.size()!=0)
    h->FillFlowHandler(h->h1D("mass_lepb", "", suffix), (l + bJets.at(0) ).M()*1e-3, weight );

  h->FillFlowHandler(h->h1D("yields", "", suffix), 1, weight);
  //mu
  h->FillFlowHandler(h->h1D("mu", "", suffix), evt.mu()*1.16, weight);
  h->FillFlowHandler(h->h1D("mu_original", "", suffix), evt.mu_original(), weight);

  //npv
  h->FillFlowHandler(h->h1D("npv", "", suffix), evt.npv(), weight);

  //z prosition of primary vertex
  h->FillFlowHandler(h->h1D("vtxz", "", suffix), evt.vtxz(), weight);

  int nTrkBtagged = 0; //nB-tagged jets
  for (size_t bidx = 0; bidx < evt.tjet().size(); ++bidx)
    if (evt.tjet()[bidx].btag_mv2c20_70_trk() && evt.tjet()[bidx].pass_trk()){
      if(nTrkBtagged==0)h->FillFlowHandler(h->h1D("leadTrkbJetPt", "", suffix), evt.tjet()[bidx].mom().Perp()*1e-3, weight);
      nTrkBtagged += 1;
    }
  h->FillFlowHandler(h->h1D("nTrkBtagJets", "", suffix), nTrkBtagged, weight);

  // Jet kinematics

  std::vector<float> jetMass_vector;
  std::vector<float> jetPt_vector;
  std::vector<float> jetEta_vector;
  std::vector<float> jetPhi_vector;

  jetMass_vector.resize(evt.jet().size());
  jetPt_vector.resize(evt.jet().size());
  jetEta_vector.resize(evt.jet().size());
  jetPhi_vector.resize(evt.jet().size());

  std::vector<TLorentzVector> jets;
  size_t iJet = 0;
  for (; iJet < evt.jet().size(); ++iJet){
    jets.push_back(evt.jet()[iJet].mom());
    const TLorentzVector &jet_p4 = evt.jet()[iJet].mom();
    jetMass_vector[iJet] =  evt.jet()[iJet].mom().M();
    jetPt_vector[iJet]   =  evt.jet()[iJet].mom().Pt();
    jetEta_vector[iJet]  =  evt.jet()[iJet].mom().Eta();
    jetPhi_vector[iJet]  =  evt.jet()[iJet].mom().Phi();
    h->FillFlowHandler(h->h1D("JetPt", "", suffix),  jet_p4.Perp()*1e-3, weight);
  }//for

  int maxNjet = (evt.jet().size()<6) ? evt.jet().size() : 6;
  for (int i = 0; i < maxNjet; ++i){

     std::string nameJet_m = "jet" + std::to_string(i)+"_m";
     h->FillFlowHandler(h->h1D(nameJet_m, "", suffix), jetMass_vector[i]*1e-3, weight);

     std::string nameJet_pt = "jet" + std::to_string(i)+"_pt";
     h->FillFlowHandler(h->h1D(nameJet_pt, "", suffix), jetPt_vector[i]*1e-3, weight);

     std::string nameJet_eta = "jet" + std::to_string(i)+"_eta";
     h->FillFlowHandler(h->h1D(nameJet_eta, "", suffix), jetEta_vector[i], weight);

     std::string nameJet_phi = "jet" + std::to_string(i)+"_phi";
     h->FillFlowHandler(h->h1D(nameJet_phi, "", suffix), jetPhi_vector[i], weight);

  }//for

  float mtt = -1;

  //deltaR between lepton and the closest narrow jet
  float closejl_deltaR  = 99;
  float deltaR_tmp      = 99;
  int closejl_idx       = -1;

  size_t jet_idx = 0;
  for (; jet_idx < evt.jet().size(); ++jet_idx){
    deltaR_tmp = 99;
    float dphi = l.DeltaPhi(evt.jet()[jet_idx].mom());
    float dy = l.Rapidity() - evt.jet()[jet_idx].mom().Rapidity();
    deltaR_tmp = std::sqrt(dy*dy + dphi*dphi);
    if (deltaR_tmp < closejl_deltaR){
        closejl_deltaR = deltaR_tmp;
        closejl_idx = jet_idx;
    }
  }//for

  float closejl_pt = -1;
  if (closejl_idx>0)    closejl_pt = evt.jet()[closejl_idx].mom().Perp();
  h->FillFlowHandler(h->h1D("minDeltaR_lepjet", "", suffix), closejl_deltaR, weight);
  h->FillFlowHandler(h->h1D("closejl_minDeltaR_effBins", "", suffix), closejl_deltaR, weight);
  h->FillFlowHandler(h->h1D("closejl_pt", "", suffix), closejl_pt*1e-3, weight);

  //min. deltaR between lepton and b-jet
  float closebjl_deltaR  = 99;
  deltaR_tmp      = 99;
  int closebjl_idx       = -1;
  for ( size_t bidx = 0; bidx < bJets.size(); ++bidx){
    deltaR_tmp = 99;
    deltaR_tmp = l.DeltaR(bJets.at(bidx));
    if (deltaR_tmp < closebjl_deltaR){
        closebjl_deltaR = deltaR_tmp;
        closebjl_idx = bidx;
    }
  }//for

  h->FillFlowHandler(h->h1D("minDeltaR_lepbjet", "", suffix),  closebjl_deltaR, weight );


  if(bJets.size()>0)h->FillFlowHandler(h->h1D("closebjl_pt", "", suffix), bJets.at(closebjl_idx).Perp()*1e-3, weight);

  h->FillFlowHandler(h->h1D("trueMtt", "", suffix), evt.MC_ttbar_beforeFSR().M()*1e-3, weight);
  //  _tree_truemtt = evt.MC_ttbar_beforeFSR().M()*1e-3;

  //DeltaR_bjet1bjet2
  if(bJets.size()>1)
    h->FillFlowHandler(h->h1D("DeltaR_bjet1bjet2", "", suffix),  bJets.at(0).DeltaR(bJets.at(1)), weight );


  int nWtags = m_vlqutils.boostedWtags( evt.largeJet(), bJets, l, eEvent, "TOP2016" );
  h->FillFlowHandler(h->h1D("nBoostedW", "", suffix), nWtags, weight);

  //_tree_mtt = mtt*1e-3;
  //    h->m_tree->Fill();



}//
void ControlPlots::CountEvent(unsigned int& icut, double weight){
  std::string suffix = "";
  HistogramService *h = &m_hSvc;
  h->h1D("cutflow", "", suffix)->Fill(icut, weight);
  ++icut;
}
