/**
 * @brief Class that reads information from the input file and puts it in a object-oriented format in Event.
 * @author Danilo Enoque Ferreira de Lima <dferreir@cern.ch>
 */
#include "TopNtupleAnalysis/MiniTree_HQT.h"
#include "TopNtupleAnalysis/Event.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include <vector>
#include <string>
#include <iostream>
#include "TChain.h"
#include "TBranch.h"
#include "TLeaf.h"
#include <algorithm>
#include <sstream>
#include "TTree.h"
#include "TFriendElement.h"



MiniTree_HQT::MiniTree_HQT(bool toWrite, const std::string &file, const std::string &name, int TreeFriends)
  : m_file(0), m_chain(0), m_name(name), m_treefriends(0) {
  //m_file = TFile::Open(file.c_str());
  std::cout << "INFO :: name of chain > "<< name.c_str()<< std::endl;
  m_chain = new TChain(name.c_str());
  ((TChain *) m_chain)->Add(file.c_str());
  m_sumWeights = 0;
  m_treefriends = TreeFriends;
  if(m_treefriends == 1){
    // (SH) PRWHash
    char* pPath;
    pPath = getenv("ANATOP");
    if(pPath==NULL)
      std::cout << "WARNING :: Please setup the global variable $ANATOP pointing to the directory that includes TopDataPreparation" << std::endl;
    stringstream ss;
    string pathfriend;
    ss << pPath;
    ss >> pathfriend;
    pathfriend.append("/vlq_wbx/TopNtupleAnalysis/share/prwTree.root");
    std::string tName = "prwTree";
    TChain* fChain = new TChain(tName.c_str());
    fChain->Add(pathfriend.c_str());//stores a variable called PileupWeight
    std::cout << "INFO :: Chain with name: "<< tName
	      << " is friendly added to chain: "<< name
	      <<" from > "<< pathfriend << std::endl;
    fChain->BuildIndex("PRWHash");
    ((TChain *) m_chain)->AddFriend(fChain);
    //(SH) Add TruthInfo
  }else if(m_treefriends == 3){
    m_branches = {"MC_t_beforeFSR_pt", "MC_t_beforeFSR_eta","MC_t_beforeFSR_phi","MC_t_beforeFSR_pt",
					 "MC_tbar_beforeFSR_pt", "MC_tbar_beforeFSR_eta","MC_tbar_beforeFSR_phi","MC_tbar_beforeFSR_pt"};
    std::string tName = "truth";
     TChain* fChain = new TChain(tName.c_str());
     fChain->Add(file.c_str());
     std::cout << "INFO :: Chain with name: "<< tName
	       << " is friendly added to chain: "<< name
	       <<" from > "<< file << std::endl;
     ((TChain *) m_chain)->AddFriend(fChain);
  }
  prepareBranches();
}

MiniTree_HQT::~MiniTree_HQT() {
  if (m_chain) delete m_chain;
  if (m_file) delete m_file;
}

//(SH)
void MiniTree_HQT::addBranchesFromFriendTree( TObjArray *l, std::string branchName ) {
  TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
  TTree* ft = (TTree*)fe->GetTree();
  TBranch* fb = (TBranch*)ft->GetBranch(branchName.c_str());
  //  ft->Print();
  TObject* fo = (TObject*)fb->Clone();
  l->Add(fo);
}


void MiniTree_HQT::read(int event, Event &e) {
  e.clear();
  m_chain->GetEntry(event);
  //(SH)  m_chain->Scan("PRWHash:PileupWeight","","colsize=30 precision=16");
  e.eventNumber() = ul64("eventNumber");
  e.PRWHash() = ul64("PRWHash");
  e.PileupWeight() = f("PileupWeight"); // (SH) Is only filled in case the PRWHashing is true
  e.runNumber() = ui("runNumber");
  e.randomRunNumber() = ui("randomRunNumber");
  e.channelNumber() = ui("mcChannelNumber");


  e.mu() = f("mu");
  e.mu_original() = f("mu_original_xAOD");
  for (std::map<std::string, char>::iterator it = m_c.begin(); it != m_c.end(); ++it) {
    if (it->first.find("HLT_") == 0) {
      e.setTrigger(it->first, it->second != 0);
    }
  }

  e.npv() = ui("npv");
  e.vtxz() = f("vtxz");

  e.weight_mc() = f("weight_mc");
  e.weight_pileup() = f("weight_pileup");
  e.weight_bTagSF() = f("weight_bTagSF_70");
  e.weight_leptonSF() = f("weight_leptonSF");
  e.weight_jvt() = f("weight_jvt");

  e.isBoosted() = i("isBoosted");
  e.weight_sherpa22() = f("weight_sherpa_22_vjets");

  //@todo (SH): read-in the truth information here, maybe add truth identifier (SH)
  if (f("MC_t_beforeFSR_pt") > 0) e.MC_t().SetPtEtaPhiM(f("MC_t_beforeFSR_pt"), f("MC_t_beforeFSR_eta"), f("MC_t_beforeFSR_phi"), f("MC_t_beforeFSR_m"));
  else                          e.MC_t().SetPtEtaPhiM(2000, -9., 0., 0.);

  if (f("MC_tbar_beforeFSR_pt") > 0)      e.MC_tbar().SetPtEtaPhiM(f("MC_tbar_beforeFSR_pt"), f("MC_tbar_beforeFSR_eta"), f("MC_tbar_beforeFSR_phi"), f("MC_tbar_beforeFSR_m"));
  else                          e.MC_tbar().SetPtEtaPhiM(2000, -9., 0., 0.);


  // >>> get electron information
  //

  for (int k = 0; k < vf("el_pt")->size(); ++k) {
    e.electron().push_back(Electron());
    e.electron()[k].mom().SetPtEtaPhiE(vf("el_pt")->at(k), vf("el_eta")->at(k), vf("el_phi")->at(k), vf("el_e")->at(k));
    e.electron()[k].charge() = vf("el_charge")->at(k);
    e.electron()[k].setTightPP(false);
    if (vc("el_isTight")) {
      e.electron()[k].setTightPP(vc("el_isTight")->at(k));
    }
    e.electron()[k].caloMom() = e.electron()[k].mom();
    e.electron()[k].trkMom() = e.electron()[k].mom();

    e.electron()[k].sd0() = vf("el_d0sig")->at(k);
    e.electron()[k].HLT_e24_lhmedium_L1EM18VH() = 0;
    e.electron()[k].HLT_e60_lhmedium() 		= 0;
    e.electron()[k].HLT_e120_lhloose() 		= 0;
    if (vc("el_trigMatch_HLT_e24_lhmedium_L1EM18VH"))		e.electron()[k].HLT_e24_lhmedium_L1EM18VH() = vc("el_trigMatch_HLT_e24_lhmedium_L1EM18VH")->at(k);
    if (vc("el_trigMatch_HLT_e24_lhmedium_L1EM20VH"))		e.electron()[k].HLT_e24_lhmedium_L1EM20VH() = vc("el_trigMatch_HLT_e24_lhmedium_L1EM20VH")->at(k);
    //    e.electron()[k].HLT_e60_lhmedium() 		= vc("el_trigMatch_HLT_e60_lhmedium")->at(k);
    //    e.electron()[k].HLT_e120_lhloose() 		= vc("el_trigMatch_HLT_e120_lhloose")->at(k);
    e.electron()[k].author() = 1;
  }

  // >>> get muon information
  //

  for (int k = 0; k < vf("mu_pt")->size(); ++k) {
    e.muon().push_back(Muon());
    e.muon()[k].mom().SetPtEtaPhiE(vf("mu_pt")->at(k), vf("mu_eta")->at(k), vf("mu_phi")->at(k), vf("mu_e")->at(k));
    //    e.muon()[k].charge() = i("mu_charge");
    e.muon()[k].setMI(0);
    e.muon()[k].setTight(false);
    if (vc("mu_isTight")) {
      e.muon()[k].setTight(vc("mu_isTight")->at(k));
    }
    e.muon()[k].sd0() = vf("mu_d0sig")->at(k);
    if (vc("mu_trigMatch_HLT_mu20_iloose_L1MU15")) e.muon()[k].HLT_mu20_iloose_L1MU15() 	= vc("mu_trigMatch_HLT_mu20_iloose_L1MU15")->at(k);
    if (vc("mu_trigMatch_HLT_mu26_imedium()")) e.muon()[k].HLT_mu26_imedium() = vc("mu_trigMatch_HLT_mu26_imedium")->at(k);
    if (vc("mu_trigMatch_HLT_mu50"))    e.muon()[k].HLT_mu50() 		 = vc("mu_trigMatch_HLT_mu50")->at(k);
    e.muon()[k].author() = 0;
    // e.muon()[k].passTrkCuts() = true;
  }




  // >>> get jet information
  //

   for (int k = 0; k < vf("jet_pt")->size(); ++k) {
    e.jet().push_back(Jet());
    e.jet()[k].mom().SetPtEtaPhiE(vf("jet_pt")->at(k), vf("jet_eta")->at(k), vf("jet_phi")->at(k), vf("jet_e")->at(k));
    //    e.jet()[k].trueFlavour() = vi("jet_truthflav")==0?-99:vi("jet_truthflav")->at(k);
    if (vi("jet_truthflav")) e.jet()[k].trueFlavour() = vi("jet_truthflav")->at(k);
    e.jet()[k].mv1() = vf("jet_mv1")==0?-99:vf("jet_mv1")->at(k);
    e.jet()[k].mv1b() = vf("jet_mv1b")==0?-99:vf("jet_mv1b")->at(k);
    e.jet()[k].mv1c() = vf("jet_mv1c")==0?-99:vf("jet_mv1c")->at(k);
    e.jet()[k].ip3dsv1() = vf("jet_ip3dsv1")==0?-99:vf("jet_ip3dsv1")->at(k);
    e.jet()[k].mv2c10() = vf("jet_mv2c10")==0?-99:vf("jet_mv2c10")->at(k);
    e.jet()[k].mv2c20() = vf("jet_mv2c20")==0?-99:vf("jet_mv2c20")->at(k);
    e.jet()[k].jvt() = vf("jet_jvt")==0?-99:vf("jet_jvt")->at(k);
  }

  // // >>> get track jet information
  // //
  // for (int k = 0; k < vf("tjet_pt")->size(); ++k) {
  //   e.tjet().push_back(Jet());
  //   e.tjet()[k].mom().SetPtEtaPhiE(vf("tjet_pt")->at(k), vf("tjet_eta")->at(k), vf("tjet_phi")->at(k), vf("tjet_e")->at(k));
  //   e.tjet()[k].trueFlavour() = vi("tjet_true_flavor")==0?-99:vi("tjet_true_flavor")->at(k);
  //   e.tjet()[k].mv1() = -99;
  //   e.tjet()[k].mv1b() = -99;
  //   e.tjet()[k].mv1c() = -99;
  //   e.tjet()[k].ip3dsv1() = -99;
  //   e.tjet()[k].mv2c20() = vf("tjet_mv2c20")==0?-99:vf("tjet_mv2c20")->at(k);
  //   e.tjet()[k].jvt() = -99;
  //   e.tjet()[k].numConstituents() = vi("tjet_numConstituents")->at(k);
  // }

  // >>> get large jet information
  //

  for (int k = 0; k < vf("ljet_pt")->size(); ++k) {
    e.largeJet().push_back(LargeJet());
    e.largeJet()[k].mom().SetPtEtaPhiE(vf("ljet_pt")->at(k), vf("ljet_eta")->at(k), vf("ljet_phi")->at(k), vf("ljet_e")->at(k));
    e.largeJet()[k].split12() = vf("ljet_sd12")->at(k);
    e.largeJet()[k].setGood((vi("ljet_isGood")->at(k) == 1)?true:false);
    e.largeJet()[k].trueFlavour() = 0; //TODO ljet_trueflav==0?-99:ljet_trueflav->at(k);
    //e.largeJet()[k].setIsSmoothTopTagged_50((vi("ljet_isSmoothTopTagged_50")->at(k) == 1)?true:false);
    //    e.largeJet()[k].setIsSmoothTopTagged_80((vi("ljet_isSmoothTopTagged_80")->at(k) == 1)?true:false);
    //    e.largeJet()[k].isSmoothTopTagged_50() = vi("ljet_isSmoothTopTagged_50")->at(k);
    //    e.largeJet()[k].isSmoothTopTagged_50() = vi("ljet_isSmoothTopTagged_50")==0?-99:vi("ljet_isSmoothTopTagged_50")->at(k);
    e.largeJet()[k].isSmoothTopTagged_50() = vi("ljet_isSmoothTopTagged_50")->at(k);
    e.largeJet()[k].isSmoothTopTagged_80() = vi("ljet_isSmoothTopTagged_80")->at(k);
    //    std::cout << "DEBUG :: top tag > "<< vi("ljet_isSmoothTopTagged_50")->at(k) << std::endl;
    //    e.largeJet()[k].isWTaggedMed() = vi("ljet_isWTaggedMed")->at(k);
    //    e.largeJet()[k].isWTaggedMed() = vi("ljet_isWTaggedMed")==0?-99:vi("ljet_isWTaggedMed")->at(k);
    e.largeJet()[k].isWTaggedMed() = vi("ljet_isWTaggedMed")->at(k);
    //    std::cout << "DEBUG :: W tag > "<< vi("ljet_isWTaggedMed")->at(k) << std::endl;
    e.largeJet()[k].isWTaggedTight() = vi("ljet_isWTaggedTight")->at(k);
    e.largeJet()[k].isZTaggedTight() = vi("ljet_isZTaggedTight")->at(k);
    e.largeJet()[k].subs("C2") = vf("ljet_C2")->at(k);
    e.largeJet()[k].subs("D2") = vf("ljet_D2")->at(k);
    e.largeJet()[k].subs("d23") = vf("ljet_d23")->at(k);
    e.largeJet()[k].subs("tau2_wta") = vf("ljet_tau2_wta")->at(k);
    e.largeJet()[k].subs("tau3_wta") = vf("ljet_tau3_wta")->at(k);
    e.largeJet()[k].subs("tau32_wta") = vf("ljet_tau32_wta")->at(k);
    e.largeJet()[k].setIsGhAssTrackJetBtagged((vi("ljet_trackjet_btag")->at(k) == 1)?true:false);
  }
  e.met(f("met_met")*std::cos(f("met_phi")), f("met_met")*std::sin(f("met_phi")));
  e.mtw() = f("mtw");
  e.vlq_evtype() = i("vlq_evtype");

  e.passes().clear();

  if (i("ejets_2015")) e.passes().push_back("ejets_2015");
  if (i("mujets_2015")) e.passes().push_back("mujets_2015");
  if (i("ejets_2016")) e.passes().push_back("ejets_2016");
  if (i("mujets_2016")) e.passes().push_back("mujets_2016");

}

ULong64_t          &MiniTree_HQT::ul64(const std::string &n) {
  return m_ul64[n];
}
unsigned int         &MiniTree_HQT::ui(const std::string &n) {
  return m_ui[n];
}
int                  &MiniTree_HQT::i(const std::string &n) {
  return m_i[n];
}
float                &MiniTree_HQT::f(const std::string &n) {
  return m_f[n];
}
char                 &MiniTree_HQT::c(const std::string &n) {
  return m_c[n];
}

std::vector<std::vector<float> > *MiniTree_HQT::vvf(const std::string &n) {
  if (m_vvf.find(n) == m_vvf.end()) return 0;
  return m_vvf[n];
}

std::vector<std::vector<int> > *MiniTree_HQT::vvi(const std::string &n) {
  if (m_vvi.find(n) == m_vvi.end()) return 0;
  return m_vvi[n];
}
std::vector<int>     *MiniTree_HQT::vi(const std::string &n) {
  if (m_vi.find(n) == m_vi.end()) return 0;
  return m_vi[n];
}
std::vector<char>    *MiniTree_HQT::vc(const std::string &n) {
  if (m_vc.find(n) == m_vc.end()) return 0;
  return m_vc[n];
}
std::vector<float>   *MiniTree_HQT::vf(const std::string &n) {
  if (m_vf.find(n) == m_vf.end()) return 0;
  return m_vf[n];
}

double &MiniTree_HQT::sumWeights() {
  return m_sumWeights;
}

void MiniTree_HQT::addFileToRead(const std::string &fname) {
  ((TChain *) m_chain)->Add(fname.c_str());
}

void MiniTree_HQT::addFileToRead(const std::string &fname, const std::string &treeName) {
  ((TChain *) m_chain)->AddFile(fname.c_str(), -1, treeName.c_str());
}

double MiniTree_HQT::getSumWeights() {
  return m_sumWeights;
}

int MiniTree_HQT::GetEntries() {
  return m_chain->GetEntries();
}

void MiniTree_HQT::prepareBranches() {
  TObjArray *l = m_chain->GetListOfBranches();

  if(m_treefriends == 1){
    //(SH) Add the PileupWeight from the fiend tree to the TobjArray
    addBranchesFromFriendTree(l, "PileupWeight");
  }  else if(m_treefriends == 3){
    //(SH) Add the truth information requested by m_branches
    for (auto &branch : m_branches){
      std::cout <<"INFO :: From chain added above, add branch : " << branch << std::endl;
      addBranchesFromFriendTree(l, branch);
    }

  }
  for (size_t z = 0; z < l->GetEntries(); ++z) {
    std::string name = l->At(z)->GetName();
    std::string type = ((TBranch *) l->At(z))->GetLeaf(name.c_str())->GetTypeName();
    if (type == "Float_t" || type == "float") {
      m_f[name] = -50000;
    } else if (type == "Int_t" || type == "int") {
      m_i[name] = -50000;
    } else if (type == "ULong64_t") {
      m_ul64[name] = 0;
    } else if (type == "UInt_t" || type == "unsigned int") {
      m_ui[name] = 0;
    } else if (type == "char" || type == "Char_t") {
      m_c[name] = 0;
    } else if (type == "vector<char>" || type == "std::vector<char>" || type == "vector<Char_t>" || type == "std::vector<Char_t>") {
      m_vc[name] = 0;
    } else if (type == "vector<float>" || type == "std::vector<float>" || type == "vector<Float_t>" || type == "std::vector<Float_t>") {
      m_vf[name] = 0;
    } else if (type == "vector<int>" || type == "std::vector<int>" || type == "vector<Int_t>" || type == "std::vector<Int_t>") {
      m_vi[name] = 0;
    } else if (type == "vector<vector<int> >" || type == "std::vector<std::vector<int> >" || type == "vector<vector<Int_t> >" || type == "std::vector<std::vector<Int_t> >") {
      m_vvi[name] = 0;
    } else if (type == "vector<vector<float> >" || type == "std::vector<std::vector<float> >" || type == "vector<vector<Float_t> >" || type == "std::vector<std::vector<Float_t> >") {
      m_vvf[name] = 0;
    } else {
      std::cout << "ERROR: I could not figure out the type of this branch! Name = " << name << ", type = " << type << std::endl;
    }
  }

  for (auto& it : m_f) {
    //(SH)
    if(m_treefriends ==1 && it.first.find("PileupWeight") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("PileupWeight");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
    }
    //(SH) currently a HACK, needs to be improved in the future
    else if(m_treefriends == 3 && it.first.find("MC_t_beforeFSR_pt") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_t_beforeFSR_pt");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }else if(m_treefriends ==3 && it.first.find("MC_t_beforeFSR_eta") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_t_beforeFSR_eta");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }else if(m_treefriends ==3 && it.first.find("MC_t_beforeFSR_phi") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_t_beforeFSR_phi");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }else if(m_treefriends ==3 && it.first.find("MC_t_beforeFSR_m") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_t_beforeFSR_m");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }else if(m_treefriends ==3 && it.first.find("MC_tbar_beforeFSR_pt") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_tbar_beforeFSR_pt");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }else if(m_treefriends ==3 && it.first.find("MC_tbar_beforeFSR_eta") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_tbar_beforeFSR_eta");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }else if(m_treefriends ==3 && it.first.find("MC_tbar_beforeFSR_phi") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_tbar_beforeFSR_phi");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }else if(m_treefriends ==3 && it.first.find("MC_tbar_beforeFSR_m") != std::string::npos){
      TFriendElement *fe = (TFriendElement*) m_chain->GetListOfFriends()->At(0);
      TTree* ft = (TTree*)fe->GetTree();
      TBranch* fb = (TBranch*)ft->GetBranch("MC_tbar_beforeFSR_m");
      ft->SetBranchAddress(it.first.c_str(), &(it.second));
      m_chain->AddFriend(ft);
    }
    else{
	m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    }

    m_brs[it.first] = mtFloat;
  }
  for (auto& it : m_ui) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtUint;
  }
  for (auto& it : m_ul64) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtULong64;
  }
  for (auto& it : m_i) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtInt;
  }
  for (auto& it : m_c) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtChar;
  }
  for (auto& it : m_vc) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVChar;
  }
  for (auto& it : m_vf) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVFloat;
  }
  for (auto& it : m_vi) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVInt;
  }
  for (auto& it : m_vvi) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVVInt;
  }
  for (auto& it : m_vvf) {
    m_chain->SetBranchAddress(it.first.c_str(), &(it.second));
    m_brs[it.first] = mtVVFloat;
  }
}

