/**
 * @brief Analysis class for VLQ->WbX.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#include <typeinfo>
#include "TopNtupleAnalysis/Analysis_HQT.h"
#include "TopNtupleAnalysis/AnaHQTVLQWbX_pre.h"
#include "TopNtupleAnalysis/Event.h"
#include "TLorentzVector.h"
#include <vector>
#include <string>
#include "TopNtupleAnalysis/HistogramService.h"

AnaHQTVLQWbX_pre::AnaHQTVLQWbX_pre(const std::string &filename, bool lepton, bool boosted, std::vector<std::string> &systList)
  : Analysis(filename, systList), m_lepton(lepton), m_boosted(boosted),
    
    m_vlqutils() {
  
//m_neutrinoBuilder("MeV"), m_chi2("MeV"),
  //  m_chi2.Init(TtresChi2::DATA2015_MC15);

  //  m_hSvc.m_tree->Branch("truemtt",    &_tree_truemtt);
  //  m_hSvc.m_tree->Branch("mtt",    &_tree_mtt);
  //  m_hSvc.m_tree->Branch("weight", &_tree_weight);
  //  m_hSvc.m_tree->Branch("cat",    &_tree_cat);
  //  m_hSvc.m_tree->Branch("syst",   &_tree_syst);

  double varBin1[] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 220, 240, 260, 280, 300, 340, 380, 450, 500};
  int varBinN1 = sizeof(varBin1)/sizeof(double) - 1;
  double varBin2[] = {300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500, 540, 580, 620, 660, 700, 800, 1e3, 1.2e3, 1.5e3};
  int varBinN2 = sizeof(varBin2)/sizeof(double) - 1;
  double varBin3[] = {0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 340, 380, 420, 460, 500};
  int varBinN3 = sizeof(varBin3)/sizeof(double) - 1;
  double varBin4[] = {80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 340, 380, 420, 460, 500};
  int varBinN4 = sizeof(varBin4)/sizeof(double) - 1;

  double varBin5[] = {0, 80, 160, 240, 320, 400, 480, 560, 640,720,800,920,1040,1160,1280,1400,1550,1700,2000,2300,2600,2900,3200,3600,4100,4600,5100,6000};
  int varBinN5 = sizeof(varBin5)/sizeof(double) - 1;

  //Plots for QCD validation
  if (m_electron) {
     if(m_boosted){
        double varBin6[6] = {30, 40, 60, 120, 400, 700};
	double varBin7[5]  = {0., 0.4, 0.6, 1.0, 1.5};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);

     }else{
        double varBin6[8] = {30, 35, 40, 50, 60, 120, 400, 700};
	double varBin7[7]  = {0., 0.4, 0.6, 1.0, 1.5, 2.5, 7.0};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);
     }//m_boosted

  }else{
     if(m_boosted){
        double varBin6[7] = {25, 30, 40, 50, 100, 400, 700};
	double varBin7[6] = {0., 0.4, 0.6, 0.8, 1.0, 1.5};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);
     }else{
        double varBin6[9] = {25, 30, 35, 40, 50, 70, 100, 400, 700};
	double varBin7[7] = {0., 0.4, 0.6, 1.0, 1.5, 2.5, 7.0};
	int varBinN6 = sizeof(varBin6)/sizeof(double) - 1;
	int varBinN7 = sizeof(varBin7)/sizeof(double) - 1;
	m_hSvc.create1DVar("lepPt_effBins", "; lepton p_{T} [GeV]; Events", varBinN6, varBin6);
	m_hSvc.create1DVar("closejl_minDeltaR_effBins", "; min #Delta R(lep, jet); Events", varBinN7, varBin7);
     }//m_boosted
  }//m_electron

  m_hSvc.create1D("yields", "; One ; Events", 1, 0.5, 1.5);
  //m_hSvc.create1DVar("lepPt", "; lepton p_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("lepPt",    "; Pt of lept [GeV]; Events", 100, 25, 525);
  m_hSvc.create1D("lepEta", "; lepton #eta ; Events", 20, -2.5, 2.5);
  m_hSvc.create1D("lepPhi", "; lepton #phi [rd] ; Events", 32, -3.2, 3.2);

  m_hSvc.create1D("nJets", "; number of jets ; Events", 10, -0.5, 9.5);
  m_hSvc.create1D("nBtagJets", "; number of b-tagged jets ; Events", 10, 0, 10);
  m_hSvc.create1D("nTrkBtagJets", "; number of b-tagged track jets ; Events", 10, 0, 10);

  m_hSvc.create1DVar("leadJetPt", "; leading Jet p_{T} [GeV]; Events", varBinN1, varBin1);  
  m_hSvc.create1DVar("leadbJetPt", "; leading b-jet p_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1DVar("leadTrkbJetPt", "; leading b-jet p_{T} [GeV]; Events", varBinN1, varBin1);
 
  m_hSvc.create1D("DeltaM_hadtopleptop", "; |#Delta m|$(T_{had},T_{lep}) [GeV] ; Events", 20, 0., 500.);
  m_hSvc.create1D("DeltaR_bjet1bjet2", "; #Delta R(b-jets) ; Events", 10, 0., 5.);
  m_hSvc.create1D("minDeltaR_hadWbjet", "; min(#Delta R(W_{had} ; Events", 12, 0., 3.);
  m_hSvc.create1D("minDeltaR_lepbjet", ";  ; Events", 12, 0., 3.);
  m_hSvc.create1D("DeltaR_lepnu", "; #Delta R(lep, #nu); Events", 12, 0., 3.);
  m_hSvc.create1D("mass_lepb", "; m_{#ell b}; Events", 32,  0.,  800.);
 
  m_hSvc.create1D("HT", "; H_{T}; Events", 39, 100., 4000.);
	
  m_hSvc.create1DVar("MET", "; missing E_{T} [GeV]; Events", varBinN1, varBin1);
  m_hSvc.create1D("MET_phi", "; missing E_{T} #phi [rd] ; Events", 32, -3.2, 3.2);

  m_hSvc.create1D("mwt", "; W transverse mass [GeV]; Events", 20, 0, 200);
  m_hSvc.create1D("mu", "; <mu>; Events", 100, 0, 100);
  m_hSvc.create1D("mu_original", "; <mu_original>; Events", 100, 0, 100);
  m_hSvc.create1D("vtxz", ";Z position of truth primary vertex; Events", 40, -400, 400);
  m_hSvc.create1D("npv", "; npv; Events", 50, 0, 50);

 m_hSvc.create1D("A_distance", "; A_{r} [GeV]; Events", 15,0.,1.5);
 m_hSvc.create1D("A_mass", "; A_{m} [GeV]; Events", 10,0.,1.);
 m_hSvc.create1D("A_dr", "; A_{R} [GeV]; Events", 10,0.,1.);

  m_hSvc.create1D("closejl_minDeltaR", "; min #Delta R(lep, jet); Events", 50, 0, 5);
  m_hSvc.create1DVar("closejl_pt", "; Pt of closest jet to lep [GeV]; Events", varBinN1, varBin1);

  m_hSvc.create1D("weight_leptSF", "; QCD weights; Events", 200, 0, 2);
  m_hSvc.create1D("weight", "; QCD weights; Events", 2000, -100, 100);
  m_hSvc.create2D("weight_leptPt", ";lept Pt (GeV); QCD weights",100, 25, 525, 2000, -100, 100);

  m_hSvc.create1D("jet0_m", "; mass of the leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet1_m", "; mass of the sub-leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet2_m", "; mass of the third leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet3_m", "; mass of the fourth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet4_m", "; mass of the fifth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);
  m_hSvc.create1D("jet5_m", "; mass of the sixth leading R=0.4 calo jet [GeV]; Events", 20, 0, 100);

  m_hSvc.create1DVar("jet0_pt", "; p_{T} of the leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet1_pt", "; p_{T} of the sub-leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet2_pt", "; p_{T} of the third leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet3_pt", "; p_{T} of the fourth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet4_pt", "; p_{T} of the fifth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);
  m_hSvc.create1DVar("jet5_pt", "; p_{T} of the sixth leading R=0.4 calo jet [GeV]; Events", varBinN3, varBin3);

  m_hSvc.create1D("jet0_eta", "; #eta of the leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet1_eta", "; #eta of the sub-leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet2_eta", "; #eta of the third leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet3_eta", "; #eta of the fourth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet4_eta", "; #eta of the fifth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);
  m_hSvc.create1D("jet5_eta", "; #eta of the sixth leading R=0.4 calo jet ; Events", 25, -2.5, 2.5);

  m_hSvc.create1D("jet0_phi", "; #phi of the leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet1_phi", "; #phi of the sub-leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet2_phi", "; #phi of the third leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet3_phi", "; #phi of the fourth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet4_phi", "; #phi of the fifth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);
  m_hSvc.create1D("jet5_phi", "; #phi of the sixth leading R=0.4 calo jet ; Events", 32, -3.2, 3.2);


   m_hSvc.create1D("nhadronicT", "; number of hadronic T's ; Events", 10, -0.5, 9.5);
   m_hSvc.create1D("hadronicT_pt", "; hadronic T p_{T} [GeV] ; Events", 15,200., 1500.);
   m_hSvc.create1D("hadronicT_m", "; hadronic T mass [GeV] ; Events", 25,  0., 1500.);
   m_hSvc.create1D("hadronicT_eta", "; hadronic T #eta ; Events", 25,-2.5,   2.5);
   m_hSvc.create1D("hadronicT_phi", "; hadronic T #phi [rad] ; Events", 32, -3.2, 3.2);

   m_hSvc.create1D("nhadronicW", "; number of hadronic W's ; Events", 10, -0.5, 9.5);
   m_hSvc.create1D("hadronicW_pt", "; hadronic W p_{T} [GeV] ; Events", 16, 200., 1000.);
   m_hSvc.create1D("hadronicW_m", "; hadronic W mass [GeV] ; Events", 14, 50.,  120.);
   m_hSvc.create1D("hadronicW_eta", "; hadronic W #eta ; Events", 20, -2.5, 2.5);
   m_hSvc.create1D("hadronicW_phi", "; hadronic W #phi [rad] ; Events", 32, -3.2, 3.2);

   m_hSvc.create1D("nleptonicT", "; number of leptonic T's ; Events", 10, -0.5, 9.5);
   m_hSvc.create1D("leptonicT_pt", "; leptonic T p_{T} [GeV] ; Events", 15,200., 1500.);
   m_hSvc.create1D("leptonicT_m", "; leptonic T mass [GeV] ; Events", 25,  0., 1500.);
   m_hSvc.create1D("leptonicT_eta", "; leptonic T #eta ; Events", 20, -2., 2.);
   m_hSvc.create1D("leptonicT_phi", "; leptonic T #phi [rad] ; Events", 32, -3.2, 3.2);

   m_hSvc.create1D("nleptonicW", "; number of leptonic W's ; Events", 10, -0.5, 9.5);
   m_hSvc.create1D("leptonicW_pt", "; leptonic W p_{T} [GeV] ; Events", 16, 200., 1000.);
   m_hSvc.create1D("leptonicW_m", "; leptonic W mass [GeV] ; Events", 14, 50.,  120.);
   m_hSvc.create1D("leptonicW_eta", "; leptonic W #eta ; Events", 20,-2.5,   2.5);
   m_hSvc.create1D("leptonicW_phi", "; leptonic W #phi [rad] ; Events", 32, -3.2, 3.2);


   
  if (m_boosted) {
    m_hSvc.create1D("nBoostedW", "; number of boosted W's ; Events", 10, -0.5, 9.5);
    m_hSvc.create1D("nBoostedWcand", "; number of boosted W candidates ; Events", 10, -0.5, 9.5);
    m_hSvc.create1DVar("boostedW_pt", "; boosted W p_{T} [GeV] ; Events", varBinN2, varBin2);
    m_hSvc.create1D("boostedW_m", "; boosted W mass [GeV] ; Events", 40,  0.,  400.);
    m_hSvc.create1D("boostedW_eta", "; boosted W #eta ; Events", 20, -2., 2.);
    m_hSvc.create1D("boostedW_phi", "; boosted W #phi [rad] ; Events", 32, -3.2, 3.2);
    
    m_hSvc.create1DVar("closeJetPt", "; selected Jet p_{T} [GeV] ; Events", varBinN1, varBin1);

    m_hSvc.create1D("nLargeJetW", "; number of large jets ; Events", 10, -0.5, 9.5);
    m_hSvc.create1DVar("largeJetPt", "; large jet p_{T} [GeV] ; Events", varBinN2, varBin2);
    m_hSvc.create1D("largeJetM", "; large jet mass [GeV] ; Events", 30, 0, 300);
    m_hSvc.create1D("largeJetEta", "; large jet #eta ; Events", 20, -2.5, 2.5);
    m_hSvc.create1D("largeJetPhi", "; large jet #phi [rad] ; Events", 32, -3.2, 3.2);
    //    m_hSvc.create1D("largeJetSd12", "; large jet #sqrt{d_{12}} [GeV] ; Events", 20, 0, 200);
    m_hSvc.create1DVar("mtlep_boo", "; leptonic top mass [GeV] ; Events", varBinN4, varBin4);
  } else {
    m_hSvc.create1DVar("mtlep_res", "; leptonic top mass [GeV]; Events", varBinN4, varBin4);
    m_hSvc.create1DVar("mthad_res", "; hadronic top mass [GeV]; Events", varBinN4, varBin4);
    m_hSvc.create1DVar("mwhad_res", "; hadronic W boson mass [GeV]; Events", 40, 0, 400);
    m_hSvc.create1D("chi2", "; log(#chi^{2}) ; Events", 50, -3, 7);
  }

  m_hSvc.create1DVar("mtt", "; mass of the top-antitop system [GeV]; Events", varBinN5, varBin5);
  //  m_hSvc.create1DVar("trueMtt", "; mass of the top-antitop system [GeV]; Events", varBinN5, varBin5);

  //  m_hSvc.create1D("largeJet_tau32", "; #tau_{32}; Events", 20, 0, 1);
  //  m_hSvc.create1D("largeJet_tau32_wta", "; #tau_{32} wta; Events", 20, 0, 1);

  //  m_hSvc.create1D("largeJet_tau21", "; #tau_{21}; Events", 20, 0, 1);
  //  m_hSvc.create1D("largeJet_tau21_wta", "; #tau_{21} wta; Events", 20, 0, 1);
}

AnaHQTVLQWbX_pre::~AnaHQTVLQWbX_pre() {
}

void AnaHQTVLQWbX_pre::run(const Event &evt, double weight, const std::string &s) {
  // check channel
  //
  bool isBoosted = (evt.isBoosted() == 1)?true:false;
  bool isResolved = (evt.isResolved() == 1)?true:false;
  //if (m_electron && (evt.electron().size() != 1 || evt.muon().size() != 0))
  //  return;

  //if (!m_electron && (evt.electron().size() != 0 || evt.muon().size() != 1))
  //  
  if (!m_lepton && (!evt.passes("ejets") || !evt.passes("mujets"))) return;
  if (m_lepton && evt.passes("ejets")) m_electron = true;
  if (m_lepton && evt.passes("mujets")) m_electron = false;

  HistogramService *h = &m_hSvc;
  TLorentzVector l;
  l = evt.lepton()[0].mom();
  std::string suffix = s;
  h->h1D("yields", "", suffix)->Fill(1, weight);
  
  h->h1D("weight_lept_eff", "", suffix)->Fill(evt.weight_lept_eff());
  h->h1D("weight", "", suffix)->Fill(weight);
  h->h2D("weight_leptPt", "", suffix)->Fill(l.Perp()*1e-3, weight);

  h->h1D("lepPt", "", suffix)->Fill(l.Perp()*1e-3, weight);
  h->h1D("lepPt_effBins", "", suffix)->Fill(l.Perp()*1e-3, weight);
  h->h1D("lepEta", "", suffix)->Fill(l.Eta(), weight);
  h->h1D("lepPhi", "", suffix)->Fill(l.Phi(), weight);

  const TLorentzVector &j = evt.jet()[0].mom();
  h->h1D("leadJetPt", "", suffix)->Fill(j.Perp()*1e-3, weight);
  h->h1D("HT", "", suffix)->Fill(evt.HT()*1e-3, weight);
  h->h1D("A_distance", "", suffix)->Fill(evt.A_distance(), weight);
  h->h1D("A_dr", "", suffix)->Fill(evt.A_dr(), weight);
  h->h1D("A_mass", "", suffix)->Fill(evt.A_mass(), weight);
 
  // for now
  int nJets = evt.jet().size(); //njets
  h->h1D("nJets", "", suffix)->Fill(nJets, weight);

  int nBtagged = 0; //nB-tagged jets
  for (size_t bidx = 0; bidx < evt.jet().size(); ++bidx)
    if (evt.jet()[bidx].btag_mv2c20_77()){
      if(nBtagged==0)h->h1D("leadbJetPt", "", suffix)->Fill(evt.jet()[bidx].mom().Perp()*1e-3, weight);
      nBtagged += 1;
    }
  h->h1D("nBtagJets", "", suffix)->Fill(nBtagged, weight);
  //  h->h1D("nBtagJets", "", suffix)->Fill(evt.btagsN(), weight);

  int nTrkBtagged = 0; //nB-tagged jets
  for (size_t bidx = 0; bidx < evt.tjet().size(); ++bidx)
    if (evt.tjet()[bidx].btag_mv2c20_70_trk() && evt.tjet()[bidx].pass_trk()){
      if(nTrkBtagged==0)h->h1D("leadTrkbJetPt", "", suffix)->Fill(evt.tjet()[bidx].mom().Perp()*1e-3, weight);
      nTrkBtagged += 1;
    }
  h->h1D("nTrkBtagJets", "", suffix)->Fill(nTrkBtagged, weight);

  // Jet kinematics

  std::vector<float> jetMass_vector;
  std::vector<float> jetPt_vector;
  std::vector<float> jetEta_vector;
  std::vector<float> jetPhi_vector;

  jetMass_vector.resize(evt.jet().size());
  jetPt_vector.resize(evt.jet().size());
  jetEta_vector.resize(evt.jet().size());
  jetPhi_vector.resize(evt.jet().size());

  size_t iJet = 0;
  for (; iJet < evt.jet().size(); ++iJet){
    const TLorentzVector &jet_p4 = evt.jet()[iJet].mom();
    jetMass_vector[iJet] =  evt.jet()[iJet].mom().M();
    jetPt_vector[iJet]   =  evt.jet()[iJet].mom().Pt();
    jetEta_vector[iJet]  =  evt.jet()[iJet].mom().Eta();
    jetPhi_vector[iJet]  =  evt.jet()[iJet].mom().Phi();
  }//for

  int maxNjet = (evt.jet().size()<6) ? evt.jet().size() : 6;
  for (int i = 0; i < maxNjet; ++i){

     std::string nameJet_m = "jet" + std::to_string(i)+"_m";
     h->h1D(nameJet_m, "", suffix)->Fill(jetMass_vector[i]*1e-3, weight);

     std::string nameJet_pt = "jet" + std::to_string(i)+"_pt";
     h->h1D(nameJet_pt, "", suffix)->Fill(jetPt_vector[i]*1e-3, weight);

     std::string nameJet_eta = "jet" + std::to_string(i)+"_eta";
     h->h1D(nameJet_eta, "", suffix)->Fill(jetEta_vector[i], weight);

     std::string nameJet_phi = "jet" + std::to_string(i)+"_phi";
     h->h1D(nameJet_phi, "", suffix)->Fill(jetPhi_vector[i], weight);

  }//for

  float mtt = -1;

  //missing et
  h->h1D("MET", "", suffix)->Fill(evt.met().Perp()*1e-3, weight);
  h->h1D("MET_phi", "", suffix)->Fill(evt.met().Phi(), weight);
  
  //transverse W mass
  h->h1D("mwt", "", suffix)->Fill(sqrt(2. * l.Perp() * evt.met().Perp() * (1. - cos(l.Phi() - evt.met().Phi())))*1e-3, weight);
  
  //mu
  h->h1D("mu", "", suffix)->Fill(evt.mu()*1.16, weight);
  h->h1D("mu_original", "", suffix)->Fill(evt.mu_original(), weight);

  //npv
  h->h1D("npv", "", suffix)->Fill(evt.npv(), weight);

  //z prosition of primary vertex
  h->h1D("vtxz", "", suffix)->Fill(evt.vtxz(), weight);

  h->h1D("DeltaM_hadtopleptop", "", suffix)->Fill( evt.DeltaM_hadtopleptop(), weight );
  h->h1D("DeltaR_bjet1bjet2", "", suffix)->Fill( evt.DeltaR_bjet1bjet2(), weight );
  h->h1D("minDeltaR_hadWbjet", "", suffix)->Fill( evt.minDeltaR_hadWbjet(), weight );
  h->h1D("minDeltaR_lepbjet", "", suffix)->Fill( evt.minDeltaR_lepbjet(), weight );
  h->h1D("mass_lepb", "", suffix)->Fill( evt.mass_lepb(), weight );
  h->h1D("DeltaR_lepnu", "", suffix)->Fill( evt.DeltaR_lepnu(), weight );

  
  
  //deltaR between lepton and the closest narrow jet
  float closejl_deltaR  = 99;
  float deltaR_tmp      = 99;
  int closejl_idx       = -1;

  size_t jet_idx = 0;
  for (; jet_idx < evt.jet().size(); ++jet_idx){
    deltaR_tmp = 99;
    float dphi = l.DeltaPhi(evt.jet()[jet_idx].mom());
    float dy = l.Rapidity() - evt.jet()[jet_idx].mom().Rapidity();
    deltaR_tmp = std::sqrt(dy*dy + dphi*dphi);
    if (deltaR_tmp < closejl_deltaR){
        closejl_deltaR = deltaR_tmp;
        closejl_idx = jet_idx;
    }
  }//for

  float closejl_pt = -1;
  if (closejl_idx>0)    closejl_pt = evt.jet()[closejl_idx].mom().Perp();
  h->h1D("closejl_minDeltaR", "", suffix)->Fill(closejl_deltaR, weight);
  h->h1D("closejl_minDeltaR_effBins", "", suffix)->Fill(closejl_deltaR, weight);
  h->h1D("closejl_pt", "", suffix)->Fill(closejl_pt*1e-3, weight);

  //  h->h1D("trueMtt", "", suffix)->Fill(evt.MC_ttbar_beforeFSR().M()*1e-3, weight);
  //  _tree_truemtt = evt.MC_ttbar_beforeFSR().M()*1e-3;


 
  if (m_boosted && isBoosted && (evt.passes("ejets") || evt.passes("mujets"))) {
    size_t close_idx = 0;
    for (; close_idx < evt.jet().size(); ++close_idx)
      //      if (evt.jet()[close_idx].closeToLepton())
      if (l.DeltaPhi(evt.jet()[close_idx].mom()) < 1.0)
    	break;
    const TLorentzVector &sj = evt.jet()[close_idx].mom();
    h->h1D("closeJetPt", "", suffix)->Fill(sj.Perp()*1e-3, weight);

    // Large jets (the naming is a bit misleading. In the n-tuple boostedW's are LargeJets or FatJets)
    int goodljet_idx = 0;
    for (; goodljet_idx < evt.boostedW().size(); ++goodljet_idx) {
      //std::cout << "jet " << goodljet_idx << " pt = " << evt.boostedW()[goodljet_idx].mom().Perp() << ", good = " << evt.boostedW()[goodljet_idx].good() << std::endl;
      if (evt.boostedW()[goodljet_idx].good())
        break;
    }
    //    std::cout << "DEBUG :: 1 > idx > "<< goodljet_idx << std::endl;//" eta : > "<<evt.boostedW()[goodljet_idx].mom().Eta() << std::endl;
    const TLorentzVector &lj = evt.boostedW()[goodljet_idx].mom();
    h->h1D("nLargeJet","",suffix)->Fill(evt.boostedW().size(),weight);
    h->h1D("largeJetPt", "", suffix)->Fill(lj.Perp()*1e-3, weight);
    h->h1D("largeJetM", "", suffix)->Fill(lj.M()*1e-3, weight);
    //    h->h1D("largeJetEta", "", suffix)->Fill(lj.Eta(), weight);
    h->h1D("largeJetPhi", "", suffix)->Fill(lj.Phi(), weight);
    //    h->h1D("largeJet_tau32_wta", "", suffix)->Fill(evt.boostedW()[goodljet_idx].subs("tau32_wta"), weight);
    //    std::cout << "DEBUG :: 2"<< std::endl;    
    // boosted W
    // W candidate implementation as described here:
    // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/Run2VLQWbX
    // returns exactly one possible boosted W candidate
    const TLorentzVector boostedWcand = m_vlqutils.boostedWcandidate(evt.boostedW(), "loose");

    //nBoostedWs
    // returns all possible boosted Ws passing the specified selection
    int nWtags = m_vlqutils.boostedWtags(evt.boostedW(),"loose");

    //    std::cout <<"DEBUG :: ana > "<< nWtags<< std::endl;    
    //W-tagging
    h->h1D("nBoostedW", "", suffix)->Fill(nWtags, weight);
    h->h1D("nBoostedWcand", "", suffix)->Fill(1, weight);
    h->h1D("boostedW_pt", "", suffix)->Fill(boostedWcand.Perp()*1e-3, weight);
    h->h1D("boostedW_m", "", suffix)->Fill(boostedWcand.M()*1e-3, weight);
    h->h1D("boostedW_eta", "", suffix)->Fill(boostedWcand.Eta(), weight);
    h->h1D("boostedW_phi", "", suffix)->Fill(boostedWcand.Phi(), weight);

    
    const TLorentzVector hadT = evt.hadronicT()[0].mom();
    h->h1D("nhadronicT", "", suffix)->Fill(1, weight);
    h->h1D("hadronicT_pt", "", suffix)->Fill(hadT.Perp()*1e-3, weight);
    h->h1D("hadronicT_m", "", suffix)->Fill(hadT.M()*1e-3, weight);
    //h->h1D("hadronicT_eta", "", suffix)->Fill(hadT.Eta(), weight);
    h->h1D("hadronicT_phi", "", suffix)->Fill(hadT.Phi(), weight);

    const TLorentzVector hadW = evt.hadronicW()[0].mom();
    h->h1D("nhadronicW", "", suffix)->Fill(1, weight);
    h->h1D("hadronicW_pt", "", suffix)->Fill(hadW.Perp()*1e-3, weight);
    h->h1D("hadronicW_m", "", suffix)->Fill(hadW.M()*1e-3, weight);
    //h->h1D("hadronicW_eta", "", suffix)->Fill(hadW.Eta(), weight);
    h->h1D("hadronicW_phi", "", suffix)->Fill(hadW.Phi(), weight);
    
    const TLorentzVector lepT = evt.leptonicT()[0].mom();
    h->h1D("nleptonicT", "", suffix)->Fill(1, weight);
    h->h1D("leptonicT_pt", "", suffix)->Fill(lepT.Perp()*1e-3, weight);
    h->h1D("leptonicT_m", "", suffix)->Fill(lepT.M()*1e-3, weight);
    //h->h1D("leptonicT_eta", "", suffix)->Fill(lepT.Eta(), weight);
    h->h1D("leptonicT_phi", "", suffix)->Fill(lepT.Phi(), weight);

    const TLorentzVector lepW = evt.leptonicW()[0].mom();
    h->h1D("nleptonicW", "", suffix)->Fill(1, weight);
    h->h1D("leptonicW_pt", "", suffix)->Fill(lepW.Perp()*1e-3, weight);
    h->h1D("leptonicW_m", "", suffix)->Fill(lepW.M()*1e-3, weight);
    //h->h1D("leptonicW_eta", "", suffix)->Fill(lepW.Eta(), weight);
    h->h1D("leptonicW_phi", "", suffix)->Fill(lepW.Phi(), weight);
    
    // recalc. mtt
    // lepton = l
    // large-R jet = hadronic top = lj
    // selected jet = leptonic top's b-jet = sj
    // neutrino px, py = met
    // std::vector<TLorentzVector*> vec_nu = m_neutrinoBuilder.candidatesFromWMass_Rotation(&l, evt.met().Perp(), evt.met().Phi(), true);
    // TLorentzVector nu(0,0,0,0);
    // if (vec_nu.size() > 0) {
    //   nu = *(vec_nu[0]);
    //   for (size_t z = 0; z < vec_nu.size(); ++z) delete vec_nu[z];
    //   vec_nu.clear();
    // }
    // //    if (evt.largeJet().size()!=0)    mtt = (lj+sj+nu+l).M();
    // //    h->h1D("mtt", "", suffix)->Fill(mtt*1e-3, weight);
    // //    h->h1D("mtlep_boo", "", suffix)->Fill((sj+nu+l).M()*1e-3, weight);

    // _tree_mtt = mtt*1e-3;
    // _tree_weight = weight;
    // _tree_cat = -1;
    // if (evt.passes("ejets") && m_boosted && m_electron) _tree_cat = 0;
    // if (evt.passes("mujets") && m_boosted && !m_electron) _tree_cat = 1;
    // _tree_syst = s;
    // h->m_tree->Fill();
  }



  else if (!m_boosted && !isBoosted && (evt.passes("ejets") || evt.passes("mujets")) ) {

    // // inputs
    // // LEPTON --> TLorentzVector for your lepton
    // // vjets -->  std::vector<TLorentzVector*> for the jets
    // // vjets_btagged --> std::vector<bool> to say if the jets are btagged or not
    // // met --> TLorentzVector for your MET

    // // outputs, they will be filled by the TTBarLeptonJetsBuilder_chi2
    // int  igj3, igj4; // index for the Whad
    // int igb3, igb4; // index for the b's
    // int  ign1;  // index for the neutrino (because chi2 can test both pz solution)
    // double chi2ming1, chi2ming1H, chi2ming1L;

    // std::vector<TLorentzVector *> vjets;
    // std::vector<bool> vjets_btagged;
    // for (size_t z = 0; z < evt.jet().size(); ++z) {
    //   vjets.push_back(new TLorentzVector(0,0,0,0));
    //   vjets[z]->SetPtEtaPhiE(evt.jet()[z].mom().Perp(), evt.jet()[z].mom().Eta(), evt.jet()[z].mom().Phi(), evt.jet()[z].mom().E());
    //   // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTagingxAODEDM
    //   // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarks
    //   vjets_btagged.push_back(evt.jet()[z].btag_mv2c20_60());
    // }
    // TLorentzVector met = evt.met();
    // bool status = m_chi2.findMinChiSquare(&l, &vjets, &vjets_btagged, &met, igj3, igj4, igb3, igb4, ign1, chi2ming1, chi2ming1H, chi2ming1L);

    // float chi2Value = 1000000; // log10(1000000) = 6
    // float mwh = -1;
    // float mtl = -1;
    // float mth = -1;

    // if (status){
    //     chi2Value = chi2ming1;
    // 	mwh = m_chi2.getResult_Mwh();
    // 	mtl = m_chi2.getResult_Mtl();
    // 	mth = m_chi2.getResult_Mth();
    // 	mtt = m_chi2.getResult_Mtt();
    // }

    // for (size_t z = 0; z < vjets.size(); ++z) {
    //   delete vjets[z];
    // }
    // vjets.clear();
    // vjets_btagged.clear();

    // //Fill histograms
    // h->h1D("mtt", "", suffix)->Fill(mtt*1e-3, weight);
    // h->h1D("mtlep_res", "", suffix)->Fill(mtl*1e-3, weight);
    // h->h1D("mthad_res", "", suffix)->Fill(mth*1e-3, weight);
    // h->h1D("mwhad_res", "", suffix)->Fill(mwh*1e-3, weight);
    // h->h1D("chi2", "", suffix)->Fill(log10(chi2Value), weight);

    // _tree_mtt = mtt*1e-3;
    // _tree_weight = weight;
    // _tree_syst = s;
    // _tree_cat = -1;
    // if (evt.passes("ejets") && !m_boosted && m_electron) _tree_cat = 2;
    // if (evt.passes("mujets") && !m_boosted && !m_electron) _tree_cat = 3;
    // h->m_tree->Fill();
  }

}
