/**
 * @brief Lepton representation for information read off input file.
 * @author Steffen Henkelmann <steffen.henkelmann@cern.ch>
 */
#include "TopNtupleAnalysis/MObject.h"
#include "TopNtupleAnalysis/Lepton.h"
#include <cmath>
#include "TopNtupleAnalysis/Jet.h"
#include <iostream>

Lepton::Lepton()
  : MObject(),
    m_mi(-1) {
  m_type = MObject::lepton;
}

Lepton::Lepton(const TLorentzVector &v)
  : MObject(v, MObject::lepton),
    m_mi(-1) {
}

Lepton::~Lepton() {
}

void Lepton::setMI(float iso) {
  m_mi = iso;
}

float Lepton::mi() const {
  return m_mi;
}

void Lepton::setTight(int t) {  
  if (t==1)	m_isTight = true;
  else		m_isTight = false;
}

bool Lepton::isTight() const {
  return m_isTight;
}

bool Lepton::pass() const {
  if (mom().Perp() < 25e3) return false;
  if (mi()/mom().Perp() > 0.05) return false;
  return true;
}

int Lepton::charge() const {
  return m_charge;
}
int &Lepton::charge() {
  return m_charge;
}
