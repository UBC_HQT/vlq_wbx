/**
 * @brief Class that handles VLQ objects
 * @author Steffen Henkelmann
 */
#include "TopNtupleAnalysis/VLQUtils.h"
#include "TLorentzVector.h"
#include <iostream>
//#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"


VLQUtils::VLQUtils(){
   m_debug = -10;
   m_Ar_min = 10;
   m_DeltaM_min = 12e6;
   if(m_debug>0)std::cout << "in constructor " << std::endl;
}

VLQUtils::~VLQUtils(){
  if(m_debug>0) std::cout << "in destructor " << std::endl;
}


VLQUtils::VLQUtils(const VLQUtils& other) {
  m_debug = other.m_debug;
}


VLQUtils& VLQUtils::operator=(const VLQUtils& other) {
  if(this != &other) {
    m_debug = other.m_debug;
  }
  return *this;
}

bool VLQUtils::TTcandidates(const TLorentzVector* L, const double var1, const double var2, std::vector<TLorentzVector*>& NC){
  if(m_debug>0) std::cout << "In TTcandidates builder"<< std::endl;
}


std::map<std::string, TLorentzVector> VLQUtils::BoostedTTcandidates(const TLorentzVector hadW, const TLorentzVector lepW, std::vector<TLorentzVector> jets, std::vector<TLorentzVector> bJets, const bool BOOL1, std::string method, double OR_dRcut){
  return this->PseudoKLFitter(hadW, lepW, jets, bJets, true, method, OR_dRcut);
}


std::map<std::string, TLorentzVector>  VLQUtils::PseudoKLFitter(const TLorentzVector hadW, const TLorentzVector lepW, std::vector<TLorentzVector> jets,  std::vector<TLorentzVector> bJets, const bool BOOL1, std::string method, double OR_dRcut){
  //////
  //Implementation following Dan Marley (PyMiniaAna) for minimizing in the asymmetry plane
  //////
  m_Ar_min = 10;
  m_DeltaM_min = 12e6;
  std::map<std::string, TLorentzVector> ObjectCollection;
  std::vector<TLorentzVector> jetComb;
  TLorentzVector dummy(0,0,0,0);

  ObjectCollection["hadT"] = dummy;
  ObjectCollection["lepT"] = dummy;
  ObjectCollection["hadW"] = dummy;
  ObjectCollection["lepW"] = dummy;
  ObjectCollection["hadb"] = dummy;
  ObjectCollection["lepb"] = dummy;

  //  1. Match small-r jets without b-tagging
  size_t jidx2 = 0;
  if(bJets.size() < 1) {
    for ( size_t jidx1 = 0; jidx1 < jets.size(); ++jidx1){
       for ( size_t jidx2 = 0; jidx2 < jets.size(); ++jidx2){
	 jetComb.push_back(jets[jidx1]); jetComb.push_back(jets[jidx2]);
	 if (method.find("asymmmin") != std::string::npos)
	   ObjectCollection = this->GetCandidatesAsymmMin(hadW, lepW, jetComb, ObjectCollection);
	 else if (method.find("massdiff") != std::string::npos)
	   ObjectCollection = this->GetCandidatesDeltaMassMin(hadW, lepW, jetComb, ObjectCollection);
	 else
	   std::cerr << "ERROR :: None or wrong reconstruction method selected"<< std::endl;
	 jetComb.clear();
       }
    }
  }
  //  2. Match one b-candidate to the b-tagged jet
  else if(bJets.size() == 1) {
    for ( size_t idx = 0; idx < jets.size(); ++idx){
      //(SH) Avoid double counting the bjet and remove the possible jet candidate that overlaps with the boosted hadronic W candidate
      if(jets[idx] == bJets.at(0) || jets[idx].DeltaR(hadW) < OR_dRcut) continue;
      //TOP 2016      if( jets[idx] == bJets.at(0) ) continue;
      jetComb.push_back(jets[idx]); jetComb.push_back(bJets.at(0));
      if (method.find("asymmmin") != std::string::npos)
	ObjectCollection = this->GetCandidatesAsymmMin(hadW, lepW, jetComb, ObjectCollection);
      else if (method.find("massdiff") != std::string::npos)
	ObjectCollection = this->GetCandidatesDeltaMassMin(hadW, lepW, jetComb, ObjectCollection);
      else
	std::cerr << "ERROR :: None or wrong reconstruction method selected"<< std::endl;
      jetComb.clear();
    }
  }
  //  3. Match b-candidates to the 2 leading b-tagged jets
  else {
    jetComb.push_back(bJets.at(0)); jetComb.push_back(bJets.at(1));
    if (method.find("asymmmin") != std::string::npos)
      ObjectCollection = this->GetCandidatesAsymmMin(hadW, lepW, jetComb, ObjectCollection);
    else if (method.find("massdiff") != std::string::npos)
      ObjectCollection = this->GetCandidatesDeltaMassMin(hadW, lepW, jetComb, ObjectCollection);
    else
      std::cerr << "ERROR :: None or wrong reconstruction method selected"<< std::endl;
  }
  return ObjectCollection;
}


std::map<std::string, TLorentzVector>  VLQUtils::GetCandidatesAsymmMin(const TLorentzVector hadW, const TLorentzVector lepW, std::vector<TLorentzVector> jets, std::map<std::string, TLorentzVector>  ObjectCollection){

  std::cout <<"DEBUG :: asymmmin " << std::endl;
  std::vector<TLorentzVector> hadbs, lepbs;

  std::vector<float> deltaR_had, deltaR_lep;
  deltaR_had.push_back( hadW.DeltaR(jets.at(0)) );
  deltaR_had.push_back( hadW.DeltaR(jets.at(1)) );
  if(m_debug > 0) std::cout <<"DEBUG (GetCandidates) :: "<< deltaR_had.at(0)<<", " <<deltaR_had.at(1) << std::endl;
  deltaR_lep.push_back( lepW.DeltaR(jets.at(1)) );
  deltaR_lep.push_back( lepW.DeltaR(jets.at(0)) );

  //Calculating asymmetry values
  std::vector<float> AdeltaR, Am, Ar;
  AdeltaR.push_back( fabs(deltaR_had.at(0) - deltaR_lep.at(0)) / fabs(deltaR_had.at(0) + deltaR_lep.at(0)) );
  AdeltaR.push_back( fabs(deltaR_had.at(1) - deltaR_lep.at(1)) / fabs(deltaR_had.at(1) + deltaR_lep.at(1)) );

  Am.push_back( this->CalcMassAsymmetry(hadW + jets.at(0), lepW + jets.at(1)) );
  Am.push_back( this->CalcMassAsymmetry(hadW + jets.at(1), lepW + jets.at(0)) );

  Ar.push_back( sqrt( AdeltaR.at(0)*AdeltaR.at(0) + Am.at(0)*Am.at(0) ) );
  Ar.push_back( sqrt( AdeltaR.at(1)*AdeltaR.at(1) + Am.at(1)*Am.at(1) ) );

  hadbs.push_back(jets.at(0));  hadbs.push_back(jets.at(1));
  lepbs.push_back(jets.at(1));  lepbs.push_back(jets.at(0));

  auto ArMin = std::min_element(std::begin(Ar), std::end(Ar));
  int Ar_idx = std::distance(std::begin(Ar), ArMin);
  if(m_debug > 0) std::cout << "DEBUG :: min element is " << *ArMin << " at position " << std::distance(std::begin(Ar), ArMin) << std::endl;
   if( *ArMin < m_Ar_min){
     m_Ar_min = *ArMin;
     ObjectCollection["hadT"] = (hadW + hadbs.at(Ar_idx));
     ObjectCollection["lepT"] = (lepW + lepbs.at(Ar_idx));
     ObjectCollection["hadW"] = hadW;
     ObjectCollection["lepW"] = hadW;
     ObjectCollection["hadb"] = hadbs.at(Ar_idx);
     ObjectCollection["lepb"] = lepbs.at(Ar_idx);
  }
  return ObjectCollection;
}

std::map<std::string, TLorentzVector>  VLQUtils::GetCandidatesDeltaMassMin(const TLorentzVector hadW, const TLorentzVector lepW, std::vector<TLorentzVector> jets, std::map<std::string, TLorentzVector>  ObjectCollection){

  std::vector<TLorentzVector> hadbs, lepbs;

  //Calculating asymmetry values
  std::vector<float> deltaM;
  deltaM.push_back( this->CalcMassDifference(hadW + jets.at(0), lepW + jets.at(1)) );
  deltaM.push_back( this->CalcMassDifference(hadW + jets.at(1), lepW + jets.at(0)) );

  hadbs.push_back(jets.at(0));  hadbs.push_back(jets.at(1));
  lepbs.push_back(jets.at(1));  lepbs.push_back(jets.at(0));

  auto deltaMmin = std::min_element(std::begin(deltaM), std::end(deltaM));
  int deltaM_idx = std::distance(std::begin(deltaM), deltaMmin);
  if(m_debug > 0) {
    std::cout << "DEBUG :: candidates hadW :> 1 > "<< (hadW + jets.at(0)).M() << " 2 > "<< (lepW + jets.at(1)).M() <<std::endl;
    std::cout << "DEBUG :: candidates lepW :> 1 > "<< (hadW + jets.at(1)).M() << " 2 > "<< (lepW + jets.at(0)).M() <<std::endl;
    std::cout << "DEBUG :: candidates :> dM 1 > " << deltaM.at(0) << " dM 2 > "<< deltaM.at(1) <<std::endl;
    std::cout << "DEBUG :: min element is " << *deltaMmin << " at position " << std::distance(std::begin(deltaM), deltaMmin) << std::endl;
    std::cout << "DEBUG :: glob var name m_ : " << m_DeltaM_min << std::endl;
  }
  if( *deltaMmin < m_DeltaM_min){
    m_DeltaM_min = *deltaMmin;
    ObjectCollection["hadT"] = (hadW + hadbs.at(deltaM_idx));
    ObjectCollection["lepT"] = (lepW + lepbs.at(deltaM_idx));
    ObjectCollection["hadW"] = hadW;
    ObjectCollection["lepW"] = hadW;
    ObjectCollection["hadb"] = hadbs.at(deltaM_idx);
    ObjectCollection["lepb"] = lepbs.at(deltaM_idx);
  }

  return ObjectCollection;
}



const TLorentzVector VLQUtils::boostedWcandidate(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, TLorentzVector lep, bool eEvent, std::string sel) {
  // (SH) W candidate implementation as described here:
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/Run2VLQWbX
  TLorentzVector Wcandidate(0,0,0,0);
  double Wmass_PDG = 80385;
  std::map<int, double> goodBoostedW;
  int nWtags = 0;
  size_t boostedW_idx = 0;
  for (; boostedW_idx< obj.size(); ++boostedW_idx) {
    if(m_debug>0) std::cout << "DEBUG :: ALL candidates for boosted W > \n No. " << boostedW_idx << ", good = " << obj[boostedW_idx].good() <<
		    " pt = " << obj[boostedW_idx].mom().Perp()*1e-3 <<
		    " M  = " << obj[boostedW_idx].mom().M()*1e-3 <<
		    " eta = "<< obj[boostedW_idx].mom().Eta() << std::endl;

    if(sel.find("MORIOND2017") != std::string::npos){// possibly other selections to be included
      if (obj[boostedW_idx].good() && obj[boostedW_idx].passWtagLoose()){
	  if(m_debug>0) std::cout << "DEBUG :: SELECTED candidates boosted W > \n No. " << boostedW_idx << ", good = " << obj[boostedW_idx].good() <<
			  " pt = " << obj[boostedW_idx].mom().Perp()*1e-3 <<
			  " M  = " << obj[boostedW_idx].mom().M()*1e-3 <<
			  " eta = "<< obj[boostedW_idx].mom().Eta() << std::endl;

	  if(eEvent){
	    //	  std::cout <<"electron event" << std::endl;
	    if(obj[boostedW_idx].mom().DeltaR(lep) > 1.0){
	      goodBoostedW[boostedW_idx] = (std::abs(obj[boostedW_idx].mom().M()-Wmass_PDG));
	      ++nWtags;
	    }else {
	      if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
	    }
	  }else{
	      goodBoostedW[boostedW_idx] = (std::abs(obj[boostedW_idx].mom().M()-Wmass_PDG));
	      ++nWtags;
	    }
      }
    } else if(sel.find("INTERNAL_WTX_WtagMed") != std::string::npos){// possibly other selections to be included
      if ( obj[boostedW_idx].good() && obj[boostedW_idx].passWtagMedForWTX()){
        if(m_debug > 0) std::cout << "DEBUG :: SELECTED candidates boosted W > \n No. " << boostedW_idx << ", good = " << obj[boostedW_idx].good() <<
                          " pt = " << obj[boostedW_idx].mom().Perp()*1e-3 <<
                          " M  = " << obj[boostedW_idx].mom().M()*1e-3 <<
                          " eta = "<< obj[boostedW_idx].mom().Eta() << std::endl;
        if(eEvent){
          //      std::cout <<"electron event" << std::endl;
          if(obj[boostedW_idx].mom().DeltaR(lep) > 1.0){
	    goodBoostedW[boostedW_idx] = (std::abs(obj[boostedW_idx].mom().M()-Wmass_PDG));
            ++nWtags;
          }else {
            if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
          }
        }else{
	  goodBoostedW[boostedW_idx] = (std::abs(obj[boostedW_idx].mom().M()-Wmass_PDG));
          ++nWtags;
        }
      }
    }

else {
      if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
    }
    // Matthias: adding new configuration for optimisation studies
    if(sel.find("Checks") != std::string::npos){// possibly other selections to be included
      if (obj[boostedW_idx].good() && obj[boostedW_idx].passWtagTight()){
	if(m_debug>0) std::cout << "DEBUG :: SELECTED candidates boosted W > \n No. " << boostedW_idx << ", good = " << obj[boostedW_idx].good() <<
			" pt = " << obj[boostedW_idx].mom().Perp()*1e-3 <<
			" M  = " << obj[boostedW_idx].mom().M()*1e-3 <<
			" eta = "<< obj[boostedW_idx].mom().Eta() << std::endl;
	goodBoostedW[boostedW_idx] = (std::abs(obj[boostedW_idx].mom().M()-Wmass_PDG));
	++nWtags;
      }
    }else {
      if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
    }
  }

  // If more than 1 large-R jet fulfills the above requirement, the one with a mass closest to that of the W boson is selected as the event's W-candidate.
  if(nWtags > 1){
    auto max=get_min(goodBoostedW);
    if(m_debug>0) std::cout << "DEBUG :: W candidate selected at "<< max.first << " with mass > " <<  obj[max.first].mom().M() << std::endl;
    Wcandidate = obj[max.first].mom();
  }else if(nWtags == 1){
    // the following works since goodBoostedW is of size one
    auto ele = get_min(goodBoostedW);
    if(m_debug>0) std::cout <<"DEBUG :: goodBoostedW size : "<<obj[ele.first].mom().M()<<  std::endl;
    Wcandidate = obj[ele.first].mom();
  }
  else{
    if(m_debug>0) std::cout << "WARNING :: no suitable W candidate with " << sel << " selection found" << std::endl;
  }

  //  std::cout << "DEBUG :: (candidate) nwtags :>> "<< nWtags<< std::endl;
  return Wcandidate;
}



int VLQUtils::boostedWtags(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, TLorentzVector lep, bool eEvent, std::string sel){
  // W candidate implementation as described here:
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/Run2VLQWbX
  int nWtags = 0;
  size_t boostedW_idx = 0;
  for (; boostedW_idx< obj.size(); ++boostedW_idx) {
    if(m_debug>0) std::cout << "DEBUG :: ALL candidates for boosted W > \n No. " << boostedW_idx << ", good = " << obj[boostedW_idx].good() <<
		    " pt = " << obj[boostedW_idx].mom().Perp()*1e-3 <<
		    " M  = " << obj[boostedW_idx].mom().M()*1e-3 <<
		    " eta = "<< obj[boostedW_idx].mom().Eta() << std::endl;
    if(sel.find("Checks") != std::string::npos){
      if (obj[boostedW_idx].good() && obj[boostedW_idx].passWtagLoose()) ++nWtags;
    }
    if(sel.find("MORIOND2017") != std::string::npos){// possibly other selections to be included
      if ( obj[boostedW_idx].good() && obj[boostedW_idx].passWtagLoose()){
	  //      if (obj[boostedW_idx].mom().M() > 50000. && obj[boostedW_idx].isWTaggedMed() > 0 && obj[boostedW_idx].isSmoothTopTagged_50() == 0){
	  //      if (obj[boostedW_idx].mom().M() > 50000. && obj[boostedW_idx].mom().Perp()*1e-3 > 200. && obj[boostedW_idx].isWTaggedMed() > 0 && obj[boostedW_idx].isSmoothTopTagged_50() == 0){
	  //      if (obj[boostedW_idx].mom().M() > 50000. && obj[boostedW_idx].mom().Perp()*1e-3 > 200. && obj[boostedW_idx].isWTaggedMed() > 0 ){
	  //      if (obj[boostedW_idx].mom().M() > 50000. && obj[boostedW_idx].mom().Perp()*1e-3 > 200. && obj[boostedW_idx].isSmoothTopTagged_50() == 0){
	  //      if (obj[boostedW_idx].mom().M() > 50000. && obj[boostedW_idx].mom().Perp()*1e-3 > 200. ){
	  //      if (obj[boostedW_idx].mom().M() > 50000.){
	if(m_debug > 0) std::cout << "DEBUG :: SELECTED candidates boosted W > \n No. " << boostedW_idx << ", good = " << obj[boostedW_idx].good() <<
			  " pt = " << obj[boostedW_idx].mom().Perp()*1e-3 <<
			  " M  = " << obj[boostedW_idx].mom().M()*1e-3 <<
			  " eta = "<< obj[boostedW_idx].mom().Eta() << std::endl;
	if(eEvent){
	  //	  std::cout <<"electron event" << std::endl;
	  if(obj[boostedW_idx].mom().DeltaR(lep) > 1.0){
	    ++nWtags;
	  }else {
	    if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
	  }
	}else{
	  ++nWtags;
	}
      }
    } else if(sel.find("INTERNAL_WTX_WtagMed") != std::string::npos){// possibly other selections to be included
      if ( obj[boostedW_idx].good() && obj[boostedW_idx].passWtagMedForWTX()){
        if(m_debug > 0) std::cout << "DEBUG :: SELECTED candidates boosted W > \n No. " << boostedW_idx << ", good = " << obj[boostedW_idx].good() <<
                          " pt = " << obj[boostedW_idx].mom().Perp()*1e-3 <<
                          " M  = " << obj[boostedW_idx].mom().M()*1e-3 <<
                          " eta = "<< obj[boostedW_idx].mom().Eta() << std::endl;
        if(eEvent){
          //      std::cout <<"electron event" << std::endl;
          if(obj[boostedW_idx].mom().DeltaR(lep) > 1.0){
            ++nWtags;
          }else {
            if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
          }
        }else{
          ++nWtags;
        }
      }
    } else{
      if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
    }
  }
  //  std::cout << "DEBUG :: nwtags :>> "<< nWtags<< std::endl;
  return nWtags;
}


const TLorentzVector VLQUtils::boostedTcandidate(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, std::string sel) {
  // (SH) T candidate implementation as described here:
  TLorentzVector Tcandidate(0,0,0,0);
  double Tmass_PDG = 173210;
  std::map<int, double> goodBoostedTop;
  int nTopTags = 0;
  size_t boostedTop_idx = 0;
  for (; boostedTop_idx< obj.size(); ++boostedTop_idx) {
    if(sel.find("INTERNAL_Smooth50") != std::string::npos){// possibly other selections to be included
      if ( obj[boostedTop_idx].good() &&
	   obj[boostedTop_idx].passTopTag50()){
	goodBoostedTop[boostedTop_idx] = (std::abs(obj[boostedTop_idx].mom().M()-Tmass_PDG));
	++nTopTags;
      }
    } else if(sel.find("INTERNAL_Smooth80") != std::string::npos){// possibly other selections to be included
      if ( obj[boostedTop_idx].good() &&
           obj[boostedTop_idx].passTopTag80()){
	goodBoostedTop[boostedTop_idx] = (std::abs(obj[boostedTop_idx].mom().M()-Tmass_PDG));
	++nTopTags;
      }
    } else{
      if(m_debug==-4) std::cout << "WARNING :: no suitable Top candidate selection selected " << std::endl;
    }
  }
  // If more than 1 large-R jet fulfills the above requirement, the one with a mass closest to that of the Top boson is selected as the event's Top-candidate.
  if(nTopTags > 1){
    auto max=get_min(goodBoostedTop);
    if(m_debug==-4) std::cout << "DEBUG :: Top candidate selected at "<< max.first << " with mass > " <<  obj[max.first].mom().M() << std::endl;
    Tcandidate = obj[max.first].mom();
  }else if(nTopTags == 1){
    // the following works since goodBoostedTop is of size one
    auto ele = get_min(goodBoostedTop);
    if(m_debug==-4) std::cout <<"DEBUG :: goodBoostedTop size : "<<obj[ele.first].mom().M()<<  std::endl;
    Tcandidate = obj[ele.first].mom();
  }
  else{
    if(m_debug==-4) std::cout << "WARNING :: no suitable Top candidate with " << sel << " selection found" << std::endl;
  }
  return Tcandidate;
}



int VLQUtils::boostedTopTags(const std::vector<LargeJet> &obj, std::vector<TLorentzVector> bJets, std::string sel){
  // T candidate implementation as described here:
  int nTopTags = 0;
  size_t boostedTop_idx = 0;
  for (; boostedTop_idx< obj.size(); ++boostedTop_idx) {
    if(sel.find("INTERNAL_Smooth50") != std::string::npos){// possibly other selections to be included
      if ( obj[boostedTop_idx].good() &&
	   obj[boostedTop_idx].passTopTag50()){

	  ++nTopTags;
      }
    } else if(sel.find("INTERNAL_Smooth80") != std::string::npos){// possibly other selections to be included
      if ( obj[boostedTop_idx].good() &&
           obj[boostedTop_idx].passTopTag80()){

	++nTopTags;
      }
    } else{
      if(m_debug>0) std::cout << "WARNING :: no suitable W candidate selection selected " << std::endl;
    }
  }
  //  std::cout << "DEBUG :: nwtags :>> "<< nWtags<< std::endl;
  return nTopTags;
}




const TLorentzVector VLQUtils::resolvedWcandidate(const std::vector<Jet> &jet,  std::vector<TLorentzVector> jets, std::vector<TLorentzVector> bJets, std::string sel){

  TLorentzVector Wcandidate(0,0,0,0);
  std::vector<TLorentzVector> jetComb;
  std::vector<TLorentzVector> resWhadCandidates;
  TLorentzVector resWhad;
  double Wmass_PDG = 80385;
  double dR_cut = 1.2;
  std::map<int, double> goodResolvedW;
  int nWcand = 0;
   //  1. Match small-r jets without b-tagging (should never be the case since selection requires >= 1 bjet)
  size_t jidx2 = 0;
  if(bJets.size() < 1) {
    nWcand = 0;
    for ( size_t jidx1 = 0; jidx1 < jets.size(); ++jidx1){
       for ( size_t jidx2 = 0; jidx2 < jets.size(); ++jidx2){
	 if(jets[jidx1].DeltaR(jets[jidx2]) < dR_cut){
	   resWhadCandidates.push_back((jets[jidx1] + jets[jidx2]));
	   if(m_debug > 2)   std::cout << "DEBUG :: >>>> M : > "<< (jets[jidx1] + jets[jidx2]).M() << "  Diff: > "<< (jets[jidx1] + jets[jidx2]).M()-Wmass_PDG << std::endl;
	    goodResolvedW[nWcand] = (std::abs((jets[jidx1] + jets[jidx2]).M()-Wmass_PDG));
	    ++nWcand;
       }
    }
  }
  }
  //  2. Veto b-tagged jet and build a W candidate from left jets
  else if(bJets.size() == 1) {
    jidx2 = 0;
    nWcand = 0;
    if(m_debug == -3) std::cout <<"DEBUG :: No. jets :> " << jets.size() << std::endl;
    for ( size_t idx = 0; idx < jets.size(); ++idx){
    if(m_debug == -3)  std::cout <<"DEBUG :: No. bjets :> " << bJets.size() << std::endl;
    if(m_debug == -3)  std::cout <<"DEBUG :: jet idx :> " << idx << "  > "<<jets[idx].Perp()<<std::endl;
      if(jets[idx] == bJets.at(0))	continue; // Veto b-jet
      for ( size_t jidx2 = 0; jidx2 < jets.size(); ++jidx2){
	if(jets[idx] == jets[jidx2]) continue; // Avoid combination with same jets
	if(m_debug == -3)	std::cout <<"DEBUG :: " << bJets.size() << "  > "<< jets[idx].DeltaR(jets[jidx2]) << " jet1 : "<< jets[idx].Perp() << "  jet2 : "<< jets[jidx2].Perp() <<" M : "<< (jets[idx] + jets[jidx2]).M()<< std::endl;
	if(jets[idx].DeltaR(jets[jidx2]) < dR_cut){
	  if(m_debug == -3) std::cout <<"DEBUG :: pass dR" << bJets.size() << "  > "<< jets[idx].DeltaR(jets[jidx2]) << " jet1 : "<< jets[idx].Perp() << "  jet2 : "<< jets[jidx2].Perp() <<" M : "<< (jets[idx] + jets[jidx2]).M()<< std::endl;
	  resWhadCandidates.push_back((jets[idx] + jets[jidx2]));
	  goodResolvedW[nWcand] = (std::abs((jets[idx] + jets[jidx2]).M()-Wmass_PDG));
	  ++nWcand;

	}
      }
    }
  }
  //  3. Veto both leading b-tagged jets (pT) and build a W candidate from left jets
  else {
    jidx2 = 0;
    nWcand = 0;
   if(m_debug == -3)  std::cout <<"DEBUG :: No. jets :> " << jets.size() << std::endl;
    for ( size_t idx = 0; idx < jets.size(); ++idx){
     if(m_debug == -3)   std::cout <<"DEBUG :: No. bjets :> " << bJets.size() << std::endl;
     if(m_debug == -3)   std::cout <<"DEBUG :: jet idx :> " << idx << "  > "<<jets[idx].Perp()<<std::endl;
      if(jets[idx] == bJets.at(0) || jets[idx] == bJets.at(1))	continue;
       for ( size_t jidx2 = 0; jidx2 < jets.size(); ++jidx2){
	 if(m_debug == -3) std::cout <<"DEBUG :: "  << std::endl;
	 if(jets[idx] == jets[jidx2]) continue;
	 if(jets[jidx2] == bJets.at(0) || jets[jidx2] == bJets.at(1))	continue;
	 if(m_debug == -3)	 std::cout <<"DEBUG :: " << bJets.size() << "  > "<< jets[idx].DeltaR(jets[jidx2]) << " jet1 : "<< jets[idx].Perp() << "  jet2 : "<< jets[jidx2].Perp() <<" M : "<< (jets[idx] + jets[jidx2]).M()<< std::endl;
	 if(jets[idx].DeltaR(jets[jidx2]) < dR_cut){
	 if(m_debug == -3)   std::cout <<"DEBUG :: pass dR " << bJets.size() << "  > "<< jets[idx].DeltaR(jets[jidx2]) << " jet1 : "<< jets[idx].Perp() << "  jet2 : "<< jets[jidx2].Perp() <<" M : "<< (jets[idx] + jets[jidx2]).M()<< std::endl;
	   resWhadCandidates.push_back((jets[idx] + jets[jidx2]));
	   goodResolvedW[nWcand] = (std::abs((jets[idx] + jets[jidx2]).M()-Wmass_PDG));
	  ++nWcand;
	}
      }
    }
  }
  // // If more than 1 large-R jet fulfills the above requirement, the one with a mass closest to that of the W boson is selected as the event's W-candidate.
  if(nWcand > 1){
    auto max=get_min(goodResolvedW);
    if(m_debug == -3) std::cout << "DEBUG :: W candidate selected at "<< max.first << " with mass > " <<  resWhadCandidates.at(max.first).M() << std::endl;
    Wcandidate = resWhadCandidates.at(max.first);
  }else if(nWcand  == 1){
    // the following works since goodResolvedW is of size one
    auto ele = get_min(goodResolvedW);
    if(m_debug == -3) std::cout <<"DEBUG :: goodResolvedW size : "<<resWhadCandidates.at(ele.first).M()<<  std::endl;
    Wcandidate = resWhadCandidates.at(ele.first);
  }
  else{
   if(m_debug == -3) std::cout << "WARNING :: no suitable W candidate with " << sel << " selection found" << std::endl;
  }
  return Wcandidate;
}



const TLorentzVector VLQUtils::GetResolvedWCandidate(std::vector<TLorentzVector> jets, const TLorentzVector resWhad){


  double DeltaM =0;
  if(jets.size() > 1)
    DeltaM = (jets.at(0) + jets.at(1)).M();

  // std::vector<TLorentzVector> hadbs, lepbs;

  // std::vector<float> deltaR_had, deltaR_lep;
  // deltaR_had.push_back( hadW.DeltaR(jets.at(0)) );
  // deltaR_had.push_back( hadW.DeltaR(jets.at(1)) );
  // if(m_debug > 0) std::cout <<"DEBUG (GetCandidates) :: "<< deltaR_had.at(0)<<", " <<deltaR_had.at(1) << std::endl;
  // deltaR_lep.push_back( lepW.DeltaR(jets.at(1)) );
  // deltaR_lep.push_back( lepW.DeltaR(jets.at(0)) );

  // //Calculating asymmetry values
  // std::vector<float> AdeltaR, Am, Ar;
  // AdeltaR.push_back( fabs(deltaR_had.at(0) - deltaR_lep.at(0)) / fabs(deltaR_had.at(0) + deltaR_lep.at(0)) );
  // AdeltaR.push_back( fabs(deltaR_had.at(1) - deltaR_lep.at(1)) / fabs(deltaR_had.at(1) + deltaR_lep.at(1)) );

  // Am.push_back( this->CalcMassAsymmetry(hadW + jets.at(0), lepW + jets.at(1)) );
  // Am.push_back( this->CalcMassAsymmetry(hadW + jets.at(1), lepW + jets.at(0)) );

  // Ar.push_back( sqrt( AdeltaR.at(0)*AdeltaR.at(0) + Am.at(0)*Am.at(0) ) );
  // Ar.push_back( sqrt( AdeltaR.at(1)*AdeltaR.at(1) + Am.at(1)*Am.at(1) ) );

  // hadbs.push_back(jets.at(0));  hadbs.push_back(jets.at(1));
  // lepbs.push_back(jets.at(1));  lepbs.push_back(jets.at(0));

  // auto ArMin = std::min_element(std::begin(Ar), std::end(Ar));
  // int Ar_idx = std::distance(std::begin(Ar), ArMin);
  // if(m_debug > 0) std::cout << "DEBUG :: min element is " << *ArMin << " at position " << std::distance(std::begin(Ar), ArMin) << std::endl;
  //  if( *ArMin < m_Ar_min){
  //    m_Ar_min = *ArMin;
  //    ObjectCollection["hadT"] = (hadW + hadbs.at(Ar_idx));
  //    ObjectCollection["lepT"] = (lepW + lepbs.at(Ar_idx));
  //    ObjectCollection["hadW"] = hadW;
  //    ObjectCollection["lepW"] = hadW;
  //    ObjectCollection["hadb"] = hadbs.at(Ar_idx);
  //    ObjectCollection["lepb"] = lepbs.at(Ar_idx);
  // }
  return resWhad;
}








std::vector<TLorentzVector*> VLQUtils::RealTTcandidates(const TLorentzVector L, Double_t VAR1, Double_t VAR2, const bool BOOL1){
    if(m_debug>0) std::cout << "in RealTTCandidates()" << std::endl;

    // initialize
    Double_t m_mWpdg = 80.4;
    Double_t pxNu = VAR1 * cos(VAR2);
    Double_t pyNu = VAR1 * sin(VAR2);
    Double_t pzNu = -1000000;
    Double_t ptNu = VAR1;
    Double_t eNu;

    std::vector<TLorentzVector*> NC;

    //Calculate stuff


    return NC;
}

float VLQUtils::GetST(float lepPt, float MET, const std::vector<LargeJet> &lJet, const std::vector<Jet> &jet, std::vector<TLorentzVector>* bjet, int nBtags){
  float ST(0.);
  ST = lepPt + MET;
  // if (!nBtags){
    if(lJet.size() > 0){
      ST+= lJet[0].mom().Perp()*1e-3;
      for (size_t jidx = 0; jidx < jet.size(); ++jidx)
	if(jet[jidx].mom().DeltaR(lJet[0].mom()) > 1.)
	  ST += jet[jidx].mom().Perp()*1e-3;
    }else{
      for (size_t jidx = 0; jidx < jet.size(); ++jidx)
	ST += jet[jidx].mom().Perp()*1e-3;
    }

// }else{
//      if(lJet.size() >= 1){
//         HT+= lJet[0].mom().Perp()*1e-3;
// 	for (size_t bidx = 0; bidx < bjet->size(); ++bidx)
// 	  HT += bjet->at(bidx).Perp()*1e-3;
//      }else{
//       for (size_t jidx = 0; jidx < jet.size(); ++jidx)
// 	HT += jet[jidx].mom().Perp()*1e-3;
//       for (size_t bidx = 0; bidx < bjet->size(); ++bidx)
// 	HT += bjet->at(bidx).Perp()*1e-3;
//     }
//   }
  return ST;
}

float VLQUtils::GetSTboosted(float lepPt, float MET, const TLorentzVector lJet, const std::vector<Jet> &jet,  int nWtags){
  float ST(0.);
  ST = lepPt + MET;
    if(nWtags > 0){
      ST+= lJet.Perp()*1e-3;
      for (size_t jidx = 0; jidx < jet.size(); ++jidx)
	if(jet[jidx].mom().DeltaR(lJet) > 1.)
	  ST += jet[jidx].mom().Perp()*1e-3;
    }else{
      for (size_t jidx = 0; jidx < jet.size(); ++jidx)
	ST += jet[jidx].mom().Perp()*1e-3;
    }

  return ST;
}

float VLQUtils::GetSTSmallJets(float lepPt, float MET, const std::vector<Jet> &jet){
  float ST(0.);
  ST = lepPt + MET;
  for (size_t jidx = 0; jidx < jet.size(); ++jidx)
    ST += jet[jidx].mom().Perp()*1e-3;
  return ST;
}

float VLQUtils::CalcMassAsymmetry(const TLorentzVector obj1, const TLorentzVector obj2){

  float num, den;
  num = fabs( obj1.M() - obj2.M() );
  den = obj1.M() + obj2.M();

  return num/den;

}

float VLQUtils::CalcMassDifference(const TLorentzVector obj1, const TLorentzVector obj2){

  float num;
  num = fabs( obj1.M() - obj2.M() );

  return num;

}


double VLQUtils::deltaR(double etajet, double etalept, double phijet, double philept)
{
  double dPhi = TVector2::Phi_mpi_pi(phijet-philept);
  double dEta = std::fabs(etajet-etalept);
  double dR = std::sqrt(std::pow(dEta,2)+std::pow(dPhi,2));
  return dR;
}

