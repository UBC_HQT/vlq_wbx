#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <cmath>
#include <memory>
#include <algorithm>
#include <iterator>

#include "TFile.h"
#include "TH1F.h"
#include "TH1.h"

#include <iomanip>
#include <sstream>

#include <signal.h>

#include "Hist.h"
#include "SystematicImplementation.h"
#include "utils.h"
#include "SampleSet.h"
#include "SystematicCalculation.h"

#include "ParseUtils.h"

#include "TCanvas.h"
#include "TH1D.h"
#include "TH1.h"
#include "TH1F.h"
#include "THStack.h"
#include "TGraphErrors.h"

using namespace std;

int main(int argc, char **argv) {
  signal(SIGSEGV, handler);

  try {

    int help = 0;
    string channel = "e";
    string prefix = "boosted";
    string h_input = "lepPt";
    int mcOnly = 0;
    string _outfile = "";
    int verbose = 0;
    int underflow = 0;
    string _extraText = "";
    string yTitle = "Events";
    string xTitle = "";
    float xMax = -9999;
    float xMin = -9999;
    int mustBeBigger = 0;
    int posLegend = 0;
    int splitLegend = 0;
    int rebin = 1;
    float yMax = -1;
    float yMin = -1;
    int arrow = 0;
    int stamp = 0;
    float lumi = 0;
    std::string config = "";
    std::string saveTH1 = "";
    int normBinWidth = 0;
    int alternateStyle = 0;
    int saveYields = 0;
    int printYields = 0;
    int includeSignal = 0;
    int blind = 0;
    float scaleSignal = 0;
    float scaleSignalType2 = 0;
    int signalType2noScaling = 0;
    int _logY = 0;
    std::string outFormat = ".pdf";

    static struct extendedOption extOpt[] = {
        {"help",            no_argument,       &help,   1, "Display help", &help, extendedOption::eOTInt},
        {"channel",         required_argument,     0, 'c', "Channel: one of e, mu, comb", &channel, extendedOption::eOTString},
        {"prefix",          required_argument,     0, 'p', "Prefix.", &prefix, extendedOption::eOTString},
        {"histogram",       required_argument,     0, 'h', "Histogram name.", &h_input, extendedOption::eOTString},
        {"mcOnly",          required_argument,     0, 'm', "Only look for the ttbarMatched histograms? (0/1)", &mcOnly, extendedOption::eOTInt},
        {"outfile",         required_argument,     0, 'o', "Output file.", &_outfile, extendedOption::eOTString},
        {"verbose",         required_argument,     0, 'v', "Verbose. (0/1)", &verbose, extendedOption::eOTInt},
        {"underflow",       required_argument,     0, 'U', "Include underflow (0/1)", &underflow, extendedOption::eOTInt},
        {"extraText",       required_argument,     0, 'T', "Extra text to add in the plot.", &_extraText, extendedOption::eOTString},
        {"yTitle",          required_argument,     0, 'E', "Y title.", &yTitle, extendedOption::eOTString},
        {"xTitle",          required_argument,     0, 't', "X title.", &xTitle, extendedOption::eOTString},
        {"xMax",            required_argument,     0, 'X', "Limit X maximum value.", &xMax, extendedOption::eOTFloat},
        {"xMin",            required_argument,     0, 'x', "Limit X minimum value.", &xMin, extendedOption::eOTFloat},
        {"mustBeBigger",    required_argument,     0, 'M', "Widen range of the Y axis in the ratio plots.", &mustBeBigger, extendedOption::eOTInt},
        {"posLegend",       required_argument,     0, 'L', "Move legend to the left.", &posLegend, extendedOption::eOTInt},
	{"splitLegend",     required_argument,     0, 'Z', "Split legend between signal and background.", &splitLegend, extendedOption::eOTInt},
        {"rebin",           required_argument,     0, 'r', "Rebin by this factor.", &rebin, extendedOption::eOTInt},
        {"yMax",            required_argument,     0, 'Y', "Maximum of the Y axis.", &yMax, extendedOption::eOTFloat},
        {"yMin",            required_argument,     0, 'y', "Minimum of the Y axis.", &yMax, extendedOption::eOTFloat},
        {"arrow",           required_argument,     0, 'a', "Draw arrow.", &arrow, extendedOption::eOTInt},
        {"stamp",           required_argument,     0, 's', "0 = ATLAS Internal, 1 = ATLAS Preliminary.", &stamp, extendedOption::eOTInt},
        {"lumi",            required_argument,     0, 'l', "Luminosity value to show", &lumi, extendedOption::eOTFloat},
        {"config",          required_argument,     0, 'C', "Configuration file for items.", &config, extendedOption::eOTString},
        {"smoothen",        required_argument,     0, 'k', "Smoothen systematics.", &smooth, extendedOption::eOTInt},
        {"saveTH1",         required_argument,     0, 'R', "Save as ROOT files called hist_[name][sufix].root with all systs. Provide the sufix.", &saveTH1, extendedOption::eOTString},
        {"normBinWidth",    required_argument,     0, 'b', "Divide bin content by bin width?", &normBinWidth, extendedOption::eOTInt},
        {"alternateStyle",  required_argument,     0, 'A', "Alternative style", &alternateStyle, extendedOption::eOTInt},
	{"saveYields",      required_argument,     0, 'S', "Saving yields to .dat file", &saveYields, extendedOption::eOTInt},
	{"printYields",     required_argument,     0, 'P', "Print yields on plots", &printYields, extendedOption::eOTInt},
	{"includeSignal",   required_argument,     0, 'J', "Include signal in plots. To include an additional signal, set value to > 1", &includeSignal, extendedOption::eOTInt},
	{"scaleSignal",     required_argument,     0, 'n', "Scale the signal by a factor", &scaleSignal, extendedOption::eOTFloat},
	{"scaleSignalType2",     required_argument,0, 'N', "Scale the type 2 signal by a factor", &scaleSignalType2, extendedOption::eOTFloat},
	{"signalType2noScaling",required_argument, 0, 'G', "Do not scale signal type 2", &signalType2noScaling, extendedOption::eOTInt},
	{"blind",           required_argument,     0, 'B', "Don\'t draw data in signal region", &blind, extendedOption::eOTInt},
	{"logY",            required_argument,     0, 'g', "Y axis in log?", &_logY, extendedOption::eOTInt},
        {"outFormat",       required_argument,     0, 'z', "Choose which output format you want to have.", &outFormat, extendedOption::eOTString},

        {0, 0, 0, 0, 0, 0, extendedOption::eOTInt}
      };


    if (!parseArguments(argc, argv, extOpt) || help) {
      dumpHelp(std::string(argv[0]), extOpt, "plot\nCalculate systematic uncertainties and make histograms with them.\n");
      return 0;
    } else {
      std::cout << "Dumping options:" << std::endl;
      dumpOptions(extOpt);
    }

    logY = _logY;
    lumi_scale = lumi;
    if (config != "")
      loadConfig(config.c_str());
    else
      loadConfig(std::string(argv[0]).substr(0, std::string(argv[0]).rfind('/'))+"/config.txt");

    _stamp = stamp;

    vector<string> h_items;
    split(h_input, '/', h_items);
    bool isRatio = false;
    std::string histogram = "";
    string histogram_num = "";
    string histogram_den = "";
    if (h_items.size() == 1)
      histogram = h_items[0];
    else {
      histogram_num = h_items[0];
      histogram_den = h_items[1];
      isRatio = true;
    }

    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(1);

    if (!isRatio) {
      // for Data/MC comparison
      SampleSetConfiguration stackConfig = makeConfigurationPlots(prefix, channel, mcOnly, includeSignal);
      SystematicCalculator systCalc(stackConfig);
      bool splitUpDw = (saveTH1 != "");
      addAllSystematics(systCalc, prefix, channel, splitUpDw);
      systCalc.calculate(histogram);
      if (underflow) stackConfig.showUnderflow();
      if (xMax > -998.0) stackConfig.limitMaxX(xMax, true);
      if (xMin > -998.0) stackConfig.limitMinX(xMin);
      if (rebin != 1) stackConfig.rebin(rebin);
      if (normBinWidth) stackConfig.normBinWidth();



      string outfile = _outfile;
      if (outfile == "") {
        outfile = prefix+"_"+histogram;
        outfile += string("_");
        if (channel == "e") {
          outfile += "e";
        } else if (channel == "mu") {
          outfile += "mu";
        } else if (channel == "comb") {
          outfile += "comb";
        } else {
          outfile += "ukn";
        }
        if (stamp == 1) outfile += "_ATLASPrelim";
        if (stamp == 2) outfile += "_ATLAS";

	if (verbose) {
	  systCalc.printBigTable(stackConfig);
	}
	cout << "Systematic uncertainties:" << endl << endl;
	if(saveYields)
	  systCalc.printSysts(stackConfig["MC"], outfile);
	else
	  systCalc.printSysts(stackConfig["MC"], "");
	cout << "Yields:" << endl << endl;
	if(saveYields)
	  systCalc.printYields(stackConfig, outfile, blind, scaleSignal, scaleSignalType2, signalType2noScaling);
	else
	  systCalc.printYields(stackConfig,"", blind, scaleSignal, scaleSignalType2, signalType2noScaling);
	outfile += outFormat;
      }



      vector<string> extraText;
      if (channel == "e") {
        extraText.push_back("e+jets");
      } else if (channel == "mu") {
        extraText.push_back("#mu+jets");
      } else if (channel == "comb") {
        //extraText.push_back("e,#mu-channel");
      }

      vector<string> split_extraText;
      split(_extraText, ';', split_extraText);
      for (vector<string>::iterator i = split_extraText.begin(); i!=split_extraText.end();++i) extraText.push_back(*i);
      if (alternateStyle) {
        drawDataMC2(stackConfig, extraText, outfile, true, xTitle, yTitle, mustBeBigger, posLegend, yMin, yMax, arrow, lumi, printYields);
      } else {
	drawDataMC(stackConfig, extraText, outfile, true, xTitle, yTitle, mustBeBigger, posLegend, yMin, yMax, arrow, lumi, printYields, includeSignal, scaleSignal, scaleSignalType2, signalType2noScaling, splitLegend, blind, normBinWidth);
      }

      if (saveTH1 != "" && !blind) {
        stackConfig["MC"].saveTH1(saveTH1, blind);
        stackConfig["Data"].saveTH1(saveTH1, blind);
	stackConfig["SignalType1"].saveTH1(saveTH1, blind);
	if(includeSignal > 1)
	  stackConfig["SignalType2"].saveTH1(saveTH1, blind);
      }
     else if (saveTH1 != "" && blind) {
        stackConfig["MC"].saveTH1(saveTH1, blind);
	stackConfig["SignalType1"].saveTH1(saveTH1, blind);
	if(includeSignal > 1)
	  stackConfig["SignalType2"].saveTH1(saveTH1, blind);
     }

    } else { // if it is a ratio plot

      shared_ptr<SystematicRatioCalculator> systRatCalcD;
      shared_ptr<SystematicRatioCalculator> systRatCalc;
      shared_ptr<SystematicRatioCalculator> systRatCalcMCAtNLO;
      shared_ptr<SystematicRatioRatioCalculator> systRatRatCalc;
      shared_ptr<SystematicRatioRatioCalculator> systRatRatCalcMCAtNLO;

      if (!mcOnly) {
        // for data eff. numerator
        SampleSetConfiguration stackConfigNumD = makeConfigurationDataEff(prefix, channel);
        SystematicCalculator systCalcNumD(stackConfigNumD);
        addAllSystematics(systCalcNumD, prefix, channel);

        systCalcNumD.calculate(histogram_num);

        if (xMax > -998.0) stackConfigNumD.limitMaxX(xMax);
        if (xMin > -998.0) stackConfigNumD.limitMinX(xMin);

        if (verbose) {
          cout << "Data numerator systs.:" << endl << endl;
          systCalcNumD.printSysts(stackConfigNumD["MC"],"");
          cout << "Data numerator yields:" << endl << endl;
          systCalcNumD.printYields(stackConfigNumD,"", blind, scaleSignal, scaleSignalType2, signalType2noScaling);
        }

        // for data eff. denominator
        SampleSetConfiguration stackConfigDenD = makeConfigurationDataEff(prefix, channel);
        SystematicCalculator systCalcDenD(stackConfigDenD);
        addAllSystematics(systCalcDenD, prefix, channel);

        systCalcDenD.calculate(histogram_den);

        if (xMax > -998.0) stackConfigDenD.limitMaxX(xMax);
        if (xMin > -998.0) stackConfigDenD.limitMinX(xMin);

        if (verbose) {
          cout << "Data denominator systs.:" << endl << endl;
          systCalcDenD.printSysts(stackConfigDenD["MC"],"");
          cout << "Data denominator yields:" << endl << endl;
          systCalcDenD.printYields(stackConfigDenD,"", blind, scaleSignal, scaleSignalType2, signalType2noScaling);
        }

        // calculate ratio
        systRatCalcD.reset(new SystematicRatioCalculator(systCalcNumD, systCalcDenD));
        systRatCalcD->calculate(histogram_num, histogram_den, true);

        cout << "Systematic uncertainties for data eff.:" << endl << endl << endl;
        cout.precision(3);
        cout << "Ratio nominal in data:" << systRatCalcD->_sr["Ratio"]._item[0].nominal << endl;
        cout.precision(1);
        systRatCalcD->printAverageSysts(systRatCalcD->_sr["Ratio"]);

        systRatCalcD->printBinSysts(systRatCalcD->_sr["Ratio"]);

      }

      // for MC eff. numerator using PP
      SampleSetConfiguration stackConfigNum = makeConfigurationMCEff(prefix, channel);
      SystematicCalculator systCalcNum(stackConfigNum);
      addAllSystematics(systCalcNum, prefix, channel);

      systCalcNum.calculate(histogram_num);

      if (xMax > -998.0) stackConfigNum.limitMaxX(xMax);
      if (xMin > -998.0) stackConfigNum.limitMinX(xMin);

      if (verbose) {
        cout << "MC numerator systs. (standard):" << endl << endl;
        systCalcNum.printSysts(stackConfigNum["MC"],"");
        cout << "MC numerator yields (standard):" << endl << endl;
        systCalcNum.printYields(stackConfigNum,"", blind, scaleSignal, scaleSignalType2, signalType2noScaling);
      }

      // for MC eff. denominator
      SampleSetConfiguration stackConfigDen = makeConfigurationMCEff(prefix, channel);
      SystematicCalculator systCalcDen(stackConfigDen);
      addAllSystematics(systCalcDen, prefix, channel);

      systCalcDen.calculate(histogram_den);

      if (xMax > -998.0) stackConfigDen.limitMaxX(xMax);
      if (xMin > -998.0) stackConfigDen.limitMinX(xMin);

      if (verbose) {
        cout << "MC denominator systs. (standard):" << endl << endl;
        systCalcDen.printSysts(stackConfigDen["MC"],"");
        cout << "MC denominator yields (standard):" << endl << endl;
        systCalcDen.printYields(stackConfigDen,"", blind, scaleSignal, scaleSignalType2, signalType2noScaling);
      }

      // calculate ratio
      systRatCalc.reset(new SystematicRatioCalculator(systCalcNum, systCalcDen));
      systRatCalc->calculate(histogram_num, histogram_den, false);
      cout.precision(3);
      cout << "Ratio nominal in MC (Standard):" << systRatCalc->_sr["Ratio"]._item[0].nominal << endl;
      cout.precision(1);
      cout << "Systematic uncertainties for MC eff. (standard):" << endl << endl << endl;
      systRatCalc->printAverageSysts(systRatCalc->_sr["Ratio"]);

      systRatCalc->printBinSysts(systRatCalc->_sr["Ratio"]);

      systRatRatCalc.reset(new SystematicRatioRatioCalculator(*systRatCalcD.get(), *systRatCalc.get()));
      systRatRatCalc->calculate(histogram_num, histogram_den, false);

      std::cout << "Systematics on ratio of ratio: " << std::endl;
      systRatRatCalc->printBinSysts(systRatRatCalc->_sr["Ratio"]);

      vector<string> extraText;
      vector<string> split_extraText;
      split(_extraText, ';', split_extraText);
      for (vector<string>::iterator i = split_extraText.begin(); i!=split_extraText.end();++i) extraText.push_back(*i);
      string outfile = _outfile;
      if (outfile == "") {
        outfile = string("sf_")+prefix+"_"+histogram_num+string("_")+histogram_den+string("_");
        outfile += channel;
        if (stamp == 1) outfile += "_ATLASPrelim";
        if (stamp == 2) outfile += "_ATLAS";
        outfile += outFormat;
      }
      if (channel == "e") {
        extraText.push_back("e channel");
      } else if (channel == "mu") {
        extraText.push_back("#mu channel");
      } else if (channel == "comb") {
        //extraText.push_back("e,#mu-channel");
      }
      if (!mcOnly) {
        drawEff(&(systRatCalc->_sr["Ratio"]), extraText, outfile, yTitle, &(systRatCalcD->_sr["Ratio"]), false, mustBeBigger, yMax, xTitle, &(systRatRatCalc->_sr["Ratio"]), lumi);
      } else {
        drawEff(&(systRatCalc->_sr["Ratio"]), extraText, outfile, yTitle, 0, false, mustBeBigger, yMax, xTitle, 0, lumi);
      }
    } // ratio?

  } catch (string s) {
    cout << "Crashed with exception: " << s << endl;
    dumpTrace();
  }

  return 0;
}

