#include "SampleSet.h"
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <iomanip>


using namespace std;

Sample::Sample(const string &_fname, const string &_legname, const string &_latex, const string &_plot,
               const string &_legendstyle, int _linestyle, int _linecolor, int _fillstyle, int _fillcolor, int _markerstyle,
               float _markersize, const string &_option)
  : fname(_fname), legname(_legname), name_latex(_latex), name_plot(_plot),
    legendstyle(_legendstyle), linestyle(_linestyle), linecolor(_linecolor), fillstyle(_fillstyle), fillcolor(_fillcolor), markerstyle(_markerstyle),
    markersize(_markersize), option(_option) {

}

shared_ptr<TH1D> Sample::makeTH1(const string &name, const string &systName) {
  shared_ptr<TH1D> me;
  if (systName == "")
    me = nominal.makeTH1(name);
  else if (syst.find(systName) != syst.end()) {
    //    std::cout <<"DEBUG :: $$$ name >"<< name << "systname : "<<systName << std::endl;
    Hist thisVar = nominal;
    thisVar += syst[systName];
    me = thisVar.makeTH1(name);
  } else {
    for (map<string, Hist>::const_iterator i = syst.begin(); i != syst.end(); ++i) {
      cout << "NOTE: Systematic uncertainty " << i->first << " is available." << endl;
    }
    throw string("Sample::makeTH1 - Did not find systematic uncertainty ")+systName+(" for sample named ")+legname;
  }

  me->SetLineWidth(1);
  me->SetLineStyle(linestyle);
  me->SetLineColor(linecolor);
  me->SetFillStyle(fillstyle);
  me->SetFillColor(fillcolor);
  me->SetMarkerStyle(markerstyle);
  me->SetMarkerSize(markersize);
  me->SetMarkerColor(linecolor);
  return me;
}

SampleSet::SampleSet() {
}

void SampleSet::add(const string &_fname, const string &_legname, const string &_latex, const string &_plot,
                    const string &_legendstyle, int _linestyle, int _linecolor, int _fillstyle, int _fillcolor, int _markerstyle,
                    float _markersize, const string &_option) {
  _item.push_back(Sample(_fname, _legname, _latex, _plot, _legendstyle, _linestyle, _linecolor, _fillstyle, _fillcolor, _markerstyle, _markersize, _option));
}



/*(SH) Systematics are handled in the following way:
  /    All systematics are handled as Sum_{sources} ((up^{source} + down^{source})/2)^2
  /    That keeps per cent variations the same and symmetrizes all asymmetric up/down variations
  /    The only exception is the ttbar gen and tt PS uncertainty where the relative uncertainty of
  /    the both is applied as up and down variation.
  /    COMMENT: all variations that are either only up(or only down) variations need to be specified
  /             by hand which will be adjusted at some point. Also the systematics handled the way as
  /             the ttbar gen and tt PS variation need to be set by hand as special cases.
*/
shared_ptr<TGraphAsymmErrors> SampleSet::makeBand(bool isRatio) {
  shared_ptr<TGraphAsymmErrors> g(new TGraphAsymmErrors());
  g->Set((int) (_item[0].nominal._size - 2));
  Hist total;
  Hist syst;
  Hist tmpsyst;
  bool ttRelativeSyst = false;
  bool lastElement = false;
  vector<string> sNames;
  bool systPair = false;
  vector<std::pair <int,int>> systPos;
  vector<string> consecSysts; consecSysts.clear();
  vector<std::pair<bool,Hist>> systList;
  bool debug = false;
  for (map<string, Hist>::iterator k = _item[0].syst.begin(); k != _item[0].syst.end(); ++k) {
    string systName = k->first;
    sNames.push_back(systName);
  }

  for (int j = 0; j < _item.size(); ++j) {
    Sample &one_item = _item[j];
    total += one_item.nominal;
  }
  for (int k = 0; k < sNames.size(); ++k) {
    Hist this_syst;

    for (int j = 0; j < _item.size(); ++j) {
      Sample &one_item = _item[j];
      this_syst += one_item.syst[sNames[k]];
      if(debug)      std::cout <<"DEBUG :: syst size ("<<_item.size()<<") >>> name : "<< sNames[k] <<" syst : > "<< this_syst<< std::endl;
      if(debug)      std::cout <<"DEBUG :: ******** this syst > nom : " << this_syst[3] << " err > "<< this_syst.e(3)<< std::endl;
    }
    bool OneSided = false;
    if((sNames.size() - k) == 1)
      lastElement = true;

    if(sNames[k].find("ttgen") != std::string::npos || sNames[k].find("ttgen2") != std::string::npos || sNames[k].find("ttps") != std::string::npos){
      ttRelativeSyst = true;
      consecSysts.push_back(sNames[k]);
      consecSysts.push_back("dummy");
    }else if(sNames[k].find("JET_JER_SINGLE_NP__1") != std::string::npos || sNames[k].find("MET_SoftTrk_Reso") != std::string::npos ){
      OneSided = true;
      consecSysts.push_back(sNames[k]);
      consecSysts.push_back("dummy");
    }else{
      consecSysts.push_back(sNames[k]);
      if(lastElement)
	consecSysts.push_back("dummy");
      else
	consecSysts.push_back(sNames[k+1]);
    }
    int extDw=0;
    int extUp=0;
    if(consecSysts.at(0).find("down") != std::string::npos ||  consecSysts.at(0).find("Down") != std::string::npos || consecSysts.at(0).find("DOWN") != std::string::npos) extDw = 4;
    if(consecSysts.at(1).find("up") != std::string::npos ||  consecSysts.at(1).find("Up") != std::string::npos || consecSysts.at(1).find("UP") != std::string::npos) extUp = 2;
    if(consecSysts.at(0).substr(0, consecSysts.at(0).size()-extDw).compare(consecSysts.at(1).substr(0, consecSysts.at(1).size()-extUp)) == 0){
      systPos.push_back(std::make_pair(k,k+1));
    }else{
      systPos.push_back(std::make_pair(k,-999));
    }

    this_syst.AbsVal();
    if(debug)    std::cout <<"DEBUG ::>> down : " << systPos.at(k).first << "  up : "<< systPos.at(k).second<< std::endl;
    if(ttRelativeSyst || OneSided){
      tmpsyst += this_syst;
    }else{
      if((k == systPos.at(k).first && k+1 == systPos.at(k).second) || systPos.at(k).first == systPos.at(k-1).second){
	if(debug)	std::cout <<"DEBUG :: > syst down : " << systPos.at(k).first << "  up : "<< systPos.at(k).second<< std::endl;
	tmpsyst += this_syst;
      }else{
	tmpsyst += this_syst;
      }
    }

    if(ttRelativeSyst || OneSided){
      systList.push_back(std::make_pair(ttRelativeSyst,tmpsyst));
      tmpsyst = NULL;
    }else{
      if((systPos.at(k).second == -999 ) && (systPos.at(k).first == systPos.at(k-1).second)){
	systList.push_back(std::make_pair(ttRelativeSyst,tmpsyst));
	tmpsyst = NULL;
      }
    }
    consecSysts.clear();
  }
  std::cout << "INFO :: no of systematics you run over :  " << systList.size()<< std::endl;
  for(auto it: systList){
    if(debug)    std::cout <<"*******************************************" << std::endl;
    if(debug)    std::cout <<"DEBUG :: syst size ("<<_item.size()<<")  syst : > "<< it.second << std::endl;
    if(!it.first)
      it.second.DivideBy2();
    if(debug)    std::cout <<"DEBUG :: syst size ("<<_item.size()<<")  syst : > "<< it.second << std::endl;
    it.second.square();
    if(debug)    std::cout <<"DEBUG :: syst size ("<<_item.size()<<")  syst : > "<< it.second << std::endl;
    syst += it.second;
  }
  syst.squareRoot();

  if(syst._size !=0){
    for (int k = 1; k < syst._size-1; ++k) {
      double delta = (total.x(k+1)-total.x(k))*0.5;
      g->SetPoint(k-1, total.x(k)+delta, total[k]);
      g->SetPointEXhigh(k-1, delta);
      g->SetPointEXlow(k-1, delta);
      double eyh = syst[k];
      double eyl = syst[k];
      if(debug)    std::cout <<"DEBUG :: nom " <<total[k]<< " eyl : "<< eyl << " eyh : " << eyh << std::endl;
      if (total[k] - eyl <= 0) eyl = total[k]*0.9999;
      if (isRatio && total[k] + eyh > 1) eyh = 1 - total[k];
      if(debug)    std::cout <<"DEBUG :: nom " <<total[k]<< " eyl : "<< eyl << " eyh : " << eyh << std::endl;
      //(SH)       std::cout <<"DEBUG :: BAND bin "<<  k <<  std::fixed << std::setprecision(3) <<" entry: "<< total[k] << " err : "<< total.e(k) << std::endl;
      g->SetPointEYhigh(k-1, eyh);
      g->SetPointEYlow(k-1, eyl);
  }
  }else{
     for (int k = 1; k < total._size-1; ++k) {
       double delta = (total.x(k+1)-total.x(k))*0.5;
       g->SetPoint(k-1, total.x(k)+delta, total[k]);
       g->SetPointEXhigh(k-1, delta);
       g->SetPointEXlow(k-1, delta);
       double eyh = total.e(k);
       double eyl = total.e(k);
       if (total[k] - eyl <= 0) eyl = total[k]*0.9999;
       if (isRatio && total[k] + eyh > 1) eyh = 1 - total[k];
       //(SH)       std::cout <<"DEBUG :: BAND bin "<<  k <<  std::fixed << std::setprecision(3) <<" entry: "<< total[k] << " err : "<< total.e(k) << std::endl;
       //(SH) fill with dummy (0) values since the errors will be filled in utils
       g->SetPointEYhigh(k-1, 0);
       g->SetPointEYlow(k-1, 0);
     }
  }
  return g;
}
//(SH) old implementation
// shared_ptr<TGraphAsymmErrors> SampleSet::makeBand(bool isRatio) {
//   shared_ptr<TGraphAsymmErrors> g(new TGraphAsymmErrors());
//   g->Set((int) (_item[0].nominal._size - 2));
//   Hist total;
//   Hist syst;
//   vector<string> sNames;
//   for (map<string, Hist>::iterator k = _item[0].syst.begin(); k != _item[0].syst.end(); ++k) {
//     string systName = k->first;
//     sNames.push_back(systName);
//   }

//   for (int j = 0; j < _item.size(); ++j) {
//     Sample &one_item = _item[j];
//     total += one_item.nominal;
//   }

//   for (int k = 0; k < sNames.size(); ++k) {
//     Hist this_syst;
//     for (int j = 0; j < _item.size(); ++j) {
//       Sample &one_item = _item[j];
//       this_syst += one_item.syst[sNames[k]];
//       //(SH)      std::cout <<"DEBUG :: " << this_syst<< std::endl;
//     }
//     syst += this_syst*this_syst;

//   }
//   //(SH)  std::cout <<"DEBUG :: " << syst << std::endl;
//   syst.squareRoot();
//   //(SH)  std::cout <<"DEBUG :: " << syst << std::endl;
//   if(syst._size !=0){
//   for (int k = 1; k < syst._size-1; ++k) {
//     double delta = (total.x(k+1)-total.x(k))*0.5;
//     g->SetPoint(k-1, total.x(k)+delta, total[k]);
//     g->SetPointEXhigh(k-1, delta);
//     g->SetPointEXlow(k-1, delta);
//     double eyh = syst[k];
//     double eyl = syst[k];
//     //(SH) std::cout <<"DEBUG :: nom " <<total[k]<< " eyl : "<< eyl << " eyh : " << eyh << std::endl;
//     if (total[k] - eyl <= 0) eyl = total[k]*0.9999;
//     if (isRatio && total[k] + eyh > 1) eyh = 1 - total[k];
//     //(SH)    std::cout <<"DEBUG :: nom " <<total[k]<< " eyl : "<< eyl << " eyh : " << eyh << std::endl;
//     g->SetPointEYhigh(k-1, eyh);
//     g->SetPointEYlow(k-1, eyl);
//   }
//   }else{
//      for (int k = 1; k < total._size-1; ++k) {
//        double delta = (total.x(k+1)-total.x(k))*0.5;
//        g->SetPoint(k-1, total.x(k)+delta, total[k]);
//        g->SetPointEXhigh(k-1, delta);
//        g->SetPointEXlow(k-1, delta);
//        double eyh = total.e(k);
//        double eyl = total.e(k);
//        if (total[k] - eyl <= 0) eyl = total[k]*0.9999;
//        if (isRatio && total[k] + eyh > 1) eyh = 1 - total[k];
//        //(SH) fill with dummy (0) values since the errors will be filled in utils
//        g->SetPointEYhigh(k-1, 0);
//        g->SetPointEYlow(k-1, 0);
//      }
//   }
//   return g;
// }





shared_ptr<TH1D> SampleSet::makeBandLine(const string &name, bool isRatio, bool up) {
  shared_ptr<TH1D> g(_item[0].makeTH1(Form("%s%d", name.c_str(), (int) up)));
  Hist total;
  Hist syst;
  vector<string> sNames;
  for (map<string, Hist>::iterator k = _item[0].syst.begin(); k != _item[0].syst.end(); ++k) {
    string systName = k->first;
    sNames.push_back(systName);
  }

  for (int j = 0; j < _item.size(); ++j) {
    Sample &one_item = _item[j];
    total += one_item.nominal;
  }

  for (int k = 0; k < sNames.size(); ++k) {
    Hist this_syst;
    for (int j = 0; j < _item.size(); ++j) {
      Sample &one_item = _item[j];
      this_syst += one_item.syst[sNames[k]];
    }
    syst += this_syst*this_syst;
  }
  syst.squareRoot();
  for (int k = 1; k < syst._size-1; ++k) {
    double factor = 1;
    if (!up) factor = -1;
    double ll = total[k] + factor*syst[k];
    if (!up && ll <= 0) ll = 0.0001;
    g->SetBinContent(k, total[k] + factor*syst[k]);
  }
  return g;
}

shared_ptr<THStack> SampleSet::makeStack(const string &name, shared_ptr<TLegend> legend, vector<shared_ptr<TH1D> > &vechist, int printYields, int normBinWidth, int includeSignal) {
  shared_ptr<THStack> stack(new THStack(name.c_str(), name.c_str()));
  vector<string> names;
  vector<string> style;
  std::stringstream msg;
  for (int j = _item.size()-1; j >= 0; --j) { // this inverts the stack
    Sample &one_item = _item[j];
    //@todo (SH) scale signal
    //     if(one_item.name_plot.find("TTS") != std::string::npos){
    //   shared_ptr<TH1D> signal;
    //   signal = one_item.makeTH1(Form("%s%d", name.c_str(), j));
    //   signal->Scale(50);
    //   vechist.push_back(signal);
    // }else{
    //   vechist.push_back(one_item.makeTH1(Form("%s%d", name.c_str(), j)));
    // }
    vechist.push_back(one_item.makeTH1(Form("%s%d", name.c_str(), j)));
    stack->Add(vechist[vechist.size()-1].get(), one_item.option.c_str());
    if(printYields && !includeSignal && normBinWidth){
      msg.str(std::string());
      msg <<one_item.name_plot << std::setw(1) <<std::left << " {" << std::fixed << std::setprecision(1) <<one_item.nominal.yieldForNormBinWidth() << "}";
    }else if(printYields && includeSignal){
      msg.str(std::string());
      msg <<one_item.name_plot << std::setw(1) <<std::left << " {" << std::fixed << std::setprecision(1) <<one_item.nominal.yield() << "}";
    }else{
      msg.str(std::string());
      msg <<one_item.name_plot;
    }
    names.push_back(msg.str());
    style.push_back(one_item.legendstyle);
  }
  for (int j = vechist.size()-1; j >= 0; --j) {
    if (style[j] != "")
      legend->AddEntry(vechist[j].get(), names[j].c_str(), style[j].c_str());
  }
  return stack;
}

shared_ptr<TH1D> SampleSet::makeTH1(const string &name, const string &systName) {
  shared_ptr<TH1D> mySum(_item[0].makeTH1(name, systName));
  for (int j = 1; j < _item.size(); ++j) {
    shared_ptr<TH1D> t(_item[j].makeTH1(Form("%s_%d", name.c_str(), j), systName));
    mySum->Add(t.get());
  }
  return shared_ptr<TH1D>(mySum);
}


void SampleSet::saveTH1(const std::string &s, int blind) {
  for (int j = 0; j < _item.size(); ++j) {
    if(blind && _item[j].fname.find("Data") != std::string::npos)
      continue;

    TFile f((string("hist_")+_item[j].legname+string(".root")).c_str(), "update");
    TH1D *t = new TH1D(*_item[j].makeTH1(Form("x%s_%d", s.c_str(), j), "").get());
    f.cd();
    t->Write(Form("x%s", s.c_str()), TObject::kOverwrite);
    delete t;
    for (map<string, Hist>::const_iterator it = _item[j].syst.begin(); it != _item[j].syst.end(); ++it) {
      TH1D *ts = new TH1D(*_item[j].makeTH1(Form("x%s_%s%d", s.c_str(), it->first.c_str(), j), it->first).get());
      f.cd();
      ts->Write(Form("x%s_%s", s.c_str(), it->first.c_str()), TObject::kOverwrite);
      delete ts;
    }
    f.Close();
  }
}


void SampleSetConfiguration::addType(const std::string &type) {
  _stack.insert(std::make_pair(type, SampleSet()));
}

void SampleSetConfiguration::add(const std::string &type, const std::string &fname, const std::string &legname, const string &_latex, const string &_plot,
                                 const string &_legendstyle, int _linestyle, int _linecolor, int _fillstyle, int _fillcolor, int _markerstyle,
                                 float _markersize, const string &_option) {

  _stack[type].add(fname, legname, _latex, _plot, _legendstyle, _linestyle, _linecolor, _fillstyle, _fillcolor, _markerstyle, _markersize, _option);
}

SampleSet &SampleSetConfiguration::operator [](const string &name) {
  return _stack[name];
}


void SampleSetConfiguration::showUnderflow() {
  for (map<string, SampleSet>::iterator i = _stack.begin(); i != _stack.end(); ++i) { // for every element in the stack (one for data, one for MC)
    // i->second is a stack
    // i->second._item is the list of items of the stack
    for (int j = 0; j < i->second._item.size(); ++j) {
      Sample &one_item = i->second._item[j];
      //one_item.nominal and one_item.syst(map of string, Hist)
      one_item.nominal.showUnderflow();
      for (map<string, Hist>::const_iterator k = one_item.syst.begin(); k != one_item.syst.end(); ++k) {
        const string &systName = k->first;
        one_item.syst[systName].showUnderflow();
      }
    }
  }
}

void SampleSetConfiguration::limitMaxX(double xMax, bool addOverflow) {
  for (map<string, SampleSet>::iterator i = _stack.begin(); i != _stack.end(); ++i) { // for every element in the stack (one for data, one for MC)
    // i->second is a stack
    // i->second._item is the list of items of the stack
    for (int j = 0; j < i->second._item.size(); ++j) {
      Sample &one_item = i->second._item[j];
      //one_item.nominal and one_item.syst(map of string, Hist)
      one_item.nominal.limitMaxX(xMax, addOverflow);
      for (map<string, Hist>::const_iterator k = one_item.syst.begin(); k != one_item.syst.end(); ++k) {
        const string &systName = k->first;
        one_item.syst[systName].limitMaxX(xMax, addOverflow);
      }
    }
  }
}

void SampleSetConfiguration::rebin(int r) {
  for (map<string, SampleSet>::iterator i = _stack.begin(); i != _stack.end(); ++i) { // for every element in the stack (one for data, one for MC)
    // i->second is a stack
    // i->second._item is the list of items of the stack
    for (int j = 0; j < i->second._item.size(); ++j) {
      Sample &one_item = i->second._item[j];
      //one_item.nominal and one_item.syst(map of string, Hist)
      one_item.nominal.rebin(r);
      for (map<string, Hist>::const_iterator k = one_item.syst.begin(); k != one_item.syst.end(); ++k) {
        const string &systName = k->first;
        one_item.syst[systName].rebin(r);
      }
    }
  }
}

void SampleSetConfiguration::normBinWidth() {
  for (map<string, SampleSet>::iterator i = _stack.begin(); i != _stack.end(); ++i) { // for every element in the stack (one for data, one for MC)
    // i->second is a stack
    // i->second._item is the list of items of the stack
    for (int j = 0; j < i->second._item.size(); ++j) {
      Sample &one_item = i->second._item[j];
      //one_item.nominal and one_item.syst(map of string, Hist)
      one_item.nominal.normBinWidth();
      for (map<string, Hist>::const_iterator k = one_item.syst.begin(); k != one_item.syst.end(); ++k) {
        const string &systName = k->first;
        one_item.syst[systName].normBinWidth();
      }
    }
  }
}

void SampleSetConfiguration::limitMinX(double xMin) {
  for (map<string, SampleSet>::iterator i = _stack.begin(); i != _stack.end(); ++i) { // for every element in the stack (one for data, one for MC)
    // i->second is a stack
    // i->second._item is the list of items of the stack
    for (int j = 0; j < i->second._item.size(); ++j) {
      Sample &one_item = i->second._item[j];
      //one_item.nominal and one_item.syst(map of string, Hist)
      one_item.nominal.limitMinX(xMin);
      for (map<string, Hist>::const_iterator k = one_item.syst.begin(); k != one_item.syst.end(); ++k) {
        const string &systName = k->first;
        one_item.syst[systName].limitMinX(xMin);
      }
    }
  }
}


void SampleSetConfiguration::normToData() {
  Hist &myData = _stack["Data"]._item[0].nominal;
  double data_yield = myData.yield();

  Sample &firstMC = _stack["MC"]._item[0];

  Hist fullMC = _stack["MC"]._item[0].nominal;
  for (int j = 1; j < _stack["MC"]._item.size(); ++j) {
    fullMC += _stack["MC"]._item[j].nominal;
  }

  for (map<string, Hist>::const_iterator k = firstMC.syst.begin(); k != firstMC.syst.end(); ++k) {
    const string &systName = k->first;

    Hist fullMCSyst = _stack["MC"]._item[0].syst[systName];
    for (int j = 1; j < _stack["MC"]._item.size(); ++j) {
      fullMCSyst += _stack["MC"]._item[j].syst[systName];
    }

    for (int j = 0; j < _stack["MC"]._item.size(); ++j) {
      Sample &myMC = _stack["MC"]._item[j];
      Hist a = myMC.nominal + myMC.syst[systName];
      a *= 1.0/(fullMC + fullMCSyst).yield();
      Hist b = myMC.nominal;
      b *= 1.0/fullMC.yield();
      a -= b;
      myMC.syst[systName] = a*data_yield;
    }
  }
  for (int j = 0; j < _stack["MC"]._item.size(); ++j) {
    Sample &myMC = _stack["MC"]._item[j];
    myMC.nominal *= data_yield/fullMC.yield();
  }
}


int SampleSetConfiguration::n() {
  return _stack.size();
}


