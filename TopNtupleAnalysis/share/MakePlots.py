import os,sys
import helpers

lumi=3.20905
configFile=sys.argv[1]
topo = helpers.getTopology( os.path.basename( os.environ['PWD'] ) ) #extracts info from folder name

lep = ['e','mu']
ch = ['merged_boosted']
plotCodeLoc = '/Users/shenkel/work/Physics/Analysis/VLQ/vlq_wbx_wbx/TopNtupleAnalysis/plotting/'
fitPlots = True
testPlots = False
controlPlots = False
resolvedPlots = False
boostedPlots = False
#hists = ['HT','nBtagJets', 'nJets', 'nTrkBtagJets']

if(fitPlots):
    print 'INFO :: Start plotting test plots'
#    hists = ['HT','nBoostedW','nJets','nBtagJets','MET']
    hists = ['HT_effBins']
    extraText = str(topo)
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(channel) + str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1) + " --includeSignal "+ str(1) + ' --outFormat .pdf' + " --rebin "+str(2)
        #            subprocess.check_output(command, shell=True)
                os.system(str(command))
                print command

if(testPlots):
    print 'INFO :: Start plotting test plots'
#    hists = ['HT','nBoostedW','nJets','nBtagJets','MET']
    hists = ['HT_effBins']
    extraText = str(topo)
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(channel) + str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1) + " --includeSignal "+ str(1)+ " --scaleSignal " + str(50) + ' --outFormat .pdf' + " --rebin "+str(2)
        #            subprocess.check_output(command, shell=True)
                os.system(str(command))
                print command

if(controlPlots):
    print 'INFO :: Start plotting control plots'
    hists = ['lepPt','lepPt_effBins','lepEta','lepPhi','leadJetPt','leadbJetPt','nJets','nBtagJets','HT','nBoostedW',
            'MET','MET_phi','DeltaR_lepnu','mwt','mass_lepb','yields','mu','mu_original','npv','vtxz',
            'nTrkBtagJets','leadTrkbJetPt','closeJetPt', 'closejl_minDeltaR', 'closejl_minDeltaR_effBins', 'closejl_pt',
            'jet0_eta', 'jet0_m', 'jet0_phi', 'jet0_pt', 'jet1_eta', 'jet1_m', 'jet1_phi', 'jet1_pt',
            'jet2_eta', 'jet2_m', 'jet2_phi', 'jet2_pt', 'jet3_eta', 'jet3_m', 'jet3_phi', 'jet3_pt',
            'jet4_eta', 'jet4_m', 'jet4_phi', 'jet4_pt', 'jet5_eta', 'jet5_m', 'jet5_phi', 'jet5_pt',
            'weight', 'weight_leptSF', 'yields', 'weight_leptPt']
    extraText = str(topo) 
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(channel) + str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1) +   " --includeSignal "+str(1)+" --scaleSignal " + str(50) + ' --outFormat .pdf'
        #            subprocess.check_output(command, shell=True)
                os.system(str(command))
                print command
                
if(boostedPlots):
    print 'INFO :: Start plotting boosted plots'
    hists = ['largeJetPt','largeJetM','largeJetPhi','largeJetSd12',
             'nhadronicT','hadronicT_m', 'hadronicT_phi', 'hadronicT_eta','hadronicT_pt',
             'nleptonicT','leptonicT_m', 'leptonicT_phi', 'leptonicT_eta','leptonicT_pt',
             'nhadronicb','hadronicb_m', 'hadronicb_phi', 'hadronicb_eta','hadronicb_pt',
             'nleptonicb','leptonicb_m', 'leptonicb_phi', 'leptonicb_eta','leptonicb_pt',
             'nleptonicW','leptonicW_m', 'leptonicW_phi', 'leptonicW_eta','leptonicW_pt',
             'nboostedWhadCand','boostedWhadCand_pt','boostedWhadCand_eta','boostedWhadCand_phi',
             'DeltaM_hadtopleptop','minDeltaR_hadWbjet','closebjHadW_pt']
    extraText = str(topo) + ";boosted loose"
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(channel) + str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1) + " --scaleSignal " + str(50) + ' --outFormat .pdf'
    #            subprocess.check_output(command, shell=True)
                os.system(str(command))
                print command

                
