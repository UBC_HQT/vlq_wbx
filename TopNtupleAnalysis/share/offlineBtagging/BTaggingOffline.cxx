// Applies b-tagging offline
// author: Steffen Henkelmann


#include <iostream>

#include "TopNtupleAnalysis/BTaggingOffline.h"

#include "TLorentzVector.h"
#include "TFile.h"

BTaggingOfflineScaleFactorParam::BTaggingOfflineScaleFactorParam() :
    m_btageff("BTaggingEfficiencyTool")
{
    if (!m_btageff.setProperty("TaggerName", "MV2c10")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("OperatingPoint", "FixedCutBEff_77")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("JetAuthor", "AntiKt4EMTopoJets")) std::cout << "Failed to set b-tagging property." << std::endl;
    //if (!m_btageff.setProperty("EfficiencyFileName", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-June27_v1.root")) std::cout << "Failed to set b-tagging property." << std::endl;
    //    if (!m_btageff.setProperty("ScaleFactorFileName", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-June27_v1.root")) std::cout << "Failed to set b-tagging property." << std::endl;
    // (SH) As cross check against ntuples inputs from AnalysisTop
    if (!m_btageff.setProperty("EfficiencyFileName", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-May31_v1.root")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("ScaleFactorFileName", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-May31_v1.root")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("ScaleFactorBCalibration", "default")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("ScaleFactorCCalibration", "default")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("ScaleFactorTCalibration", "default")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("ScaleFactorLightCalibration", "default")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("ExcludeFromEigenVectorTreatment", "")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("EfficiencyBCalibrations", "410000;410004;410006;410187")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("EfficiencyCCalibrations", "410000;410004;410006;410187")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("EfficiencyTCalibrations", "410000;410004;410006;410187")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.setProperty("EfficiencyLightCalibrations", "410000;410004;410006;410187")) std::cout << "Failed to set b-tagging property." << std::endl;
    if (!m_btageff.initialize()) std::cout << "Failed to initialize b-tagging eff." << std::endl;
    CP::SystematicSet s = m_btageff.affectingSystematics();
    if (!m_btageff.setProperty("TaggerName", "MV2c10")) std::cout << "Failed to set b-tagging property." << std::endl;
    //  for (CP::SystematicSet::const_iterator it = s.begin(); it != s.end(); it++) {
    //    std::cout << "DEBUG :: B-tagging Syst. " << it->name() << std::endl;
    //  }

}

BTaggingOfflineScaleFactorParam::~BTaggingOfflineScaleFactorParam() {
}

float BTaggingOfflineScaleFactorParam::applyBTagSF( Event sel, std::string s ){
 if (sel.channelNumber() == 0)
     return 1;

  //   int ptbinInS = -1;
  //   if(s.find("btag") != std::string::npos && s.find("_pt") != std::string::npos){
  //     size_t last = s.find("__1");
  //     size_t first = std::string("btagbSF_0_pt").size();
  //     ptbinInS = atoi(s.substr(first, last - first).c_str());
  //   }
   std::vector<TLorentzVector> jets;
   std::vector<int> jetFlavour;
   std::vector<float> jetWeight; 


   int ptbin  = -1;
   size_t iJet = 0;
   for (; iJet < sel.jet().size(); ++iJet) {
     if(!sel.jet()[iJet].pass())
       continue;
     jets.push_back(sel.jet()[iJet].mom());
     jetFlavour.push_back(sel.jet()[iJet].trueFlavour());
     jetWeight.push_back(sel.jet()[iJet].mv2c10());
     //  if(ptbinInS >= 0){
     //     if(sel.jet()[iJet].mom().Perp()*1e-3 < 50.)
     // 	 ptbin = 1;
     //     else if(sel.jet()[iJet].mom().Perp()*1e-3 < 100.)
     // 	 ptbin = 2;
     //     else
     // 	 ptbin = 3;
     //   }
     //   if(ptbinInS == ptbin)
     //     vetoSyst.push_back(false);
     //   else
     //     vetoSyst.push_back(true);
   }
   std::string syst = "";
   std::string direction = "";
   std::vector<std::string> tagSF = {"btagbSF_","btagcSF_","btaglSF_","btageSF_0","btageSF_1"};
   size_t lastSF = s.find("__1");
   if (s.find("down") != std::string::npos)
     direction = "down";
   else
     direction = "up";
   
   std::string eig = "";
   for(int l = 0; l < tagSF.size(); ++l){
     if (s.find(tagSF.at(l)) != std::string::npos){
       size_t firstSF = std::string(tagSF.at(l)).size();
       eig = s.substr(firstSF, lastSF - firstSF).c_str();
       //Assign same factor to _pt variations
       //   std::string key = "_pt";
       //       std::size_t found = eig.rfind(key);
       //       if (found!=std::string::npos)
       //	 eig.replace (found,key.length()+1,"");
       //       std::cout <<"DEBUG :: syst. : " << s <<" eigen: "<< eig << " SF : " << tagSF.at(l) << " first entry : "<< tagSF.at(0)<<std::endl;
       if(tagSF.at(l).compare(tagSF.at(0)) == 0){
	 syst = Form("FT_EFF_Eigen_B_%s__1%s", eig.c_str(), direction.c_str());
       }
       else if(tagSF.at(l).compare(tagSF.at(1)) == 0){
	 syst = Form("FT_EFF_Eigen_C_%s__1%s", eig.c_str(), direction.c_str());
       }
       else if(tagSF.at(l).compare(tagSF.at(2)) == 0){
 	 syst = Form("FT_EFF_Eigen_Light_%s__1%s", eig.c_str(), direction.c_str());
       }
       else if(tagSF.at(l).compare(tagSF.at(3)) == 0){
	 syst = Form("FT_EFF_Eigen_extrapolation__1%s", direction.c_str());
       }
       else if(tagSF.at(l).compare(tagSF.at(4)) == 0){
	 syst = Form("FT_EFF_Eigen_extrapolation_from_charm__1%s", direction.c_str());
        }
       else{
	 std::cerr << "ERROR:: No b-tagging SF specified... Exiting" << std::endl;
	 std::exit(-1);
       }
       //       std::cout <<"DEBUG :: >>>>>>> "<< syst << std::endl;
   }

   }
   return getBTaggingSF( jets, jetFlavour, jetWeight, syst );
   
}





float BTaggingOfflineScaleFactorParam::getBTaggingSF( std::vector<TLorentzVector> jet, std::vector<int> flavour, std::vector<float> mv2c10, std::string syst ){
   m_btageff.setMapIndex("B", 0);
   m_btageff.setMapIndex("C", 0);
   m_btageff.setMapIndex("T", 0);
   m_btageff.setMapIndex("Light", 0);
   
   float btagsf = 1.0;
    for (int k = 0; k < jet.size(); ++k) {
      float sf = 1;
      Analysis::CalibrationDataVariables v;
      v.jetPt = jet[k].Perp();
      v.jetEta = jet[k].Eta();
      v.jetTagWeight = mv2c10[k];
      v.jetAuthor = "AntiKt4EMTopoJets";
      if (!m_btageff.applySystematicVariation(CP::SystematicSet(syst))) {
        std::cout << "Could not apply the sys variation " << syst << std::endl;
        exit(-1);
      } 
      if (mv2c10[k] > 0.645925) { //@77% WP
      //      if (mv2c10[k] > 0.8244273) {//@70% WP
	//	std::cout <<"DEBUG :: " << flavour[k]<<" jPT: " << jet[k].Perp() <<" jETA : "<< jet[k].Eta() << " jTagWeight : "<< mv2c10[k] << std::endl;
        if (!m_btageff.getScaleFactor(flavour[k], v, sf)) {
    	 std::cout << "Could not get btag SF for " << syst << ", jet pt, eta, weight = " << v.jetPt << ", " << v.jetEta << ", " << v.jetTagWeight << std::endl;
    	 exit(-1);
        }
      }else {
       if (!m_btageff.getInefficiencyScaleFactor(flavour[k], v, sf)) {
    	 std::cout << "Could not get btag ineff. SF for " << syst << ", jet pt, eta, weight = " << v.jetPt << ", " << v.jetEta << ", " << v.jetTagWeight << std::endl;
    	 exit(-1);
        }
      }
      btagsf *= sf;
}
  return btagsf;
}
