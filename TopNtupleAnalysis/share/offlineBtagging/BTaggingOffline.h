#ifndef BTAGOFFLINESCALEFACTORPARAM_H
#define BTAGOFFLINESCALEFACTORPARAM_H
// weak corrections for powheg ttbar MC
// using parametrisations based on the HATHOR
// implementation of calculations from J. Kuehn, P. Uwer


#include <vector>
#include "TopNtupleAnalysis/Event.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "TLorentzVector.h"

class BTaggingOfflineScaleFactorParam {
  
public:
  BTaggingOfflineScaleFactorParam();    
  ~BTaggingOfflineScaleFactorParam();
  float applyBTagSF( Event sel, std::string s = "");
  float getBTaggingSF( std::vector<TLorentzVector> jet, std::vector<int> flavour, std::vector<float> mv2c10, std::string syst = "");

 private:
  //     Lhapdf m_pdf;
  // use pointer here to ease dictionary creation:
  //     Hathor* m_weak;
  BTaggingEfficiencyTool m_btageff;
};

#endif //BTAGOFFLINESCALEFACTORPARAM_H
