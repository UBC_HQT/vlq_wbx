sample    0data	    Data_13TeV_VLQ 			         "data"         "data"         1
sample    1ttbar    13TeV_ttbar	    			        "t#bar{t}"         "$t\bar{t}$"         0
sample    2singletop    mc15b_singletop                          "single top"       "single top"         62
sample    3Wjets           mc15b_Wjets "W+jets" "$W$+jets"           92
sample    4Zjets           mc15b_Zjets  "Z+jets"           "$Z$+jets"           91
sample    5VV               mc15b_diboson_sherpa                                  "diboson"          "diboson"            5
sample     6ttv	    mc15b_ttV				       "ttV"	   "t#bar{t}V"		       7
#sample    7QCD          QCD_13TeV_VLQ                               "multi-jet"        "multi-jet"          9
sample	   8TTS800WbWbsignal  13TeV_TTS_M800_WbWb "TT(#rightarrow WbWb) m_{T}=800 GeV" "TTS 800 WbWb" 21
