import os, glob, json, sys
import TopExamples.grid

import SampleList_data, SampleList_signal, SampleList_mc
# input directory
#ntuplesDir = '/data/VLQ/13TeV/production_AT-2.4.19/SamplePool/'
ntuplesDir = '/home/mdanning/analysis/VLQ13/NTUPLEproduction_WbX/AnalysisTop-2.4.19/set1/'

#ntuplesDir = '/Users/shenkel/data/VLQ/fromGrid/'
# output directory
#outputDir = 'nominal_AT2.3.41_test'
outputDir = 'TEST'
os.system( "mkdir -p " + str( outputDir ) )

nentries = 5000000


# the default is AnaTtresSL, which produces many control pltos for tt res.
# The Mtt version produces a TTree to do the limit setting
# the QCD version aims at plots for QCD studies using the matrix method
# look into read_hqt.cxx to see what is available
# create yours, if you wish


extraOpt = " --skipRegionPlots 0 "
<<<<<<< HEAD
showWeights = False
=======

showWeights = False

>>>>>>> b0732a9182be629d0bf89a217a566ca09f53794f
enableNtuple = False

enableNtupleWeightSys = False

# use a 1 for mini extension (standard analysis)
# use a 2 for combination extension (combined analysis checks)
if(enableNtuple):
    extraOpt += " --write_ntuple 0 "

data = False
dataTest = False
#mc15a = False
mc15c = False
ttbarOnly = True
ttbar_sherpa = False
Wjetsonly = False
QCD = False
#QCD16 = False
signalOnly = False
signalNtuple = False
NtupCombination = False

ttbar_AFII = False

analysisType='AnaHQTVLQWbX'
#selection = 'medium_v0'
selection = ['',# resolved selection
             'custom_Mor17'  # boosted selection
]
#    "",""]
# leave it for nominal to run only the nominal
#systematics = 'nominal,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up'
systematics = 'nominal'

#systematicsForNtuple = 'nominal,JET_19NP_JET_EffectiveNP_2__1up'
systematicsForNtuple = 'nominal'
systematicsForNtupleWeight = 'btagbSF_1__1up,btagbSF_1__1down'

systematicsAll = 'nominal,EG_RESOLUTION_ALL__1down,EG_RESOLUTION_ALL__1up,EG_SCALE_ALL__1down,EG_SCALE_ALL__1up,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up,JET_19NP_JET_EffectiveNP_3__1down,JET_19NP_JET_EffectiveNP_3__1up,JET_19NP_JET_EffectiveNP_4__1down,JET_19NP_JET_EffectiveNP_4__1up,JET_19NP_JET_EffectiveNP_5__1down,JET_19NP_JET_EffectiveNP_5__1up,JET_19NP_JET_EffectiveNP_6restTerm__1down,JET_19NP_JET_EffectiveNP_6restTerm__1up,JET_19NP_JET_EtaIntercalibration_Modelling__1down,JET_19NP_JET_EtaIntercalibration_Modelling__1up,JET_19NP_JET_EtaIntercalibration_NonClosure__1down,JET_19NP_JET_EtaIntercalibration_NonClosure__1up,JET_19NP_JET_EtaIntercalibration_TotalStat__1down,JET_19NP_JET_EtaIntercalibration_TotalStat__1up,JET_19NP_JET_Flavor_Composition__1down,JET_19NP_JET_Flavor_Composition__1up,JET_19NP_JET_Flavor_Response__1down,JET_19NP_JET_Flavor_Response__1up,JET_19NP_JET_Pileup_OffsetMu__1down,JET_19NP_JET_Pileup_OffsetMu__1up,JET_19NP_JET_Pileup_OffsetNPV__1down,JET_19NP_JET_Pileup_OffsetNPV__1up,JET_19NP_JET_Pileup_PtTerm__1down,JET_19NP_JET_Pileup_PtTerm__1up,JET_19NP_JET_Pileup_RhoTopology__1down,JET_19NP_JET_Pileup_RhoTopology__1up,JET_19NP_JET_PunchThrough_MC15__1down,JET_19NP_JET_PunchThrough_MC15__1up,JET_19NP_JET_SingleParticle_HighPt__1down,JET_19NP_JET_SingleParticle_HighPt__1up,JET_JER_SINGLE_NP__1up,LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1down,LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1up,LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1down,LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1up,LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1down,LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1up,LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1down,LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1up,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1down,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1up,LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1down,LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1up,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1down,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1up,LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1down,LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1up,LARGERJET_Strong_JET_Rtrk_Baseline_All__1down,LARGERJET_Strong_JET_Rtrk_Baseline_All__1up,LARGERJET_Strong_JET_Rtrk_Modelling_All__1down,LARGERJET_Strong_JET_Rtrk_Modelling_All__1up,LARGERJET_Strong_JET_Rtrk_TotalStat_All__1down,LARGERJET_Strong_JET_Rtrk_TotalStat_All__1up,LARGERJET_Strong_JET_Rtrk_Tracking_All__1down,LARGERJET_Strong_JET_Rtrk_Tracking_All__1up,LARGERJET_Weak_JET_Rtrk_Baseline_D2__1down,LARGERJET_Weak_JET_Rtrk_Baseline_D2__1up,LARGERJET_Weak_JET_Rtrk_Baseline_Tau32__1down,LARGERJET_Weak_JET_Rtrk_Baseline_Tau32__1up,LARGERJET_Weak_JET_Rtrk_Baseline_mass__1down,LARGERJET_Weak_JET_Rtrk_Baseline_mass__1up,LARGERJET_Weak_JET_Rtrk_Baseline_pT__1down,LARGERJET_Weak_JET_Rtrk_Baseline_pT__1up,LARGERJET_Weak_JET_Rtrk_Modelling_D2__1down,LARGERJET_Weak_JET_Rtrk_Modelling_D2__1up,LARGERJET_Weak_JET_Rtrk_Modelling_Tau32__1down,LARGERJET_Weak_JET_Rtrk_Modelling_Tau32__1up,LARGERJET_Weak_JET_Rtrk_Modelling_mass__1down,LARGERJET_Weak_JET_Rtrk_Modelling_mass__1up,LARGERJET_Weak_JET_Rtrk_Modelling_pT__1down,LARGERJET_Weak_JET_Rtrk_Modelling_pT__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_D2__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_D2__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_mass__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_mass__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1up,LARGERJET_Weak_JET_Rtrk_Tracking_D2__1down,LARGERJET_Weak_JET_Rtrk_Tracking_D2__1up,LARGERJET_Weak_JET_Rtrk_Tracking_Tau32__1down,LARGERJET_Weak_JET_Rtrk_Tracking_Tau32__1up,LARGERJET_Weak_JET_Rtrk_Tracking_mass__1down,LARGERJET_Weak_JET_Rtrk_Tracking_mass__1up,LARGERJET_Weak_JET_Rtrk_Tracking_pT__1down,LARGERJET_Weak_JET_Rtrk_Tracking_pT__1up,MET_SoftTrk_ResoPara,MET_SoftTrk_ResoPerp,MET_SoftTrk_ScaleDown,MET_SoftTrk_ScaleUp,MUONS_ID__1down,MUONS_ID__1up,MUONS_MS__1down,MUONS_MS__1up,MUONS_SCALE__1down,MUONS_SCALE__1up'


#Choose from predefined topology
region = "3jin_0bin_0Win"
with open('../share/topologies.json') as data_file:
    topo = json.load(data_file)
print ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) +' --njets '+ str(topo[region]["njet"]) + ' --njetEx '+ str(topo[region]["njetEx"])
names   = []
if(data):
    names  += ["Data_13TeV"]

if(dataTest):
    names  += ["Data_13TeV_test"]

if(QCD):
    names += ["QCD_13TeV"]

if(ttbar_AFII):
    ntuplesDir = '/data/VLQ/13TeV/production_AT-2.4.19/SamplePool_AFII/'
    names += ["13TeV_ttbar_PowPy8_radHi_AFII"]

if(ttbarOnly):
    names += ["13TeV_ttbar_nominal"]

if(NtupCombination):
    names += ["13TeV_ttbar_nominal"]
    names += ["Overlap_Sig_TTS"]
    names += ["Overlap_Sig_BBS"]

if(ttbar_sherpa):
    names += ["13TeV_ttbar_sherpa_lplus"]
    names += ["13TeV_ttbar_sherpa_lminus"]

if(signalNtuple):
    names += ["13TeV_TTS_M1000"]

if(signalOnly):
    names += ["13TeV_TTS_M500_WbWb"]
    names += ["13TeV_TTS_M600_WbWb"]
    names += ["13TeV_TTS_M700_WbWb"]
    names += ["13TeV_TTS_M750_WbWb"]
    names += ["13TeV_TTS_M800_WbWb"]
    names += ["13TeV_TTS_M850_WbWb"]
    names += ["13TeV_TTS_M900_WbWb"]
    names += ["13TeV_TTS_M950_WbWb"]
    names += ["13TeV_TTS_M1000_WbWb"]
    names += ["13TeV_TTS_M1050_WbWb"]
    names += ["13TeV_TTS_M1100_WbWb"]
    names += ["13TeV_TTS_M1150_WbWb"]
    names += ["13TeV_TTS_M1200_WbWb"]
    names += ["13TeV_TTS_M1300_WbWb"]
    names += ["13TeV_TTS_M1400_WbWb"]


if (mc15c):
    names += ["13TeV_ttbar_nominal"]
    names += ["mc15c_diboson_sherpa"]

    names += ["mc15c_Wbbcc_sherpa22"]
    names += ["mc15c_Wc_sherpa22"]
    names += ["mc15c_Wlight_sherpa22"]

    names += ["mc15c_Zbbcc_sherpa22"]
    names += ["mc15c_Zc_sherpa22"]
    names += ["mc15c_Zlight_sherpa22"]

    names += ["mc15c_singletop"]
    names += ["mc15c_ttV"]

if(Wjetsonly):
    names += ["mc15c_Wjets_sherpa22"]

files = glob.glob(ntuplesDir+'/*.root*')
samples = TopExamples.grid.Samples(names)

# get list of processed datasets
dirs = glob.glob(ntuplesDir+'/user*')

# each "sample" below means an item in the list names above
# there may contain multiple datasets
# for each sample we want to read
for sample in samples:
    extraOptions = " "
    if extraOpt:
        extraOptions += " " + str(extraOpt)
    if showWeights:
        extraOptions += " --printWeights 1 "

    print 'INFO :: Processing sample ... > '+str(sample.name)
    # write list of files to be read when processing this sample
    f = open(outputDir+"/input_"+sample.name+'.txt', 'w')
    # output file after running read
    #outfile = outputDir+"/"+sample.name
    outfile = sample.name

    # go over all directories in the ntuplesDir
    for d in dirs:
      # remove path and get only dir name in justfile
      justfile = d.split('/')[-1]
#      print justfile
      dsid_dir = justfile.split('.')[2] # get the DSID of the directory
      # this will include all directories, so check if this director is in the sample

      # now go over the list of datasets in sample
      # and check if this DSID is there
      for s in sample.datasets:
        if len(s.split(':')) > 1:
          s = s.split(':')[1] # skip mc15_13TeV
        dsid_sample = s.split('.')[1] # get DSID
        if dsid_dir == dsid_sample: # this dataset belongs in the sample in the big for loop
          # get all files in the directory
          files = glob.glob(d+'/*.root*')
          # and write it in ht elist of input files to process
          for item in files:
              if not '.part' in item:
                  f.write(item+'\n')
          # go to the next directory in the same sample
          break
    f.close()
    theSysts = systematics
    isData = '0'
    qcdPar = ''
    if "Data" in sample.name:
        theSysts = "nominal"
        isData = '1'
    elif "QCD_13TeV" in sample.name:
        theSysts = "nominal"
        isData = '1'
        qcdPar = ' --runMM 1 --loose 1 --DSyear 2015 '
#        qcdPar = ' --runMM 1 --loose 1 --DSyear 2015 '
#    elif "QCD_13TeV_2016" in sample.name:
#        theSysts = "nominal"
#        weightSysts = " --doWeightSystematics 0 "
#        isData = '2'
#        qcdPar = ' --runMM 1 --loose 1 '
    elif "13TeV_ttbar_nominal" in sample.name:
#        theSysts = systematicsAll
        theSysts = systematics
#--doWeightSystematics 1
    if(enableNtuple and enableNtupleWeightSys):
        systList = [x.strip() for x in systematicsForNtupleWeight.split(',')]
        for syst in systList:
            theSysts = syst
            this_outfile = outfile
            if "nominal" not in syst:
                this_outfile += syst
            command = '/usr/bin/time ./read_hqt  --data ' + isData + ' --applyPtRew 0 --doWeightSystematics 0 --grid2pre 1 --btagSFoffline 0 --PRWHashing 0 --TreeFriends 0 --resolvedSelection='+ str(selection[0]) + ' --boostedSelection='+ str(selection[1]) + ' ' + qcdPar + ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) +' --njets '+ str(topo[region]["njet"]) + ' --njetEx '+ str(topo[region]["njetEx"]) + ' --Wtags ' + str(topo[region]["Wtag"]) + ' --WtagEx '+ str(topo[region]["WtagEx"]) +' --files ' + outputDir + "/input_" + sample.name + '.txt' + ' --nentries ' + str(nentries) + ' --analysis ' + analysisType + ' --output ' + outputDir + '/resolved_e_' + this_outfile + '.root,' + outputDir + '/resolved_mu_' + this_outfile + '.root,'+ outputDir + '/resolved_comb_' + this_outfile + '.root,' + outputDir + '/boosted_e_' + this_outfile + '.root,' + outputDir + '/boosted_mu_' + this_outfile + '.root,' +outputDir + '/boosted_comb_' + this_outfile + '.root '+' --doSingleWeightSystematics' + theSysts  + " " +str(extraOptions)

            os.system( command )
            print command

    else:
        if(enableNtuple):
            systList = [x.strip() for x in systematicsForNtuple.split(',')]
        else:
            systList = [x.strip() for x in systematics.split(',')]
        for syst in systList:
            theSysts = syst
            this_outfile = outfile
            if "nominal" not in syst:
                this_outfile += syst
            command = '/usr/bin/time ./read_hqt  --data ' + isData + ' --applyPtRew 0 --doWeightSystematics 0 --grid2pre 1 --btagSFoffline 0 --PRWHashing 0 --TreeFriends 0 --resolvedSelection='+ str(selection[0]) + ' --boostedSelection='+ str(selection[1]) + ' ' + qcdPar + ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) +' --njets '+ str(topo[region]["njet"]) + ' --njetEx '+ str(topo[region]["njetEx"]) + ' --Wtags ' + str(topo[region]["Wtag"]) + ' --WtagEx '+ str(topo[region]["WtagEx"]) +' --files ' + outputDir + "/input_" + sample.name + '.txt' + ' --nentries ' + str(nentries) + ' --analysis ' + analysisType + ' --output ' + outputDir + '/resolved_e_' + this_outfile + '.root,' + outputDir + '/resolved_mu_' + this_outfile + '.root,'+ outputDir + '/resolved_comb_' + this_outfile + '.root,' + outputDir + '/boosted_e_' + this_outfile + '.root,' + outputDir + '/boosted_mu_' + this_outfile + '.root,' +outputDir + '/boosted_comb_' + this_outfile + '.root '+' --systs ' + theSysts + " "+str(extraOptions)

            os.system( command )
            print command

