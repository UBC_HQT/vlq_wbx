import os, glob, json
import TopExamples.grid

import SampleList_data, SampleList_signal, SampleList_mc
# input directory
#ntuplesDir = '/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/VLQ/production_AT-2.3.41/'
#ntuplesDir = '/data/VLQ/13TeV/production_AT-2.3.41/FinalSamples/'
ntuplesDir = '/data/VLQ/13TeV/production_AT-2.4.12/SamplePool/2015/'
#ntuplesDir = '/Users/shenkel/data/VLQ/fromGrid/'
# output directory
#outputDir = 'nominal_AT2.3.41_test'
outputDir = 'TEST'
os.system( "mkdir -p " + str( outputDir ) )
nentries = -1
# the default is AnaTtresSL, which produces many control pltos for tt res.
# The Mtt version produces a TTree to do the limit setting
# the QCD version aims at plots for QCD studies using the matrix method
# look into read_hqt.cxx to see what is available
# create yours, if you wish
data = True
#mc15a = False
analysisType='AnaHQTVLQWbX'

# leave it for nominal to run only the nominal
systematics = 'nominal'

systematicsAll = 'nominal,EG_RESOLUTION_ALL__1down,EG_RESOLUTION_ALL__1up,EG_SCALE_ALL__1down,EG_SCALE_ALL__1up,MUONS_ID__1down,MUONS_ID__1up,MUONS_MS__1down,MUONS_MS__1up,MUONS_SCALE__1down,MUONS_SCALE__1up,JET_19NP_JET_BJES_Response__1down,JET_19NP_JET_BJES_Response__1up,JET_19NP_JET_EffectiveNP_1__1down,JET_19NP_JET_EffectiveNP_1__1up,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up,JET_19NP_JET_EffectiveNP_3__1down,JET_19NP_JET_EffectiveNP_3__1up,JET_19NP_JET_EffectiveNP_4__1down,JET_19NP_JET_EffectiveNP_4__1up,JET_19NP_JET_EffectiveNP_5__1down,JET_19NP_JET_EffectiveNP_5__1up,JET_19NP_JET_EffectiveNP_6restTerm__1down,JET_19NP_JET_EffectiveNP_6restTerm__1up,JET_19NP_JET_EtaIntercalibration_Modelling__1down,JET_19NP_JET_EtaIntercalibration_Modelling__1up,JET_19NP_JET_EtaIntercalibration_TotalStat__1down,JET_19NP_JET_EtaIntercalibration_TotalStat__1up,JET_19NP_JET_Flavor_Composition__1down,JET_19NP_JET_Flavor_Composition__1up,JET_19NP_JET_Flavor_Response__1down,JET_19NP_JET_Flavor_Response__1up,JET_19NP_JET_GroupedNP_1__1down,JET_19NP_JET_GroupedNP_1__1up,JET_19NP_JET_Pileup_OffsetMu__1down,JET_19NP_JET_Pileup_OffsetMu__1up,JET_19NP_JET_Pileup_OffsetNPV__1down,JET_19NP_JET_Pileup_OffsetNPV__1up,JET_19NP_JET_Pileup_PtTerm__1down,JET_19NP_JET_Pileup_PtTerm__1up,JET_19NP_JET_Pileup_RhoTopology__1down,JET_19NP_JET_Pileup_RhoTopology__1up,JET_19NP_JET_PunchThrough_MC15__1down,JET_19NP_JET_PunchThrough_MC15__1up,JET_19NP_JET_SingleParticle_HighPt__1down,JET_19NP_JET_SingleParticle_HighPt__1up,JET_JER_SINGLE_NP__1down,JET_JER_SINGLE_NP__1up,LARGERJET_JET_WZ_CrossCalib__1down,LARGERJET_JET_WZ_CrossCalib__1up,LARGERJET_JET_WZ_Run1__1down,LARGERJET_JET_WZ_Run1__1up,LARGERJET_SplitScales1_JET_WZ_CrossCalib__1down,LARGERJET_SplitScales1_JET_WZ_CrossCalib__1up,LARGERJET_SplitScales1_JET_WZ_Run1_D2__1down,LARGERJET_SplitScales1_JET_WZ_Run1_D2__1up,LARGERJET_SplitScales1_JET_WZ_Run1_mass__1down,LARGERJET_SplitScales1_JET_WZ_Run1_mass__1up,LARGERJET_SplitScales1_JET_WZ_Run1_pT__1down,LARGERJET_SplitScales1_JET_WZ_Run1_pT__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_D2__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_D2__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_mass__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_mass__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_pT__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_pT__1up,LARGERJET_SplitScales2_JET_WZ_Run1_D2__1down,LARGERJET_SplitScales2_JET_WZ_Run1_D2__1up,LARGERJET_SplitScales2_JET_WZ_Run1_mass__1down,LARGERJET_SplitScales2_JET_WZ_Run1_mass__1up,LARGERJET_SplitScales2_JET_WZ_Run1_pT__1down,LARGERJET_SplitScales2_JET_WZ_Run1_pT__1up,MET_SoftTrk_ResoPara,MET_SoftTrk_ResoPerp,MET_SoftTrk_ScaleDown,MET_SoftTrk_ScaleUp'

selection = ["res_medium_v0", "medium_v0"]
#Choose from predefined topology
region = "4jin_2bin_0Win"
with open('../share/topologies.json') as data_file:
    topo = json.load(data_file)
print ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) +' --njets '+ str(topo[region]["njet"]) + ' --njetEx '+ str(topo[region]["njetEx"])
names   = []
if(data):
#    names  += ["Data_13TeV_2015"]
    names  += ["QCD_13TeV_2015"]


files = glob.glob(ntuplesDir+'/*.root*')
samples = TopExamples.grid.Samples(names)

# get list of processed datasets
dirs = glob.glob(ntuplesDir+'/user*')

# each "sample" below means an item in the list names above
# there may contain multiple datasets
# for each sample we want to read
for sample in samples:
    print 'INFO :: Processing sample ... > '+str(sample.name)
    # write list of files to be read when processing this sample
    f = open(outputDir+"/input_"+sample.name+'.txt', 'w')
    # output file after running read
    #outfile = outputDir+"/"+sample.name
    outfile = sample.name

    # go over all directories in the ntuplesDir
    for d in dirs:
      # remove path and get only dir name in justfile
      justfile = d.split('/')[-1]
#      print justfile
      dsid_dir = justfile.split('.')[2] # get the DSID of the directory
      # this will include all directories, so check if this director is in the sample

      # now go over the list of datasets in sample
      # and check if this DSID is there
      for s in sample.datasets:
        if len(s.split(':')) > 1:
          s = s.split(':')[1] # skip mc15_13TeV
        dsid_sample = s.split('.')[1] # get DSID
        if dsid_dir == dsid_sample: # this dataset belongs in the sample in the big for loop
          # get all files in the directory
          files = glob.glob(d+'/*.root*')
          # and write it in ht elist of input files to process
          for item in files:
              if not '.part' in item:
                  f.write(item+'\n')
          # go to the next directory in the same sample
          break
    f.close()
    theSysts = systematics
    isData = '0'
    qcdPar = ' --runMM 0 --loose 0'
    if "Data" in sample.name:
        theSysts = "nominal"
        isData = '1'
    elif "QCD" in sample.name:
        theSysts = "nominal"
        isData = '1'
        qcdPar = ' --runMM 1 --loose 1 '
    elif "13TeV_ttbar" in sample.name:
#        theSysts = systematicsAll
        theSysts = systematics
    command = './read_hqt  --grid2pre 1  --data ' + isData +  ' --resolvedSelection='+ str(selection[0]) + ' --boostedSelection='+ str(selection[1]) + ' ' + qcdPar + ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) +' --njets '+ str(topo[region]["njet"]) + ' --njetEx '+ str(topo[region]["njetEx"]) +' --files ' + outputDir + "/input_" + sample.name + '.txt' + ' --nentries ' + str(nentries) + ' --analysis ' + analysisType + ' --output ' + outputDir + '/resolved_e_' + outfile + '.root,' + outputDir + '/resolved_mu_' + outfile + '.root,'  + outputDir + '/boosted_comb_' + outfile + '.root,' + outputDir + '/boosted_e_' + outfile + '.root,' + outputDir + '/boosted_mu_' + outfile + '.root,' +outputDir + '/boosted_comb_' + outfile + '.root '+' --systs ' + theSysts
    os.system( command )
    print command
