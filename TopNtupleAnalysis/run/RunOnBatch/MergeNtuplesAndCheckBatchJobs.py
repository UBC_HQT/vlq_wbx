from __future__ import division
from subprocess import Popen,PIPE,STDOUT
import subprocess
import os,sys,json
import shutil

# output directory
outdir = sys.argv[1]#'jobsOnFlash_testMerge_2016-03-22-06-23/'

#region = ['4jin_2bin_0Win']
#region = ['3jin_0bin_0Win','3jin_1bin_0Win','3jin_2bin_0Win','4jex_0bin_0Win','4jex_1bin_0Win','4jin_2bin_0Win']
#region = ['3jin_0bin_0Win']
region = []

#region = ['0jin_0bin_0Win']
with open(outdir + '/jobSummary.json') as job_file:
    jobinfo = json.load(job_file)
for keys in jobinfo.keys():
    region += [keys]
for reg in region:
    samples = jobinfo[reg].keys()
    systAll = jobinfo[reg].values()
print systAll
systematics = list(set(systAll[0]))



print samples
print systematics

# 25 ns datasets
#region = ['3jex_0bin_0Win']
print 'INFO :: Merging the following regions ...'
#for reg in region:
#    print reg
print region
#channels = ['resolved_e', 'resolved_mu', 'boosted_e', 'boosted_mu','boosted_comb']
#channels = ['resolved_e', 'resolved_mu','resolved_comb','boosted_e', 'boosted_mu','boosted_comb']
#channels = ['boosted_e', 'boosted_mu','boosted_comb']
channels = ['boosted_comb']
#channels = ['resolved_comb']
#channels = ['boosted_e', 'boosted_mu']

import glob
loc = os.environ['PWD']+"/"+outdir
os.chdir(loc)

os.system('pwd')

os.system('mkdir -p /tmp/VLQ/'+str(outdir))
os.system('mkdir -p /tmp/VLQ/'+str(outdir)+'/mergedJobs/')

os.system('mkdir -p '+str(loc)+'/mergedJobs')
listFailedJobs = {}
for reg in region:
    os.system('mkdir -p /tmp/VLQ/'+str(outdir)+'/mergedJobs/'+str(reg))
    os.chdir(loc+"/mergedJobs")
    os.system('mkdir -p '+str(reg))

    print 'INFO :: Check if all jobs ran successfully...'
    os.chdir(loc+"/BatchJobHandler/PBS/"+reg)
    for sample in samples:
        codeCalls = 0
        yieldC = 0
        os.chdir(loc+"/BatchJobHandler/PBS/"+reg+"/"+sample)
        for filename in glob.iglob('*.log'):
#            line = subprocess.check_output(['tail', '-100 '+ str(filename)], shell=True)
            try:
                line = subprocess.check_output('tail -100 ' + str(filename)+ " | grep \"Called analysis code\"", shell=True)
            except subprocess.CalledProcessError as e:
                reexec =  'qsub -V ' + str(outdir) + 'BatchJobHandler/batchscripts/' + str(reg) + '/'+str(sample)+'/run_' + str(sample) + "_" + str(filename.split('_')[-1]).split('.')[0] + '.pbs'
                listFailedJobs[str(filename.split('_')[-1]).split('.')[0]] = reexec
                line = ""
#            else:
#                reexec =  'qsub -V ' + str(outdir) + 'BatchJobHandler/batchscripts/' + str(reg) + '/'+str(sample)+'/run_' + str(sample) + "_" + str(filename.split('_')[-1]).split('.')[0] + '.pbs'
#                listFailedJobs[str(filename.split('_')[-1]).split('.')[0]] = reexec
#            head = Popen(str('head -1500 '+str(filename)).split(), stdout=subprocess.PIPE)
#            grep = Popen('grep -m 1 (nominal)'.split(), stdin=head.stdout, stdout=subprocess.PIPE)
#            entry =  grep.communicate()[0].split('/',1)[1]
#            print entry
#            head.stdout.close()
#            grep.stdout.close()
#            head.wait()
            #entry = subprocess.check_output("tail -n +1 | head -1500 " + str(filename)+ " | grep -m 1 \"(nominal) Entry\" | awk -F\'/\' \'{print $2}\'", shell=True)
            calls = [int(s) for s in line.split() if s.isdigit()]
            if(not calls):
                reexec =  'qsub -V ' + str(outdir) + 'BatchJobHandler/batchscripts/' + str(reg) + '/'+str(sample)+'/run_' + str(sample) + "_" + str(filename.split('_')[-1]).split('.')[0] + '.pbs'
                listFailedJobs[str(filename.split('_')[-1]).split('.')[0]] = reexec
            elif (not line.find('Called analysis code '+str(calls))):
                 reexec =  'qsub -V ' + str(outdir) + 'BatchJobHandler/batchscripts/' + str(reg) + '/'+str(sample)+'/run_' + str(sample) + "_" + str(filename.split('_')[-1]).split('.')[0] + '.pbs'
                 listFailedJobs[str(filename.split('_')[-1]).split('.')[0]] = reexec
            else:
                codeCalls += int(calls[0])
#                yieldC += int(entry)
                yieldC += 1
            print 'SUCCESS :: sample', filename.rsplit('.log',1)[0], ' was called ', codeCalls, ' times'# and had ', yieldC, ' entries in total after grid selection'
    if listFailedJobs:
        print 'ERROR :: JOB(s) ', listFailedJobs.keys(), ' did not run successfully '
        print 'INFO :: Suggestion to increase wall-time first and then re-run with : \n'
        for cmd in listFailedJobs.values():
            print cmd
        sys.exit()
    else:
        print 'SUCCESS :: No samples failed ...'# and had ', yieldC, ' entries in total after grid selection'
    response = raw_input("\n QUESTION :: Do you want to continue and merge? >> : ").lower()
    yes = set(['yes','y', 'ye', 'YES','Yes','j','Ja','ja'])
    no = set(['no','n','NO','No'])
    if response not in yes:
        print "INFO :: OK leaving without merging ..."
        sys.exit()
    else:
        print "INFO :: Start copying to /tmp and merge"

    os.chdir(loc+'/BatchJobOutput/')
    os.chdir(loc+"/BatchJobOutput/"+reg)
    print '##########'
    os.system('cp -r * /tmp/VLQ/'+str(outdir)+'/mergedJobs/'+reg)
    for sample in samples:
        os.chdir('/tmp/VLQ/'+str(outdir)+'/mergedJobs/'+reg+"/"+sample)
        os.system('pwd')
        for ch in channels:
            for syst in systematics:
                syst = syst.replace(samples[0],sample)
                print 'DEBUG :: ', syst
                filesToMerge = []
                for subdirs,dirs, files in os.walk('/tmp/VLQ/'+str(outdir)+'/mergedJobs/'+reg+"/"+sample):
                    for file in files:
                        if file.find(ch+"_"+syst+"_mini.root") != -1:
                            filesToMerge.append(os.path.join(subdirs, file))
                        else:
                            continue

            #           cmd = 'hadd -f -k '+loc+"/mergedJobs/"+str(reg)+"/merged_"+ch+"_"+sample+".root"+"  "
                cmd = 'hadd -f -k '+"/tmp/VLQ/"+str(outdir)+"/mergedJobs/"+str(reg)+"/merged_"+ch+"_"+syst+".root"+"  "
                if len(filesToMerge) == 0:
                    print "Failed to find any result from channel "+ch+" for sample "+sample
                    continue
                for i in filesToMerge:
                    cmd = cmd + "  " + i

                print(cmd)
                os.system(cmd)
                os.system("cp -r /tmp/VLQ/"+str(outdir)+"/mergedJobs/"+str(reg)+"/merged_"+ch+"_"+syst+".root " +str(loc)+"/mergedJobs/"+str(reg)+"/")
    print 'INFO :: Clean /tmp/VLQ/'+str(outdir)+'/mergedJobs/'+str(reg)+'/ folder'
    shutil.rmtree('/tmp/VLQ/'+str(outdir)+'/mergedJobs/'+reg+"/")
print 'INFO :: Clean /tmp/'+str(outdir)+' folder'
shutil.rmtree('/tmp/VLQ/'+str(outdir))

#for ch in channels:
#    os.system('hadd -f -k '+outdir+"/"+ch+'_MC15_13TeV_25ns_FS_EXOT4_ttbarPowhegPythia_all.root '+outdir+"/"+ch+'_MC15_13TeV_25ns_FS_EXOT4_ttbarPowhegPythia.root '+outdir+"/"+ch+'_MC15_13TeV_25ns_FS_EXOT4_ttbarPowhegPythia_mttsliced.root')
