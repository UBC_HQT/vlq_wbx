import os, glob
import TopExamples.grid

import HQTVLQWbXTools.vlqData15
import HQTVLQWbXTools.MC15a_exot4
import HQTVLQWbXTools.MC15b_exot4

from subprocess import Popen,PIPE

# lxbatch queue to submit to
queue = 'atlasb1'

# email to use to tell us when the job is done
email = 'steffen.henkelmann@cern.ch'

# directory with the base of the RootCore stuff
rundir = '/afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/VLQ/AnalysisFramework/AnalysisTop-2.3.41/TopNtupleAnalysis/run/runOnBatch'

analysisFolder = '/afs/cern.ch/work/s/sthenkel/work/Physics/Analysis/VLQ/AnalysisFramework/AnalysisTop-2.3.41/TopNtupleAnalysis/'
# number of files per job
nFilesPerJob = 2

# input directory
ntuplesDir = '/eos/atlas/user/s/sthenkel/13TeV/VLQ/production_AT-2.3.41/'

#eosrun = '/afs/cern.ch/project/eos/installation/0.3.84-aquamarine.user/bin/eos.select'
eosrun='/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select'
#entry = 'root://eosuser.cern.ch/'
entry = 'root://eosatlas.cern.ch/'

# output directory
outputDir = rundir+'/TopNtupleAnalysis/VLQWbX_test'
os.system("mkdir -p " + str(outputDir))

# the default is AnaTtresSL, which produces many control pltos for tt res.
# The Mtt version produces a TTree to do the limit setting
# the QCD version aims at plots for QCD studies using the matrix method
# look into read.cxx to see what is available
# create yours, if you wish
#analysisType='AnaTtresSLMtt'
data = True
mc15a = False
mc15b = True
analysisType='AnaHQTVLQWbX'

# leave it for nominal to run only the nominal
#systematics = 'nominal'
systematics = 'nominal,EG_RESOLUTION_ALL__1down,EG_RESOLUTION_ALL__1up,EG_SCALE_ALL__1down,EG_SCALE_ALL__1up,JET_19NP_JET_BJES_Response__1down,JET_19NP_JET_BJES_Response__1up,JET_19NP_JET_EffectiveNP_1__1down,JET_19NP_JET_EffectiveNP_1__1up,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up,JET_19NP_JET_EffectiveNP_3__1down,JET_19NP_JET_EffectiveNP_3__1up,JET_19NP_JET_EffectiveNP_4__1down,JET_19NP_JET_EffectiveNP_4__1up,JET_19NP_JET_EffectiveNP_5__1down,JET_19NP_JET_EffectiveNP_5__1up,JET_19NP_JET_EffectiveNP_6restTerm__1down,JET_19NP_JET_EffectiveNP_6restTerm__1up,JET_19NP_JET_EtaIntercalibration_Modelling__1down,JET_19NP_JET_EtaIntercalibration_Modelling__1up,JET_19NP_JET_EtaIntercalibration_TotalStat__1down,JET_19NP_JET_EtaIntercalibration_TotalStat__1up,JET_19NP_JET_Flavor_Composition__1down,JET_19NP_JET_Flavor_Composition__1up,JET_19NP_JET_Flavor_Response__1down,JET_19NP_JET_Flavor_Response__1up,JET_19NP_JET_GroupedNP_1__1down,JET_19NP_JET_GroupedNP_1__1up,JET_19NP_JET_Pileup_OffsetMu__1down,JET_19NP_JET_Pileup_OffsetMu__1up,JET_19NP_JET_Pileup_OffsetNPV__1down,JET_19NP_JET_Pileup_OffsetNPV__1up,JET_19NP_JET_Pileup_PtTerm__1down,JET_19NP_JET_Pileup_PtTerm__1up,JET_19NP_JET_Pileup_RhoTopology__1down,JET_19NP_JET_Pileup_RhoTopology__1up,JET_19NP_JET_PunchThrough_MC15__1down,JET_19NP_JET_PunchThrough_MC15__1up,JET_19NP_JET_SingleParticle_HighPt__1down,JET_19NP_JET_SingleParticle_HighPt__1up,JET_JER_SINGLE_NP__1up,LARGERJET_JET_WZ_CrossCalib__1down,LARGERJET_JET_WZ_CrossCalib__1up,LARGERJET_JET_WZ_Run1__1down,LARGERJET_JET_WZ_Run1__1up,LARGERJET_SplitScales1_JET_WZ_CrossCalib__1down,LARGERJET_SplitScales1_JET_WZ_CrossCalib__1up,LARGERJET_SplitScales1_JET_WZ_Run1_D2__1down,LARGERJET_SplitScales1_JET_WZ_Run1_D2__1up,LARGERJET_SplitScales1_JET_WZ_Run1_mass__1down,LARGERJET_SplitScales1_JET_WZ_Run1_mass__1up,LARGERJET_SplitScales1_JET_WZ_Run1_pT__1down,LARGERJET_SplitScales1_JET_WZ_Run1_pT__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_D2__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_D2__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_mass__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_mass__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_pT__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_pT__1up,LARGERJET_SplitScales2_JET_WZ_Run1_D2__1down,LARGERJET_SplitScales2_JET_WZ_Run1_D2__1up,LARGERJET_SplitScales2_JET_WZ_Run1_mass__1down,LARGERJET_SplitScales2_JET_WZ_Run1_mass__1up,LARGERJET_SplitScales2_JET_WZ_Run1_pT__1down,LARGERJET_SplitScales2_JET_WZ_Run1_pT__1up,MET_SoftTrk_ResoPara,MET_SoftTrk_ResoPerp,MET_SoftTrk_ScaleDown,MET_SoftTrk_ScaleUp,MUONS_ID__1down,MUONS_ID__1up,MUONS_MS__1down,MUONS_MS__1up,MUONS_SCALE__1down,MUONS_SCALE__1up'


# set to 1 to run the loose selection for QCD
loose = 0

# number of btags (negative for track jet btagging)
btags = -1

# apply electroweak correction in ttbar?
applyEWK = 0

names   = []
if(data):
    names  += ["Data_13TeV_VLQ"]

if (mc15a):
    #signal
    names += ["mc15a_TTS"]
    names += ["mc15a_VLQ"]
    #background
    names += ["mc15a_singletop"]
    names += ["mc15a_vjets_powhegpythia_p2432"]
    names += ["mc15a_diboson"]
    
if (mc15b):
    #signal
    
    #background
    names += ["13TeV_ttbar"]
    names += ["mc15b_singletop"]
    names += ["mc15b_ttV"]
    names += ["mc15b_Vjets"]
    names += ["mc15b_diboson"]

    

import TopExamples.grid

import glob
#files = glob.glob(ntuplesDir+'/*.root')
samples = TopExamples.grid.Samples(names)

import glob
import os
# get list of processed datasets
#dirs = glob.glob(ntuplesDir+'/*')
dirs = Popen([eosrun, "ls", ntuplesDir], stdout=PIPE).communicate()[0].split('\n')

# each "sample" below means an item in the list names above
# there may contain multiple datasets
# for each sample we want to read
for sample in samples:
    # write list of files to be read when processing this sample
    f = open(outputDir+"/input_"+sample.name+'.txt', 'w')
    # output file after running read
    #outfile = outputDir+"/"+sample.name
    outfile = sample.name

    forJobs = {}
    nJob = 0
    nFile = 0
    
    # go over all directories in the ntuplesDir
    for d in dirs:
      if d == '':
         continue

      # remove path and get only dir name in justfile
      #justfile = d.split('/')[-1]
      #dsid_dir = justfile.split('.')[2] # get the DSID of the directory
      dsid_dir = d.split('.')[2] # get the DSID of the directory
      # this will include all directories, so check if this director is in the sample

      # now go over the list of datasets in sample
      # and check if this DSID is there
      for s in sample.datasets:
        if len(s.split(':')) > 1:
          s = s.split(':')[1] # skip mc15_13TeV
        dsid_sample = s.split('.')[1] # get DSID
        if dsid_dir == dsid_sample: # this dataset belongs in the sample in the big for loop
          # get all files in the directory
          #files = glob.glob(d+'/*.root*')
          files = Popen([eosrun, "ls", ntuplesDir+"/"+d], stdout=PIPE).communicate()[0].split('\n')
          # and write it in ht elist of input files to process
          for item in files:
              if not '.part' in item and item != '':
                  #fullname = item
                  fullname = entry+ntuplesDir+"/"+d+'/'+item
                  f.write(fullname+'\n')

                  if nFile == nFilesPerJob:
                     nFile = 0
                     nJob = nJob + 1
                  if nFile == 0:
                     forJobs[str(nJob)] = []
                  forJobs[str(nJob)].append(fullname+'\n')
                  nFile = nFile + 1
          # go to the next directory in the same sample
          break
    f.close()
    theSysts = systematics
    isData = 0
    if "Data" in sample.name:
      theSysts = "nominal"
      isData = 1

    for job in forJobs:
        jobName = sample.name+'_'+job
        infile = outputDir+"/input_"+jobName+'.txt'
        infullfile = outputDir+"/input_"+sample.name+'.txt'
        f = open(infile, 'w')
        for item in forJobs[job]:
            f.write(item)
        f.close()
        errfile = outputDir+"/stderr_"+jobName+'.txt'
        logfile = outputDir+"/stdout_"+jobName+'.txt'
        runfile = outputDir+"/run_"+jobName+'.sh'
        fr = open(runfile, 'w')
        fr.write('#!/bin/sh\n')
        fr.write('cd '+rundir+'\n')
        fr.write('source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh\n')
        fr.write('lsetup rcsetup\n')
        fr.write('cd TopNtupleAnalysis\n')
        fr.write('ls\n')
        fr.write('ln -s ' + str(analysisFolder) + '/read_hqt read_hqt\n')
        command = './read_hqt --removeOverlapHighMtt 1 --data ' + str(isData) + ' ' + ' --btags ' + str(btags) + ' --files ' + outputDir + "/input_" + sample.name + '.txt' +  ' --analysis ' + analysisType + ' --output ' + outputDir + '/resolved_e_' + outfile + '.root,' + outputDir + '/resolved_mu_' + outfile + '.root,' + outputDir + '/boosted_e_' + outfile + '.root,' + outputDir + '/boosted_mu_' + outfile + '.root --systs ' + theSysts
        fr.write(command)
        fr.close()
        os.system('chmod a+x '+runfile)
        subcmd = 'bsub -e '+errfile+' -o '+logfile+' -q '+queue+' -N -u '+email+' -J tna_'+jobName+' '+runfile
        print '###############################'
        print  subcmd
        print '###############################'
        os.system(subcmd)
        #print(subcmd)
        #import sys
        #sys.exit(0)

