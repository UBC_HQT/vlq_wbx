import os, glob, sys, json, time, datetime
from collections import defaultdict
import pickle
import TopExamples.grid
#helper scripts defined in TopNtupleAnalysis/python
import SampleList_data, SampleList_signal, SampleList_signal_WtX, SampleList_mc
import JobMonitoringHelpers as jobhelpers
import jobSteerings

# email to use to tell us when the job is done
user = os.environ['USER']
#email = user + '@cern.ch'
#rundir = '/home/sthenkel/work/Physics/Analysis/VLQ/WbXWbX/AnalysisTop-2.3.41/vlq_wbx_wbx/TopNtupleAnalysis/run/RunOnBatch/jobsOnFlash_' +str(UserJobName)+"_"+ str(timestamp)
#analysisFolder = '/home/sthenkel/work/Physics/Analysis/VLQ/WbX/EOYE2016/NTUPLEproduction_WbX/AnalysisTop-2.4.19/vlq_wbx/TopNtupleAnalysis'
try:
    analysisFolder = os.environ['ANATOP'] + "vlq_wbx/TopNtupleAnalysis/"
    print("INFO :: The configured analysis folder is", analysisFolder)
except KeyError, e:
    print 'ERROR :: You forgot to run the setup script ($> . setup.sh) in the analysis folder which exports the global variable "%s"' % str(e)
    sys.exit(1)

# Call the jobs
###############
#Set="Set4"
Set="Set4"
UserJobName = "_prod2419_Ntuple_Allsys%s"%Set

nentries = -1
walltime="04:30:00"
grid2pre=1 # 0 is false
wSysts = False
AFII = False
TtbarModel = False
QCD = False

# the default is AnaHQTVLQWbX, which produces many control plots and plots in the specified region
analysisType='AnaHQTVLQWbX'

extraOpt = " --skipRegionPlots 1 "
showWeights = False


enableNtuple = True
enableNtupleWeightSys = False

jobSteer = {
    "fileLocation" :    [
        #"/data/VLQ/13TeV/production_AT-2.4.19/SamplePool/"
        "/data/VLQ/13TeV/production_AT-2.4.19/SamplePool_ORisoChecks/%s/"%Set
        ],
    "regionTopo" : [
        '3jin_0bin_0Win'
        ],
    "samples" :    {
        "IsoSets": True,
        "data": False,
        "mc15c": False,
        "signalNtuple": False,
        "mc15cModel" : False,
        "mc15c_Wjets_comp" : False,
        "mc15c_AFII" : False,
        "QCD" : False,
        },
    "selection" :    {
        "resolved": '',
        "boosted":  'custom_Mor17',
        }
    }

# Set by hand for tests
ttbarOnly = False
ttbar_PowPy8 = False
ttbar_sherpa = False
mcsingletop = False
WjetsOnly = False
s13TeV_TTS_M1100_WbWbOnly = False
s13TeV_TTS_M1300_ZtHt =False
mc15c_Wbbcc_only = False
signalWbWb = False
signal = False
signal_WtX =False
diboson = False
# Different ttbar baseline
ttbarAlternative = False
stop_ttV = False

#predefined job steerings using different directories
if AFII:
    jobSteer = jobSteerings.jobsAFII(jobSteer["regionTopo"][0], jobSteer["selection"]["resolved"], jobSteer["selection"]["boosted"])
elif TtbarModel:
    jobSteer = jobSteerings.jobsTtbarModel(jobSteer["regionTopo"][0], jobSteer["selection"]["resolved"], jobSteer["selection"]["boosted"])
elif QCD:
    jobSteer = jobSteerings.jobsQCDdata(jobSteer["regionTopo"][0], jobSteer["selection"]["resolved"], jobSteer["selection"]["boosted"])
else:
    jobSteer = jobSteer

print 'INFO :: Will run with the following specifications ...\n'
print jobSteer
print 'INFO :: userJobName > ', UserJobName
print 'INFO :: nentries > ', nentries
print 'INFO :: walltime > ', walltime
print 'INFO :: grid2pre > ', grid2pre
print 'INFO :: AFII > ', AFII
print 'INFO :: TtbarModel > ', TtbarModel
print 'INFO :: QCD > ', QCD


# input directory
ntuplesDir = jobSteer["fileLocation"][0]

# Those need to be run for input to TrexFitter
IsoSets = jobSteer["samples"]["IsoSets"]
data = jobSteer["samples"]["data"]
mc15c = jobSteer["samples"]["mc15c"]
signalNtuple = jobSteer["samples"]["signalNtuple"]
mc15cModel = jobSteer["samples"]["mc15cModel"]
mc15c_Wjets_comp = jobSteer["samples"]["mc15c_Wjets_comp"]

# Paths need to be modified for the following two (ntuplesDir)
mc15c_AFII = jobSteer["samples"]["mc15c_AFII"]
QCD = jobSteer["samples"]["QCD"]


#Choose from predefined topology, when [] or [''], you will run on all predefined topologies
#regionTopo = ['3jin_0bin_0Win']
regionTopo = jobSteer["regionTopo"]

#choose selection
print 'INFO :: ', jobSteer["selection"]["boosted"]
selection=[
    jobSteer["selection"]["resolved"],  # resolved selection
    jobSteer["selection"]["boosted"]    # boosted selection
    ]


# Set to 1 in case you do not ran new MC when including new data
prwHashing = 0
btagSFoffline = 0



# set to 1 to run the loose selection for QCD
loose = 0
# apply electroweak correction in ttbar?
applyEWK = 0


# leave it for nominal to run only the nominal
systematics = 'nominal'

if(TtbarModel or mc15cModel):
    systematicsForNtuple = 'nominal'
else:

    #systematicsForNtuple = 'nominal,EG_RESOLUTION_ALL__1down,EG_RESOLUTION_ALL__1up,EG_SCALE_ALL__1down,EG_SCALE_ALL__1up,JET_19NP_JET_BJES_Response__1down,JET_19NP_JET_BJES_Response__1up,JET_19NP_JET_EffectiveNP_1__1down,JET_19NP_JET_EffectiveNP_1__1up,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up,JET_19NP_JET_EffectiveNP_3__1down,JET_19NP_JET_EffectiveNP_3__1up,JET_19NP_JET_EffectiveNP_4__1down,JET_19NP_JET_EffectiveNP_4__1up,JET_19NP_JET_EffectiveNP_5__1down,JET_19NP_JET_EffectiveNP_5__1up,JET_19NP_JET_EffectiveNP_6restTerm__1down,JET_19NP_JET_EffectiveNP_6restTerm__1up,JET_19NP_JET_EtaIntercalibration_Modelling__1down,JET_19NP_JET_EtaIntercalibration_Modelling__1up,JET_19NP_JET_EtaIntercalibration_NonClosure__1down,JET_19NP_JET_EtaIntercalibration_NonClosure__1up,JET_19NP_JET_EtaIntercalibration_TotalStat__1down,JET_19NP_JET_EtaIntercalibration_TotalStat__1up,JET_19NP_JET_Flavor_Composition__1down,JET_19NP_JET_Flavor_Composition__1up,JET_19NP_JET_Flavor_Response__1down,JET_19NP_JET_Flavor_Response__1up,JET_19NP_JET_Pileup_OffsetMu__1down,JET_19NP_JET_Pileup_OffsetMu__1up,JET_19NP_JET_Pileup_OffsetNPV__1down,JET_19NP_JET_Pileup_OffsetNPV__1up,JET_19NP_JET_Pileup_PtTerm__1down,JET_19NP_JET_Pileup_PtTerm__1up,JET_19NP_JET_Pileup_RhoTopology__1down,JET_19NP_JET_Pileup_RhoTopology__1up,JET_19NP_JET_PunchThrough_MC15__1down,JET_19NP_JET_PunchThrough_MC15__1up,JET_19NP_JET_SingleParticle_HighPt__1down,JET_19NP_JET_SingleParticle_HighPt__1up,JET_JER_SINGLE_NP__1up,LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1down,LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1up,LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1down,LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1up,LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1down,LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1up,LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1down,LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1up,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1down,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1up,LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1down,LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1up,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1down,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1up,LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1down,LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1up,MET_SoftTrk_ResoPara,MET_SoftTrk_ResoPerp,MET_SoftTrk_ScaleDown,MET_SoftTrk_ScaleUp,MUONS_ID__1down,MUONS_ID__1up,MUONS_MS__1down,MUONS_MS__1up,MUONS_SCALE__1down,MUONS_SCALE__1up'
    systematicsForNtuple = 'nominal'
    #systematicsForNtuple = 'nominal,JET_JER_SINGLE_NP__1up,JET_19NP_JET_EffectiveNP_3__1down,JET_19NP_JET_EffectiveNP_3__1up,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up,JET_19NP_JET_EtaIntercalibration_Modelling__1down,JET_19NP_JET_EtaIntercalibration_Modelling__1up,JET_19NP_JET_Pileup_RhoTopology__1down,JET_19NP_JET_Pileup_RhoTopology__1up,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1down,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1up,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1down,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1up'

#systematicsForNtupleWeight = 'btagbSF_0__1up,btagbSF_0__1down,btagbSF_1__1up,btagbSF_1__1down,btageSF_0__1up,btageSF_0__1down'

systematicsForNtupleWeight = 'btagbSF_0__1up,btagbSF_1__1up,btagbSF_2__1up,btagbSF_3__1up,btagbSF_4__1up,btagcSF_0__1up,btagcSF_1__1up,btagcSF_2__1up,btagcSF_3__1up,btageSF_1__1up,btageSF_0__1up,btaglSF_0__1up,btaglSF_1__1up,btaglSF_2__1up,btaglSF_3__1up,btaglSF_4__1up,btaglSF_5__1up,btaglSF_6__1up,btaglSF_7__1up,btaglSF_8__1up,btaglSF_9__1up,btaglSF_10__1up,btaglSF_11__1up,btaglSF_12__1up,btaglSF_13__1up,eTrigSF__1up,eRecoSF__1up,eIDSF__1up,eIsolSF__1up,muTrigStatSF__1up,muTrigSystSF__1up,muIDStatSF__1up,muIDSystSF__1up,muIDlowPTStatSF__1up,muIDlowPTSystSF__1up,muIsolStatSF__1up,muIsolSystSF__1up,muTTVAStatSF__1up,muTTVASystSF__1up,btagbSF_0__1down,btagbSF_1__1down,btagbSF_2__1down,btagbSF_3__1down,btagbSF_4__1down,btagcSF_0__1down,btagcSF_1__1down,btagcSF_2__1down,btagcSF_3__1down,btageSF_1__1down,btageSF_0__1down,btaglSF_0__1down,btaglSF_1__1down,btaglSF_2__1down,btaglSF_3__1down,btaglSF_4__1down,btaglSF_5__1down,btaglSF_6__1down,btaglSF_7__1down,btaglSF_8__1down,btaglSF_9__1down,btaglSF_10__1down,btaglSF_11__1down,btaglSF_12__1down,btaglSF_13__1down,eTrigSF__1down,eRecoSF__1down,eIDSF__1down,eIsolSF__1down,muTrigStatSF__1down,muTrigSystSF__1down,muIDStatSF__1down,muIDSystSF__1down,muIDlowPTStatSF__1down,muIDlowPTSystSF__1down,muIsolStatSF__1down,muIsolSystSF__1down,muTTVAStatSF__1down,muTTVASystSF__1down,pileup__1up,pileup__1down,jvt__1up,jvt__1down'


systematicsAll = 'nominal,EG_RESOLUTION_ALL__1down,EG_RESOLUTION_ALL__1up,EG_SCALE_ALL__1down,EG_SCALE_ALL__1up,JET_19NP_JET_BJES_Response__1down,JET_19NP_JET_BJES_Response__1up,JET_19NP_JET_EffectiveNP_1__1down,JET_19NP_JET_EffectiveNP_1__1up,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up,JET_19NP_JET_EffectiveNP_3__1down,JET_19NP_JET_EffectiveNP_3__1up,JET_19NP_JET_EffectiveNP_4__1down,JET_19NP_JET_EffectiveNP_4__1up,JET_19NP_JET_EffectiveNP_5__1down,JET_19NP_JET_EffectiveNP_5__1up,JET_19NP_JET_EffectiveNP_6restTerm__1down,JET_19NP_JET_EffectiveNP_6restTerm__1up,JET_19NP_JET_EtaIntercalibration_Modelling__1down,JET_19NP_JET_EtaIntercalibration_Modelling__1up,JET_19NP_JET_EtaIntercalibration_NonClosure__1down,JET_19NP_JET_EtaIntercalibration_NonClosure__1up,JET_19NP_JET_EtaIntercalibration_TotalStat__1down,JET_19NP_JET_EtaIntercalibration_TotalStat__1up,JET_19NP_JET_Flavor_Composition__1down,JET_19NP_JET_Flavor_Composition__1up,JET_19NP_JET_Flavor_Response__1down,JET_19NP_JET_Flavor_Response__1up,JET_19NP_JET_Pileup_OffsetMu__1down,JET_19NP_JET_Pileup_OffsetMu__1up,JET_19NP_JET_Pileup_OffsetNPV__1down,JET_19NP_JET_Pileup_OffsetNPV__1up,JET_19NP_JET_Pileup_PtTerm__1down,JET_19NP_JET_Pileup_PtTerm__1up,JET_19NP_JET_Pileup_RhoTopology__1down,JET_19NP_JET_Pileup_RhoTopology__1up,JET_19NP_JET_PunchThrough_MC15__1down,JET_19NP_JET_PunchThrough_MC15__1up,JET_19NP_JET_SingleParticle_HighPt__1down,JET_19NP_JET_SingleParticle_HighPt__1up,JET_JER_SINGLE_NP__1up,LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1down,LARGERJET_Medium_JET_Rtrk_Baseline_Kin__1up,LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1down,LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1up,LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1down,LARGERJET_Medium_JET_Rtrk_Modelling_Kin__1up,LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1down,LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1up,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1down,LARGERJET_Medium_JET_Rtrk_TotalStat_Kin__1up,LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1down,LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1up,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1down,LARGERJET_Medium_JET_Rtrk_Tracking_Kin__1up,LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1down,LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1up,LARGERJET_Strong_JET_Rtrk_Baseline_All__1down,LARGERJET_Strong_JET_Rtrk_Baseline_All__1up,LARGERJET_Strong_JET_Rtrk_Modelling_All__1down,LARGERJET_Strong_JET_Rtrk_Modelling_All__1up,LARGERJET_Strong_JET_Rtrk_TotalStat_All__1down,LARGERJET_Strong_JET_Rtrk_TotalStat_All__1up,LARGERJET_Strong_JET_Rtrk_Tracking_All__1down,LARGERJET_Strong_JET_Rtrk_Tracking_All__1up,LARGERJET_Weak_JET_Rtrk_Baseline_D2__1down,LARGERJET_Weak_JET_Rtrk_Baseline_D2__1up,LARGERJET_Weak_JET_Rtrk_Baseline_Tau32__1down,LARGERJET_Weak_JET_Rtrk_Baseline_Tau32__1up,LARGERJET_Weak_JET_Rtrk_Baseline_mass__1down,LARGERJET_Weak_JET_Rtrk_Baseline_mass__1up,LARGERJET_Weak_JET_Rtrk_Baseline_pT__1down,LARGERJET_Weak_JET_Rtrk_Baseline_pT__1up,LARGERJET_Weak_JET_Rtrk_Modelling_D2__1down,LARGERJET_Weak_JET_Rtrk_Modelling_D2__1up,LARGERJET_Weak_JET_Rtrk_Modelling_Tau32__1down,LARGERJET_Weak_JET_Rtrk_Modelling_Tau32__1up,LARGERJET_Weak_JET_Rtrk_Modelling_mass__1down,LARGERJET_Weak_JET_Rtrk_Modelling_mass__1up,LARGERJET_Weak_JET_Rtrk_Modelling_pT__1down,LARGERJET_Weak_JET_Rtrk_Modelling_pT__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_D2__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_D2__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_mass__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_mass__1up,LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1down,LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1up,LARGERJET_Weak_JET_Rtrk_Tracking_D2__1down,LARGERJET_Weak_JET_Rtrk_Tracking_D2__1up,LARGERJET_Weak_JET_Rtrk_Tracking_Tau32__1down,LARGERJET_Weak_JET_Rtrk_Tracking_Tau32__1up,LARGERJET_Weak_JET_Rtrk_Tracking_mass__1down,LARGERJET_Weak_JET_Rtrk_Tracking_mass__1up,LARGERJET_Weak_JET_Rtrk_Tracking_pT__1down,LARGERJET_Weak_JET_Rtrk_Tracking_pT__1up,MET_SoftTrk_ResoPara,MET_SoftTrk_ResoPerp,MET_SoftTrk_ScaleDown,MET_SoftTrk_ScaleUp,MUONS_ID__1down,MUONS_ID__1up,MUONS_MS__1down,MUONS_MS__1up,MUONS_SCALE__1down,MUONS_SCALE__1up'


print '\nINFO :: Analysis and Selection'
print '+++++++++++++++++++++++++++++++'
print '+++ ANALYSIS           : ' + analysisType
print '+++ RESOLVED selection : ' + selection[0]
print '+++ BOOSTED  selection : ' + selection[1]
print '+++ '
if len(sys.argv) > 1:
    print '+++ running on ', sys.argv[1], ' data'
if(wSysts):
    print '+++ running with systematics'
else:
    print '+++ no systematics'

names   = []
# This list needs to be in accordance with the files provided in the SampleLists in /python
if(data):
    names  += ["Data_13TeV"]

if(QCD):
    names += ["QCD_13TeV"]

if(IsoSets):
    names += ["13TeV_ttbar_nominal"]
    names += ["13TeV_TTS_M1000"]
    names += ["13TeV_TTS_M1200"]

if(diboson):
    names += ["mc15c_diboson_sherpa"]

if (mc15c):
    names += ["13TeV_ttbar_nominal"]
    names += ["mc15c_diboson_sherpa"]
    if not mc15c_Wjets_comp:
        names += ["mc15c_Wjets_sherpa22"]
    names += ["mc15c_Zjets_sherpa22"]
    names += ["mc15c_singletop"]
    names += ["mc15c_ttV"]

if(stop_ttV):
    names += ["mc15c_singletop"]
    names += ["mc15c_ttV"]


if(mcsingletop):
    names += ["mc15c_singletop"]
    names += ["mc15c_singletop_tchan"]
    names += ["mc15c_singletop_schan"]
    names += ["mc15c_singletop_Wt"]


if(mc15c_Wjets_comp):
    names += ["mc15c_Wbbcc_sherpa22"]
    names += ["mc15c_Wc_sherpa22"]
    names += ["mc15c_Wlight_sherpa22"]

if(mc15c_Wbbcc_only):
    names += ["mc15c_Wbbcc_sherpa22"]

if(WjetsOnly):
    names += ["mc15c_Wjets_sherpa22"]

if(mc15cModel):
    names += ["13TeV_ttbarRadHi"]
    names += ["13TeV_ttbarRadLow"]
    names += ["13TeV_ttbaraMcAtNloHerwigpp"]
    names += ["13TeV_ttbarPowPy8"]
    names += ["13TeV_ttbaraMCAtNLOPy8"]
    names += ["13TeV_ttbarPowhegHerwigpp"]


if(signal):
    names += ["13TeV_TTS_M800_WbWb"]
    names += ["13TeV_TTS_M800_ZtZt"]
    names += ["13TeV_TTS_M800_HtHt"]
    names += ["13TeV_TTS_M800_WbZt"]
    names += ["13TeV_TTS_M800_WbHt"]
    names += ["13TeV_TTS_M800_ZtHt"]

    names += ["13TeV_TTS_M1050_WbWb"]
    names += ["13TeV_TTS_M1050_ZtZt"]
    names += ["13TeV_TTS_M1050_HtHt"]
    names += ["13TeV_TTS_M1050_WbZt"]
    names += ["13TeV_TTS_M1050_WbHt"]
    names += ["13TeV_TTS_M1050_ZtHt"]

    names += ["13TeV_TTS_M1100_WbWb"]
    names += ["13TeV_TTS_M1100_ZtZt"]
    names += ["13TeV_TTS_M1100_HtHt"]
    names += ["13TeV_TTS_M1100_WbZt"]
    names += ["13TeV_TTS_M1100_WbHt"]
    names += ["13TeV_TTS_M1100_ZtHt"]

if(signal_WtX):
    names += ["13TeV_BBS_M800_WbWb"]
    names += ["13TeV_BBS_M800_ZtZt"]
    names += ["13TeV_BBS_M800_HtHt"]
    names += ["13TeV_BBS_M800_WbZt"]
    names += ["13TeV_BBS_M800_WbHt"]
    names += ["13TeV_BBS_M800_ZtHt"]

    names += ["13TeV_BBS_M1050_WbWb"]
    names += ["13TeV_BBS_M1050_ZtZt"]
    names += ["13TeV_BBS_M1050_HtHt"]
    names += ["13TeV_BBS_M1050_WbZt"]
    names += ["13TeV_BBS_M1050_WbHt"]
    names += ["13TeV_BBS_M1050_ZtHt"]

    names += ["13TeV_BBS_M1100_WbWb"]
    names += ["13TeV_BBS_M1100_ZtZt"]
    names += ["13TeV_BBS_M1100_HtHt"]
    names += ["13TeV_BBS_M1100_WbZt"]
    names += ["13TeV_BBS_M1100_WbHt"]
    names += ["13TeV_BBS_M1100_ZtHt"]




if(signalWbWb):
#    names += ["13TeV_TTS_M500_WbWb"]
#    names += ["13TeV_TTS_M600_WbWb"]
#    names += ["13TeV_TTS_M700_WbWb"]
#    names += ["13TeV_TTS_M750_WbWb"]
    names += ["13TeV_TTS_M800_WbWb"]
    names += ["13TeV_TTS_M850_WbWb"]
    names += ["13TeV_TTS_M900_WbWb"]
    names += ["13TeV_TTS_M950_WbWb"]
    names += ["13TeV_TTS_M1000_WbWb"]
    names += ["13TeV_TTS_M1050_WbWb"]
    names += ["13TeV_TTS_M1100_WbWb"]
    names += ["13TeV_TTS_M1150_WbWb"]
    names += ["13TeV_TTS_M1200_WbWb"]
    names += ["13TeV_TTS_M1300_WbWb"]
    names += ["13TeV_TTS_M1400_WbWb"]

if(s13TeV_TTS_M1100_WbWbOnly):
    names += ["13TeV_TTS_M1100_WbWb"]
if(s13TeV_TTS_M1300_ZtHt):
    names += ["13TeV_TTS_M1300_ZtHt"]

if(signalNtuple):
    names += ["13TeV_TTS_M500"]
    names += ["13TeV_TTS_M600"]
    names += ["13TeV_TTS_M700"]
    names += ["13TeV_TTS_M750"]
    names += ["13TeV_TTS_M800"]
    names += ["13TeV_TTS_M850"]
    names += ["13TeV_TTS_M900"]
    names += ["13TeV_TTS_M950"]
    names += ["13TeV_TTS_M1000"]
    names += ["13TeV_TTS_M1050"]
    names += ["13TeV_TTS_M1100"]
    names += ["13TeV_TTS_M1150"]
    names += ["13TeV_TTS_M1200"]
    names += ["13TeV_TTS_M1300"]
    names += ["13TeV_TTS_M1400"]

if(mc15c_AFII):
    names += ["13TeV_ttbar_AFII"]
    names += ["13TeV_ttbarPowhegHerwigpp_AFII"]
    names += ["13TeV_ttbaraMcAtNloHerwigpp_AFII"]

if(ttbarOnly):
    names += ["13TeV_ttbar_nominal"]

if(ttbar_PowPy8):
    names += ["13TeV_ttbar_PowPy8_nominal"]

if(ttbar_sherpa):
    names += ["13TeV_ttbar_sherpa_lminus"]
    names += ["13TeV_ttbar_sherpa_lplus"]

if(ttbarAlternative):
    names += ["13TeV_ttbarPowPy8"]
    names += ["13TeV_ttbaraMCAtNLOPy8"]


#Load specified topology from this json file
with open('../../share/topologies.json') as data_file:
    topo = json.load(data_file)

# directory with the base of the RootCore stuff
ts = time.time()
timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M')
if wSysts:
    rundir = os.environ['PWD']+'/jobsOnFlashy_'+ str(timestamp) + "_resSel-"+str(selection[0])+"_booSel-"+str(selection[1])+"_"+str(UserJobName)+"_wSysts"
elif AFII:
    rundir = os.environ['PWD']+'/jobsOnFlashy_'+ str(timestamp) + "_resSel-"+str(selection[0])+"_booSel-"+str(selection[1])+"_"+str(UserJobName)+"_AFII"
elif TtbarModel:
    rundir = os.environ['PWD']+'/jobsOnFlashy_'+ str(timestamp) + "_resSel-"+str(selection[0])+"_booSel-"+str(selection[1])+"_"+str(UserJobName)+"_TtbarModel"
elif QCD:
    rundir = os.environ['PWD']+'/jobsOnFlashy_'+ str(timestamp) + "_resSel-"+str(selection[0])+"_booSel-"+str(selection[1])+"_"+str(UserJobName)+"_QCD"
else:
    rundir = os.environ['PWD']+'/jobsOnFlashy_'+ str(timestamp) + "_resSel-"+str(selection[0])+"_booSel-"+str(selection[1])+"_"+str(UserJobName)
# output directory
outputDir = rundir+"/BatchJobOutput"
outputHandler = rundir+"/BatchJobHandler"
os.system("mkdir -p "+str(rundir)+"/BatchJobHandler")
os.system("mkdir -p "+str(rundir)+"/BatchJobOutput")
os.system("mkdir -p "+str(rundir)+"/BatchJobHandler/batchscripts")
os.system("mkdir -p "+str(rundir)+"/BatchJobHandler/PBS")

file = open(str(rundir)+'/jobSteerings.txt', 'w')
pickle.dump(jobSteer, file)
file.close()

import glob
files = glob.glob(ntuplesDir+'/*.root*')
samples = TopExamples.grid.Samples(names)

#print files

import glob
import os
# get list of processed datasets
dirs = glob.glob(ntuplesDir+'/*')


jobID = defaultdict(list)
regionCollection = defaultdict(dict)
# each "sample" below means an item in the list names above
# there may contain multiple datasets
# for each sample we want to read

forJobHandling = defaultdict(dict)
totalJobs = []

print '\nINFO :: File Collection'
print '+++++++++++++++++++++++++++++++'
for sample in samples:
    print 'INFO :: Collecting files for ... > '+str(sample.name)
    # write list of files to be read when processing this sample
    f = open(outputDir+"/input_"+sample.name+'.txt', 'w')
    # output file after running read
    #outfile = outputDir+"/"+sample.name
    outfile = sample.name

    forJobs = {}
    nJob = 0
    nFile = 0

    # go over all directories in the ntuplesDir
    for d in dirs:
        #      if d == '':
        #         continue
        # remove path and get only dir name in justfile
        justfile = d.split('/')[-1]
        dsid_dir = justfile.split('.')[2] # get the DSID of the directory
      #dsid_dir = d.split('.')[2] # get the DSID of the directory
      # this will include all directories, so check if this director is in the sample
        # now go over the list of datasets in sample
        # and check if this DSID is there
        for s in sample.datasets:
            if len(s.split(':')) > 1:
                s = s.split(':')[1] # skip mc15_13TeV
            dsid_sample = s.split('.')[1] # get DSID
            if dsid_dir == dsid_sample: # this dataset belongs in the sample in the big for loop
#                print 'DEBUG :: ', dsid_sample
            # get all files in the directory
                files = glob.glob(d+'/*.root*')
#          files = Popen([eosrun, "ls", ntuplesDir+"/"+d], stdout=PIPE).communicate()[0].split('\n')
# and write it in ht elist of input files to process
                for item in files:
                    if not '.part' in item and item != '':
                  #fullname = item
                    #                  fullname = ntuplesDir+"/"+d+'/'+item
                    #                  fullname = ntuplesDir+'/'+item


                        fullname = item

                        f.write(fullname+'\n')
                        # number of files per job (SH)(set to 1 for now, possible feature)
                        nFilesPerJob = 1

                        if nFile == nFilesPerJob:
                            nFile = 0
                            nJob = nJob + 1
                        if nFile == 0:
                            forJobs[nJob] = []
                        ####HACK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#                        if jhaleyDownload:
#                            if "jhaley" in fullname:
#                                print 'DEBUG :: ',fullname
#                                forJobs[nJob].append(fullname+'\n')
#                        else:
                        forJobs[nJob].append(fullname+'\n')
                        nFile = nFile + 1
            # go to the next directory in the same sample             # go to the next directory in the same sample
                break

    forJobHandling[str(outfile)] = forJobs
    print 'INFO :: sample ',outfile, ' has ', len(forJobHandling[str(outfile)].values()[0]) * len(forJobHandling[str(outfile)].keys()), ' inputfiles'

#    print 'INFO :: will send ',len(forJobHandling[str(outfile)].keys()), ' jobs with ', len(forJobHandling[str(outfile)].values()[0]), ' inputfiles'
    totalJobs.append(len(forJobHandling[str(outfile)].keys()))
    f.close()

print '\nINFO :: Job Summary and validity check'
print '+++++++++++++++++++++++++++++++'
systList = [x.strip() for x in systematicsForNtuple.split(',')]
if(sum(totalJobs) < 1500):
    print 'INFO :: Sending ', sum(totalJobs)*len(systList), 'to PBS', '(1500 allowed)'
else:
    print 'WARNING :: Number of jobs: ', sum(totalJobs)*len(systList), 'exceeds allowed size of 1500'
    sys.exit(0)

for reg in topo.keys():
    region = reg
    if (not regionTopo or regionTopo[0] == ''):
        print 'INFO :: you did not choose a specific region which is why we now run on all'
    elif reg not in regionTopo:
        continue
    print '\n'
    print 'INFO :: Processing region ... >', region
    print '+++++++++++++++++++++++++++++++'
    for sample in samples:
        outfile = sample.name

        isData = 0
        qcdPar = ' --runMM 0 --loose 0'
        theWeightSysts = ""
        if(wSysts):
            print 'INFO :: enabling systematics'
            weightSysts = " --doWeightSystematics 1 --skipRegionPlots 1 "
            theSysts = systematicsAll
        else:
            weightSysts = " --doWeightSystematics 0 "
            theSysts = systematics
        if "Data" in sample.name:
            theSysts = "nominal"
            weightSysts = " --doWeightSystematics 0 "
            isData = 1
        elif "QCD_13TeV" in sample.name:
            theSysts = "nominal"
            weightSysts = " --doWeightSystematics 0 "
            isData = 1
            qcdPar = ' --runMM 1 --loose 1 --DSyear 2015 '
#        elif "QCD_13TeV_2016" in sample.name:
#            theSysts = "nominal"
#            weightSysts = " --doWeightSystematics 0 "
#            isData = 2
#            qcdPar = ' --runMM 1 --loose 1 '



        extraOptions = " "
        if extraOpt:
            extraOptions += " " + str(extraOpt)
        if showWeights:
            extraOptions += " --printWeights 1 "
        if enableNtuple:
            extraOptions += " --write_ntuple 1 "

        print 'INFO :: Processing sample ... > '+str(sample.name)

        for job in forJobHandling[sample.name].keys():
             outfiles = []
            #run on systematics individually for the n-tuple case
             if enableNtuple and enableNtupleWeightSys:
                 systList = [x.strip() for x in systematicsForNtupleWeight.split(',')]
             elif enableNtuple:
                 systList = [x.strip() for x in systematicsForNtuple.split(',')]
             else:
                 systList = [x.strip() for x in systematics.split(',')]
             for syst in systList:
                 outFile = outfile
                 extraoptions = extraOptions
                 if enableNtupleWeightSys:
                     theSysts = "nominal"
                     theWeightSysts = syst
                     extraoptions += '  --doSingleWeightSystematics ' + str(theWeightSysts)
                 else:
                     theSysts = syst


                 if "nominal" not in syst:
                     outFile += syst


                 jobName = sample.name+'_' + str(job)
                 if enableNtuple:
                     jobName += "_"+syst
                 os.system("mkdir -p "+str(outputDir)+"/"+str(region)+"/"+str(sample.name))
                 os.system("mkdir -p "+str(outputDir)+"/"+str(region)+"/"+str(sample.name)+"/"+str(job))
                 os.system("mkdir -p "+str(outputHandler)+"/batchscripts/"+str(region)+"/"+str(sample.name))
                 os.system("mkdir -p "+str(outputHandler)+"/PBS/"+str(region)+"/"+str(sample.name))

                 infile = outputDir+"/"+str(region)+"/"+str(sample.name)+"/input_"+jobName+'.txt'
                 infullfile = outputDir+"/input_"+sample.name+'.txt'
                 outputLoc = outputDir + "/"+str(region)+"/"+str(sample.name)+"/"+"/"+str(job)
                 f = open(infile, 'w')

                 for item in forJobHandling[sample.name].values()[job]:
                     f.write(item)
                 f.close()
                 errfile = outputHandler+"/PBS/"+str(region)+"/"+str(sample.name)+"/stdout_"+jobName+'.err'
                 logfile = outputHandler+"/PBS/"+str(region)+"/"+str(sample.name)+"/stdout_"+jobName+'.log'
                 runfile = outputHandler+"/batchscripts/"+str(region)+"/"+str(sample.name)+"/run_"+jobName+'.pbs'
                 fr = open(runfile, 'w')
        #        fr.write('#!/bin/sh\n')
                 fr.write('#PBS -o '+logfile+'\n')
                 fr.write('#PBS -e '+errfile+'\n')
        #        fr.write('#PBS -pe multicores 4 \n')
                 fr.write('#PBS -l walltime='+str(walltime)+'\n')
                 if(wSysts):
                     if("ttbar" in str(sample.name) or "mc15c_" in str(sample.name) or "TTS_" in str(sample.name)):
                    #print "Start : %s" % time.ctime()
                    #time.sleep( 20 )
                    #print "End : %s" % time.ctime()
                         fr.write('#PBS -l nodes=1:ppn=2 \n')
                             #                fr.write('#PBS -l nodes=1:ppn=4 \n')
                             #            fr.write('#PBS -l walltime='+str(walltime)+' \n')
                             #            fr.write('#PBS -l nodes=1:ppn=10')
                 fr.write('#PBS -m ae \n')
                 #fr.write('#PBS -M '+email+'\n \n')
                 fr.write('cd '+rundir+'\n')
                 fr.write('source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh\n')
                 #            fr.write('lsetup "rcsetup -u"\n')
                 #            fr.write('lsetup rcsetup\n')
                 fr.write('cd ' + rundir +' \n')
                 fr.write('ls\n')
                 fr.write('ln -s ' + str(analysisFolder) + '/read_hqt read_hqt\n')
                 command = '/usr/bin/time ./read_hqt --grid2pre ' + str(grid2pre) +  ' --btagSFoffline '+str(btagSFoffline)+' --PRWHashing '+str(prwHashing)+ ' --resolvedSelection='+ str(selection[0]) + ' --boostedSelection='+ str(selection[1]) +'  ' + str(weightSysts)+' --removeOverlapHighMtt 0 '+ ' --data ' + str(isData) + ' ' + qcdPar + ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) +' --njets '+ str(topo[region]["njet"]) + ' --njetEx '+ str(topo[region]["njetEx"]) + ' --Wtags ' + str(topo[region]["Wtag"]) + ' --WtagEx '+ str(topo[region]["WtagEx"]) + ' --fullFiles '+ infullfile + ' --files ' + infile +  ' --analysis ' + analysisType + ' --output ' + outputLoc + '/resolved_e_' + outFile + '.root,' + outputLoc + '/resolved_mu_' + outFile + '.root,'+ outputLoc + '/resolved_comb_' + outFile + '.root,' + outputLoc + '/boosted_e_' + outFile + '.root,' + outputLoc + '/boosted_mu_' + outFile + '.root,' + outputLoc + '/boosted_comb_' + outFile + '.root '+' --systs ' + theSysts + ' --nentries ' + str(nentries) + " "+str(extraoptions)

                 fr.write(command + '\n')
                 fr.close()
                 os.system('chmod a+x ' + runfile)
                 subcmd = 'qsub -V ' + runfile
            #get jobID and start the job with the following line
                 jobID[sample.name].append(jobhelpers.getJobIDAndRun(runfile))
                 print '################# SUBMIT > :',jobName
                 outfiles.append(outFile)
                 #os.system("sleep 3")

             regionCollection[region][sample.name] = outfiles
with open(rundir + '/jobSummary.json', 'a') as outfile:
#    json.dump({'region':regionCollection }, outfile, indent=4)
    json.dump(regionCollection, outfile, indent=4)
