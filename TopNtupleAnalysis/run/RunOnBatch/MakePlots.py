import os,sys
import helpers

lumi=14.68888 #3.212956160 11475853312
#lumi=11.475853312
configFile=sys.argv[1]
topo = helpers.getTopology( os.path.basename( os.environ['PWD'] ) ) #extracts info from folder name

lep = ['comb']
#lep = ['e','mu','comb']
ch = ['merged_boosted']
plotCodeLoc = '/Users/shenkel/work/Physics/Analysis/VLQ/vlq_wbx/TopNtupleAnalysis/plotting/'
plotCodeLoc = '/home/sthenkel/work/Physics/Analysis/VLQ/WbX/EOYE2016/NTUPLEproduction_WbX/AnalysisTop-2.4.19/vlq_wbx/TopNtupleAnalysis/plotting/'
fitPlots = False
testPlots = False
controlPlots = False
resolvedPlots = False
boostedPlots = True


if(fitPlots):
    print 'INFO :: Start plotting test plots'
    hists = ['ST_boo','leptonicT_m','hadronicT_m','avgMassT_boo']
    extraText = str(topo)
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(hist)+str(channel)+ str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields "+ str(1) + " --printYields " + str(1) + " --includeSignal "+ str(1) + ' --outFormat .pdf'
        #            subprocess.check_output(command, shell=True)
                os.system(str(command))
                print command

if(testPlots):
    print 'INFO :: Start plotting test plots'
    hists = ['mwt','leptonicT_m']
    extraText = str(topo)
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(channel) + str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1) + " --includeSignal "+ str(1)+ " --scaleSignal " + str(1) + ' --outFormat .pdf' + "  "

        #            subprocess.check_output(command, shell=True)
                os.system(str(command))
                print command

if(controlPlots):
    print 'INFO :: Start plotting control plots'
    hists = ['lepPt','lepPt_effBins','lepEta','lepPhi','JetPt','leadJetPt','leadbJetPt','nJets','nBtagJets','ST','nBoostedW',
            'MET','MET_phi','DeltaR_lepnu','mwt','mass_lepb','mass_lepblead','yields','mu','mu_original','npv','vtxz',
            'nTrkBtagJets','leadTrkbJetPt', 'closejl_minDeltaR', 'closejl_minDeltaR_effBins', 'closejl_pt','minDeltaR_lepjet',
            'jet0_eta', 'jet0_m', 'jet0_phi', 'jet0_pt', 'jet1_eta', 'jet1_m', 'jet1_phi', 'jet1_pt',
            'jet2_eta', 'jet2_m', 'jet2_phi', 'jet2_pt', 'jet3_eta', 'jet3_m', 'jet3_phi', 'jet3_pt',
            'jet4_eta', 'jet4_m', 'jet4_phi', 'jet4_pt', 'jet5_eta', 'jet5_m', 'jet5_phi', 'jet5_pt',
            'weight', 'weight_leptSF', 'yields', 'weight_leptPt','mass_lepb','DeltaR_bjet1bjet2']
#    hists = ['mwt']
    extraText = str(topo)
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(channel) + str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1)+ ' --outFormat .pdf '
                #+   " --includeSignal "+str(1)+" --scaleSignal " + str(20)

                os.system(str(command))
                print command

if(boostedPlots):
    print 'INFO :: Start plotting boosted plots'
    hists = ['ST_boo','MET_boo','MWT_boo','DeltaR_lepnu_boo','nBtagJets_boo','nBoostedW_boo',
             'bjet1Pt_boo','bjet2Pt_boo','lepPt_boo','lepEta_boo','lepPhi_boo',
#             'closeJetPt_boo','largeJetPt','largeJetM','largeJetPhi','largeJetSd12',
             'nhadronicT','hadronicT_m', 'hadronicT_phi', 'hadronicT_eta','hadronicT_pt',
             'nleptonicT','leptonicT_m', 'leptonicT_phi', 'leptonicT_eta','leptonicT_pt',
             'nleptonicW','leptonicW_m', 'leptonicW_phi', 'leptonicW_eta','leptonicW_pt',
             'avgMassT_boo','nboostedWhadCand','boostedWhadCand_pt','boostedWhadCand_eta','boostedWhadCand_phi','boostedWhadCand_m',
             'DeltaM_hadtopleptop','minDeltaR_hadWbjet','closebjHadW_pt','mass_lepblead_boo','mass_lepb_boo','minDeltaR_lepbjet',
             'DeltaR_ThadTlep','DeltaR_WhadThad','DeltaR_WhadTlep','minMass_lepb_boo',
             'mass_lepjet_boo','mass_lepjetlead_boo','minMass_lepjet_boo','DeltaR_lepblead_boo','DeltaR_lepbsublead_boo',
             'DeltaR_lepjetlead_boo','DeltaR_lepjetsublead_boo','DeltaR_lepjetthird_boo','DeltaR_lepjetfourth_boo','DeltaR_lepWhad',
             'DeltaR_WlepThad','DeltaR_WlepTlep','minDeltaR_hadWbjet_boo','closebjHadW_pt_boo','minDeltaR_lepbjet_boo','closebjl_pt_boo',
 #            'DeltaR_Whadjetlead_boo','DeltaR_Whadjetsublead_boo','DeltaR_Whadjetthird_boo','DeltaR_Whadjetfourth_boo',
             'DeltaR_Whadbhad','DeltaR_Whadblep','DeltaR_Wlepbhad','DeltaR_Wlepblep','mass_lepleptonicb_boo'
    ]
    hists = ['ST_boo','MET_boo','MWT_boo','DeltaR_lepnu_boo','nBtagJets_boo', 'nBoostedW_boo', 'mass_lepb']
    extraText = "cut 3"
    for hist in hists:
        for channel in ch:
            for lepton in lep:
                print hist, lep
                command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --saveTH1 ' + str(channel) + str(lepton) +' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1) +   " --includeSignal "+str(1)+ " --scaleSignal " + str(90) + ' --outFormat .pdf'
  #              command = str(plotCodeLoc)+'plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '"' + ' --arrow ' + str(1) +' -C ' + str(configFile) + " --saveYields " + str(1) + " --printYields " + str(1) +   " --includeSignal "+str(1)+ " --scaleSignal " + str(90) + ' --blind 1 --outFormat .pdf'
                os.system(str(command))
                print command
