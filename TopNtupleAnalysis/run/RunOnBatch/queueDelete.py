from subprocess import Popen,PIPE
import subprocess
import os,sys,json
import shutil
import JobMonitoringHelpers as jobhelpers

# output directory
outdir = sys.argv[1]#'jobsOnFlash_testMerge_2016-03-22-06-23/'
region = []
jobs = []
#region = ['0jin_0bin_0Win']
with open(outdir + '/jobSummary.json') as job_file:
    jobinfo = json.load(job_file)
for keys in jobinfo.keys():
    region += [keys]
for reg in region:
    samples = jobinfo[reg].keys()
    jobs = jobinfo[reg].values()[0]
print jobs

for job in jobs:
    print 'INFO :: delete job number: ', job.rsplit('.')[0]
    jobhelpers.deleteJobs( job.rsplit('.')[0]  )
