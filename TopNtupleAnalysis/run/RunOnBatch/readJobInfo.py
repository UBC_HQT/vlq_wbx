import pickle
import sys
from pprint import pprint
# output directory
outdir = sys.argv[1]

with open(outdir + '/jobSteerings.txt') as file:
    dict = pickle.load(file)

print '\n\nINFO :: Those job options where specified to run: ', outdir
print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
pprint( dict )
