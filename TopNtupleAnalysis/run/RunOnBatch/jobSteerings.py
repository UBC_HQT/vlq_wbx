#!/env/python

def jobsAFII(region, resolvedSelection, boostedSelection):
    jobSteer = {
        "fileLocation" :    [
            "/data/VLQ/13TeV/production_AT-2.4.19/SamplePool_AFII/"
            ],
        "regionTopo" : [
            region
            ],
        "samples" :    {
            "data": False,
            "mc15c": False,
            "signalAll": False,
            "mc15cModel" : False,
            "mc15c_Wjets_comp" : False,
            "mc15c_AFII_model" : True,
            "ttbar_PowPy8": True,
            "QCD" : False,
            },
        "selection" :    {
            "resolved": resolvedSelection,
            "boosted": boostedSelection,
            }
        }
    return jobSteer


def jobsTtbarModel(region, resolvedSelection, boostedSelection):
    jobSteer = {
        "fileLocation" :    [
            "/data/VLQ/13TeV/production_AT-2.4.19/SamplePool/"
            ],
        "regionTopo" : [
            region
            ],
        "samples" :    {
            "data": False,
            "mc15c": False,
            "signalAll": False,
            "mc15cModel" : True,
            "mc15c_Wjets_comp" : False,
            "mc15c_AFII_model" : False,
            "QCD" : False,
            },
        "selection" :    {
            "resolved": resolvedSelection,
            "boosted": boostedSelection,
            }
        }
    return jobSteer


def jobsQCDdata(region, resolvedSelection, boostedSelection):
    jobSteer = {
        "fileLocation" :    [
            "/data/VLQ/13TeV/production_AT-2.4.19/SamplePool/"
            ],
        "regionTopo" : [
            region
            ],
        "samples" :    {
            "data": False,
            "mc15c": False,
            "signalAll": False,
            "mc15cModel" : False,
            "mc15c_Wjets_comp" : False,
            "mc15c_AFII" : False,
            "QCD" : True,
            },
        "selection" :    {
            "resolved": resolvedSelection,
            "boosted": boostedSelection,
            }
        }
    return jobSteer
