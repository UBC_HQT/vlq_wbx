import os, glob, json
from collections import defaultdict

#SETTINGS
sampleFile = 'input_data.json'
nentries=-1
analysisType='AnaHQTVLQWbX_pre'
theSysts = "nominal"
#region = "6jin2bex0Win"
#region = "0jin_0bin_0Win"
#region = "3jin_1bin_0Win"
#region =  "4jin_2bin_1Win"
region = "0jin_0bin_0Win"
addition = "_pre"

outputDir = region + addition
os.system( 'mkdir -p ' + str(outputDir) )

with open('../../share/topologies.json') as ftopos:
    topo = json.load(ftopos)
samples = defaultdict(list)


with open(sampleFile) as fsamples:
    files = json.load(fsamples)
print files

for sample in files.keys():
    sampleFiles = []
    print sample
    print len(files[sample]["files"])
    for inputs in range( len(files[sample]["files"]) ):
        sampleFiles.append(files[sample]["files"][inputs])
    sampleFiles = ','.join(sampleFiles)
    outfile = sample
    if "data" in sample:
        theSysts = "nominal"
        isData = '1'
    else:
        isData = '0'

   # command = './read_hqt_pre  --data ' + isData + ' --analysis ' + analysisType +  ' --files ' + sampleFiles + ' --nentries ' + str(nentries) + ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) + ' --njets '+ str(topo[region]["njet"]) + ' --njetEx ' + str(topo[region]["njetEx"]) + ' --Wtags ' + str(topo[region]["Wtag"]) + ' --WtagEx ' + str(topo[region]["njetEx"]) + ' --output ' + outputDir + '/resolved_e_' + outfile + '.root,' + outputDir + '/resolved_mu_' + outfile + '.root,' + outputDir + '/boosted_e_' + outfile + '.root,' + outputDir + '/boosted_mu_' + outfile + '.root' +' --systs ' + theSysts

    command = './read_hqt_pre  --data ' + isData + ' --analysis ' + analysisType +  ' --files ' + sampleFiles + ' --nentries ' + str(nentries) + ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) + ' --njets '+ str(topo[region]["njet"]) + ' --njetEx ' + str(topo[region]["njetEx"]) + ' --Wtags ' + str(topo[region]["Wtag"]) + ' --WtagEx ' + str(topo[region]["njetEx"]) + ' --output ' + outputDir + '/boosted_comb_' + outfile + '.root'  +' --systs ' + theSysts

    print command


    os.system( str(command) )

