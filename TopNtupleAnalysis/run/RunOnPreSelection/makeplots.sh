
S=1
#LUMI=3.31668
LUMI=3.20905

configFile=$1

# for ch in resolved boosted ; do
#   for lep in e mu ; do
#     plot -c $lep -p $ch -h lepPt -l $LUMI --smoothen 0 --rebin 9 --xTitle "Lepton p_{T} [GeV]" --yTitle "Events / 45 GeV" -C $configFile 
#     plot -c $lep -p $ch -h MET -l $LUMI --smoothen 0 --normBinWidth 1 --yTitle "Events / GeV" --xTitle "E_{T}^{miss} [GeV]" -C $configFile
#     plot -c $lep -p $ch -h closeJetPt -l $LUMI --smoothen 0 --normBinWidth 1 --yTitle "Events / GeV" --xTitle "Selected jet p_{T} [GeV]"
#     plot -c $lep -p $ch -h largeJetPt -l $LUMI --smoothen 0 --normBinWidth 1 --yTitle "Events / GeV" --xTitle "Large-R jet p_{T} [GeV]" -C $configFile
#     plot -c $lep -p $ch -h mtlep_boo -l $LUMI --smoothen 0 --normBinWidth 1 --yTitle "Events / GeV" --xTitle "Mass of the leptonic top candidate [GeV]" -C $configFile
#     plot -c $lep -p $ch -h largeJetM -l $LUMI --smoothen 0 --yTitle "Events / 10 GeV" --xTitle "Large-R jet mass [GeV]"
# done
# done



#for hist in MET MET_phi boostedW_eta boostedW_m boostedW_phi boostedW_pt closeJetPt closejl_minDeltaR closejl_minDeltaR_effBins closejl_pt dR_lepnu jet0_eta jet0_m jet0_phi jet0_pt jet1_eta jet1_m jet1_phi jet1_pt jet2_eta jet2_m jet2_phi jet2_pt jet3_eta jet3_m jet3_phi jet3_pt jet4_eta jet4_m jet4_phi jet4_pt jet5_eta jet5_m jet5_phi jet5_pt leadJetPt leadTrkbJetPt leadbJetPt lepEta lepPhi lepPt lepPt_effBins mtlep_boo mtt mu mu_original mwt nBoostedW nBtagJets nJets nTrkBtagJets npv vtxz weight weight_leptSF yields weight_leptPt; do
for hist in MET; do
for ch in resolved boosted ; do
  for lep in e ; do
    plot -c $lep -p $ch -h $hist -l $LUMI --smoothen 0 -C $configFile
  done
done
done

for hist in jet0_pt tjet0_pt ; do
for ch in resolved boosted ; do
  for lep in e ; do
    plot -c $lep -p $ch -h $hist -l $LUMI --smoothen 0 --normBinWidth 1 -C $configFile
  done
done
done

ch=boosted
hist=largeJetPt
for lep in e mu ; do
  plot -c $lep -p $ch -h $hist -l $LUMI --smoothen 0 --normBinWidth 1 --logY 1 --yMax 1e4 -o ${ch}_${hist}_${lep}_log.pdf
done


