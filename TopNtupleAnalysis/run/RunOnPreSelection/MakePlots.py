import os,sys
import helpers

lumi=3.20905
configFile=sys.argv[1]
extraText = helpers.getTopology( os.path.basename( os.environ['PWD'] ) ) #extracts info from folder name
extraText += ";loose"
lep = ['e']
ch = ['boosted']
hists = ['HT', 'MET', 'MET_phi', 'boostedW_eta', 'boostedW_m', 'boostedW_phi', 'boostedW_pt', 'closeJetPt', 'closejl_minDeltaR', 'closejl_minDeltaR_effBins', 'closejl_pt', 'DeltaR_lepnu','minDeltaR_lepbjet','minDeltaR_hadWbjet', 'A_distance', 'A_mass','A_dr', 'jet0_eta', 'jet0_m', 'jet0_phi', 'jet0_pt', 'jet1_eta', 'jet1_m', 'jet1_phi', 'jet1_pt', 'jet2_eta', 'jet2_m', 'jet2_phi', 'jet2_pt', 'jet3_eta', 'jet3_m', 'jet3_phi', 'jet3_pt', 'jet4_eta', 'jet4_m', 'jet4_phi', 'jet4_pt', 'jet5_eta', 'jet5_m', 'jet5_phi', 'jet5_pt', 'leadJetPt', 'leadTrkbJetPt', 'leadbJetPt', 'lepEta', 'lepPhi', 'lepPt', 'lepPt_effBins', 'mtlep_boo', 'mtt', 'mu', 'mu_original', 'mwt', 'nBoostedW', 'nBtagJets', 'nJets', 'nTrkBtagJets', 'npv', 'vtxz', 'weight', 'weight_leptSF', 'yields', 'weight_leptPt', 'hadronicT_eta', 'hadronicT_m', 'hadronicT_phi', 'hadronicT_pt', 'hadronicW_eta', 'hadronicW_m', 'hadronicW_phi', 'hadronicW_pt', 'hadronicT_eta', 'hadronicT_m', 'hadronicT_phi', 'hadronicT_pt', 'hadronicW_eta', 'hadronicW_m', 'hadronicW_phi', 'hadronicW_pt','leptonicT_eta', 'leptonicT_m', 'leptonicT_phi', 'leptonicT_pt', 'leptonicW_eta', 'leptonicW_m', 'leptonicW_phi', 'leptonicW_pt', 'leptonicT_eta', 'leptonicT_m', 'leptonicT_phi', 'leptonicT_pt', 'leptonicW_eta', 'leptonicW_m', 'leptonicW_phi', 'leptonicW_pt']
#hists = ['HT','MET','nBtagJets','nJets']

#hists = ['mass_lepb']
print 'INFO :: Start plotting ...'
for hist in hists:
    for channel in ch:
        for lepton in lep:
            print hist, lep
            command = '../../../plotting/plot -c ' + str( lepton ) + ' -p ' + str( channel ) + ' -h ' + str(hist) + ' -l ' + str(lumi) + ' --smoothen ' + str( 0 ) + ' --mcOnly ' + str( 0 ) + ' --extraText "' + str( extraText ) + '" -C ' + str(configFile)
#            subprocess.check_output(command, shell=True)
            os.system(str(command))
            print command
