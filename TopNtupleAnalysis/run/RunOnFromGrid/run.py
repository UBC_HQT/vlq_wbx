import os, glob, json
from collections import defaultdict

#SETTINGS
sampleFile = 'input.json'
nentries=20000
analysisType='AnaHQTVLQWbX'
theSysts = "nominal"
region = "4jin_2bin_0Win"

outputDir = region
os.system( 'mkdir -p ' + str(outputDir) )

with open('../../share/topologies.json') as ftopos:
    topo = json.load(ftopos)
samples = defaultdict(list)
theSystsAll="nominal,EG_RESOLUTION_ALL__1down,EG_RESOLUTION_ALL__1up,EG_SCALE_ALL__1down,EG_SCALE_ALL__1up,JET_19NP_JET_BJES_Response__1down,JET_19NP_JET_BJES_Response__1up,JET_19NP_JET_EffectiveNP_1__1down,JET_19NP_JET_EffectiveNP_1__1up,JET_19NP_JET_EffectiveNP_2__1down,JET_19NP_JET_EffectiveNP_2__1up,JET_19NP_JET_EffectiveNP_3__1down,JET_19NP_JET_EffectiveNP_3__1up,JET_19NP_JET_EffectiveNP_4__1down,JET_19NP_JET_EffectiveNP_4__1up,JET_19NP_JET_EffectiveNP_5__1down,JET_19NP_JET_EffectiveNP_5__1up,JET_19NP_JET_EffectiveNP_6restTerm__1down,JET_19NP_JET_EffectiveNP_6restTerm__1up,JET_19NP_JET_EtaIntercalibration_Modelling__1down,JET_19NP_JET_EtaIntercalibration_Modelling__1up,JET_19NP_JET_EtaIntercalibration_TotalStat__1down,JET_19NP_JET_EtaIntercalibration_TotalStat__1up,JET_19NP_JET_Flavor_Composition__1down,JET_19NP_JET_Flavor_Composition__1up,JET_19NP_JET_Flavor_Response__1down,JET_19NP_JET_Flavor_Response__1up,JET_19NP_JET_GroupedNP_1__1down,JET_19NP_JET_GroupedNP_1__1up,JET_19NP_JET_Pileup_OffsetMu__1down,JET_19NP_JET_Pileup_OffsetMu__1up,JET_19NP_JET_Pileup_OffsetNPV__1down,JET_19NP_JET_Pileup_OffsetNPV__1up,JET_19NP_JET_Pileup_PtTerm__1down,JET_19NP_JET_Pileup_PtTerm__1up,JET_19NP_JET_Pileup_RhoTopology__1down,JET_19NP_JET_Pileup_RhoTopology__1up,JET_19NP_JET_PunchThrough_MC15__1down,JET_19NP_JET_PunchThrough_MC15__1up,JET_19NP_JET_SingleParticle_HighPt__1down,JET_19NP_JET_SingleParticle_HighPt__1up,JET_JER_SINGLE_NP__1up,LARGERJET_JET_WZ_CrossCalib__1down,LARGERJET_JET_WZ_CrossCalib__1up,LARGERJET_JET_WZ_Run1__1down,LARGERJET_JET_WZ_Run1__1up,LARGERJET_SplitScales1_JET_WZ_CrossCalib__1down,LARGERJET_SplitScales1_JET_WZ_CrossCalib__1up,LARGERJET_SplitScales1_JET_WZ_Run1_D2__1down,LARGERJET_SplitScales1_JET_WZ_Run1_D2__1up,LARGERJET_SplitScales1_JET_WZ_Run1_mass__1down,LARGERJET_SplitScales1_JET_WZ_Run1_mass__1up,LARGERJET_SplitScales1_JET_WZ_Run1_pT__1down,LARGERJET_SplitScales1_JET_WZ_Run1_pT__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_D2__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_D2__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_mass__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_mass__1up,LARGERJET_SplitScales2_JET_WZ_CrossCalib_pT__1down,LARGERJET_SplitScales2_JET_WZ_CrossCalib_pT__1up,LARGERJET_SplitScales2_JET_WZ_Run1_D2__1down,LARGERJET_SplitScales2_JET_WZ_Run1_D2__1up,LARGERJET_SplitScales2_JET_WZ_Run1_mass__1down,LARGERJET_SplitScales2_JET_WZ_Run1_mass__1up,LARGERJET_SplitScales2_JET_WZ_Run1_pT__1down,LARGERJET_SplitScales2_JET_WZ_Run1_pT__1up,MET_SoftTrk_ResoPara,MET_SoftTrk_ResoPerp,MET_SoftTrk_ScaleDown,MET_SoftTrk_ScaleUp,MUONS_ID__1down,MUONS_ID__1up,MUONS_MS__1down,MUONS_MS__1up,MUONS_SCALE__1down,MUONS_SCALE__1up"

with open('./input.json') as fsamples:
    files = json.load(fsamples)
print files

# with open (sampleFile,'r') as fsamples:
#     for line in fsamples:
#         samplename = str(line.split("_pre.root")[0]).split("pre/")[1]
#         samples[samplename].append(line)
for sample in files.keys():
    sampleFiles = []
    print sample
    print len(files[sample]["files"])
    for inputs in range( len(files[sample]["files"]) ):
        sampleFiles.append(files[sample]["files"][inputs])
    sampleFiles = ','.join(sampleFiles)
    outfile = sample
    if "data" in sample:
        theSysts = "nominal"
        isData = '1'
    else:
        theSysts = "nominal"#theSystsAll
        isData = '0'

    command = './read_hqt   --data ' + isData + ' --analysis ' + analysisType +  ' --files ' + sampleFiles + ' --nentries ' + str(nentries) + ' --btags ' + str(topo[region]["btag"]) + ' --btagEx '+ str(topo[region]["btagEx"]) + ' --njets '+ str(topo[region]["njet"]) + ' --njetEx ' + str(topo[region]["njetEx"]) + ' --output ' + outputDir + '/resolved_e_' + outfile + '.root,' + outputDir + '/resolved_mu_' + outfile + '.root,' + outputDir + '/boosted_e_' + outfile + '.root,' + outputDir + '/boosted_mu_' + outfile + '.root' +' --systs ' + theSysts

    print command


    os.system( str(command) )
